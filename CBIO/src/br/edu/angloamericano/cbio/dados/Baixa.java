package br.edu.angloamericano.cbio.dados;


import java.util.Date;


public class Baixa {

	private int idBaixa;
	private String Desc;
	private int idUser;
	private Date dtBaixa;
	
	public Date getDtBaixa() {
		return dtBaixa;
	}
	public void setDtBaixa(Date dtBaixa) {
		this.dtBaixa = dtBaixa;
	}
	public int getIdBaixa() {
		return idBaixa;
	}
	public void setIdBaixa(int idBaixa) {
		this.idBaixa = idBaixa;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
