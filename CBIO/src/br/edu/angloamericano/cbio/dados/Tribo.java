package br.edu.angloamericano.cbio.dados;

public class Tribo implements Data{
	
	private int idTribo;
	private String nmTribo;
	private int idFamilia;
	
	
	public Tribo(){}
	
	public Tribo(int id, String nm, int idFamilia){
		this.idTribo= id;
		this.nmTribo = nm;
		this.idFamilia = idFamilia;
	}
	
	public String toString(){
		return nmTribo;
	}
	
	public int getIdTribo() {
		return idTribo;
	}
	public void setIdTribo(int idTribo) {
		this.idTribo = idTribo;
	}
	public String getNmTribo() {
		return nmTribo;
	}
	public void setNmTribo(String nmTribo) {
		this.nmTribo = nmTribo;
	}
	public int getIdFamilia() {
		return idFamilia;
	}
	public void setIdFamilia(int idFamilia) {
		this.idFamilia = idFamilia;
	}

}
