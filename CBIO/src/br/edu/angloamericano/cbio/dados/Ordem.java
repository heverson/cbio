package br.edu.angloamericano.cbio.dados;

public class Ordem {
	private int idOrdem;
	private String nmOrdem;
	private int idClasse;
	
	
	
	public Ordem(){}
	
	public Ordem(int id , String nm , int idClasse){
		this.idOrdem = id;
		this.nmOrdem = nm;
		this.idClasse = idClasse;
	}
	
	
	public String toString(){
		return nmOrdem;
	}
	public int getIdOrdem() {
		return idOrdem;
	}
	public void setIdOrdem(int idOrdem) {
		this.idOrdem = idOrdem;
	}
	public String getNmOrdem() {
		return nmOrdem;
	}
	public void setNmOrdem(String nmOrdem) {
		this.nmOrdem = nmOrdem;
	}
	public int getIdClasse() {
		return idClasse;
	}
	public void setIdClasse(int idClasse) {
		this.idClasse = idClasse;
	}

}
