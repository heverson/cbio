package br.edu.angloamericano.cbio.dados;

public class Processo {
	private int idProcesso;
	private String nmProcesso;
	private String dsProcesso;
	
	public Processo(int id , String nome, String desc){
		this.idProcesso = id;
		this.nmProcesso = nome;
		this.dsProcesso = desc;
	}
	public Processo(){}
	
	public int getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}
	public String getNmProcesso() {
		return nmProcesso;
	}
	public void setNmProcesso(String nmProcesso) {
		this.nmProcesso = nmProcesso;
	}
	public String getDsProcesso() {
		return dsProcesso;
	}
	public void setDsProcesso(String dsProcesso) {
		this.dsProcesso = dsProcesso;
	}


}
