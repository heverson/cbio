package br.edu.angloamericano.cbio.dados;

public class Local implements Data{
	int idLocal;
	String nmLocal;
	
	public Local(){
		this.idLocal = 0;
		this.nmLocal = "";
	}
	
	public Local(String nm){
		this.nmLocal = nm;
	}
	
	public Local(int id , String nm){
		this.idLocal = id;
		this.nmLocal = nm;
	}
	
	
	public String toString(){                  //quando requerido um objeto na JTree ira retornar o nome nao o endereco de memoria
		return getNmLocal();
	}
	
	public int getIdLocal() {
		return idLocal;
	}
	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}
	public String getNmLocal() {
		return nmLocal;
	}
	public void setNmLocal(String nmLocal) {
		this.nmLocal = nmLocal;
	}
	
	
}
