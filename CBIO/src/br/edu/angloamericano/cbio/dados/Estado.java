package br.edu.angloamericano.cbio.dados;

public class Estado {
	private int idEstado;
	private String nmEstado;

	
	public Estado(int id, String nome){
        this.idEstado = id;
        this.nmEstado = nome;
    }   
 public Estado(){

    }
 
 public String toString(){
	 return nmEstado;
 }
public int getIdEstado() {
	return idEstado;
}
public void setIdEstado(int idEstado) {
	this.idEstado = idEstado;
}
public String getNmEstado() {
	return nmEstado;
}
public void setNmEstado(String nmEstado) {
	this.nmEstado = nmEstado;
}
 



}
