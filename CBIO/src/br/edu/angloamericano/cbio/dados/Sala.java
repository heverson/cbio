package br.edu.angloamericano.cbio.dados;

public class Sala implements Data {
	private int idSala;
	private String nmSala;
	private int idBloco;
	
	public Sala(){
		this.idSala = 0;
		this.nmSala = "";
		this.idBloco = 0;
	}
	
	public Sala(int id , String nm , int idBloco){
		this.idSala = id;
		this.nmSala = nm;
		this.idBloco = idBloco;
	}
	
	public String toString(){
		return nmSala;
	}
	
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
	public String getNmSala() {
		return nmSala;
	}
	public void setNmSala(String nmSala) {
		this.nmSala = nmSala;
	}
	public int getIdBloco() {
		return idBloco;
	}
	public void setIdBloco(int idBloco) {
		this.idBloco = idBloco;
	}
}
