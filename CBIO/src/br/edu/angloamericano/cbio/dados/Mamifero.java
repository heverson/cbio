package br.edu.angloamericano.cbio.dados;

public class Mamifero implements Data {
	
	private int id_classe_mamiferos;
	private float nu_comprimento_total_corpo;
	private float nu_comprimento_total_calda;
	private float nu_comprimento_total_cabeca;
	private float nu_largura_cabeca;
	private float nu_largura_toraxica;
	private float nu_largura_base_orelha;
	private float nu_comprimento_membro_posterior;
	private int nu_total_dentes;
	private int nu_caninos_superior_direito;
	private int nu_caninos_inferior_direito;
	private int nu_incisivos_inferior_direito;
	private int nu_incisivos_superior_direito;
	private int nu_premolares_superior_direito;
	private int nu_premolares_inferior_direito;
	private int nu_molares_superior_direito;
	private int nu_molares_inferior_direito;
	private int idAnimal;
	
	
	
	public int getId_classe_mamiferos() {
		return id_classe_mamiferos;
	}
	public void setId_classe_mamiferos(int id_classe_mamiferos) {
		this.id_classe_mamiferos = id_classe_mamiferos;
	}
	public float getNu_comprimento_total_corpo() {
		return nu_comprimento_total_corpo;
	}
	public void setNu_comprimento_total_corpo(float nu_comprimento_total_corpo) {
		this.nu_comprimento_total_corpo = nu_comprimento_total_corpo;
	}
	public float getNu_comprimento_total_calda() {
		return nu_comprimento_total_calda;
	}
	public void setNu_comprimento_total_calda(float nu_comprimento_total_calda) {
		this.nu_comprimento_total_calda = nu_comprimento_total_calda;
	}
	public float getNu_comprimento_total_cabeca() {
		return nu_comprimento_total_cabeca;
	}
	public void setNu_comprimento_total_cabeca(float nu_comprimento_total_cabeca) {
		this.nu_comprimento_total_cabeca = nu_comprimento_total_cabeca;
	}
	public float getNu_largura_cabeca() {
		return nu_largura_cabeca;
	}
	public void setNu_largura_cabeca(float nu_largura_cabeca) {
		this.nu_largura_cabeca = nu_largura_cabeca;
	}
	public float getNu_largura_toraxica() {
		return nu_largura_toraxica;
	}
	public void setNu_largura_toraxica(float nu_largura_toraxica) {
		this.nu_largura_toraxica = nu_largura_toraxica;
	}
	public float getNu_largura_base_orelha() {
		return nu_largura_base_orelha;
	}
	public void setNu_largura_base_orelha(float nu_largura_base_orelha) {
		this.nu_largura_base_orelha = nu_largura_base_orelha;
	}
	public float getNu_comprimento_membro_posterior() {
		return nu_comprimento_membro_posterior;
	}
	public void setNu_comprimento_membro_posterior(
			float nu_comprimento_membro_posterior) {
		this.nu_comprimento_membro_posterior = nu_comprimento_membro_posterior;
	}
	public int getNu_total_dentes() {
		return nu_total_dentes;
	}
	public void setNu_total_dentes(int nu_total_dentes) {
		this.nu_total_dentes = nu_total_dentes;
	}
	public int getNu_caninos_superior_direito() {
		return nu_caninos_superior_direito;
	}
	public void setNu_caninos_superior_direito(int nu_caninos_superior_direito) {
		this.nu_caninos_superior_direito = nu_caninos_superior_direito;
	}
	public int getNu_caninos_inferior_direito() {
		return nu_caninos_inferior_direito;
	}
	public void setNu_caninos_inferior_direito(int nu_caninos_inferior_direito) {
		this.nu_caninos_inferior_direito = nu_caninos_inferior_direito;
	}
	public int getNu_incisivos_inferior_direito() {
		return nu_incisivos_inferior_direito;
	}
	public void setNu_incisivos_inferior_direito(int nu_incisivos_inferior_direito) {
		this.nu_incisivos_inferior_direito = nu_incisivos_inferior_direito;
	}
	public int getNu_incisivos_superior_direito() {
		return nu_incisivos_superior_direito;
	}
	public void setNu_incisivos_superior_direito(int nu_incisivos_superior_direito) {
		this.nu_incisivos_superior_direito = nu_incisivos_superior_direito;
	}
	public int getNu_premolares_superior_direito() {
		return nu_premolares_superior_direito;
	}
	public void setNu_premolares_superior_direito(int nu_premolares_superior_direito) {
		this.nu_premolares_superior_direito = nu_premolares_superior_direito;
	}
	public int getNu_premolares_inferior_direito() {
		return nu_premolares_inferior_direito;
	}
	public void setNu_premolares_inferior_direito(int nu_premolares_inferior_direito) {
		this.nu_premolares_inferior_direito = nu_premolares_inferior_direito;
	}
	public int getNu_molares_superior_direito() {
		return nu_molares_superior_direito;
	}
	public void setNu_molares_superior_direito(int nu_molares_superior_direito) {
		this.nu_molares_superior_direito = nu_molares_superior_direito;
	}
	public int getNu_molares_inferior_direito() {
		return nu_molares_inferior_direito;
	}
	public void setNu_molares_inferior_direito(int nu_molares_inferior_direito) {
		this.nu_molares_inferior_direito = nu_molares_inferior_direito;
	}
	public int getIdAnimal() {
		return idAnimal;
	}
	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}


}
