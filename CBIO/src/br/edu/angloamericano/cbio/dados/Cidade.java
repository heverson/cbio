package br.edu.angloamericano.cbio.dados;

public class Cidade implements Data{
	private int id;
	private String nome;
	private int idEstado;
	
	public Cidade(int id, String nome,int estado){
	        this.id = id;   
	        this.nome = nome;  
	        idEstado=estado;
	    }   
	 public Cidade(){
		 this.id = 0;
		 this.nome = "";
		 this.idEstado = 0;
	    }
	 
	 public String toString(){
		 return nome;
	 }
	 public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public int getIdEstado() {
			return idEstado;
		}
		public void setIdEstado(int idEstado) {
			this.idEstado = idEstado;
		}


}
