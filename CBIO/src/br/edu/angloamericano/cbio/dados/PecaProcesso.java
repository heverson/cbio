package br.edu.angloamericano.cbio.dados;

public class PecaProcesso implements Data{
	
	private int idPeca;
	private int idProcesso;
	private String nmResponsavelProcesso;
	private String dsProcessoRealizado;
	private String UsuarioResponsavel;
	

	public PecaProcesso(){}
	
	public PecaProcesso(int idPeca , int idProcesso , String nmRespon , String nmUsuario, String dsProc){
		this.idPeca = idPeca;
		this.idProcesso = idProcesso;
		this.nmResponsavelProcesso = nmRespon;
		this.UsuarioResponsavel = nmUsuario;
		this.dsProcessoRealizado = dsProc;
	}
	
	public int getIdPeca() {
		return idPeca;
	}
	public void setIdPeca(int idPeca) {
		this.idPeca = idPeca;
	}
	public int getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}
	public String getNmResponsavelProcesso() {
		return nmResponsavelProcesso;
	}
	public void setNmResponsavelProcesso(String nmResponsavelProcesso) {
		this.nmResponsavelProcesso = nmResponsavelProcesso;
	}
	public String getDsProcessoRealizado() {
		return dsProcessoRealizado;
	}
	public void setDsProcessoRealizado(String dsProcessoRealizado) {
		this.dsProcessoRealizado = dsProcessoRealizado;
	}
	public String getUsuarioResponsavel() {
		return UsuarioResponsavel;
	}
	public void setUsuarioResponsavel(String usuarioResponsavel) {
		UsuarioResponsavel = usuarioResponsavel;
	}


}
