package br.edu.angloamericano.cbio.dados;

public class Familia implements Data{
	private int idFamilia;
	private String nmFamilia;
	private int idOrdem;
	
	
	
	public Familia(){}
	
	public Familia(int id , String nm , int idOrdem)
	{
		this.idFamilia= id;
		this.nmFamilia = nm;
		this.idOrdem = idOrdem;
	}
	
	public String toString(){
		return nmFamilia;
	}
	public int getIdFamilia() {
		return idFamilia;
	}
	public void setIdFamilia(int idFamilia) {
		this.idFamilia = idFamilia;
	}
	public String getNmFamilia() {
		return nmFamilia;
	}
	public void setNmFamilia(String nmFamilia) {
		this.nmFamilia = nmFamilia;
	}
	public int getIdOrdem() {
		return idOrdem;
	}
	public void setIdOrdem(int idOrdem) {
		this.idOrdem = idOrdem;
	}
}
