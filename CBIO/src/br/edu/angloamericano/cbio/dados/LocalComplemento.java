package br.edu.angloamericano.cbio.dados;

public class LocalComplemento implements Data{
	private int idComplemento;
	private String nmComplemento;
	private  String dsComplemento;
	private int idSala;
	
	
	public LocalComplemento(int id , String nm , String ds , int idSala){
		this.idComplemento = id;
		this.nmComplemento = nm;
		this.dsComplemento = ds;
		this.idSala = idSala;
	}
	
	
	public LocalComplemento(){
		
		this.idComplemento = 0;
		this.nmComplemento = "";
		this.dsComplemento = "" ;
		this.idSala = 0;
		
	}
	
	public String toString(){
		return nmComplemento;
	}
	public int getIdComplemento() {
		return idComplemento;
	}
	public void setIdComplemento(int idComplemento) {
		this.idComplemento = idComplemento;
	}
	public String getNmComplemento() {
		return nmComplemento;
	}
	public void setNmComplemento(String nmComplemento) {
		this.nmComplemento = nmComplemento;
	}
	public String getDsComplemento() {
		return dsComplemento;
	}
	public void setDsComplemento(String dsComplemento) {
		this.dsComplemento = dsComplemento;
	}
	public int getIdSala() {
		return idSala;
	}
	public void setIdSala(int idSala) {
		this.idSala = idSala;
	}
}
