package br.edu.angloamericano.cbio.dados;

public class Especie implements Data{

private int idEspecie;
private String nmEspecie;
private int idTribo;


public Especie(){}

public Especie(int id, String nm, int idTribo){
	this.idEspecie = id;
	this.nmEspecie = nm;
	this.idTribo = idTribo;
}

public String toString(){
	return nmEspecie;
}
public int getIdEspecie() {
	return idEspecie;
}
public void setIdEspecie(int idEspecie) {
	this.idEspecie = idEspecie;
}
public String getNmEspecie() {
	return nmEspecie;
}
public void setNmEspecie(String nmEspecie) {
	this.nmEspecie = nmEspecie;
}
public int getIdTribo() {
	return idTribo;
}
public void setIdTribo(int idTribo) {
	this.idTribo = idTribo;
}

	
	
}
