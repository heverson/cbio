package br.edu.angloamericano.cbio.dados;

public class Login {

	private String login;
	private String senha;
	private int id;
	private int idUsuaio = 0;
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdUsuaio() {
		return idUsuaio;
	}

	public void setIdUsuaio(int idUsuaio) {
		this.idUsuaio = idUsuaio;
	}
}
