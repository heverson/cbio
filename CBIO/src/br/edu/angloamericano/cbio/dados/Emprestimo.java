package br.edu.angloamericano.cbio.dados;

public class Emprestimo implements Data{
	
	private int idLocal;
	private String nmLocal;
	private String dsLocal;
	private boolean foraFacul;
	private String dsEndereco;
	private String cdCep;
	private int idEndereco;
	
	
	public int getIdLocal() {
		return idLocal;
	}
	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}
	public String getNmLocal() {
		return nmLocal;
	}
	public void setNmLocal(String nmLocal) {
		this.nmLocal = nmLocal;
	}
	public String getDsLocal() {
		return dsLocal;
	}
	public void setDsLocal(String dsLocal) {
		this.dsLocal = dsLocal;
	}
	public boolean isForaFacul() {
		return foraFacul;
	}
	public void setForaFacul(boolean foraFacul) {
		this.foraFacul = foraFacul;
	}
	public String getDsEndereco() {
		return dsEndereco;
	}
	public void setDsEndereco(String dsEndereco) {
		this.dsEndereco = dsEndereco;
	}
	public String getCdCep() {
		return cdCep;
	}
	public void setCdCep(String cdCep) {
		this.cdCep = cdCep;
	}
	public int getIdEndereco() {
		return idEndereco;
	}
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}

}
