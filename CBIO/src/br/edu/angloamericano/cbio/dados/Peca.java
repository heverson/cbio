/**
 * 
 */
package br.edu.angloamericano.cbio.dados;

import java.util.Date;

/**
 *
 */
public class Peca implements Data {

	int idPeca;
	int blAnimal = 1;
	int blDnaTecidoVisceral;
	int blDnaCartilagem;
	int blDnaTecidoMuscular;
	int blPeloInterescapularDorsal;
	int blPeloAbdominal;
	int blPeloCabecaDorsal;
	int blEscama;
	int blPena;
	String dsOssos;
	Date dtEntrada;
	Date dtRetorno;
	int blPecaEmprestada;
	int idEmprestimo;
	int idAnimal;
	int idLocalArmazenado = 0;
	int cdTomboPeca;
	String dsPeca;
	int idPecaBase;
	/**
	 * @return the idPeca
	 */
	public int getIdPeca() {
		return idPeca;
	}
	/**
	 * @param idPeca the idPeca to set
	 */
	public void setIdPeca(int idPeca) {
		this.idPeca = idPeca;
	}
	/**
	 * @return the blAnimal
	 */
	public int getBlAnimal() {
		return blAnimal;
	}
	/**
	 * @param blAnimal the blAnimal to set
	 */
	public void setBlAnimal(int blAnimal) {
		this.blAnimal = blAnimal;
	}
	/**
	 * @return the blDnaTecidoVisceral
	 */
	public int getBlDnaTecidoVisceral() {
		return blDnaTecidoVisceral;
	}
	/**
	 * @param blDnaTecidoVisceral the blDnaTecidoVisceral to set
	 */
	public void setBlDnaTecidoVisceral(int blDnaTecidoVisceral) {
		this.blDnaTecidoVisceral = blDnaTecidoVisceral;
	}
	public void setBlEscama(int blEscama){
		this.blEscama = blEscama;
	}
	public int getBlEscama(){
		return blEscama;
	}
	public void setBlPena(int blPena){
		this.blPena = blPena;
	}
	public int getBlPena(){
		return blPena;
	}
	
	/**
	 * @return the blDnaCartilagem
	 */
	public int getBlDnaCartilagem() {
		return blDnaCartilagem;
	}
	/**
	 * @param blDnaCartilagem the blDnaCartilagem to set
	 */
	public void setBlDnaCartilagem(int blDnaCartilagem) {
		this.blDnaCartilagem = blDnaCartilagem;
	}
	/**
	 * @return the blDnaTecidoMuscular
	 */
	public int getBlDnaTecidoMuscular() {
		return blDnaTecidoMuscular;
	}
	/**
	 * @param blDnaTecidoMuscular the blDnaTecidoMuscular to set
	 */
	public void setBlDnaTecidoMuscular(int blDnaTecidoMuscular) {
		this.blDnaTecidoMuscular = blDnaTecidoMuscular;
	}
	/**
	 * @return the blPeloInterescapularDorsal
	 */
	public int getBlPeloInterescapularDorsal() {
		return blPeloInterescapularDorsal;
	}
	/**
	 * @param blPeloInterescapularDorsal the blPeloInterescapularDorsal to set
	 */
	public void setBlPeloInterescapularDorsal(int blPeloInterescapularDorsal) {
		this.blPeloInterescapularDorsal = blPeloInterescapularDorsal;
	}
	/**
	 * @return the blPeloAbdominal
	 */
	public int getBlPeloAbdominal() {
		return blPeloAbdominal;
	}
	/**
	 * @param blPeloAbdominal the blPeloAbdominal to set
	 */
	public void setBlPeloAbdominal(int blPeloAbdominal) {
		this.blPeloAbdominal = blPeloAbdominal;
	}
	/**
	 * @return the blPeloCabecaDorsal
	 */
	public int getBlPeloCabecaDorsal() {
		return blPeloCabecaDorsal;
	}
	/**
	 * @param blPeloCabecaDorsal the blPeloCabecaDorsal to set
	 */
	public void setBlPeloCabecaDorsal(int blPeloCabecaDorsal) {
		this.blPeloCabecaDorsal = blPeloCabecaDorsal;
	}
	/**
	 * @return the dsOssos
	 */
	public String getDsOssos() {
		return dsOssos;
	}
	/**
	 * @param dsOssos the dsOssos to set
	 */
	public void setDsOssos(String dsOssos) {
		this.dsOssos = dsOssos;
	}
	/**
	 * @return the dtEntrada
	 */
	public Date getDtEntrada() {
		return dtEntrada;
	}
	/**
	 * @param dtEntrada the dtEntrada to set
	 */
	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}
	/**
	 * @return the dtRetorno
	 */
	public Date getDtRetorno() {
		return dtRetorno;
	}
	/**
	 * @param dtRetorno the dtRetorno to set
	 */
	public void setDtRetorno(Date dtRetorno) {
		this.dtRetorno = dtRetorno;
	}
	/**
	 * @return the blPecaEmprestada
	 */
	public int getBlPecaEmprestada() {
		return blPecaEmprestada;
	}
	/**
	 * @param blPecaEmprestada the blPecaEmprestada to set
	 */
	public void setBlPecaEmprestada(int blPecaEmprestada) {
		this.blPecaEmprestada = blPecaEmprestada;
	}
	/**
	 * @return the idEmprestimo
	 */
	public int getIdEmprestimo() {
		return idEmprestimo;
	}
	/**
	 * @param idEmprestimo the idEmprestimo to set
	 */
	public void setIdEmprestimo(int idEmprestimo) {
		this.idEmprestimo = idEmprestimo;
	}
	/**
	 * @return the idAnimal
	 */
	public int getIdAnimal() {
		return idAnimal;
	}
	/**
	 * @param idAnimal the idAnimal to set
	 */
	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}
	/**
	 * @return the idLocalArmazenado
	 */
	public int getIdLocalArmazenado() {
		return idLocalArmazenado;
	}
	/**
	 * @param idLocalArmazenado the idLocalArmazenado to set
	 */
	public void setIdLocalArmazenado(int idLocalArmazenado) {
		this.idLocalArmazenado = idLocalArmazenado;
	}
	/**
	 * @return the cdTomboPeca
	 */
	public int getCdTomboPeca() {
		return cdTomboPeca;
	}
	/**
	 * @param cdTomboPeca the cdTomboPeca to set
	 */
	public void setCdTomboPeca(int cdTomboPeca) {
		this.cdTomboPeca = cdTomboPeca;
	}
	/**
	 * @return the dsPeca
	 */
	public String getDsPeca() {
		return dsPeca;
	}
	/**
	 * @param dsPeca the dsPeca to set
	 */
	public void setDsPeca(String dsPeca) {
		this.dsPeca = dsPeca;
	}
	
	public int getIdPecaBase() {
		return idPecaBase;
	}
	public void setIdPecaBase(int idPecaBase) {
		this.idPecaBase = idPecaBase;
	}
	
}
