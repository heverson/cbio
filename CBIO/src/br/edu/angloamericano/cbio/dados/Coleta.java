package br.edu.angloamericano.cbio.dados;

import java.util.Date;

public class Coleta implements Data{
	
	private int idColeta;
	private String nmColetor;
	private Date dtRecolhimento;
	private String dsOrigemAnimal;
	private String dsMetodoColeta;
	private String dsObjetivoColeta;
	private int idEnderecoColeta;
	
	public Coleta(int idColeta, String nmColetor, Date dtRecolhimento,String dsOrigemAnimal, String dsMetodoColeta,	String dsObjetivoColeta, int idEnderecoColeta) {
		this.idColeta = idColeta;
		this.nmColetor = nmColetor;
		this.dtRecolhimento = dtRecolhimento;
		this.dsOrigemAnimal = dsOrigemAnimal;
		this.dsMetodoColeta = dsMetodoColeta;
		this.dsObjetivoColeta = dsObjetivoColeta;
		this.idEnderecoColeta = idEnderecoColeta;
	}

	public Coleta(){
		this.idColeta = 0;
		this.nmColetor = "";
		this.dtRecolhimento = new Date();
		this.dsOrigemAnimal = "";
		this.dsMetodoColeta = "";
		this.dsObjetivoColeta = "";
		this.idEnderecoColeta = 0;
	}
	
	

	
	public int getIdColeta() {
		return idColeta;
	}
	public void setIdColeta(int idColeta) {
		this.idColeta = idColeta;
	}
	public String getNmColetor() {
		return nmColetor;
	}
	public void setNmColetor(String nmColetor) {
		this.nmColetor = nmColetor;
	}
	public Date getDtRecolhimento() {
		return dtRecolhimento;
	}
	public void setDtRecolhimento(Date dtRecolhimento) {
		this.dtRecolhimento = dtRecolhimento;
	}
	public String getDsOrigemAnimal() {
		return dsOrigemAnimal;
	}
	public void setDsOrigemAnimal(String dsOrigemAnimal) {
		this.dsOrigemAnimal = dsOrigemAnimal;
	}
	public String getDsMetodoColeta() {
		return dsMetodoColeta;
	}
	public void setDsMetodoColeta(String dsMetodoColeta) {
		this.dsMetodoColeta = dsMetodoColeta;
	}
	public String getDsObjetivoColeta() {
		return dsObjetivoColeta;
	}
	public void setDsObjetivoColeta(String dsObjetivoColeta) {
		this.dsObjetivoColeta = dsObjetivoColeta;
	}
	public int getIdEnderecoColeta() {
		return idEnderecoColeta;
	}
	public void setIdEnderecoColeta(int idEnderecoColeta) {
		this.idEnderecoColeta = idEnderecoColeta;
	}


}
