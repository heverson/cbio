package br.edu.angloamericano.cbio.dados;

public class Bloco implements Data{
private int idBloco;
private String nmBloco;
private int idLocal;

public Bloco(){
	this.idBloco = 0;
	this.nmBloco = "";
	this.idLocal = 0;
}
public Bloco(int id, String nm , int idLocal){
	this.idBloco = id;
	this.nmBloco = nm;
	this.idLocal = idLocal;
}

public String toString(){
	return nmBloco;
}
public int getIdBloco() {
	return idBloco;
}
public void setIdBloco(int idBloco) {
	this.idBloco = idBloco;
}
public String getNmBloco() {
	return nmBloco;
}
public void setNmBloco(String nmBloco) {
	this.nmBloco = nmBloco;
}
public int getIdLocal() {
	return idLocal;
}
public void setIdLocal(int idLocal) {
	this.idLocal = idLocal;
}
}
