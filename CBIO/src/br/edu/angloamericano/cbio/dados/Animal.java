/**
 * Package com os models do CBio
 */
package br.edu.angloamericano.cbio.dados;

/**
 * @author F�bio Nami Carlesso 
 * @email  fabio.carlesso@gmail.com
 *
 */
public class Animal implements Data{
	
	int idAnimal; //chave primaria do animal
	int BlAnimal = 0;
	String cdTombo; 
	String nmAnimal;
	String nmCientifico;
	String dsIdade;
	float nuPeso;
	String dsSinais;
	char sgSexo;
	String dsCor;
	String dsDadosBiometricos;
	int idEspecie; //chave estrangeira da especie
	int idColeta; //chave estrangeira da coleta
	
	/**
	 * @return the idAnimal
	 */
	public int getIdAnimal() {
		return idAnimal;
	}
	/**
	 * @param idAnimal the idAnimal to set
	 */
	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}
	
	public void setBlAnimal(int blAnima){
		this.BlAnimal = blAnima;
	}
	public int getBlAnimal(){
		return BlAnimal;
	}
	/**
	 * @return the cdTombo
	 */
	public String getCdTombo() {
		return cdTombo;
	}
	/**
	 * @param cdTombo the cdTombo to set
	 */
	public void setCdTombo(String cdTombo) {
		this.cdTombo = cdTombo;
	}
	/**
	 * @return the nmAnimal
	 */
	public String getNmAnimal() {
		return nmAnimal;
	}
	/**
	 * @param nmAnimal the nmAnimal to set
	 */
	public void setNmAnimal(String nmAnimal) {
		this.nmAnimal = nmAnimal;
	}
	/**
	 * @return the nmCientifico
	 */
	public String getNmCientifico() {
		return nmCientifico;
	}
	/**
	 * @param nmCientifico the nmCientifico to set
	 */
	public void setNmCientifico(String nmCientifico) {
		this.nmCientifico = nmCientifico;
	}
	/**
	 * @return the dsIdade
	 */
	public String getDsIdade() {
		return dsIdade;
	}
	/**
	 * @param dsIdade the dsIdade to set
	 */
	public void setDsIdade(String dsIdade) {
		this.dsIdade = dsIdade;
	}
	/**
	 * @return the nuPeso
	 */
	public float getNuPeso() {
		return nuPeso;
	}
	/**
	 * @param nuPeso the nuPeso to set
	 */
	public void setNuPeso(float nuPeso) {
		this.nuPeso = nuPeso;
	}
	/**
	 * @return the dsSinais
	 */
	public String getDsSinais() {
		return dsSinais;
	}
	/**
	 * @param dsSinais the dsSinais to set
	 */
	public void setDsSinais(String dsSinais) {
		this.dsSinais = dsSinais;
	}
	/**
	 * @return the sgSexo
	 */
	public char getSgSexo() {
		return sgSexo;
	}
	/**
	 * @param sgSexo the sgSexo to set
	 */
	public void setSgSexo(char sgSexo) {
		this.sgSexo = sgSexo;
	}
	/**
	 * @return the dsCor
	 */
	public String getDsCor() {
		return dsCor;
	}
	/**
	 * @param dsCor the dsCor to set
	 */
	public void setDsCor(String dsCor) {
		this.dsCor = dsCor;
	}
	/**
	 * @return the dsDadosBiometricos
	 */
	public String getDsDadosBiometricos() {
		return dsDadosBiometricos;
	}
	/**
	 * @param dsDadosBiometricos the dsDadosBiometricos to set
	 */
	public void setDsDadosBiometricos(String dsDadosBiometricos) {
		this.dsDadosBiometricos = dsDadosBiometricos;
	}
	/**
	 * @return the idEspecie
	 */
	public int getIdEspecie() {
		return idEspecie;
	}
	/**
	 * @param idEspecie the idEspecie to set
	 */
	public void setIdEspecie(int idEspecie) {
		this.idEspecie = idEspecie;
	}
	/**
	 * @return the idColeta
	 */
	public int getIdColeta() {
		return idColeta;
	}
	/**
	 * @param idColeta the idColeta to set
	 */
	public void setIdColeta(int idColeta) {
		this.idColeta = idColeta;
	}
	
}
