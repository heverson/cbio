package br.edu.angloamericano.cbio.dados;

public class Usuario {
	private int id;
	private String nome;
	private String sobrenome;
	private String registro;
	private String cargo;
	private String TelRes;
	private String TelCel;
	private String Email;
	private int tipo;
	private String obs;
	private int bl_usuario = 0;
	
	public int getBlUsuario(){
		return bl_usuario;
	}
	public void setBlUsuario(int bl){
		bl_usuario = bl;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getTelRes() {
		return TelRes;
	}
	public void setTelRes(String telRes) {
		TelRes = telRes;
	}
	public String getTelCel() {
		return TelCel;
	}
	public void setTelCel(String telCel) {
		TelCel = telCel;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
}
