package br.edu.angloamericano.cbio.dados;

public class Endereco implements Data{
	
	private int idEndereco;
	private String nmRua;
	private String nuComplemento;
	private String cdCep;
	private String nmLocal;
	private String cdCoordenadaGeografica;
	private String dsLocal;
	private int idCidade;
	
	public Endereco(int idEndereco, String nmRua, String nuComplemento,
			String cdCep, String nmLocal, String cdCoordenadaGeografica,
			String dsLocal, int idCidade) {
		
		this.idEndereco = idEndereco;
		this.nmRua = nmRua;
		this.nuComplemento = nuComplemento;
		this.cdCep = cdCep;
		this.nmLocal = nmLocal;
		this.cdCoordenadaGeografica = cdCoordenadaGeografica;
		this.dsLocal = dsLocal;
		this.idCidade = idCidade;
	}
	
	public Endereco(){}
		
	public String toString(){
		return (nmRua + " " + nuComplemento + " Cep = " + cdCep);
	}
	
	public int getIdEndereco() {
		return idEndereco;
	}
	public void setIdEndereco(int idEndereco) {
		this.idEndereco = idEndereco;
	}
	public String getNmRua() {
		return nmRua;
	}
	public void setNmRua(String nmRua) {
		this.nmRua = nmRua;
	}
	public String getNuComplemento() {
		return nuComplemento;
	}
	public void setNuComplemento(String nuComplemento) {
		this.nuComplemento = nuComplemento;
	}
	public String getCdCep() {
		return cdCep;
	}
	public void setCdCep(String cdCep) {
		this.cdCep = cdCep;
	}
	public String getNmLocal() {
		return nmLocal;
	}
	public void setNmLocal(String nmLocal) {
		this.nmLocal = nmLocal;
	}
	public String getCdCoordenadaGeografica() {
		return cdCoordenadaGeografica;
	}
	public void setCdCoordenadaGeografica(String cdCoordenadaGeografica) {
		this.cdCoordenadaGeografica = cdCoordenadaGeografica;
	}
	public String getDsLocal() {
		return dsLocal;
	}
	public void setDsLocal(String dsLocal) {
		this.dsLocal = dsLocal;
	}
	public int getIdCidade() {
		return idCidade;
	}
	public void setIdCidade(int idCidade) {
		this.idCidade = idCidade;
	}
}
