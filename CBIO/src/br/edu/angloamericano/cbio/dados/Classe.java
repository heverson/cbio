package br.edu.angloamericano.cbio.dados;

public class Classe implements Data{
private int idClasse;
private String nmClasse;

public Classe(int id, String nm){
	this.idClasse = id;
	this.nmClasse = nm;
}

public Classe(){}

public int getIdClasse() {
	return idClasse;
}


public String toString(){
	return nmClasse;
}

public void setIdClasse(int idClasse) {
	this.idClasse = idClasse;
}
public String getNmClasse() {
	return nmClasse;
}
public void setNmClasse(String nmClasse) {
	this.nmClasse = nmClasse;
}

}
