package br.edu.angloamericano.cbio.util;

import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

// Classe utilizada na limitação de caracteres dos campos de interface gráfica.
// Importante para garantir a integridade dos dados no banco, já que os atributos
// no banco tambem possuem um número limite de caracteres prédefinidos (Ex.: a VARCHAR(20))
@SuppressWarnings("serial")
public class LimitaCaracteres extends PlainDocument{
	int tamMax;

	public LimitaCaracteres(int tam){
		tamMax = tam;
	}

	public void insertString(int offs, String str, AttributeSet a){
		if((getLength() + str.length()) <= tamMax)
			try {
				super.insertString(offs, str, a);
			} catch (BadLocationException e) {
				JOptionPane.showMessageDialog(null,"Erro na janela. Por favor reabra!");
			}
	}

} 
