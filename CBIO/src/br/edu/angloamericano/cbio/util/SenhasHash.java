package br.edu.angloamericano.cbio.util;

import java.security.MessageDigest;

/**
 * Classe que faz hash das senhas de usuário. 
 * Ao cadastrar um novo usuário a senha passa por essa classe para gerar o hash que é armazenado na base de dados
 * E ao fazer login a senha digitada pelo usuário também passa por essa classe antes de ser comparada com a senha armazenada na base de dados.
 * @author Thiago R. M. Bitencourt
 *
 */
public class SenhasHash {

	private static String entradaHash = "";

	/*
	 * Método que recebe uma string de entrada, faz o hash dessa string e retorna a string contendo o hash
	 */
	public static String doHash(String entrada){

		try{
			// MD5 - algoritmo utilizado para fazer hash da String de entrada
			MessageDigest mDigest = MessageDigest.getInstance("MD5");
			// passa os bytes da string a ser gerada o hash
			mDigest.update(entrada.getBytes());
			// faz o hash da string e retorna um array de bytes
			byte[] hash = mDigest.digest();
			
			// Converte os bytes retornado pelo algoritmo para um valor hexa decimal
			StringBuffer hexString = new StringBuffer();
			for(int i = 0; i < hash.length; i++){
				if((0xff & hash[i]) < 0x10)
					hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
				else
					hexString.append(Integer.toHexString((0xFF & hash[i])));
			}
			
			// Converte o hexa decimal para string.
			entradaHash = hexString.toString();

		}catch(Exception e){
			System.out.println("ERRO: " + e);
		}
		
		// retorna o hash da string de entrada
		return entradaHash;
	}
}
