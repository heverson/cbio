package br.edu.angloamericano.cbio.util;

import java.io.File;
import java.util.Formatter;
import java.util.Scanner;

public class FileTXT {

	// Faz a leitura de um arquivo TXT cujo caminho é passado por parâmetro
	public static String readFileTXT(String caminho) throws Exception {
		Scanner input;
		String texto = "";

		try
		{
			input = new Scanner(new File(caminho));
			while(input.hasNext())
			{
				texto += input.nextLine() + "\n";
			}
			if(input!=null)
				input.close();
		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}


		return texto;
	}

	// Faz a escrita de um arquivo TXT através do texto e caminho do arquivo passados por parâmetro
	public static boolean writeFileTXT(String texto, String caminho) throws Exception {
		Formatter output;

		try
		{
			output = new Formatter(caminho);
			output.format(texto);
			if(output!=null)
				output.close();
			return true;
		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}

	}
}
