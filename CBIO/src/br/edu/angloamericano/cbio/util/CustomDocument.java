package br.edu.angloamericano.cbio.util;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

@SuppressWarnings("serial")
public class CustomDocument extends PlainDocument {  
	  
    private int pintMaxLength = 0;  
    private boolean pblnUpperCase = false;  
    private boolean pblnSpecialChar = false;  
    private boolean pblnAllowNumbers = true;  
    private boolean pblnAllowFloat = true;  
    private boolean pblnAllowLetters = true;  
    private boolean pblnAllowSpaces = true;  
  
    /** 
     * Contrutor do <tt>PlainDocument</tt>. 
     *  
     * @param maxLength Tamanho máximo permitido no campo. 
     * @param upperCase <tt>true</tt> coloca a string digitada em caixa alta, <tt>false</tt> permance como está. 
     * @param specialChars <tt>true</tt> permite a digitação de caracteres especiais, <tt>false</tt> não permite. 
     * @param allowNumbers <tt>true</tt> permite a digitação de números, <tt>false</tt> não permite. 
     * @param allowFloat <tt>true</tt> permite a digitação de valores monentários, <tt>false</tt> não permite. 
     * @param allowLetters <tt>true</tt> permite a digitação de letras, <tt>false</tt> não permite. 
     * @param allowSpaces <tt>true</tt> permite a digitação de espaços, <tt>false</tt> não permite. 
     */  
    public CustomDocument(int maxLength, boolean upperCase, boolean specialChars, boolean allowNumbers, boolean allowFloat, boolean allowLetters, boolean allowSpaces) {  
        super();  
        pintMaxLength = maxLength;  
        pblnUpperCase = upperCase;  
        pblnSpecialChar = specialChars;  
        pblnAllowNumbers = allowNumbers;  
        pblnAllowFloat = allowFloat;  
        pblnAllowLetters = allowLetters;  
        pblnAllowSpaces = allowSpaces;  
    }  
  
    @Override  
    public void insertString(int offs, String str, AttributeSet att) throws BadLocationException {  
  
        str = removeAcentos(str);  
  
        if (pblnUpperCase) {  
            str = str.toUpperCase();  
        }  
  
        if (pintMaxLength <= 0 && pblnSpecialChar && pblnAllowNumbers && pblnAllowFloat && pblnAllowLetters && pblnAllowSpaces) {  
            super.insertString(offs, str, att);  
            return;  
        }  
  
        if (!pblnSpecialChar && pblnAllowNumbers && pblnAllowLetters && pblnAllowSpaces) {  
            Pattern p = Pattern.compile("^[a-zA-Z0-9\\s]+$");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        if (!pblnSpecialChar && !pblnAllowNumbers && pblnAllowLetters && pblnAllowSpaces) {  
            Pattern p = Pattern.compile("^[a-zA-Z\\s]+$");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        if (!pblnSpecialChar && !pblnAllowNumbers && pblnAllowLetters && !pblnAllowSpaces) {  
            Pattern p = Pattern.compile("^[a-zA-Z]+$");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        if (!pblnSpecialChar && pblnAllowNumbers && !pblnAllowLetters && !pblnAllowSpaces && !pblnAllowFloat) {  
            Pattern p = Pattern.compile("^[0-9]+$");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        if (!pblnSpecialChar && pblnAllowNumbers && !pblnAllowLetters && pblnAllowSpaces && !pblnAllowFloat) {  
            Pattern p = Pattern.compile("^[0-9\\s]+$");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        if (!pblnSpecialChar && pblnAllowNumbers && !pblnAllowLetters && !pblnAllowSpaces & pblnAllowFloat) {  
            Pattern p = Pattern.compile("^/[0-9]|\\,/");  
            Matcher m = p.matcher(str);  
            if (!m.matches()) {  
                return;  
            }  
        }  
  
        int intLen = getLength() + str.length();  
        if (intLen > pintMaxLength) {  
            return;  
        }  
  
        super.insertString(offs, str, att);  
    }  
    
    /** 
     * Remove acentos de uma string passada como parâmetro. 
     * 
     * @param str String na qual serão removidos os acentos. 
     * @return String limpa, ou seja, sem acentos. 
     */  
    private String removeAcentos(String str) {  
        String strTexto;  
        strTexto = Normalizer.normalize(str, Normalizer.Form.NFD);  
        strTexto = strTexto.replaceAll("[^\\p{ASCII}]", "");  
        return strTexto;  
    } 
}
