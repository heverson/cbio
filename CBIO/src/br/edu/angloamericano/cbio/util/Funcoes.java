package br.edu.angloamericano.cbio.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/* Classe com funções estáticas usadas por algumas classes durante a execução do programa */
public class Funcoes {

	/* Esse método retorna um objeto de 'dimensão' com a resolução que o usuário
	 * esta usando no computador no momento. Na classe que chama este método estático abaixo, basta
	 * efetuar uma divisão para que seja centralizado o componente. Isso é importante para que
	 * o sistema não fique 'deformado' em diferentes resoluções de tela*/
	public static Dimension centraliza(){
		double alt = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		double larg = Toolkit.getDefaultToolkit().getScreenSize().getWidth(); 
		Dimension dim = new Dimension ((int) larg,(int) alt);
		return dim;
	}

	// Método para verificar se uma String contêm NÚMERO. útil para validação
	// de campos de senha, nomes, etc.
	public static boolean temNumero(String texto) {
		if (texto.isEmpty())
			return false;
		char[] textoTmp = texto.toCharArray();
		for(int i = 0; i < textoTmp.length; i++)
			if(Character.isDigit(textoTmp[i]))
				return true;
		return false;
	}

	// Método para verificar se uma String contêm LETRA. útil para validação
	// de campos de senha, nomes, etc.
	public static boolean temLetra(String texto) {
		if (texto.isEmpty())
			return false;
		char[] textoTmp = texto.toCharArray();
		for(int i = 0; i < textoTmp.length; i++)
			if(! Character.isDigit(textoTmp[i]))
				return true;
		return false;
	}
	
	//Calcula a Idade baseado em String. Exemplo: calculaIdade("20/08/1977","dd/MM/yyyy");
    public static int calculaIdade(String dataNasc, String formato){
        DateFormat format = new SimpleDateFormat(formato);
        Date dataNascInput = null;
        try {
            dataNascInput= format.parse(dataNasc);
        } catch (Exception e) {}
        
        Calendar dateOfBirth = new GregorianCalendar();
        dateOfBirth.setTime(dataNascInput);
        
        // Cria um objeto calendar com a data atual
        Calendar today = Calendar.getInstance();
        
       // Obtêm a idade baseado no ano
        int idade = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
        
        dateOfBirth.add(Calendar.YEAR, idade);
        
        if (today.before(dateOfBirth)) {
        	idade--;
        }
        return idade;
    }
}
