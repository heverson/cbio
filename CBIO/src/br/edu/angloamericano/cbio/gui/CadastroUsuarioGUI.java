package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.text.MaskFormatter;

import br.edu.angloamericano.cbio.util.CustomDocument;
import br.edu.angloamericano.cbio.util.Funcoes;
/**
 * Interface de Cadastro de um novo usuário
 * @author Thiago R. M. Bitencourt
 *
 */

@SuppressWarnings("serial")
public class CadastroUsuarioGUI extends JDialog{

	// Icones
	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private Icon altIcon = new ImageIcon("images/altIcon.gif");
	private Icon remIcon = new ImageIcon("images/remIcon.gif");
	private Icon moreIcon = new ImageIcon("images/more.png");

	// Botões
	public JButton btnAlterar = new JButton("Alterar", altIcon);
	public JButton btnAdicionar = new JButton("Adicionar", addIcon);
	public JButton btnCancelar = new JButton("Cancelar", remIcon);
	public JButton btnLimpar = new JButton("Limpar");
	public JButton btnAddTipo = new JButton("", moreIcon);

	// Campos de inserção de Texto
	public JTextField textNome;
	public JTextField textSobrenome;
	public JTextField textCargo;
	public JFormattedTextField textRegistro;
	public JFormattedTextField textTelRes;
	public JFormattedTextField textTelCel;
	public JTextField textEmail;
	public JTextField textLogin;
	public JPasswordField textSenha;
	public JPasswordField textRepSenha;
	public JTextArea textObs = new JTextArea();

	public JComboBox BoxTipo = new JComboBox();

	/**
	 * Construtor da classe
	 * @param Super - Janela 'Pai'
	 * @param flag - Inteiro que define se a janela é aberta para adição ou para edição de um usuário
	 * @throws Exception
	 */
	public CadastroUsuarioGUI(JDialog Super, int flag) throws Exception{
		super(Super);
		setSize(426,573);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		setResizable(false);
		getContentPane().setLayout(null);
		configuraJanela(flag); // Configura a janela, de acordo com a flag recebida
//		showComponentes();
	}

	/**
	 * Método responsável por carregar e configurar a janela
	 */
	private void showComponentes(){

		JPanel Usuario = new JPanel();
		Usuario.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Usuario.setBounds(12, 12, 395, 255);
		getContentPane().add(Usuario);
		Usuario.setLayout(null);

		JLabel lblUsurio = new JLabel("Dados do Usuário:");
		lblUsurio.setBounds(12, 12, 157, 15);
		Usuario.add(lblUsurio);

		JLabel lblNome = new JLabel("Nome*:");
		lblNome.setBounds(12, 39, 70, 15);
		Usuario.add(lblNome);

		textNome = new JTextField();
		textNome.setDocument(new CustomDocument(50, false, false, false, false, true, true));
		textNome.setBounds(12, 55, 177, 27);
		Usuario.add(textNome);
		textNome.setColumns(10);

		JLabel lblSobrenome = new JLabel("Sobrenome:");
		lblSobrenome.setBounds(200, 39, 105, 15);
		Usuario.add(lblSobrenome);

		textSobrenome = new JTextField();
		textSobrenome.setDocument(new CustomDocument(50, false, false, false, false, true, true));
		textSobrenome.setBounds(201, 55, 177, 27);
		Usuario.add(textSobrenome);
		textSobrenome.setColumns(10);

		JLabel lblCargo = new JLabel("Cargo*: ");
		lblCargo.setBounds(12, 92, 70, 15);
		Usuario.add(lblCargo);

		textCargo = new JTextField();
		textCargo.setDocument(new CustomDocument(50, false, false, false, false, true, true));
		textCargo.setBounds(12, 108, 177, 27);
		Usuario.add(textCargo);
		textCargo.setColumns(10);

		JLabel lblRegistro = new JLabel("Registro*: ");
		lblRegistro.setBounds(200, 92, 105, 15);
		Usuario.add(lblRegistro);

		try  
        {
			textRegistro = new JFormattedTextField(new MaskFormatter("##########"));
        }catch (ParseException excp) {}
		textRegistro.setBounds(200, 108, 178, 27);
		Usuario.add(textRegistro);
		textRegistro.setColumns(10);

		JLabel lblTelefoneResidencial = new JLabel("Telefone Residencial: ");
		lblTelefoneResidencial.setBounds(12, 145, 177, 15);
		Usuario.add(lblTelefoneResidencial);

		try  
        {
			textTelRes = new JFormattedTextField(new MaskFormatter("(##)####-####"));
        }catch (ParseException excp) {}
		textTelRes.setBounds(12, 161, 177, 27);
		Usuario.add(textTelRes);
		textTelRes.setColumns(10);

		JLabel lblTelefoneCelular = new JLabel("Telefone Celular: ");
		lblTelefoneCelular.setBounds(200, 145, 147, 15);
		Usuario.add(lblTelefoneCelular);

		try  
        {
			textTelCel = new JFormattedTextField(new MaskFormatter("(##)####-####"));
        }catch (ParseException excp) {}
		textTelCel.setBounds(200, 161, 178, 27);
		Usuario.add(textTelCel);
		textTelCel.setColumns(10);

		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(12, 198, 70, 15);
		Usuario.add(lblEmail);

		textEmail = new JTextField();
		textEmail.setBounds(12, 214, 366, 27);
		Usuario.add(textEmail);
		textEmail.setColumns(10);

		JPanel Login = new JPanel();
		Login.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		Login.setBounds(12, 279, 395, 233);
		getContentPane().add(Login);
		Login.setLayout(null);

		JLabel lblDadosDeLogin = new JLabel("Dados de Login: ");
		lblDadosDeLogin.setBounds(12, 12, 143, 15);
		Login.add(lblDadosDeLogin);

		JLabel lblLogin = new JLabel("Login*: ");
		lblLogin.setBounds(12, 39, 70, 15);
		Login.add(lblLogin);

		textLogin = new JTextField();
		textLogin.setBounds(12, 55, 143, 27);
		Login.add(textLogin);
		textLogin.setColumns(10);

		JLabel lblSenha = new JLabel("Senha*: ");
		lblSenha.setBounds(12, 92, 70, 15);
		Login.add(lblSenha);

		textSenha = new JPasswordField();
		textSenha.setBounds(12, 106, 143, 27);
		Login.add(textSenha);
		textSenha.setColumns(10);

		JLabel lblTipo = new JLabel("Tipo*: ");
		lblTipo.setBounds(167, 39, 70, 15);
		Login.add(lblTipo);

		//BoxTipo.setModel(new DefaultComboBoxModel(new String[] {":: Selecione um Tipo ::"}));
		BoxTipo.setBounds(167, 55, 177, 27);
		Login.add(BoxTipo);

		JLabel lblRepetirSenha = new JLabel("Repetir Senha*: ");
		lblRepetirSenha.setBounds(167, 92, 143, 15);
		Login.add(lblRepetirSenha);

		textRepSenha = new JPasswordField();
		textRepSenha.setBounds(167, 106, 143, 27);
		Login.add(textRepSenha);
		textRepSenha.setColumns(10);

		JScrollPane scrollObs = new JScrollPane();
		scrollObs.setBounds(12, 141, 371, 80);
		Login.add(scrollObs);

		scrollObs.setViewportView(textObs);
		
		btnAddTipo.setBounds(343, 55, 28, 27);
		btnAddTipo.setToolTipText("Adicionar Tipo");
		Login.add(btnAddTipo);

		btnCancelar.setBounds(285, 524, 117, 37);
		getContentPane().add(btnCancelar);

		btnLimpar.setBounds(156, 524, 117, 37);
		getContentPane().add(btnLimpar);

	}

	/**
	 * Configura a janela
	 * @param flag - Define se a janela será aberta para adiconar novo usuário ou alterar usuário existente
	 */
	private void configuraJanela(int flag){

		if(flag == 0){ // Tela de adição
			setTitle("Adicionar novo Usuário");
			btnAdicionar.setBounds(27, 524, 117, 37);
			getContentPane().add(btnAdicionar);
			showComponentes();
		}

		if(flag == 1){ // Tela de alteração
			setTitle("Alterar Usuário");
			btnAlterar.setBounds(27, 524, 117, 37);
			getContentPane().add(btnAlterar);
			showComponentes();
		}
	}

	/**
	 * Adiciona ouvinte aos componentes da janela. Esse método é chamado pela classe de controle
	 * @param ouvinte - Classe que faz o controle das ações
	 */
	public void addOuvintes(ActionListener ouvinte){
		btnAlterar.addActionListener(ouvinte);
		btnAdicionar.addActionListener(ouvinte);
		btnLimpar.addActionListener(ouvinte);
		btnCancelar.addActionListener(ouvinte);
		btnAddTipo.addActionListener(ouvinte);
	}
}
