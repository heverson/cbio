package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import br.edu.angloamericano.cbio.util.Funcoes;
import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class UsuarioGUI extends JDialog{
	public JTextField textField = new JTextField();
	public JTable tabela = new JTable(){
		public boolean isCellEditable(int rowIndex, int mColIndex) {  
			return false;
		}
	};
	private Icon lupaIcon = new ImageIcon("images/lupaIcon.gif");
	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private Icon altIcon = new ImageIcon("images/altIcon.gif");
	private Icon remIcon = new ImageIcon("images/remIcon.gif");
	private JScrollPane scrollTabela = new JScrollPane(tabela);
	public JButton btnAdicionar = new JButton("Adicionar",addIcon);
	public JButton btnAlterar = new JButton("Alterar     ",altIcon);
	public JButton btnRemover = new JButton("Remover",remIcon);
	public JButton btnConsultar = new JButton("Consultar",lupaIcon);
	private JLabel lblBuscar = new JLabel("Buscar:");
	public JComboBox buscaPor = new JComboBox();
	public JButton btnProcurar = new JButton("Procurar",lupaIcon);
	public JCheckBox cbxInativos;

	public UsuarioGUI(JFrame Super){
		super(Super);
		setTitle("Cadastro de Usuarios");
		setSize(700,254);
		getContentPane().setLayout(null);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-350),(int) (Funcoes.centraliza().getHeight()/2-127));
		setResizable(false);

		showComponentes();
	}
	
	private void showComponentes(){
		textField.setBounds(75, 19, 187, 25);
		getContentPane().add(textField);
		textField.setColumns(10);
		lblBuscar.setBounds(13, 24, 61, 14);
		getContentPane().add(lblBuscar);
		scrollTabela.setBounds(0, 52, 545, 156);
		getContentPane().add(scrollTabela);	
		btnAdicionar.setBounds(555, 52, 129, 36);
		getContentPane().add(btnAdicionar);
		
		btnAlterar.setBounds(555, 89, 129, 36);
		getContentPane().add(btnAlterar);
		
		btnRemover.setBounds(555, 126, 129, 36);
		getContentPane().add(btnRemover);
		
		btnConsultar.setBounds(555, 163, 129, 36);
		getContentPane().add(btnConsultar);
		
		buscaPor.setBounds(274, 19, 150, 25);
		getContentPane().add(buscaPor);
		btnProcurar.setBounds(436, 15, 109, 33);
		getContentPane().add(btnProcurar);
		tabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		
		cbxInativos = new JCheckBox("Usuários Inativos");
		cbxInativos.setBounds(555, 20, 129, 23);
		getContentPane().add(cbxInativos);
		tabela.getTableHeader().setReorderingAllowed(false);
		
		String[] colunaPrincipal = {"ID", "Nome", "Cargo", "Tipo", "Registro"};
		tabela.setModel(new DefaultTableModel(null, colunaPrincipal));
	}

	public void addOuvintes(ActionListener ouvinte){
		btnAdicionar.addActionListener(ouvinte);
		btnAlterar.addActionListener(ouvinte);
		btnRemover.addActionListener(ouvinte);
		btnConsultar.addActionListener(ouvinte);
		btnProcurar.addActionListener(ouvinte);
	}
	
	public boolean validar(){
		boolean ok = true;
		
		return ok;
	}
}

