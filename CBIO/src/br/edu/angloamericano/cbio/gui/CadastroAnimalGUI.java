package br.edu.angloamericano.cbio.gui;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EtchedBorder;

import br.edu.angloamericano.cbio.util.Funcoes;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public class CadastroAnimalGUI extends JDialog{

	public Icon addIcon = new ImageIcon("images/addIcon.gif");
	public Icon altIcon = new ImageIcon("images/altIcon.gif");
	public JTabbedPane tabbed = new JTabbedPane();
	public JButton btnAdicionar = new JButton("Adicionar", addIcon);
	public JButton btnAlterar = new JButton("Alterar", altIcon);
	public JButton cidadeAdd = new JButton("...");
	public JButton localidadeAdd = new JButton("...");
	
	public JPanel p1 = new JPanel();
	public JPanel p2 = new JPanel();
	public JPanel p4 = new JPanel();
	
	public JPanel p1p1 = new JPanel();
	public JPanel p2p1 = new JPanel();
	public JTextField coletor = new JTextField();
	public JLabel lEstado = new JLabel("Estado*:");
	public JLabel lLocalidade = new JLabel("Localidade*:");
	public JLabel lCidade = new JLabel("Cidade*:");
	public JLabel lColetor = new JLabel("Coletor*:");
	public JLabel lblDtColeta = new JLabel("Data Coleta* :");
	public JLabel lblOrigem = new JLabel("Origem :");
	public JLabel lblColeta = new JLabel("Coleta :");
	public JComboBox localidade = new JComboBox();
	public JComboBox cidade = new JComboBox();
	public String[] vetEstados = { ":: Selecione um Estado ::", "Acre", "Alagoas", "Amap\u00E1", "Amazonas", "Bahia", "Cear\u00E1", "Distrito Federal",
			"Esp\u00EDrito Santo", "Goi\u00E1s", "Maranh\u00E3o", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Par\u00E1", 
			"Para\u00EDba", "Paran\u00E1", "Pernambuco", "Piau\u00ED", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", 
			"Rond\u00F4nia", "Roraima", "Santa Catarina", "S\u00E3o Paulo", "Sergipe", "Tocantins"};
	public JComboBox estado = new JComboBox(vetEstados);
	public JSpinner dtRecolhim = new JSpinner(new SpinnerDateModel(new Date(), null, null, Calendar.MONTH));
	public ButtonGroup grupo1 = new ButtonGroup();
	public JScrollPane origemScroll = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	public ButtonGroup grupo2 = new ButtonGroup();
	public JTextArea objetivoColeta = new JTextArea();
	public JScrollPane objetivoColetaScroll = new JScrollPane(objetivoColeta,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	public JLabel lObjetivoColeta = new JLabel("Objetivo da Coleta :");
	public JLabel lMetodoColeta = new JLabel("M\u00E9todo de Coleta :");
	public JTextArea metodoColeta = new JTextArea();
	public JScrollPane metodoColetaScroll = new JScrollPane(metodoColeta,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	public JPanel p2p1p2 = new JPanel();
	public JLabel lMorfologicas = new JLabel("Características Morfológicas");
	public JLabel lClasse = new JLabel("Classe*:");
	public JComboBox classe = new JComboBox();
	public JLabel lOrdem = new JLabel("Ordem*:");
	public JComboBox ordem = new JComboBox(new Object[]{});
	public JButton ordemAdd = new JButton("...");
	public JLabel lFamilia = new JLabel("Fam\u00EDlia*:");
	public JComboBox familia = new JComboBox(new Object[]{});
	public JButton familiaAdd = new JButton("...");
	public JLabel lTribo = new JLabel("Tribo*:");
	public JComboBox tribo = new JComboBox(new Object[]{});
	public JButton triboAdd = new JButton("...");
	public JLabel lEspecie = new JLabel("Esp\u00E9cie*:");
	public JComboBox especie = new JComboBox(new Object[]{});
	public JButton especieAdd = new JButton("...");
	public JPanel p2p1p3 = new JPanel();
	public JLabel lGerais = new JLabel();
	public JLabel lPeso = new JLabel("Peso*:");
	public JLabel lSexo = new JLabel("Sexo*:");
	public JLabel lIdade = new JLabel("Idade*:");
	public JLabel lCorPelagem = new JLabel("Cor da Pelagem*:");
	public JSpinner peso = new JSpinner();
	public String[] tiposPeso = { "--","kg","hg","dag","g","dg","cg","mg"};
	public String[] sexos = {"Macho", "Fêmea"};
	public JComboBox sexo = new JComboBox(sexos);
	public String[] cores = { ":: Selecione uma cor ::", "Azul", "Azul marinho", "Azul piscina", "Azul-turquesa", "Azul-Royal", "Verde-Veronese",
			"Verde Ing�s", "Verde", "Amarelo", "Preto", "Cinza claro", "Cinza escuro", "Laranja", "Vermelho", "Rosa",
			"Branco", "Creme", "Marrom", "Caramelo", "Dourado", "Esmeralda", "Feldspato", "Grená", "Hortelâ", "Indigo",
			"Jade", "Lilás", "Malva", "Naval", "Ocre", "Prata", "Quantum", "Roxo", "Salmão", "Turquesa", "Urucum",
			"Magenta", "Ciano "};
	public JPanel p2p1p5 = new JPanel();
	public JPanel p4p1p1 = new JPanel();
	public JLabel dentada = new JLabel("Dentada");
	public JLabel lCanSupDir = new JLabel("Caninos Superior Direito:");
	public JSpinner canSupDir = new JSpinner();
	public JLabel lCanInfDir = new JLabel("Caninos Inferior Direito:");
	public JSpinner canInfDir = new JSpinner();
	public JLabel label_18 = new JLabel("Incisivos Superior Direito:");
	public JSpinner incSupDir = new JSpinner();
	public JLabel lIncInfDir = new JLabel("Incisivos Inferior Direito:");
	public JSpinner incInfDir = new JSpinner();
	public JLabel lPreMolSupDir = new JLabel("Pré-Molares Superior Direito:");
	public JSpinner preMolSupDir = new JSpinner();
	public JLabel lPreMolInfDir = new JLabel("Pré-Molares Inferior Direito:");
	public JSpinner preMolInfDir = new JSpinner();
	public JLabel lMolSupDir = new JLabel("Molares Superior Direito:");
	public JSpinner molSupDir = new JSpinner();
	public JLabel lMolInfDir = new JLabel("Molares Inferior Direito:");
	public JSpinner molInfDir = new JSpinner();
	public JLabel lTotalDeArcada = new JLabel("Total de Arcada Dentaria:");
	public JTextField totalArcada = new JTextField();
	public JButton limparArcada = new JButton("Limpar Campos");
	public JButton calcularArcada = new JButton("Calcular");
	public JPanel p2p1p1 = new JPanel();
	public JLabel lNTombo = new JLabel();
	public JLabel tombo1 = new JLabel("CCBSZooFaa");
	public JTextField tombo2 = new JTextField();
	public JTextField tombo3 = new JTextField();
	public JPanel p1p1p2 = new JPanel();
	public JPanel p1p1p1 = new JPanel();
	public ButtonGroup grupo3 = new ButtonGroup();
	public String[] numeros = {":: Selecione o freezer ::","1","2","3","4","5","6","7","8","9","10","11","12"};
	private final JLabel lblProcedencia = new JLabel("Procedencia :");
	public JRadioButton rdbtnProcesso = new JRadioButton("Processo");
	public JRadioButton rdbtnArmazenamento = new JRadioButton("Armazenamento");
	public JComboBox cBoxIdade = new JComboBox(new String[] {":: Selecione uma idade ::", "Rec\u00E1m-Nascido", "Jovem", "Adulto", "Meia-Idade", "Velho"});
	public JPanel p4p1 = new JPanel();
	public JTextArea origem = new JTextArea();
	
	public JPanel p4p1p2 = new JPanel();
	
	public JSpinner comprimentoCalda = new JSpinner();
	public JSpinner comprimentoCabeca = new JSpinner();
	public JSpinner comprimentoMembroPost = new JSpinner();
	public JSpinner larguraToraxica = new JSpinner();
	public JSpinner larguraBaseOrelha = new JSpinner();
	public JSpinner larguraCabeca = new JSpinner();
	
	public JSpinner comprimentoCorpo = new JSpinner();
	
	public JTextField txtNmAnimal;
	public JTextField txtNmCientifico;
	private final JPanel panel = new JPanel();
	private final JScrollPane scrollPane_1 = new JScrollPane();
	public JTextArea textAreaDadosBiometricos = new JTextArea();
	private final JLabel lblDadosBiomtricos = new JLabel("Dados Biom\u00E9tricos :");
	
	public JButton btnProximoP2 = new JButton("Proximo");
	public JButton btnLimparP2 = new JButton("Limpar");
	public JButton btnProximoP4 = new JButton("Proximo");
	public JButton btnCadastrar = new JButton("Cadastrar");
	public JTextArea txtSinais = new JTextArea();
	public JTextField txtFieldCorPelagem;

	// CONSTRUTOR: Cria a GUI e configura conforme a flag de identifica��o.
	// A flag identifica GUI de adi��o, altera��o ou consulta.
	public CadastroAnimalGUI(JDialog Super){
		super(Super);
		setSize(800,580);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		getContentPane().setLayout(null);
		setResizable(false);
	
		showComponentes();
	}

	public void showComponentes(){
		double min = 0;    
		double valor = 0;    
		double max = 100000000;    
		double incremento = 1;    
//		SpinnerNumberModel modelo = new SpinnerNumberModel(valor, min, max, incremento);

		min = 0.00;    
		valor = 0.00;    
		max = 100000000.00;    
		incremento = 0.01;    
		SpinnerNumberModel modelo2 = new SpinnerNumberModel(valor, min, max, incremento);
		peso.setModel(modelo2);

		min = 0;    
		valor = 0;    
		max = 10;    
		incremento = 1;    
//		SpinnerNumberModel modelo3 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo4 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo5 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo6 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo7 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo8 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo9 = new SpinnerNumberModel(valor, min, max, incremento);
//		SpinnerNumberModel modelo10 = new SpinnerNumberModel(valor, min, max, incremento);

		getContentPane().add(tabbed);
		tabbed.setBounds(0,0,794,545);
		//tabbed.set
		//dtSaida.setEditor(new JSpinner.DateEditor(dtSaida, "dd/MM/yyyy"));
		
		// ********************************************************************************************************************
		/**
		 * TAB 2 Taxonomia
		 */
		tabbed.addTab("Taxonomia", null, p2, null);
		p2.setLayout(null);
		p2p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1.setBounds(10, 11, 778, 500);
		p2.add(p2p1);
		p2p1.setLayout(null);
		p2p1p3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p3.setLayout(null);
		p2p1p3.setBounds(new Rectangle(15, 240, 245, 175));
		p2p1p3.setBounds(319, 105, 443, 135);
		p2p1.add(p2p1p3);
		lGerais.setFont(new Font("Tahoma", Font.BOLD, 11));
		lGerais.setText("Caracteristicas Gerais");
		lGerais.setBounds(new Rectangle(5, 0, 225, 20));
		lGerais.setBounds(5, 0, 225, 20);
		p2p1p3.add(lGerais);
		lPeso.setFont(new Font("SansSerif", Font.BOLD, 12));
		lPeso.setBounds(new Rectangle(7, 47, 33, 20));
		lPeso.setBounds(5, 69, 58, 20);
		p2p1p3.add(lPeso);
		lSexo.setFont(new Font("SansSerif", Font.BOLD, 12));
		lSexo.setBounds(new Rectangle(100, 22, 31, 20));
		lSexo.setBounds(214, 69, 58, 20);
		p2p1p3.add(lSexo);
		lIdade.setFont(new Font("SansSerif", Font.BOLD, 12));
		lIdade.setBounds(new Rectangle(5, 22, 35, 20));
		lIdade.setBounds(5, 32, 58, 20);
		p2p1p3.add(lIdade);
		peso.setBounds(new Rectangle(40, 45, 75, 24));
		peso.setBounds(61, 66, 75, 25);
		p2p1p3.add(peso);
		sexo.setModel(new DefaultComboBoxModel(new String[] {":: Selecione o Sexo ::", "Macho", "F\u00EAmea"}));
		sexo.setBounds(new Rectangle(130, 20, 100, 24));
		sexo.setBounds(263, 67, 168, 25);
		p2p1p3.add(sexo);
		
		JLabel lblPeso = new JLabel("Kg");
		lblPeso.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPeso.setBounds(146, 70, 33, 16);
		p2p1p3.add(lblPeso);
		cBoxIdade.setModel(new DefaultComboBoxModel(new String[] {":: Selecione uma idade ::", "Rec\u00E9m-Nascido", "Jovem", "Adulto", "Meia-Idade", "Velho"}));
		
		cBoxIdade.setBounds(new Rectangle(130, 20, 100, 24));
		cBoxIdade.setBounds(61, 31, 203, 25);
		p2p1p3.add(cBoxIdade);
		p2p1p5.setEnabled(false);
		p2p1p5.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p5.setLayout(null);
		p2p1p5.setBounds(new Rectangle(261, 240, 225, 175));
		p2p1p5.setBounds(10, 244, 752, 200);
		p2p1.add(p2p1p5);
		
		JLabel lblCaractersticas = new JLabel("Caracter\u00EDsticas :");
		lblCaractersticas.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblCaractersticas.setBounds(6, 6, 116, 16);
		p2p1p5.add(lblCaractersticas);
		
		JLabel lblNomeAnimal = new JLabel("Nome Animal*:");
		lblNomeAnimal.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblNomeAnimal.setBounds(6, 40, 116, 16);
		p2p1p5.add(lblNomeAnimal);
		
		txtNmAnimal = new JTextField();
		txtNmAnimal.setBounds(130, 34, 223, 28);
		p2p1p5.add(txtNmAnimal);
		txtNmAnimal.setColumns(10);
		
		JLabel lblNomeCientifico = new JLabel("Nome Cientifico*:");
		lblNomeCientifico.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblNomeCientifico.setBounds(6, 80, 132, 16);
		p2p1p5.add(lblNomeCientifico);
		
		txtNmCientifico = new JTextField();
		txtNmCientifico.setBounds(130, 74, 223, 28);
		p2p1p5.add(txtNmCientifico);
		txtNmCientifico.setColumns(10);
		lCorPelagem.setFont(new Font("SansSerif", Font.BOLD, 12));
		lCorPelagem.setBounds(6, 120, 132, 20);
		p2p1p5.add(lCorPelagem);
		
		JLabel lblSinaisOuMarcaes = new JLabel("Sinais ou Marca\u00E7\u00F5es :");
		lblSinaisOuMarcaes.setBounds(413, 6, 159, 16);
		p2p1p5.add(lblSinaisOuMarcaes);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(413, 40, 322, 142);
		p2p1p5.add(scrollPane);
		
		scrollPane.setViewportView(txtSinais);
		
		txtFieldCorPelagem = new JTextField();
		txtFieldCorPelagem.setBounds(130, 116, 223, 28);
		p2p1p5.add(txtFieldCorPelagem);
		txtFieldCorPelagem.setColumns(10);
	
		
		p2p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p1.setLayout(null);
		p2p1p1.setBounds(new Rectangle(261, 45, 225, 100));
		p2p1p1.setBounds(319, 18, 443, 75);
		
		p2p1.add(p2p1p1);
		lNTombo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lNTombo.setText("Número Tombo");
		lNTombo.setBounds(new Rectangle(5, 0, 225, 20));
		lNTombo.setBounds(5, 0, 225, 20);

		p2p1p1.add(lNTombo);
		tombo1.setFont(new Font("SansSerif", Font.BOLD, 12));
		tombo1.setBounds(new Rectangle(10, 40, 160, 21));
		tombo1.setBounds(5, 36, 90, 21);

		p2p1p1.add(tombo1);
		tombo2.setEditable(false);
		tombo2.setBounds(new Rectangle(90, 38, 40, 25));
		tombo2.setBounds(103, 32, 40, 30);

		p2p1p1.add(tombo2);
		tombo3.setEditable(false);
		tombo3.setText("");
		tombo3.setBounds(new Rectangle(130, 38, 90, 25));
		tombo3.setBounds(143, 32, 102, 30);
		p2p1p1.add(tombo3);
		
		// ********************************************************************************************************************
		/**
		 * TAB 3 Destino
		 */
		canSupDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		canInfDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		incSupDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		incInfDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		preMolSupDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		preMolInfDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		molSupDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		molInfDir.setModel(new SpinnerNumberModel(0, 0, 10, 1));
		tabbed.addTab("Dados Biometricos", p4);
		p4.setLayout(null);
		p4p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		
		p4p1.setBounds(10, 11, 778, 500);
		p4.add(p4p1);
		p4p1.setLayout(null);
		p4p1p1.setPreferredSize(new Dimension(500, 200));
		
		p4p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p4p1p1.setLayout(null);
		p4p1p1.setBounds(new Rectangle(30, 30, 435, 155));
		p4p1p1.setBounds(10, 11, 753, 132);
		
		p4p1.add(p4p1p1);
		
		dentada.setFont(new Font("Tahoma", Font.BOLD, 11));
		dentada.setBounds(new Rectangle(5, 0, 100, 20));
		dentada.setBounds(5, 0, 100, 20);
		p4p1p1.add(dentada);
		lCanSupDir.setBounds(new Rectangle(30, 20, 140, 20));
		lCanSupDir.setBounds(5, 20, 190, 20);
		p4p1p1.add(lCanSupDir);
		canSupDir.setBounds(new Rectangle(170, 18, 50, 24));
		canSupDir.setBounds(195, 18, 50, 25);
		p4p1p1.add(canSupDir);
		lCanInfDir.setBounds(new Rectangle(245, 20, 131, 20));
		lCanInfDir.setBounds(250, 22, 182, 20);
		p4p1p1.add(lCanInfDir);
		canInfDir.setBounds(new Rectangle(375, 18, 50, 24));
		canInfDir.setBounds(422, 20, 50, 25);
		p4p1p1.add(canInfDir);
		label_18.setBounds(new Rectangle(30, 45, 140, 20));
		label_18.setBounds(5, 44, 190, 20);
		
		p4p1p1.add(label_18);
		incSupDir.setBounds(new Rectangle(170, 43, 50, 24));
		incSupDir.setBounds(195, 42, 50, 25);
				
		p4p1p1.add(incSupDir);
		lIncInfDir.setBounds(new Rectangle(245, 45, 131, 20));
		lIncInfDir.setBounds(250, 47, 182, 20);
						
		p4p1p1.add(lIncInfDir);
		incInfDir.setBounds(new Rectangle(375, 43, 50, 24));
		incInfDir.setBounds(422, 42, 50, 25);
								
		p4p1p1.add(incInfDir);
		lPreMolSupDir.setBounds(new Rectangle(10, 70, 160, 20));
		lPreMolSupDir.setBounds(484, 20, 222, 20);
										
		p4p1p1.add(lPreMolSupDir);
		preMolSupDir.setBounds(new Rectangle(170, 68, 50, 24));
		preMolSupDir.setBounds(694, 18, 50, 25);
												
		p4p1p1.add(preMolSupDir);
		lPreMolInfDir.setBounds(new Rectangle(225, 70, 151, 20));
		lPreMolInfDir.setBounds(484, 45, 222, 20);
														
		p4p1p1.add(lPreMolInfDir);
		preMolInfDir.setBounds(new Rectangle(375, 68, 50, 24));
		preMolInfDir.setBounds(694, 43, 50, 25);
																
		p4p1p1.add(preMolInfDir);
		lMolSupDir.setBounds(new Rectangle(33, 95, 137, 20));
		lMolSupDir.setBounds(5, 66, 190, 20);
																		
		p4p1p1.add(lMolSupDir);
		molSupDir.setBounds(new Rectangle(170, 93, 50, 24));
		molSupDir.setBounds(195, 64, 50, 25);
																				
		p4p1p1.add(molSupDir);
		lMolInfDir.setBounds(new Rectangle(248, 95, 128, 20));
		lMolInfDir.setBounds(250, 67, 182, 20);
																						
		p4p1p1.add(lMolInfDir);
		molInfDir.setBounds(new Rectangle(375, 93, 50, 24));
		molInfDir.setBounds(422, 64, 50, 25);
																								
		p4p1p1.add(molInfDir);
		lTotalDeArcada.setBounds(new Rectangle(10, 125, 139, 20));
		lTotalDeArcada.setBounds(6, 102, 189, 20);
																										
		p4p1p1.add(lTotalDeArcada);
		totalArcada.setEditable(false);
		totalArcada.setBounds(new Rectangle(148, 125, 72, 24));
		totalArcada.setBounds(205, 100, 72, 25);
																												
		p4p1p1.add(totalArcada);
		limparArcada.setBounds(new Rectangle(305, 124, 120, 25));
		limparArcada.setBounds(549, 100, 157, 25);
																														
		p4p1p1.add(limparArcada);
		calcularArcada.setBounds(new Rectangle(220, 124, 75, 25));
		calcularArcada.setBounds(296, 100, 127, 25);
																																
		p4p1p1.add(calcularArcada);
																																		
		p4p1p1.setEnabled(true);
		p4p1p1.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{dentada, lCanSupDir, canSupDir, lCanInfDir, canInfDir, label_18, incSupDir, lIncInfDir, incInfDir, lPreMolSupDir, preMolSupDir, lPreMolInfDir, preMolInfDir, lMolSupDir, molSupDir, lMolInfDir, molInfDir, lTotalDeArcada, totalArcada, limparArcada, calcularArcada}));
		p4p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p4p1p2.setBounds(10, 155, 753, 124);
																																		
		p4p1.add(p4p1p2);
		p4p1p2.setLayout(null);
																																		
		JLabel dadosBiometricos = new JLabel("Dados Biometricos");
		dadosBiometricos.setFont(new Font("Tahoma", Font.BOLD, 11));
		dadosBiometricos.setBounds(10, 11, 173, 14);
		p4p1p2.add(dadosBiometricos);
																																		
		JLabel lCompCorpo = new JLabel("Comprimento Total Corpo:");
		lCompCorpo.setBounds(10, 37, 202, 14);
		p4p1p2.add(lCompCorpo);
																																		
		JLabel lComprimentoCalda = new JLabel("Comprimento Total Calda:");
		lComprimentoCalda.setBounds(10, 61, 183, 14);
		p4p1p2.add(lComprimentoCalda);
																																		
		JLabel lblComprimentoTotalCabeca = new JLabel("Comprimento Total Cabeca:");
		lblComprimentoTotalCabeca.setBounds(10, 87, 202, 14);
		p4p1p2.add(lblComprimentoTotalCabeca);
																																		
		JLabel lLarguraCabeca = new JLabel("Largura Cabe\u00E7a:");
		lLarguraCabeca.setBounds(345, 61, 129, 14);
		p4p1p2.add(lLarguraCabeca);
																																		
		JLabel lLarguraToraxica = new JLabel("Largura Toraxica:");
		lLarguraToraxica.setBounds(538, 37, 134, 14);
		p4p1p2.add(lLarguraToraxica);
																																		
		JLabel lLarguraBaseOrelha = new JLabel("Largura Base Orelha:");
		lLarguraBaseOrelha.setBounds(536, 61, 169, 14);
		p4p1p2.add(lLarguraBaseOrelha);
																																		
		JLabel lComprimentoMembroPosterior = new JLabel("Comp. Membro Posterior:");
		lComprimentoMembroPosterior.setBounds(284, 37, 190, 14);
		p4p1p2.add(lComprimentoMembroPosterior);
		comprimentoCorpo.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
																																		
		
		comprimentoCorpo.setBounds(216, 32, 50, 25);
		p4p1p2.add(comprimentoCorpo);
		comprimentoCalda.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		comprimentoCalda.setBounds(216, 56, 50, 25);
																																		
		p4p1p2.add(comprimentoCalda);
		comprimentoCabeca.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		comprimentoCabeca.setBounds(216, 82, 50, 25);
																																		
		p4p1p2.add(comprimentoCabeca);
		comprimentoMembroPost.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		comprimentoMembroPost.setBounds(470, 32, 50, 25);
																																		
		p4p1p2.add(comprimentoMembroPost);
		larguraToraxica.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		larguraToraxica.setBounds(690, 30, 50, 25);
																																		
		p4p1p2.add(larguraToraxica);
		larguraBaseOrelha.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		larguraBaseOrelha.setBounds(690, 55, 50, 25);
																																		
		p4p1p2.add(larguraBaseOrelha);
		larguraCabeca.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		larguraCabeca.setBounds(470, 56, 50, 25);
																																		
		p4p1p2.add(larguraCabeca);
																																		
		JButton btnLimparCampos = new JButton("Limpar Campos");
		btnLimparCampos.setBounds(536, 82, 159, 25);
		p4p1p2.add(btnLimparCampos);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 291, 753, 141);
																																		
		p4p1.add(panel);
		panel.setLayout(null);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setBounds(6, 33, 741, 102);
																																		
		panel.add(scrollPane_1);
																																		
		scrollPane_1.setViewportView(textAreaDadosBiometricos);
		lblDadosBiomtricos.setBounds(6, 6, 155, 16);
																																		
		panel.add(lblDadosBiomtricos);
		btnProximoP4.setBounds(626, 451, 137, 28);
																																		
		p4p1.add(btnProximoP4);
		tabbed.addTab("Identificação e Procedência", p1);
		p1.setLayout(null);
		p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1.setBounds(6, 6, 772, 500);
		p1.add(p1p1);
		p1p1.setLayout(null);
		p1p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2.setBounds(10, 177, 750, 262);
		p1p1.add(p1p1p2);
		p1p1p2.setLayout(null);
		lMetodoColeta.setBounds(391, 137, 174, 20);
		p1p1p2.add(lMetodoColeta);
		metodoColetaScroll.setBounds(379, 154, 364, 97);
		p1p1p2.add(metodoColetaScroll);
		metodoColeta.setWrapStyleWord(true);
		metodoColeta.setLineWrap(true);
		objetivoColetaScroll.setBounds(10, 154, 362, 97);
		p1p1p2.add(objetivoColetaScroll);
		objetivoColeta.setLineWrap(true);
		objetivoColeta.setWrapStyleWord(true);
		lObjetivoColeta.setBounds(20, 137, 188, 21);
		p1p1p2.add(lObjetivoColeta);
		origemScroll.setBounds(381, 37, 362, 94);
		p1p1p2.add(origemScroll);
		
		origemScroll.setViewportView(origem);
		lblOrigem.setBounds(395, 10, 137, 20);
		p1p1p2.add(lblOrigem);
		lblColeta.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblColeta.setBounds(21, 9, 161, 21);
		p1p1p2.add(lblColeta);
		lColetor.setFont(new Font("SansSerif", Font.BOLD, 12));
		lColetor.setBounds(20, 42, 63, 21);
		p1p1p2.add(lColetor);
		coletor.setBounds(85, 37, 276, 30);
		p1p1p2.add(coletor);
		lblDtColeta.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblDtColeta.setBounds(20, 89, 100, 21);
		p1p1p2.add(lblDtColeta);
		dtRecolhim.setBounds(120, 86, 129, 25);
		p1p1p2.add(dtRecolhim);
		dtRecolhim.setEditor(new JSpinner.DateEditor(dtRecolhim, "dd/MM/yyyy"));
		p1p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p1.setBounds(10, 14, 750, 152);
		
				p1p1.add(p1p1p1);
				p1p1p1.setLayout(null);
				localidadeAdd.setBounds(697, 108, 25, 25);
				p1p1p1.add(localidadeAdd);
				localidadeAdd.setEnabled(false);
				cidade.setBounds(95, 73, 599, 25);
				p1p1p1.add(cidade);
				cidade.setEnabled(false);
				cidade.setEditable(false);
				localidade.setBounds(95, 108, 599, 25);
				localidade.addItem(":: Selecione uma localidade ::");
				p1p1p1.add(localidade);
				localidade.setEnabled(false);
				localidade.setEditable(false);
				cidadeAdd.setBounds(697, 73, 25, 25);
				p1p1p1.add(cidadeAdd);
				cidadeAdd.setEnabled(false);
				lCidade.setFont(new Font("SansSerif", Font.BOLD, 12));
				lCidade.setBounds(10, 75, 70, 21);
				p1p1p1.add(lCidade);
				estado.setModel(new DefaultComboBoxModel(new String[] {":: Selecione um Estado ::"}));
				estado.setBounds(95, 39, 596, 25);
				p1p1p1.add(estado);
				lEstado.setFont(new Font("SansSerif", Font.BOLD, 12));
				lEstado.setBounds(10, 41, 70, 21);
				p1p1p1.add(lEstado);
				lLocalidade.setFont(new Font("SansSerif", Font.BOLD, 12));
				lLocalidade.setBounds(8, 110, 88, 21);
				p1p1p1.add(lLocalidade);
				cidade.addItem(":: Selecione uma cidade ::");
				
				lblProcedencia.setFont(new Font("Tahoma", Font.BOLD, 11));
				lblProcedencia.setBounds(10, 6, 109, 21);
				
						p1p1p1.add(lblProcedencia);
						btnCadastrar.setBounds(640, 451, 126, 28);
						
						p1p1.add(btnCadastrar);
						
						JLabel lblCampos_1 = new JLabel("(*) - Campos Obrigatórios");
						lblCampos_1.setBounds(10, 458, 189, 15);
						p1p1.add(lblCampos_1);
		btnProximoP2.setBounds(672, 456, 90, 28);
		
		p2p1.add(btnProximoP2);
	
		btnLimparP2.setBounds(574, 456, 90, 28);
		
		p2p1.add(btnLimparP2);
		p2p1p2.setBounds(10, 18, 297, 221);
		p2p1.add(p2p1p2);
		p2p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p2.setLayout(null);
		lMorfologicas.setFont(new Font("Tahoma", Font.BOLD, 11));
		lMorfologicas.setBounds(new Rectangle(5, 0, 225, 20));
		lMorfologicas.setBounds(5, 0, 225, 20);
		p2p1p2.add(lMorfologicas);
		lClasse.setFont(new Font("SansSerif", Font.BOLD, 12));
		lClasse.setBounds(new Rectangle(15, 34, 44, 20));
		lClasse.setBounds(5, 28, 60, 20);
		p2p1p2.add(lClasse);
		classe.setModel(new DefaultComboBoxModel(new String[] {":: Selecione uma classe ::"}));
		classe.setBounds(new Rectangle(59, 32, 150, 24));
		classe.setBounds(75, 26, 212, 25);
		p2p1p2.add(classe);
		lOrdem.setFont(new Font("SansSerif", Font.BOLD, 12));
		lOrdem.setBounds(new Rectangle(17, 64, 42, 20));
		lOrdem.setBounds(5, 73, 60, 20);
		p2p1p2.add(lOrdem);
		ordem.setEnabled(false);
		ordem.setEditable(false);
		ordem.setBounds(new Rectangle(59, 62, 150, 24));
		ordem.setBounds(75, 71, 180, 24);
		p2p1p2.add(ordem);
		ordemAdd.setEnabled(false);
		ordemAdd.setBounds(new Rectangle(210, 62, 30, 24));
		ordemAdd.setBounds(259, 71, 28, 25);
		p2p1p2.add(ordemAdd);
		lFamilia.setFont(new Font("SansSerif", Font.BOLD, 12));
		lFamilia.setBounds(new Rectangle(14, 94, 45, 20));
		lFamilia.setBounds(5, 113, 61, 20);
		p2p1p2.add(lFamilia);
		familia.setEnabled(false);
		familia.setEditable(false);
		familia.setBounds(new Rectangle(59, 92, 150, 24));
		familia.setBounds(75, 107, 180, 25);
		p2p1p2.add(familia);
		
				familiaAdd.setEnabled(false);
				familiaAdd.setBounds(new Rectangle(210, 92, 30, 24));
				familiaAdd.setBounds(259, 107, 28, 25);
				p2p1p2.add(familiaAdd);
				lTribo.setFont(new Font("SansSerif", Font.BOLD, 12));
				lTribo.setBounds(new Rectangle(27, 124, 32, 20));
				lTribo.setBounds(5, 149, 60, 20);
				p2p1p2.add(lTribo);
				tribo.setEnabled(false);
				tribo.setEditable(false);
				tribo.setBounds(new Rectangle(59, 122, 150, 24));
				tribo.setBounds(75, 144, 180, 25);
				p2p1p2.add(tribo);
				triboAdd.setEnabled(false);
				triboAdd.setBounds(new Rectangle(210, 122, 30, 24));
				triboAdd.setBounds(259, 143, 28, 25);
				p2p1p2.add(triboAdd);
				lEspecie.setFont(new Font("SansSerif", Font.BOLD, 12));
				lEspecie.setBounds(new Rectangle(10, 154, 49, 20));
				lEspecie.setBounds(5, 183, 72, 20);
				p2p1p2.add(lEspecie);
				especie.setEnabled(false);
				especie.setEditable(false);
				especie.setBounds(new Rectangle(59, 152, 150, 24));
				especie.setBounds(75, 181, 180, 25);
				p2p1p2.add(especie);
				especieAdd.setEnabled(false);
				especieAdd.setBounds(new Rectangle(210, 152, 30, 24));
				especieAdd.setBounds(259, 181, 28, 24);
				p2p1p2.add(especieAdd);
				ordem.addItem(":: Selecione uma ordem ::");
				familia.addItem(":: Selecione uma fam�lia ::");
				tribo.addItem(":: Selecione uma tribo ::");
				especie.addItem(":: Selecione uma esp�cie ::");
				
				JLabel lblCampos = new JLabel("(*) - Campos Obrigatórios");
				lblCampos.setBounds(10, 463, 195, 15);
				p2p1.add(lblCampos);
		
		// ********************************************************************************************************************
		/**
		 * TAB 4 Dados Biometricos
		 */
		

	}

	public void configuraJanela(int flag){
		if(flag == 1){ // Tela de adição
			setTitle("Adicionar novo animal");
		}
		if(flag == 2){ // Tela de alteração
			setTitle("Alterar animal");
			btnAlterar.setBounds(499, 503, 130, 31);
			getContentPane().add(btnAlterar);
		}
		if(flag == 3){ // Tela de consulta
			setTitle("Consultar animal");
		}
	}

	public void addOuvintes(ActionListener ouvinte){
		btnAlterar.addActionListener(ouvinte);
		estado.addActionListener(ouvinte);
		cidade.addActionListener(ouvinte);
		cidadeAdd.addActionListener(ouvinte);
		localidadeAdd.addActionListener(ouvinte);
		classe.addActionListener(ouvinte);
		ordem.addActionListener(ouvinte);
		familia.addActionListener(ouvinte);
		tribo.addActionListener(ouvinte);
		especie.addActionListener(ouvinte);
		calcularArcada.addActionListener(ouvinte);
		limparArcada.addActionListener(ouvinte);
		rdbtnArmazenamento.addActionListener(ouvinte);
		rdbtnProcesso.addActionListener(ouvinte);
		ordemAdd.addActionListener(ouvinte);
		familiaAdd.addActionListener(ouvinte);
		triboAdd.addActionListener(ouvinte);
		especieAdd.addActionListener(ouvinte);
		btnCadastrar.addActionListener(ouvinte);
		btnProximoP2.addActionListener(ouvinte);
		
		btnProximoP4.addActionListener(ouvinte);
		tabbed.addMouseListener((MouseListener)ouvinte);
		btnLimparP2.addActionListener(ouvinte);
		}
}
