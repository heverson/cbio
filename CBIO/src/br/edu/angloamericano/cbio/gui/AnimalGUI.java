package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import br.edu.angloamericano.cbio.util.Funcoes;
import java.awt.Rectangle;
import javax.swing.JCheckBox;

@SuppressWarnings("serial")
public class AnimalGUI extends JDialog{
	public JTable tabela = new JTable(){
		public boolean isCellEditable(int rowIndex, int mColIndex) {  
			return false;
		}
	};
	private Icon lupaIcon = new ImageIcon("images/lupaIcon.gif");
	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private Icon altIcon = new ImageIcon("images/altIcon.gif");
//	private Icon remIcon = new ImageIcon("images/remIcon.gif");
	private Icon listIcon = new ImageIcon("images/lista.png");
	
	private JScrollPane scrollTabela = new JScrollPane(tabela);
	//botoes
	public JButton btnAdicionar = new JButton("Adicionar",addIcon);
	public JButton btnAlterar = new JButton("Alterar     ",altIcon);
	public JButton btnRemover = new JButton("Processo",listIcon);
//	public JButton btnRemover = new JButton("Processo",remIcon);
	public JButton btnConsultar = new JButton("Consultar",lupaIcon);
	public JButton btnFiltros = new JButton("Filtros");
	public JButton btnBuscaTombo = new JButton(lupaIcon);
	public JButton btnUsar = new JButton("Usar");
	
	//textFileds
	public JTextField txtClasse = new JTextField();
	public JTextField txtNuTombo = new JTextField();
	//labels
	public JLabel lblCCBSZooFaa = new JLabel("CCBSZooFaa");
	public JLabel lblNmeroTombo = new JLabel("N\u00FAmero Tombo:");

	public JCheckBox chckbxAnimaisIndisponveis;
	/**
	 * Essa classe pode ser acessada de duas formas: 
	 *1 - através do menu manter animais
	 *2 - através do menu manter peças, para cadastrar uma nova peça, a mesma deve ser associada a um anima,
	 * portanto ao cadastrar uma nova peça, usa-se essa classe para selecionar o animal que terá a peça associada. 
	 * O parâmetro flag, serve para saber quem está chamando a classe
	 * @param Super
	 * @param flag
	 */
	public AnimalGUI(JFrame Super, int flag){
		super(Super);
		setTitle("Cadastro de animais");
		setSize(700,254);
		getContentPane().setLayout(null);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-350),(int) (Funcoes.centraliza().getHeight()/2-127));
		setResizable(false);

		showComponentes(flag);
	}
	
	/**
	 * Método utilizado para criar e configurar os componentes da interface
	 * @param flag - Define a configuração da janela
	 */
	private void showComponentes(int flag){
		scrollTabela.setBounds(0, 52, 545, 156);
		getContentPane().add(scrollTabela);	
		
		btnAdicionar.setBounds(555, 52, 129, 36);
		getContentPane().add(btnAdicionar);
		
		btnAlterar.setBounds(555, 89, 129, 36);
		getContentPane().add(btnAlterar);
		
		btnRemover.setBounds(555, 126, 129, 36);
//		btnRemover.setEnabled(false);
		getContentPane().add(btnRemover);
		
		btnConsultar.setBounds(555, 163, 129, 36);
		getContentPane().add(btnConsultar);
		tabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		tabela.getTableHeader().setReorderingAllowed(false);
		tabela.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C\u00F3digo Tombo", "Nome", "Nome Cient\u00EDfico", "Espécie"
			}
		));
		
		lblCCBSZooFaa.setBounds(new Rectangle(10, 40, 160, 21));
		lblCCBSZooFaa.setBounds(124, 16, 90, 21);
		getContentPane().add(lblCCBSZooFaa);
		txtClasse.setBounds(new Rectangle(90, 38, 40, 25));
		txtClasse.setBounds(218, 11, 40, 30);
		getContentPane().add(txtClasse);
		
		txtNuTombo.setText("");
		txtNuTombo.setBounds(new Rectangle(130, 38, 90, 25));
		txtNuTombo.setBounds(258, 11, 102, 30);
		getContentPane().add(txtNuTombo);
		
		lblNmeroTombo.setBounds(12, 20, 100, 14);
		getContentPane().add(lblNmeroTombo);
		
		btnFiltros.setBounds(400, 15, 102, 25);
		getContentPane().add(btnFiltros);
		
		btnBuscaTombo.setBounds(364, 15, 34, 25);
		getContentPane().add(btnBuscaTombo);
		
		chckbxAnimaisIndisponveis = new JCheckBox("Animais Indisponíveis");
		chckbxAnimaisIndisponveis.setBounds(507, 15, 177, 23);
		getContentPane().add(chckbxAnimaisIndisponveis);
		
		/*  se a flag recebida for igual a 1, então a janela é aberta para selecionar um animal, portanto
		* o botão Usar é acionado no lugar o checkBox AnimaisIndisponiveis
		*/
		if(flag == 1){
			chckbxAnimaisIndisponveis.setVisible(false);
			btnUsar.setBounds(507, 15, 108, 25);
			getContentPane().add(btnUsar);
		}
	}

	/*
	 * Método utilizado para criar os ouvintes da interface
	 */
	public void addOuvintes(ActionListener ouvinte){
		btnAdicionar.addActionListener(ouvinte);
		btnAlterar.addActionListener(ouvinte);
		btnRemover.addActionListener(ouvinte);
		btnConsultar.addActionListener(ouvinte);
		btnFiltros.addActionListener(ouvinte);
		btnBuscaTombo.addActionListener(ouvinte);
		btnUsar.addActionListener(ouvinte);
	}
}
