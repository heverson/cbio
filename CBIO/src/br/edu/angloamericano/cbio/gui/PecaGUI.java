package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.util.Funcoes;
import java.awt.Rectangle;
import javax.swing.JCheckBox;

@SuppressWarnings("serial")
public class PecaGUI extends JDialog{
	public JTable tabela = new JTable(){
		public boolean isCellEditable(int rowIndex, int mColIndex) {  
			return false;
		}
	};
	private Icon lupaIcon = new ImageIcon("images/lupaIcon.gif");
	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private Icon altIcon = new ImageIcon("images/altIcon.gif");
	private Icon remIcon = new ImageIcon("images/remIcon.gif");
	private JScrollPane scrollTabela = new JScrollPane(tabela);
	public JButton btnAdicionar = new JButton("Adicionar",addIcon);
	public JButton btnAlterar = new JButton("Alterar     ",altIcon);
	public JButton btnRemover = new JButton("Remover",remIcon);
	public JButton btnConsultar = new JButton("Consultar",lupaIcon);
	public JButton btnFiltro = new JButton("", lupaIcon);
	public JLabel tombo1;
	public JTextField textTombo2;
	public JTextField textTombo3;
	public JCheckBox chckbxMostrarTudo;

	public PecaGUI(JFrame Super){
		super(Super);
		setTitle("Cadastro de peças");
		setSize(700,254);
		getContentPane().setLayout(null);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-350),(int) (Funcoes.centraliza().getHeight()/2-127));
		setResizable(false);

		showComponentes();
		verificaUser();
	}
	
	/**
	 * Método que cria e configura a interface gráfica
	 */
	private void showComponentes(){
		scrollTabela.setBounds(0, 52, 545, 156);
		getContentPane().add(scrollTabela);	
		btnAdicionar.setBounds(555, 52, 129, 36);
		getContentPane().add(btnAdicionar);

		btnAlterar.setBounds(555, 89, 129, 36);
		getContentPane().add(btnAlterar);
		
		btnRemover.setBounds(555, 126, 129, 36);
		getContentPane().add(btnRemover);
		
		btnConsultar.setBounds(555, 163, 129, 36);
		getContentPane().add(btnConsultar);
		tabela.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		tabela.getTableHeader().setReorderingAllowed(false);
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.addColumn("Detalhe 1");
		modelo.addColumn("Detalhe 2");
		modelo.addColumn("Detalhe 3");
		modelo.addColumn("Detalhe 4");
		tabela.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"C\u00F3digo Tombo Pe\u00E7a", "Descri\u00E7\u00E3o", "C\u00F3digo Tombo Animal", "Nome Animal"
			}
		));
		
		JLabel lblCdigotomboAnimal = new JLabel("Tombo Animal:");
		lblCdigotomboAnimal.setBounds(12, 23, 119, 14);
		getContentPane().add(lblCdigotomboAnimal);
		
		tombo1 = new JLabel("CCBSZooFaa");
		tombo1.setBounds(new Rectangle(10, 40, 160, 21));
		tombo1.setBounds(121, 20, 90, 21);
		getContentPane().add(tombo1);
		
		textTombo2 = new JTextField();
		textTombo2.setBounds(new Rectangle(90, 38, 40, 25));
		textTombo2.setBounds(217, 10, 40, 30);
		getContentPane().add(textTombo2);
		
		textTombo3 = new JTextField();
		textTombo3.setText("");
		textTombo3.setBounds(new Rectangle(130, 38, 90, 25));
		textTombo3.setBounds(257, 10, 102, 30);
		getContentPane().add(textTombo3);
		
		btnFiltro.setBounds(364, 12, 34, 25);
		getContentPane().add(btnFiltro);
		
		chckbxMostrarTudo = new JCheckBox("Mostrar Peças Não Disponíveis");
		chckbxMostrarTudo.setBounds(416, 13, 268, 23);
		getContentPane().add(chckbxMostrarTudo);
	}

	/**
	 * Adiciona os ouvintes para as ações da interface
	 * @param ouvinte
	 */
	public void addOuvintes(ActionListener ouvinte){
		btnAdicionar.addActionListener(ouvinte);
		btnAlterar.addActionListener(ouvinte);
		btnRemover.addActionListener(ouvinte);
		btnConsultar.addActionListener(ouvinte);
		btnFiltro.addActionListener(ouvinte);
	}
	
	/**
	 * Métoque que verifica o usuário cadastrado, se não for usuário administrador restringe o acesso a algumas funcionalidades
	 */
	private void verificaUser(){
		if(!App.getTipoUser().equals("Administrador")){
			btnAlterar.setEnabled(false);
			btnRemover.setEnabled(false);
		}
	}
}
