package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class ProcessoGUI extends JDialog{
	public JTextField textTombo;
	public JTextField textNomeComum;
	public JTextField textNomeCie;
	public JTextField textEspecie;
	public JTextField textGenero;
	public JTextField textColetor;
	public JTextField textDataColeta;
	public JTextField textIdade;
	public JTextField textPeso;
	public JTextField textCor;
	public JTextField textLocal;

	public JButton btnFechar;
	public JButton btnDetalhes;

	public JList listProcesso;
	public JCheckBox chckbxDisponvel;

	public ProcessoGUI() 
	{
		setModal(true);
		setSize(554, 653); //Tamanho da janel
		showComponets(); // Cria os componentes
		setResizable(false); // Não permite redimensionar a janela
	}

	/*
	 * Método que cria e configura os componentes da interface gráfica.
	 */
	private void showComponets(){

		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(6, 6, 537, 218);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lbltombo = new JLabel("Tombo:");
		lbltombo.setBounds(5, 12, 51, 15);
		panel.add(lbltombo);

		textTombo = new JTextField();
		textTombo.setBounds(56, 6, 185, 27);
		panel.add(textTombo);
		textTombo.setColumns(10);

		JLabel lblNomeCientifico = new JLabel("Nome Científico: ");
		lblNomeCientifico.setBounds(5, 89, 113, 15);
		panel.add(lblNomeCientifico);

		textNomeComum = new JTextField();
		textNomeComum.setBounds(5, 54, 185, 27);
		panel.add(textNomeComum);
		textNomeComum.setColumns(10);

		JLabel lblNomeComum = new JLabel("Nome Comum:");
		lblNomeComum.setBounds(5, 35, 95, 15);
		panel.add(lblNomeComum);

		textNomeCie = new JTextField();
		textNomeCie.setBounds(5, 104, 185, 27);
		panel.add(textNomeCie);
		textNomeCie.setColumns(10);

		JLabel lblEspcie = new JLabel("Espécie: ");
		lblEspcie.setBounds(202, 35, 60, 15);
		panel.add(lblEspcie);

		textEspecie = new JTextField();
		textEspecie.setBounds(202, 54, 179, 27);
		panel.add(textEspecie);
		textEspecie.setColumns(10);

		JLabel lblGnero = new JLabel("Gênero: ");
		lblGnero.setBounds(202, 89, 60, 15);
		panel.add(lblGnero);

		textGenero = new JTextField();
		textGenero.setBounds(202, 104, 149, 27);
		panel.add(textGenero);
		textGenero.setColumns(10);

		chckbxDisponvel = new JCheckBox("Disponível");
		chckbxDisponvel.setBounds(253, 10, 95, 18);
		panel.add(chckbxDisponvel);

		JLabel lblColetor = new JLabel("Coletor:");
		lblColetor.setBounds(5, 143, 60, 15);
		panel.add(lblColetor);

		textColetor = new JTextField();
		textColetor.setBounds(5, 165, 185, 27);
		panel.add(textColetor);
		textColetor.setColumns(10);

		JLabel lblDataDeColeta = new JLabel("Data de Coleta:");
		lblDataDeColeta.setBounds(202, 143, 113, 15);
		panel.add(lblDataDeColeta);

		textDataColeta = new JTextField();
		textDataColeta.setBounds(202, 165, 149, 27);
		panel.add(textDataColeta);
		textDataColeta.setColumns(10);

		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(393, 35, 60, 15);
		panel.add(lblIdade);

		textIdade = new JTextField();
		textIdade.setBounds(393, 54, 138, 27);
		panel.add(textIdade);
		textIdade.setColumns(10);

		JLabel lblPeso = new JLabel("Peso: ");
		lblPeso.setBounds(360, 89, 51, 15);
		panel.add(lblPeso);

		JLabel lblCor = new JLabel("Cor: ");
		lblCor.setBounds(444, 89, 60, 15);
		panel.add(lblCor);

		textPeso = new JTextField();
		textPeso.setBounds(360, 104, 78, 27);
		panel.add(textPeso);
		textPeso.setColumns(10);

		textCor = new JTextField();
		textCor.setBounds(444, 104, 87, 27);
		panel.add(textCor);
		textCor.setColumns(10);

		JLabel lblArmazenado = new JLabel("Armazenado: ");
		lblArmazenado.setBounds(360, 143, 93, 15);
		panel.add(lblArmazenado);

		textLocal = new JTextField();
		textLocal.setBounds(363, 165, 168, 27);
		panel.add(textLocal);
		textLocal.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 232, 538, 372);
		getContentPane().add(scrollPane);

		listProcesso = new JList();
		scrollPane.setViewportView(listProcesso);

		btnFechar = new JButton("Fechar");
		btnFechar.setBounds(425, 615, 100, 27);
		getContentPane().add(btnFechar);

		btnDetalhes = new JButton("Detalhes");
		btnDetalhes.setBounds(318, 615, 100, 27);
		getContentPane().add(btnDetalhes);
	}
	
	/*
	 * Adiciona os ouvintes aos botões da interface.
	 */
	public void addOuvintes(ActionListener ouvinte){
		btnFechar.addActionListener(ouvinte);
		btnDetalhes.addActionListener(ouvinte);
	}
}
