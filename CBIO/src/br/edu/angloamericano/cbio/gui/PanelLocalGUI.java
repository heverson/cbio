package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.border.EtchedBorder;

import br.edu.angloamericano.cbio.controller.HierarquiaArvoreLocalController;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class PanelLocalGUI extends JPanel{

	public JTree tree;

	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private Icon altIcon = new ImageIcon("images/altIcon.gif");
	private Icon remIcon = new ImageIcon("images/remIcon.gif");

	public JButton btnAdicionar = new JButton("Adicionar", addIcon);;
	public JButton btnAlterar = new JButton("Alterar", altIcon);
	public JButton btnRemover = new JButton("Remover", remIcon);
	public JButton btnTerminar;
	public JTextArea textDesc;

	public PanelLocalGUI() {
		setLocation((int)(Funcoes.centraliza().getWidth()/2-350),(int) (Funcoes.centraliza().getHeight()/2-127));
		showComponentes();	
	}
	
	private void showComponentes(){
		setSize(769,485);

		setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 769, 450);
		panel.setLayout(null);
		add(panel);

		JPanel panelTree = new JPanel();
		panelTree.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTree.setBounds(10, 11, 479, 428);
		panel.add(panelTree);
		panelTree.setLayout(null);

		JScrollPane scrollTree = new JScrollPane();
		scrollTree.setBounds(0, 0, 479, 428);
		panelTree.add(scrollTree);

		tree =  new JTree(new HierarquiaArvoreLocalController());
		scrollTree.setViewportView(tree);

		JPanel panelButton = new JPanel();
		panelButton.setLayout(null);
		panelButton.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelButton.setBounds(522, 11, 235, 428);
		panel.add(panelButton);

		btnAdicionar.setBounds(31, 25, 174, 36);
		panelButton.add(btnAdicionar);

		btnAlterar.setBounds(31, 73, 174, 36);
		panelButton.add(btnAlterar);

		btnRemover.setBounds(31, 116, 174, 36);
		panelButton.add(btnRemover);

		JScrollPane scrollDesc = new JScrollPane();
		scrollDesc.setBounds(12, 282, 211, 134);
		panelButton.add(scrollDesc);

		textDesc = new JTextArea();
		textDesc.setEditable(false);
		scrollDesc.setViewportView(textDesc);

		JLabel lblDescLocal = new JLabel("Descrição do Local:");
		lblDescLocal.setBounds(12, 265, 211, 15);
		panelButton.add(lblDescLocal);
		
		btnTerminar = new JButton("Terminar");
		btnTerminar.setBounds(31, 164, 174, 36);
		panelButton.add(btnTerminar);

		JLabel lblHierarquia = new JLabel("Hierarquia: ");
		lblHierarquia.setBounds(10, 451, 80, 15);
		add(lblHierarquia);

		JLabel lblLocalBloco = new JLabel("Local -> Bloco -> Sala -> Complemento Local (freezer, armario , etc...)");
		lblLocalBloco.setBounds(86, 451, 444, 15);
		add(lblLocalBloco);
	}
	
	public void addOuvintes(ActionListener ouvinte){
		btnAdicionar.addActionListener(ouvinte);
		btnAlterar.addActionListener(ouvinte);
		btnRemover.addActionListener(ouvinte);
		btnTerminar.addActionListener(ouvinte);
	}
}
