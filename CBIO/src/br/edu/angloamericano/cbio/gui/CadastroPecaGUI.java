package br.edu.angloamericano.cbio.gui;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.TreeSelectionListener;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.controller.HierarquiaArvoreLocalController;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class CadastroPecaGUI extends JDialog{

	private Icon altIcon = new ImageIcon("images/altIcon.gif");
	private Icon IconAdd = new ImageIcon("images/addIcon.gif");
	private Icon remIcon = new ImageIcon("images/remIcon.gif");
	public JTabbedPane tabbed = new JTabbedPane();
	//botoes
	//public JButton btnAdicionar = new JButton("Adicionar", addIcon);
	public JButton btnAlterar = new JButton("Alterar", altIcon);
	public JButton btnConsultar = new JButton("Consultar");
	public JButton btnFiltros = new JButton("Filtrar");
	public JButton btnProximoP1 = new JButton("Pr\u00F3ximo");
	public JButton btnLimparP1 = new JButton("Limpar");
	public JButton btnProximoP2 = new JButton("Pr\u00F3ximo");
	public JButton btnLimparP2 = new JButton("Limpar");
	public JButton btnProximoP3 = new JButton("Pr\u00F3ximo");
	public JButton btnLimparP3 = new JButton("Limpar");
	public JButton btnAdicionar = new JButton("Adicionar");
	public JButton btnCancelar = new JButton("Cancelar");
	public JButton btnAddPeca = new JButton("...");
	
	public JButton btnRemoverArvore;
	public JButton btnAlterarArvore;
	public JButton btnAdicionarArvore;
	//Paineis
	private JPanel p1 = new JPanel();
	private JPanel p4 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	//JTextFields
	public JTextField textFieldIdTomboFinal = new JTextField();
	public JTextField textFieldIdTombo = new JTextField();
	public JTextField txtNmAnimal;
	public JTextField txtNmCientifico;
	public JTextField txtClasse;
	public JTextField txtOrdem;
	public JTextField txtFamilia;
	public JTextField txtTribo;
	public JTextField txtEspecie;
	public JTextField txtIdade;
	public JTextField txtSexo;
	public JTextField txtPeso;
	public JTextField txtCorPelagem;
	public JTextField txtSinais;
	public JTextField txtNmResp;
	//JCheckBoxs
	public JCheckBox chckbxEscama;
	public JCheckBox chckbxPena;
	public JCheckBox chckbxEsteAnimalAinda = new JCheckBox("Este animal ainda n\u00E3o possui pe\u00E7as");
	public JCheckBox chckbxDNAVisceral = new JCheckBox("DNA Tecido Visceral");
	public JCheckBox chckbxDnaCartilagem = new JCheckBox("DNA Cartilagem");
	public JCheckBox chckbxDnaTecidoMuscular = new JCheckBox("DNA Tecido Muscular");
	public JCheckBox chckbxPeloInterescapularDorsal = new JCheckBox("Pelo Interescapular Dorsal");
	public JCheckBox chckbxPeloAbdominal = new JCheckBox("Pelo Abdominal");
	public JCheckBox chckbxPeloCabeaDorsal = new JCheckBox("Pelo Cabe\u00E7a Dorsal");
	//JTextAreas
	public JTextArea txtAreaOssos = new JTextArea();
	public JTextArea txtDsPeca = new JTextArea();
	public JTextArea txtDsProc = new JTextArea();
	//JSpinners
	public JSpinner spinner = new JSpinner();
	public JSpinner spDtEntrada = new JSpinner(new SpinnerDateModel(new Date(), null, null, Calendar.MONTH)); // Seta a data atual
	public JSpinner spDtSaida = new JSpinner(new SpinnerDateModel(new Date(), null, null, Calendar.MONTH));
	public JLabel lblCdTombo = new JLabel("CCBSZooFaa");
	
	public JComboBox cBoxBase;
	public JComboBox cBoxPecas = new JComboBox();
	public JComboBox cBoxNmProcesso = new JComboBox();
	public JTree tree;
	public JTextField textTombo;
	public JTextArea textDesc;
	public JLabel lblLocalAtual;
	public JTextField textNmUser;
	
	// CONSTRUTOR: Cria a GUI e configura conforme a flag de identificação.
	// A flag identifica GUI de adição, alteração ou consulta.
	public CadastroPecaGUI(JDialog Super){
		super(Super);
		setSize(800,573);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		getContentPane().setLayout(null);
		setResizable(false);

		showComponentes();
		validaPermissao();
	}

	private void showComponentes(){
		getContentPane().add(tabbed);
		tabbed.setBounds(0,0,794,545);
		p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tabbed.addTab("Animal", p1);
		p1.setLayout(null);
		
		JPanel p1p1 = new JPanel();
		p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1.setBounds(10, 11, 769, 450);
		p1.add(p1p1);
		p1p1.setLayout(null);
		
		JPanel p1p1p1 = new JPanel();
		p1p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p1.setBounds(10, 11, 336, 82);
		p1p1.add(p1p1p1);
		p1p1p1.setLayout(null);
		
		
		textFieldIdTomboFinal.setText("");
		textFieldIdTomboFinal.setBounds(new Rectangle(130, 38, 90, 25));
		textFieldIdTomboFinal.setBounds(140, 36, 102, 30);
		p1p1p1.add(textFieldIdTomboFinal);
		
		textFieldIdTombo.setBounds(new Rectangle(90, 38, 40, 25));
		textFieldIdTombo.setBounds(100, 36, 40, 30);
		p1p1p1.add(textFieldIdTombo);
		
		lblCdTombo.setBounds(new Rectangle(10, 40, 160, 21));
		lblCdTombo.setBounds(5, 41, 90, 21);
		p1p1p1.add(lblCdTombo);
		
		JLabel lblTitleTombo = new JLabel("N\u00FAmero Tombo:");
		lblTitleTombo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTitleTombo.setBounds(10, 11, 107, 14);
		p1p1p1.add(lblTitleTombo);
		
		btnFiltros.setBounds(247, 39, 82, 25);
		p1p1p1.add(btnFiltros);
		
		JPanel p1p1p2 = new JPanel();
		p1p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2.setBounds(10, 104, 749, 335);
		p1p1.add(p1p1p2);
		p1p1p2.setLayout(null);
		
		JLabel lblCaractersticasDoAnimal = new JLabel("Caracter\u00EDsticas do Animal:");
		lblCaractersticasDoAnimal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCaractersticasDoAnimal.setBounds(10, 11, 308, 14);
		p1p1p2.add(lblCaractersticasDoAnimal);
		
		JPanel p1p1p2p1 = new JPanel();
		p1p1p2p1.setLayout(null);
		p1p1p2p1.setBounds(new Rectangle(15, 45, 245, 185));
		p1p1p2p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2p1.setBounds(21, 99, 351, 225);
		p1p1p2.add(p1p1p2p1);
		
		JLabel lblCaractersticasMorfolgicas = new JLabel("Caracter\u00EDsticas Morfol\u00F3gicas:");
		lblCaractersticasMorfolgicas.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCaractersticasMorfolgicas.setBounds(new Rectangle(5, 0, 225, 20));
		lblCaractersticasMorfolgicas.setBounds(5, 0, 225, 20);
		p1p1p2p1.add(lblCaractersticasMorfolgicas);
		
		JLabel lblClasse = new JLabel("Classe:");
		lblClasse.setBounds(new Rectangle(15, 34, 44, 20));
		lblClasse.setBounds(15, 19, 61, 20);
		p1p1p2p1.add(lblClasse);
		
		JLabel lblOrdem = new JLabel("Ordem:");
		lblOrdem.setBounds(new Rectangle(17, 64, 42, 20));
		lblOrdem.setBounds(15, 60, 56, 20);
		p1p1p2p1.add(lblOrdem);
		
		JLabel lblFamilia = new JLabel("Fam\u00EDlia:");
		lblFamilia.setBounds(new Rectangle(14, 94, 45, 20));
		lblFamilia.setBounds(15, 100, 57, 20);
		p1p1p2p1.add(lblFamilia);
		
		JLabel lblTribo = new JLabel("Tribo:");
		lblTribo.setBounds(new Rectangle(27, 124, 32, 20));
		lblTribo.setBounds(15, 140, 53, 20);
		p1p1p2p1.add(lblTribo);
		
		JLabel lblEspecie = new JLabel("Esp\u00E9cie:");
		lblEspecie.setBounds(new Rectangle(10, 154, 49, 20));
		lblEspecie.setBounds(15, 180, 59, 20);
		p1p1p2p1.add(lblEspecie);
		
		txtClasse = new JTextField();
		txtClasse.setEditable(false);
		txtClasse.setBounds(82, 20, 249, 24);
		p1p1p2p1.add(txtClasse);
		txtClasse.setColumns(10);
		
		txtOrdem = new JTextField();
		txtOrdem.setEditable(false);
		txtOrdem.setBounds(82, 60, 249, 24);
		p1p1p2p1.add(txtOrdem);
		txtOrdem.setColumns(10);
		
		txtFamilia = new JTextField();
		txtFamilia.setEditable(false);
		txtFamilia.setColumns(10);
		txtFamilia.setBounds(82, 100, 249, 24);
		p1p1p2p1.add(txtFamilia);
		
		txtTribo = new JTextField();
		txtTribo.setEditable(false);
		txtTribo.setColumns(10);
		txtTribo.setBounds(82, 140, 249, 24);
		p1p1p2p1.add(txtTribo);
		
		txtEspecie = new JTextField();
		txtEspecie.setEditable(false);
		txtEspecie.setColumns(10);
		txtEspecie.setBounds(83, 180, 249, 24);
		p1p1p2p1.add(txtEspecie);
		
		JPanel p1p1p2p2 = new JPanel();
		p1p1p2p2.setLayout(null);
		p1p1p2p2.setBounds(new Rectangle(15, 240, 245, 175));
		p1p1p2p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2p2.setBounds(382, 99, 358, 225);
		p1p1p2.add(p1p1p2p2);
		
		JLabel lblCaracteristicasGerais = new JLabel();
		lblCaracteristicasGerais.setText("Caracteristicas Gerais:");
		lblCaracteristicasGerais.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCaracteristicasGerais.setBounds(new Rectangle(5, 0, 225, 20));
		lblCaracteristicasGerais.setBounds(5, 0, 225, 20);
		p1p1p2p2.add(lblCaracteristicasGerais);
		
		JLabel lblPeso = new JLabel("Peso:");
		lblPeso.setBounds(new Rectangle(7, 47, 33, 20));
		lblPeso.setBounds(170, 35, 46, 20);
		p1p1p2p2.add(lblPeso);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(new Rectangle(100, 22, 31, 20));
		lblSexo.setBounds(21, 66, 56, 20);
		p1p1p2p2.add(lblSexo);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(new Rectangle(5, 22, 35, 20));
		lblIdade.setBounds(21, 35, 56, 20);
		p1p1p2p2.add(lblIdade);
		
		JLabel lblCorPelagem = new JLabel("Cor da Pelagem:");
		lblCorPelagem.setBounds(new Rectangle(5, 72, 95, 20));
		lblCorPelagem.setBounds(21, 97, 128, 20);
		p1p1p2p2.add(lblCorPelagem);
		
		JLabel lblSinais = new JLabel("Sinais ou Marca\u00E7\u00F5es:");
		lblSinais.setBounds(new Rectangle(5, 97, 120, 20));
		lblSinais.setBounds(21, 153, 209, 20);
		p1p1p2p2.add(lblSinais);
		
		txtIdade = new JTextField();
		txtIdade.setEditable(false);
		txtIdade.setBounds(77, 35, 72, 24);
		p1p1p2p2.add(txtIdade);
		txtIdade.setColumns(10);
		
		txtSexo = new JTextField();
		txtSexo.setEditable(false);
		txtSexo.setBounds(77, 66, 126, 24);
		p1p1p2p2.add(txtSexo);
		txtSexo.setColumns(10);
		
		txtPeso = new JTextField();
		txtPeso.setEditable(false);
		txtPeso.setBounds(224, 35, 112, 24);
		p1p1p2p2.add(txtPeso);
		txtPeso.setColumns(10);
		
		txtCorPelagem = new JTextField();
		txtCorPelagem.setEditable(false);
		txtCorPelagem.setBounds(21, 117, 315, 24);
		p1p1p2p2.add(txtCorPelagem);
		txtCorPelagem.setColumns(10);
		
		txtSinais = new JTextField();
		txtSinais.setEditable(false);
		txtSinais.setBounds(21, 172, 315, 47);
		p1p1p2p2.add(txtSinais);
		txtSinais.setColumns(10);
		
		JPanel p1p1p2p3 = new JPanel();
		p1p1p2p3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2p3.setBounds(20, 36, 720, 53);
		p1p1p2.add(p1p1p2p3);
		p1p1p2p3.setLayout(null);
		
		JLabel labelNmAnimal = new JLabel("Nome:");
		labelNmAnimal.setBounds(26, 28, 47, 14);
		p1p1p2p3.add(labelNmAnimal);
		
		txtNmAnimal = new JTextField();
		txtNmAnimal.setEditable(false);
		txtNmAnimal.setBounds(83, 25, 254, 24);
		p1p1p2p3.add(txtNmAnimal);
		txtNmAnimal.setColumns(10);
		
		JLabel labelNmCientifico = new JLabel("Nome cient\u00EDfico:");
		labelNmCientifico.setBounds(348, 25, 122, 21);
		p1p1p2p3.add(labelNmCientifico);
		
		JLabel lblDadossDoAnimal = new JLabel("Dados do Animal:");
		lblDadossDoAnimal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDadossDoAnimal.setBounds(10, 3, 308, 14);
		p1p1p2p3.add(lblDadossDoAnimal);
		
		txtNmCientifico = new JTextField();
		txtNmCientifico.setEditable(false);
		txtNmCientifico.setBounds(468, 25, 242, 24);
		p1p1p2p3.add(txtNmCientifico);
		txtNmCientifico.setColumns(10);
		
		btnConsultar.setBounds(371, 11, 117, 30);
		p1p1.add(btnConsultar);
		
		btnProximoP1.setBounds(635, 472, 126, 34);
		p1.add(btnProximoP1);
		
		btnLimparP1.setBounds(499, 472, 126, 34);
		p1.add(btnLimparP1);
		tabbed.addTab("Peça", null, p2, null);
		p2.setLayout(null);
		
		JPanel p2p1 = new JPanel();
		p2p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1.setBounds(10, 11, 769, 450);
		p2.add(p2p1);
		p2p1.setLayout(null);
		
		JPanel p2p1p1 = new JPanel();
		p2p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p1.setBounds(10, 171, 361, 267);
		p2p1.add(p2p1p1);
		p2p1p1.setLayout(null);
		
		JLabel labelCaracPeca = new JLabel("Características da Peça");
		labelCaracPeca.setFont(new Font("Tahoma", Font.BOLD, 11));
		labelCaracPeca.setBounds(10, 12, 198, 14);
		p2p1p1.add(labelCaracPeca);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 120, 341, 135);
		p2p1p1.add(scrollPane_3);
		
		scrollPane_3.setViewportView(txtDsPeca);
		
		JLabel lblDsDaPeca = new JLabel("Descrição da Peça*:");
		lblDsDaPeca.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDsDaPeca.setBounds(10, 99, 198, 14);
		p2p1p1.add(lblDsDaPeca);
		
		cBoxPecas.setEnabled(false);
		cBoxPecas.setModel(new DefaultComboBoxModel(new String[] {":: Selecione uma Pe\u00E7a ::"}));
		cBoxPecas.setBounds(10, 67, 303, 24);
		p2p1p1.add(cBoxPecas);
		
		btnAddPeca.setBounds(new Rectangle(210, 62, 30, 24));
		btnAddPeca.setBounds(325, 65, 28, 25);
		p2p1p1.add(btnAddPeca);
		
		JLabel lblNovasPeas = new JLabel("Novas Peças: ");
		lblNovasPeas.setBounds(12, 40, 106, 15);
		p2p1p1.add(lblNovasPeas);
		
		spinner = new JSpinner();
		spinner.setValue(1);
		spinner.setBounds(117, 38, 55, 20);
		p2p1p1.add(spinner);
		
		JPanel p2p1p2 = new JPanel();
		p2p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p2.setBounds(383, 11, 374, 206);
		p2p1.add(p2p1p2);
		p2p1p2.setLayout(null);
		
		chckbxDNAVisceral.setBounds(8, 34, 170, 25);
		p2p1p2.add(chckbxDNAVisceral);
		
		chckbxDnaCartilagem.setBounds(8, 61, 141, 23);
		p2p1p2.add(chckbxDnaCartilagem);
		
		chckbxDnaTecidoMuscular.setBounds(8, 88, 177, 25);
		p2p1p2.add(chckbxDnaTecidoMuscular);
		
		chckbxPeloInterescapularDorsal.setBounds(8, 173, 214, 25);
		p2p1p2.add(chckbxPeloInterescapularDorsal);
		
		chckbxPeloAbdominal.setBounds(8, 117, 157, 25);
		p2p1p2.add(chckbxPeloAbdominal);
		
		chckbxPeloCabeaDorsal.setBounds(8, 146, 189, 25);
		p2p1p2.add(chckbxPeloCabeaDorsal);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTipo.setBounds(8, 12, 147, 14);
		p2p1p2.add(lblTipo);
		
		chckbxEscama = new JCheckBox("Escama");
		chckbxEscama.setBounds(237, 35, 129, 23);
		p2p1p2.add(chckbxEscama);
		
		chckbxPena = new JCheckBox("Pena");
		chckbxPena.setBounds(237, 62, 129, 23);
		p2p1p2.add(chckbxPena);
		
		JPanel p2p1p3 = new JPanel();
		p2p1p3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p2p1p3.setBounds(383, 228, 374, 211);
		p2p1.add(p2p1p3);
		p2p1p3.setLayout(null);
		
		JLabel labelOssos = new JLabel("Descri\u00E7\u00E3o dos Ossos:");
		labelOssos.setFont(new Font("Tahoma", Font.BOLD, 11));
		labelOssos.setBounds(10, 11, 198, 14);
		p2p1p3.add(labelOssos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 354, 164);
		p2p1p3.add(scrollPane);
		
		scrollPane.setViewportView(txtAreaOssos);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(10, 11, 361, 148);
		p2p1.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAnimal = new JLabel("Animal:");
		lblAnimal.setBounds(12, 12, 71, 15);
		panel_1.add(lblAnimal);
		chckbxEsteAnimalAinda.setBounds(8, 60, 331, 25);
		panel_1.add(chckbxEsteAnimalAinda);
		chckbxEsteAnimalAinda.setEnabled(false);
		
		textTombo = new JTextField();
		textTombo.setEditable(false);
		textTombo.setBounds(12, 27, 179, 25);
		panel_1.add(textTombo);
		textTombo.setColumns(10);
		
		cBoxBase = new JComboBox();
		cBoxBase.setModel(new DefaultComboBoxModel(new String[] {":: Selecione uma Peça ::"}));
		cBoxBase.setBounds(12, 112, 303, 24);
		panel_1.add(cBoxBase);
		
		JLabel lblPeasCadastradas = new JLabel("Peças Base:");
		lblPeasCadastradas.setBounds(12, 93, 179, 15);
		panel_1.add(lblPeasCadastradas);
		
		btnProximoP2.setBounds(653, 472, 126, 34);
		p2.add(btnProximoP2);
		
		btnLimparP2.setBounds(517, 472, 126, 34);
		p2.add(btnLimparP2);
		
		JLabel lblCampos = new JLabel("* - Campos Obrigatórios");
		lblCampos.setBounds(20, 473, 183, 15);
		p2.add(lblCampos);
		tabbed.addTab("Processo", null, p3, null);
		p3.setLayout(null);
		
		JPanel p3p1 = new JPanel();
		p3p1.setBounds(10, 11, 769, 450);
		p3.add(p3p1);
		p3p1.setLayout(null);
		
		JPanel p3p1p1 = new JPanel();
		p3p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p3p1p1.setBounds(10, 11, 379, 162);
		p3p1.add(p3p1p1);
		p3p1p1.setLayout(null);
		
		JLabel lblNmProcesso = new JLabel("Nome do Processo*:");
		lblNmProcesso.setBounds(31, 23, 185, 14);
		p3p1p1.add(lblNmProcesso);
		
		cBoxNmProcesso.setModel(new DefaultComboBoxModel(new String[] {":: Selecione o Processo ::", "Viscerais", "Esqueleto", "Anat\u00F4mico", "Taxonomia"}));
		cBoxNmProcesso.setBounds(31, 38, 338, 28);
		p3p1p1.add(cBoxNmProcesso);
		
		txtNmResp = new JTextField();
		txtNmResp.setColumns(10);
		txtNmResp.setBounds(31, 130, 338, 28);
		p3p1p1.add(txtNmResp);
		
		JLabel lblNmRespProcesso = new JLabel("Nome do Responsável pelo Processo*:");
		lblNmRespProcesso.setBounds(31, 115, 290, 14);
		p3p1p1.add(lblNmRespProcesso);
		
		JLabel lblProcesso = new JLabel("Processo:");
		lblProcesso.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProcesso.setBounds(10, 5, 90, 14);
		p3p1p1.add(lblProcesso);
		
		JLabel lblUsuarioResponsvel = new JLabel("Usuário Responsável: ");
		lblUsuarioResponsvel.setBounds(31, 70, 185, 15);
		p3p1p1.add(lblUsuarioResponsvel);
		
		textNmUser = new JTextField();
		textNmUser.setBounds(31, 86, 338, 27);
		textNmUser.setEditable(false);
		p3p1p1.add(textNmUser);
		textNmUser.setColumns(10);
		
		JPanel p3p1p2 = new JPanel();
		p3p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p3p1p2.setBounds(399, 11, 360, 162);
		p3p1.add(p3p1p2);
		p3p1p2.setLayout(null);
		
		JLabel lblDataProc = new JLabel("Data:");
		lblDataProc.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDataProc.setBounds(10, 11, 46, 14);
		p3p1p2.add(lblDataProc);
		
		JLabel lblDataEntrada = new JLabel("Data Entrada:");
		lblDataEntrada.setBounds(31, 40, 181, 14);
		p3p1p2.add(lblDataEntrada);
		
		JLabel lblDataSaida = new JLabel("Data Saida:");
		lblDataSaida.setBounds(31, 96, 181, 14);
		p3p1p2.add(lblDataSaida);
		
		
		spDtEntrada.setBounds(71, 65, 120, 25);
		p3p1p2.add(spDtEntrada);
		spDtEntrada.setEditor(new JSpinner.DateEditor(spDtEntrada, "dd/MM/yyyy"));
		
		spDtSaida.setBounds(71, 121, 120, 25);
		spDtSaida.setEnabled(false); // Mudar quando implementar empréstimo
		p3p1p2.add(spDtSaida);
		spDtSaida.setEditor(new JSpinner.DateEditor(spDtSaida, "dd/MM/yyyy"));
		
		JPanel p3p1p3 = new JPanel();
		p3p1p3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p3p1p3.setBounds(10, 184, 749, 255);
		p3p1.add(p3p1p3);
		p3p1p3.setLayout(null);
		
		JLabel lblDescriesDoProcesso = new JLabel("Descri\u00E7\u00F5es do Processo:");
		lblDescriesDoProcesso.setBounds(10, 11, 327, 14);
		lblDescriesDoProcesso.setFont(new Font("Tahoma", Font.BOLD, 11));
		p3p1p3.add(lblDescriesDoProcesso);
		
		JLabel lblDescDoProcesso = new JLabel("Descrição do Processo*:");
		lblDescDoProcesso.setBounds(31, 40, 233, 14);
		p3p1p3.add(lblDescDoProcesso);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(31, 70, 334, 174);
		p3p1p3.add(scrollPane_1);
		
		scrollPane_1.setViewportView(txtDsProc);
		
		btnProximoP3.setBounds(653, 472, 126, 34);
		p3.add(btnProximoP3);
		
		btnLimparP3.setBounds(517, 472, 126, 34);
		p3.add(btnLimparP3);
		
		JLabel lblCampos_1 = new JLabel("* - Campos Obrigatórios");
		lblCampos_1.setBounds(20, 482, 194, 15);
		p3.add(lblCampos_1);
		tabbed.addTab("Localidade", null, p4, null);
		p4.setLayout(null);
		
		JPanel p4p1 = new JPanel();
		p4p1.setBounds(10, 11, 769, 450);
		p4.add(p4p1);
		p4p1.setLayout(null);
		
		JPanel p4p1p1 = new JPanel();
		p4p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p4p1p1.setBounds(10, 11, 479, 428);
		p4p1.add(p4p1p1);
		p4p1p1.setLayout(null);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(0, 0, 479, 428);
		p4p1p1.add(scrollPane_4);
		
		tree =  new JTree(new HierarquiaArvoreLocalController());
		scrollPane_4.setViewportView(tree);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(522, 11, 235, 428);
		p4p1.add(panel);
		panel.setLayout(null);
		
		btnAdicionarArvore = new JButton("Adicionar", IconAdd);
		btnAdicionarArvore.setBounds(31, 25, 174, 36);
		panel.add(btnAdicionarArvore);
		
		btnAlterarArvore = new JButton("Alterar", altIcon);
		btnAlterarArvore.setBounds(31, 73, 174, 36);
		panel.add(btnAlterarArvore);
		
		btnRemoverArvore = new JButton("Remover", remIcon);
	
		btnRemoverArvore.setBounds(31, 116, 174, 36);
		panel.add(btnRemoverArvore);
		
		btnAdicionar.setBounds(653, 472, 126, 34);
		p4.add(btnAdicionar);
		
		btnCancelar.setBounds(517, 472, 126, 34);
		p4.add(btnCancelar);
		
		JScrollPane scrollDesc = new JScrollPane();
		scrollDesc.setBounds(12, 282, 211, 134);
		panel.add(scrollDesc);
		
		textDesc = new JTextArea();
		textDesc.setEditable(false);
		scrollDesc.setViewportView(textDesc);

		JLabel lblDescLocal = new JLabel("Descrição do Local:");
		lblDescLocal.setBounds(12, 265, 211, 15);
		panel.add(lblDescLocal);
		
		lblLocalAtual = new JLabel("");
		lblLocalAtual.setBounds(20, 460, 485, 15);
		p4.add(lblLocalAtual);
		
		
		tabbed.setEnabledAt(1, false); //peca
		tabbed.setEnabledAt(2, false); //processo
		tabbed.setEnabledAt(3, false); //localidade
	}

	public boolean validar(){
		boolean ok = true;

		return ok;
	}
	
	private void validaPermissao(){
		if(!App.getTipoUser().equals("Administrador")){
			btnAlterarArvore.setEnabled(false);
			btnRemoverArvore.setEnabled(false);
		}
	}

	public void addOuvintes(ActionListener ouvinte){
		btnAdicionar.addActionListener(ouvinte);
		btnConsultar.addActionListener(ouvinte);
		btnFiltros.addActionListener(ouvinte);
		btnProximoP1.addActionListener(ouvinte);
		btnProximoP2.addActionListener(ouvinte);
		btnProximoP3.addActionListener(ouvinte);
		btnLimparP1.addActionListener(ouvinte);
		btnLimparP2.addActionListener(ouvinte);
		btnLimparP3.addActionListener(ouvinte);
		btnCancelar.addActionListener(ouvinte);
		btnAddPeca.addActionListener(ouvinte);
		tree.addTreeSelectionListener((TreeSelectionListener) ouvinte);
		btnAdicionarArvore.addActionListener(ouvinte);
		btnAlterarArvore.addActionListener(ouvinte);
		btnRemoverArvore.addActionListener(ouvinte);
		tabbed.addMouseListener((MouseListener)ouvinte);
	}
}

