package br.edu.angloamericano.cbio.gui;

import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class CadastroDadosGUI extends JDialog{

	private Icon addIcon = new ImageIcon("images/addIcon.gif");
	private JPanel pDadosPessoais = new JPanel();
	private JLabel lEstado = new JLabel("Estado a qual pertence");
	public JComboBox estado = new JComboBox();
	private JLabel lClasse = new JLabel("Classe a qual pertence");
	public JComboBox classe = new JComboBox();
	private JLabel lCidade = new JLabel("Nome da cidade");
	public JTextField cidade = new JTextField();
	private JLabel lOrdem = new JLabel("Nome da ordem");
	public JTextField ordem = new JTextField();
	private JLabel lEspecie = new JLabel("Nome da espécie");
	public JTextField especie = new JTextField();
	private JLabel lTribo = new JLabel("Nome da tribo");
	public JTextField tribo = new JTextField();
	public JButton btnAdd = new JButton("Adicionar",addIcon);
	private JLabel lLocalidade = new JLabel("Nome da localidade");
	public JTextField localidade = new JTextField();
	private JLabel lQualFamilia = new JLabel("Família a qual pertence");
	private JLabel lFamilia = new JLabel("Nome da família");
	public JTextField familia = new JTextField();
	private JLabel lQualCidade = new JLabel("Cidade a qual pertence");
	private JLabel lQualOrdem = new JLabel("Ordem a qual pertence");
	private JLabel lQualTribo = new JLabel("Tribo a qual pertence");
	public JComboBox cidadeCombo = new JComboBox();
	public JComboBox ordemCombo = new JComboBox();
	public JComboBox familiaCombo = new JComboBox();
	public JComboBox triboCombo = new JComboBox();

	public CadastroDadosGUI(JDialog Super){
		super(Super);
		setSize(444,342);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-154),(int) (Funcoes.centraliza().getHeight()/2-102));
		getContentPane().setLayout(null);
		setResizable(false);
		configuraJanela(2);
	}	

	public void configuraJanela(int flag){
		pDadosPessoais.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pDadosPessoais.setLayout(null);
		pDadosPessoais.setBounds(10, 11, 286, 119);
		getContentPane().add(pDadosPessoais);
		lEstado.setBounds(13, 66, 151, 14);
		estado.setBounds(13, 80, 182, 25);
		lClasse.setBounds(13, 66, 151, 14);
		classe.setBounds(13, 80, 182, 25);
		cidade.setBounds(10, 25, 258, 30);
		lCidade.setBounds(13, 11, 179, 14);
		ordem.setBounds(10, 25, 258, 30);
		lOrdem.setBounds(13, 11, 179, 14);
		btnAdd.setBounds(173, 137, 119, 30);
		getContentPane().add(btnAdd);

		if(flag == 1){ // Configura como janela de cadastro de cidades
			setTitle("Adicionar cidade");
			pDadosPessoais.add(cidade);
			pDadosPessoais.add(lCidade);
			pDadosPessoais.add(lEstado);
			pDadosPessoais.add(estado);

		}else
			if(flag == 2){ // Configura como janela de cadastro de localidades
				setTitle("Adicionar localidade");
				setSize(308,262);
				setLocation((int)(Funcoes.centraliza().getWidth()/2-154),
						(int) (Funcoes.centraliza().getHeight()/2-131));
				pDadosPessoais.setBounds(10, 11, 286, 177);
				btnAdd.setBounds(173, 195, 119, 30);
				localidade.setBounds(10, 25, 258, 30);
				pDadosPessoais.add(localidade);
				lLocalidade.setBounds(13, 11, 179, 14);
				pDadosPessoais.add(lLocalidade);
				cidadeCombo.setBounds(13, 130, 182, 25);
				pDadosPessoais.add(cidadeCombo);
				cidadeCombo.addItem(":: Selecione uma cidade ::");
				cidadeCombo.setEnabled(false);
				lQualCidade.setBounds(13, 116, 151, 14);
				pDadosPessoais.add(lQualCidade);
				pDadosPessoais.add(lEstado);
				pDadosPessoais.add(estado);
			}else
				if(flag == 3){ // Configura como janela de cadastro de ordens
					setTitle("Adicionar ordem");
					pDadosPessoais.add(ordem);
					pDadosPessoais.add(lOrdem);
					pDadosPessoais.add(classe);
					pDadosPessoais.add(lClasse);
				}else
					if(flag == 4){ // Configura como janela de cadastro de fam�lias
						setTitle("Adicionar família");
						setSize(308,262);
						setLocation((int)(Funcoes.centraliza().getWidth()/2-154),
								(int) (Funcoes.centraliza().getHeight()/2-131));
						pDadosPessoais.setBounds(10, 11, 286, 177);
						btnAdd.setBounds(173, 195, 119, 30);
						familia.setBounds(10, 25, 258, 30);
						pDadosPessoais.add(familia);
						lFamilia.setBounds(13, 11, 179, 14);
						pDadosPessoais.add(lFamilia);
						ordemCombo.setBounds(13, 130, 182, 25);
						pDadosPessoais.add(ordemCombo);
						ordemCombo.addItem(":: Selecione uma ordem ::");
						ordemCombo.setEnabled(false);
						lQualOrdem.setBounds(13, 116, 151, 14);
						pDadosPessoais.add(lQualOrdem);
						pDadosPessoais.add(lClasse);
						pDadosPessoais.add(classe);
					}else
						if(flag == 5){ // Configura como janela de cadastro de tribos
							setTitle("Adicionar tribo");
							setSize(308,321);
							setLocation((int)(Funcoes.centraliza().getWidth()/2-154),
									(int) (Funcoes.centraliza().getHeight()/2-161));
							pDadosPessoais.setBounds(10, 11, 286, 235);
							btnAdd.setBounds(173, 253, 119, 30);
							ordemCombo.setBounds(13, 130, 182, 25);
							familiaCombo.setBounds(13, 180, 182, 25);
							pDadosPessoais.add(lQualOrdem);
							pDadosPessoais.add(ordemCombo);
							pDadosPessoais.add(lQualFamilia);
							pDadosPessoais.add(familiaCombo);
							ordemCombo.addItem(":: Selecione uma ordem ::");
							familiaCombo.addItem(":: Selecione uma família ::");
							ordemCombo.setEnabled(false);
							familiaCombo.setEnabled(false);
							lQualOrdem.setBounds(13, 116, 151, 14);
							lQualFamilia.setBounds(13, 166, 151, 14);
							pDadosPessoais.add(lClasse);
							pDadosPessoais.add(classe);
							tribo.setBounds(10, 25, 258, 30);
							lTribo.setBounds(13, 11, 179, 14);
							pDadosPessoais.add(tribo);
							pDadosPessoais.add(lTribo);
						}else
							if(flag == 6){ // Configura como janela de cadastro de esp�cies
								setTitle("Adicionar espécie");
								setSize(308,380);
								setLocation((int)(Funcoes.centraliza().getWidth()/2-154),
										(int) (Funcoes.centraliza().getHeight()/2-190));
								pDadosPessoais.setBounds(10, 11, 286, 293);
								btnAdd.setBounds(173, 311, 119, 30);
								ordemCombo.setBounds(13, 130, 182, 25);
								familiaCombo.setBounds(13, 180, 182, 25);
								triboCombo.setBounds(13, 230, 182, 25);
								pDadosPessoais.add(lQualOrdem);
								pDadosPessoais.add(ordemCombo);
								pDadosPessoais.add(lQualFamilia);
								pDadosPessoais.add(familiaCombo);
								pDadosPessoais.add(lQualTribo);
								pDadosPessoais.add(triboCombo);
								ordemCombo.addItem(":: Selecione uma ordem ::");
								familiaCombo.addItem(":: Selecione uma fam�lia ::");
								triboCombo.addItem(":: Selecione uma tribo ::");
								triboCombo.setEnabled(false);
								ordemCombo.setEnabled(false);
								familiaCombo.setEnabled(false);
								lQualOrdem.setBounds(13, 116, 151, 14);
								lQualFamilia.setBounds(13, 166, 151, 14);
								lQualTribo.setBounds(13, 216, 151, 14);
								pDadosPessoais.add(lClasse);
								pDadosPessoais.add(classe);
								especie.setBounds(10, 25, 258, 30);
								lEspecie.setBounds(13, 11, 179, 14);
								pDadosPessoais.add(especie);
								pDadosPessoais.add(lEspecie);
							}
	}

	public boolean validar(int flag){
		boolean ok = true;

		if(flag == 1){
			if(cidade.getText().isEmpty()){
				lCidade.setForeground(Color.RED);	ok = false;	}
			if(estado.getSelectedIndex() < 1){
				lEstado.setForeground(Color.RED);	ok = false;	}
		}else
			if(flag == 2){
				if(cidadeCombo.getSelectedIndex() < 1){
					lQualCidade.setForeground(Color.RED);	ok = false;	}
				if(localidade.getText().isEmpty()){
					lLocalidade.setForeground(Color.RED);	ok = false;	}
			}else
				if(flag == 3){
					if(ordem.getText().isEmpty()){
						lOrdem.setForeground(Color.RED);	ok = false;	}
					if(classe.getSelectedIndex() < 1){
						lClasse.setForeground(Color.RED);	ok = false;	}
				}else
					if(flag == 4){
						if(ordemCombo.getSelectedIndex() < 1){
							lQualOrdem.setForeground(Color.RED);	ok = false;	}
						if(familia.getText().isEmpty()){
							lFamilia.setForeground(Color.RED);	ok = false;	}
					}else
						if(flag == 5){
							if(familiaCombo.getSelectedIndex() < 1){
								lQualFamilia.setForeground(Color.RED);	ok = false;	}
							if(tribo.getText().isEmpty()){
								lTribo.setForeground(Color.RED);	ok = false;	}
						}else
							if(flag == 6){
								if(triboCombo.getSelectedIndex() < 1){
									lQualTribo.setForeground(Color.RED);	ok = false;	}
								if(especie.getText().isEmpty()){
									lEspecie.setForeground(Color.RED);	ok = false;	}
							}

		return ok;
	}

	public void reconfigLabels(){
		lCidade.setForeground(Color.BLACK);
		lEstado.setForeground(Color.BLACK);
		lQualCidade.setForeground(Color.BLACK);
		lLocalidade.setForeground(Color.BLACK);
		lOrdem.setForeground(Color.BLACK);
		lClasse.setForeground(Color.BLACK);
		lFamilia.setForeground(Color.BLACK);
		lQualOrdem.setForeground(Color.BLACK);
		lQualFamilia.setForeground(Color.BLACK);
		lTribo.setForeground(Color.BLACK);
		lEspecie.setForeground(Color.BLACK);
		lQualTribo.setForeground(Color.BLACK);
	}

	public void addOuvintes(ActionListener ouvinte){
		btnAdd.addActionListener(ouvinte);
		estado.addActionListener(ouvinte);
		classe.addActionListener(ouvinte);
		ordemCombo.addActionListener(ouvinte);
		familiaCombo.addActionListener(ouvinte);
		triboCombo.addActionListener(ouvinte);
	}
}
