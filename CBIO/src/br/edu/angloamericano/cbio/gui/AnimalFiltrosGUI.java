package br.edu.angloamericano.cbio.gui;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionListener;

import javax.swing.border.EtchedBorder;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextField;

import br.edu.angloamericano.cbio.util.Funcoes;

public class AnimalFiltrosGUI extends JDialog {
	
	private static final long serialVersionUID = 1L;
	public JTextField txtNmAnimal = new JTextField();
	public JTextField txtNmCientifico = new JTextField();
	public JComboBox cBoxIdade = new JComboBox(new String[] {":: Selecione uma idade ::", "Rec\u00E1m-Nascido", "Jovem", "Adulto", "Meia-Idade", "Velho"});
	public JComboBox cBoxSexo = new JComboBox(new String[] {":: Selecione o Sexo ::", "Macho", "F\u00EAmea"});
	public JComboBox classe = new JComboBox(new String[] {":: Selecione uma classe::"});
	public JComboBox ordem = new JComboBox(new String[] {":: Selecione uma ordem::"});
	public JComboBox familia = new JComboBox(new String[] {":: Selecione uma familia::"});
	public JComboBox tribo = new JComboBox(new String[] {":: Selecione uma tribo::"});
	public JComboBox especie = new JComboBox(new String[] {":: Selecione uma especie::"});
	public JButton btnLimpar = new JButton("Limpar");
	public JButton btnOk = new JButton("OK");
	public JButton btnCancelar = new JButton("Cancelar");
	
	public AnimalFiltrosGUI(JDialog Super) {
		super(Super);
		setTitle("Filtros de Busca");
		setSize(690, 360);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		//getContentPane().setLayout(null);
		setResizable(false);
		showComponentes();
	}
	
	public void showComponentes(){
		JPanel p1 = new JPanel();
		getContentPane().add(p1, BorderLayout.CENTER);
		p1.setLayout(null);
		
		JPanel p1p1 = new JPanel();
		p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1.setBounds(12, 13, 657, 306);
		p1.add(p1p1);
		p1p1.setLayout(null);
		
		JLabel lblFiltros = new JLabel("Filtros:");
		lblFiltros.setBounds(12, 13, 67, 14);
		lblFiltros.setFont(new Font("Tahoma", Font.BOLD, 11));
		p1p1.add(lblFiltros);
		
		JPanel p1p1p2 = new JPanel();
		p1p1p2.setLayout(null);
		p1p1p2.setBounds(new Rectangle(15, 45, 245, 185));
		p1p1p2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p2.setBounds(349, 41, 297, 221);
		p1p1.add(p1p1p2);
		
		JLabel lblCaractersticasMorfolgicas = new JLabel("Caracter\u00EDsticas Morfol\u00F3gicas:");
		lblCaractersticasMorfolgicas.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCaractersticasMorfolgicas.setBounds(new Rectangle(5, 0, 225, 20));
		lblCaractersticasMorfolgicas.setBounds(5, 0, 225, 20);
		p1p1p2.add(lblCaractersticasMorfolgicas);
		
		JLabel lblClasse = new JLabel("Classe:");
		lblClasse.setBounds(new Rectangle(15, 34, 44, 20));
		lblClasse.setBounds(16, 28, 74, 20);
		p1p1p2.add(lblClasse);
		
		classe.setBounds(new Rectangle(59, 32, 150, 24));
		classe.setBounds(69, 26, 218, 25);
		//classe.addItem(":: Selecione uma classe::");
		p1p1p2.add(classe);
		
		JLabel lblOrdem = new JLabel("Ordem:");
		lblOrdem.setBounds(new Rectangle(17, 64, 42, 20));
		lblOrdem.setBounds(16, 73, 61, 20);
		p1p1p2.add(lblOrdem);
				
		ordem.setEnabled(false);
		ordem.setEditable(false);
		ordem.setBounds(new Rectangle(59, 62, 150, 24));
		ordem.setBounds(69, 71, 218, 24);
		p1p1p2.add(ordem);
		
		JLabel lblFamlia = new JLabel("Fam\u00EDlia:");
		lblFamlia.setBounds(new Rectangle(14, 94, 45, 20));
		lblFamlia.setBounds(15, 113, 50, 20);
		p1p1p2.add(lblFamlia);
		
		familia.setEnabled(false);
		familia.setEditable(false);
		familia.setBounds(new Rectangle(59, 92, 150, 24));
		familia.setBounds(69, 107, 218, 25);
		p1p1p2.add(familia);
		
		JLabel lblTribo = new JLabel("Tribo:");
		lblTribo.setBounds(new Rectangle(27, 124, 32, 20));
		lblTribo.setBounds(16, 149, 49, 20);
		p1p1p2.add(lblTribo);
		
		tribo.setEnabled(false);
		tribo.setEditable(false);
		tribo.setBounds(new Rectangle(59, 122, 150, 24));
		tribo.setBounds(69, 144, 218, 25);
		p1p1p2.add(tribo);
		
		JLabel lblEspcie = new JLabel("Esp\u00E9cie:");
		lblEspcie.setBounds(new Rectangle(10, 154, 49, 20));
		lblEspcie.setBounds(16, 183, 61, 20);
		p1p1p2.add(lblEspcie);
		
		especie.setEnabled(false);
		especie.setEditable(false);
		especie.setBounds(new Rectangle(59, 152, 150, 24));
		especie.setBounds(69, 181, 218, 25);
		p1p1p2.add(especie);
		
		JPanel p1p1p1 = new JPanel();
		p1p1p1.setLayout(null);
		p1p1p1.setBounds(new Rectangle(15, 240, 245, 175));
		p1p1p1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		p1p1p1.setBounds(12, 41, 325, 221);
		p1p1.add(p1p1p1);
		
		JLabel lblCaracteristicasGerais = new JLabel();
		lblCaracteristicasGerais.setText("Caracteristicas Gerais:");
		lblCaracteristicasGerais.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCaracteristicasGerais.setBounds(new Rectangle(5, 0, 225, 20));
		lblCaracteristicasGerais.setBounds(12, 0, 225, 20);
		p1p1p1.add(lblCaracteristicasGerais);
		
		JLabel label_12 = new JLabel("Sexo:");
		label_12.setBounds(new Rectangle(100, 22, 31, 20));
		label_12.setBounds(29, 173, 58, 20);
		p1p1p1.add(label_12);
		
		JLabel label_13 = new JLabel("Idade:");
		label_13.setBounds(new Rectangle(5, 22, 35, 20));
		label_13.setBounds(29, 135, 35, 20);
		p1p1p1.add(label_13);
		
		cBoxSexo.setBounds(new Rectangle(130, 20, 100, 24));
		cBoxSexo.setBounds(85, 171, 158, 25);
		p1p1p1.add(cBoxSexo);
		
		cBoxIdade.setBounds(new Rectangle(130, 20, 100, 24));
		cBoxIdade.setBounds(85, 133, 225, 25);
		p1p1p1.add(cBoxIdade);
		
		JLabel label = new JLabel("Nome:");
		label.setBounds(29, 28, 47, 14);
		p1p1p1.add(label);
		
		JLabel label_1 = new JLabel("Nome cient\u00EDfico:");
		label_1.setBounds(29, 79, 109, 21);
		p1p1p1.add(label_1);
		
		txtNmAnimal.setBounds(29, 44, 281, 22);
		p1p1p1.add(txtNmAnimal);
		txtNmAnimal.setColumns(10);
		
		txtNmCientifico.setBounds(29, 98, 281, 22);
		p1p1p1.add(txtNmCientifico);
		txtNmCientifico.setColumns(10);
		
		btnCancelar.setBounds(551, 275, 95, 25);
		p1p1.add(btnCancelar);
		
		btnOk.setBounds(444, 275, 95, 25);
		p1p1.add(btnOk);
		
		btnLimpar.setBounds(337, 275, 95, 25);
		p1p1.add(btnLimpar);
	}

	public void addOuvintes(ActionListener ouvinte) {
		btnCancelar.addActionListener(ouvinte);
		btnLimpar.addActionListener(ouvinte);
		btnOk.addActionListener(ouvinte);
		
		classe.addActionListener(ouvinte);
		ordem.addActionListener(ouvinte);
		familia.addActionListener(ouvinte);
		tribo.addActionListener(ouvinte);
		especie.addActionListener(ouvinte);
	}
	
}
