package br.edu.angloamericano.cbio.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class LoginGUI extends JFrame{
	private ImageIcon imagemTituloJanela = new ImageIcon("images/icon.gif");  
	public JTextField login = new JTextField();
	public JPasswordField senha = new JPasswordField();
	public JButton btnEntrar = new JButton("Entrar");
	public JButton btnSair = new JButton("Sair");
	
	private JPanel back = new JPanel(){
		// Foi alterado o m�todo paintComponent desse JPanel para usar uma imagem de background
		protected void paintComponent(Graphics g) {
			Image planoDeFundo = Toolkit.getDefaultToolkit().getImage("images/login.jpg");
			super.paintComponent(g);
			g.drawImage(planoDeFundo, 0, 0, this );   
		}
	};

	/**
	 * Construtor ja janela de login
	 */
	public LoginGUI() {
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setLayout(null);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,220);
		setResizable(false);
		setLocation((int)(Funcoes.centraliza().getWidth()/2-200),(int)(Funcoes.centraliza().getHeight()/2-110));

		showComponentes();
	}

	/**
	 * Configura a janela de login
	 */
	private void showComponentes(){
		setIconImage(imagemTituloJanela.getImage());
		back.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		back.setBackground(Color.WHITE);
		getContentPane().add(back);
		back.setLayout(null);
		back.setBounds(0,0,400,220);
		login.setBounds(213, 70, 165, 25);
		back.add(login);
		login.setBorder(null);
		login.setColumns(10);
		senha.setBounds(213, 122, 165, 25);
		back.add(senha);
		senha.setBorder(null);
		btnEntrar.setBackground(null);
		btnEntrar.setBorder(null);
		btnEntrar.setBounds(232, 164, 63, 25);
		back.add(btnEntrar);
		btnSair.setBackground(null);
		btnSair.setBorder(null);
		btnSair.setBounds(305, 164, 63, 25);
		back.add(btnSair);
		getRootPane().setDefaultButton(btnEntrar); //Adiciona o botão entrar com ação para o botão enter
	}
	
	// Adiciona as ações aos componentes da janela
	public void addOuvinte(ActionListener ouvinte) {
		btnEntrar.addActionListener(ouvinte);
		btnSair.addActionListener(ouvinte);
	}
}
