package br.edu.angloamericano.cbio.gui;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.text.MaskFormatter;

import br.edu.angloamericano.cbio.util.Funcoes;


public class AdicionarEnderecoGUI extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JTextField textFieldNmRua;
	public JTextField textField_ComplementoRua;
	public JFormattedTextField textField_CEP; 
	public JTextField textField_NomeLocal;
	public JFormattedTextField textField_CoordenadaGeografica;
	
	public JTextArea textDsLocal = new JTextArea();
	
	public JButton btnOk = new JButton("OK");
	public JButton btnCancelar = new JButton("Cancelar");
	
	/*
	 * Construtor da classe
	 */
	public AdicionarEnderecoGUI(JDialog Super) {
		super(Super);
		setTitle("Nova Localidade");
		setSize(600,500);
		setResizable(false);
		
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		
		showComponents(); // Mostra os componentes
	}
	
	// Construtor sem parametros, criado apenas para teste
	public AdicionarEnderecoGUI() {
		super();
		setVisible(true);
		setSize(600,500);
		setResizable(false);
		
		setLocation((int)(Funcoes.centraliza().getWidth()/2-400),(int) (Funcoes.centraliza().getHeight()/2-287));
		
		showComponents();
	}
	
	public void showComponents(){
		setTitle("Cadastrar Endere\u00E7o");
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 11, 561, 453);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNmRua = new JLabel("**Nome Rua :");
		lblNmRua.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNmRua.setBounds(48, 11, 89, 14);
		panel.add(lblNmRua);
		
		textFieldNmRua = new JTextField("");
		textFieldNmRua.setBounds(136, 8, 357, 23);
		panel.add(textFieldNmRua);
		textFieldNmRua.setColumns(10);
		
		JLabel lblComplemento = new JLabel("**Complemento :");
		lblComplemento.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblComplemento.setBounds(20, 42, 117, 14);
		panel.add(lblComplemento);
		
		textField_ComplementoRua = new JTextField("");
		textField_ComplementoRua.setBounds(136, 39, 177, 23);
		panel.add(textField_ComplementoRua);
		textField_ComplementoRua.setColumns(10);
		
		try  
        {
			textField_CEP = new JFormattedTextField(new MaskFormatter("#####-###"));
        }catch (ParseException excp) {}
			textField_CEP.setBounds(136, 70, 177, 23);
			panel.add(textField_CEP);
			textField_CEP.setColumns(10);
		
		JLabel lblCep = new JLabel("** CEP :");
		lblCep.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCep.setBounds(80, 67, 57, 14);
		panel.add(lblCep);
		
		textField_NomeLocal = new JTextField("");
		textField_NomeLocal.setBounds(136, 101, 355, 23);
		panel.add(textField_NomeLocal);
		textField_NomeLocal.setColumns(10);
		
		JLabel lblNomeLocal = new JLabel("Nome Local : ");
		lblNomeLocal.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNomeLocal.setBounds(48, 104, 89, 14);
		panel.add(lblNomeLocal);
		
		JLabel lblCoordenada = new JLabel("Ponto Geografico :");
		lblCoordenada.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCoordenada.setBounds(10, 135, 147, 14);
		panel.add(lblCoordenada);
		
		MaskFormatter mask = null;
		try {
			mask = new MaskFormatter("##,##º ##,##\'' ##,##\" ? - ##,##º ##,##\'' ##,##\" ?");
			mask.setValidCharacters("0123456789ESNWesnw");
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		textField_CoordenadaGeografica = new JFormattedTextField(mask);
		textField_CoordenadaGeografica.setBounds(136, 132, 355, 23);
		panel.add(textField_CoordenadaGeografica);
		textField_CoordenadaGeografica.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(10, 178, 541, 231);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 521, 209);
		panel_1.add(scrollPane);
		
		
		scrollPane.setViewportView(textDsLocal);
		
		JLabel lblDescriaoLocal = new JLabel("Descri\u00E7ao Local :");
		lblDescriaoLocal.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDescriaoLocal.setBounds(10, 160, 127, 14);
		panel.add(lblDescriaoLocal);
		
		
		btnOk.setBounds(351, 420, 89, 23);
		panel.add(btnOk);
		
		
		btnCancelar.setBounds(452, 420, 99, 23);
		panel.add(btnCancelar);
		
		JLabel lblCamposObrigtorios = new JLabel("** Campos Obrigátorios");
		lblCamposObrigtorios.setBounds(20, 421, 177, 15);
		panel.add(lblCamposObrigtorios);

	}

	public void addOuvintes(ActionListener ouvinte) {
		textFieldNmRua.addActionListener(ouvinte);
		textField_ComplementoRua.addActionListener(ouvinte);
		textField_CEP.addActionListener(ouvinte);
		textField_NomeLocal.addActionListener(ouvinte);
		textField_CoordenadaGeografica.addActionListener(ouvinte);
		
		btnOk.addActionListener(ouvinte);
		btnCancelar.addActionListener(ouvinte);
	}
}
