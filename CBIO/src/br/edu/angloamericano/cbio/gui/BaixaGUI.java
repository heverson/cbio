package br.edu.angloamericano.cbio.gui;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import br.edu.angloamericano.cbio.app.App;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Calendar;

@SuppressWarnings("serial")
public class BaixaGUI extends JDialog{
	
	public JButton btnCancelar = new JButton("Cancelar");
	public JButton btnSalvar = new JButton("Salvar");
	public JLabel lblNomeresp = new JLabel();
	public JTextArea textArea = new JTextArea();
	public JSpinner spinner = new JSpinner(new SpinnerDateModel(new Date(), null, null, Calendar.MONTH));
	public BaixaGUI(JDialog Super){
		super(Super);
		setTitle("Remover Peça do Acervo");
		setSize(458,221);
		setResizable(false);
		showComponents(); // Mostra os componentes da janel
	}
	
	/*
	 *Configura a interface gráfica 
	 */
	private void showComponents(){
		
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 458, 221);
		getContentPane().add(panel);
		panel.setLayout(null);

		btnCancelar.setBounds(321, 169, 117, 25);
		panel.add(btnCancelar);

		btnSalvar.setBounds(201, 169, 117, 25);
		panel.add(btnSalvar);
		
		JLabel lblResponsvel = new JLabel("Responsável: ");
		lblResponsvel.setBounds(12, 12, 106, 15);
		panel.add(lblResponsvel);

		lblNomeresp.setText(App.getUsuario().getNome());
		lblNomeresp.setBounds(112, 12, 189, 15);
		panel.add(lblNomeresp);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 61, 426, 106);
		panel.add(scrollPane);
		
		scrollPane.setViewportView(textArea);
		
		JLabel lblMotivoDaRemoo = new JLabel("Motivo da remoção da Peça:");
		lblMotivoDaRemoo.setBounds(12, 39, 289, 15);
		panel.add(lblMotivoDaRemoo);
		
		spinner.setBounds(313, 33, 125, 26); 
		spinner.setEditor(new JSpinner.DateEditor(spinner, "dd/MM/yyyy"));
//		spinner.setEditor(new JSpinner.DateEditor(spinner, "yyyy-MM-dd"));
		panel.add(spinner);
		
		JLabel lblDataDa = new JLabel("Data da Remoção:");
		lblDataDa.setBounds(312, 6, 126, 15);
		panel.add(lblDataDa);
	}
	
	/*
	 * Adiciona os ouvintes da interface
	 */
	public void Ouvintes (ActionListener ouvinte){
		btnCancelar.addActionListener(ouvinte);
		btnSalvar.addActionListener(ouvinte);		
	}
}
