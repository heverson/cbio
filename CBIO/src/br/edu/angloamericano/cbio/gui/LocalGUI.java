package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class LocalGUI extends JDialog{
	public JTextField nomeLocal;
	public JTextArea descLocal;
	public JButton btnOK;
	public JButton btnCancelar;
	private JScrollPane scrollPane;

	String descricao = "";
	String nome = "";

	public LocalGUI() {
		setTitle("Cadastrar Local");
		JPanel panel = new JPanel();
		panel.setLayout(null);

		JLabel lblNomeDoLocal = new JLabel("Nome do Local*:");
		lblNomeDoLocal.setBounds(12, 12, 114, 15);
		panel.add(lblNomeDoLocal);

		nomeLocal = new JTextField();
		nomeLocal.setBounds(12, 29, 216, 25);
		panel.add(nomeLocal);
		nomeLocal.setColumns(10);

		JLabel lblDescrio = new JLabel("Descrição:");
		lblDescrio.setBounds(12, 60, 114, 15);
		panel.add(lblDescrio);

		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ev) {
				if(ev.getSource() == btnOK){
					// TODO Auto-generated method stub
					if(!nomeLocal.getText().equals("")){
						setNome(nomeLocal.getText());
						if(!descLocal.equals("")){
							setDesc(descLocal.getText());
						}
						dispose();
					}
					else
						JOptionPane.showMessageDialog(null, "Entre com o nome do Local");
				}
			}});
		btnOK.setBounds(240, 29, 106, 25);
		panel.add(btnOK);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ev) {
				if(ev.getSource() == btnCancelar){
					// TODO Auto-generated method stub
					dispose();
				}
			}
		});
		btnCancelar.setBounds(240, 66, 106, 25);
		panel.add(btnCancelar);

		setSize(366, 193);
		getContentPane().add(panel);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 87, 216, 70);
		panel.add(scrollPane);

		descLocal = new JTextArea();
		scrollPane.setViewportView(descLocal);
		setModal(true);
		setVisible(true);
	}

	public void setNome(String nome){
		this.nome = nome;
	}
	public void setDesc(String desc){
		descricao = desc;
	}
	public String getNome(){
		return nome;
	}
	public String getDesc(){
		return descricao;
	}
}
