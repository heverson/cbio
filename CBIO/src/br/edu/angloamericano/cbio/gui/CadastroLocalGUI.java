package br.edu.angloamericano.cbio.gui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class CadastroLocalGUI extends JDialog{
	public JButton btnOk = new JButton("Ok");
	
	public JPanel p1 = new JPanel();
	public PanelLocalGUI panel = new PanelLocalGUI();
	

	public CadastroLocalGUI(JDialog Super){
		super(Super);
		
		setVisible(true);
		setResizable(false);

		setLocation((int)(Funcoes.centraliza().getWidth()/2-350),(int) (Funcoes.centraliza().getHeight()/2-127));
		showComponentes();
		verificaUser();
	}
	
	private void showComponentes(){
		setTitle("Cadastro de Local de Armazenamento");
		setSize(769,522);
		
		getContentPane().add(panel);
	}
	
	// Verifica as permissões do usuário logado
	private void verificaUser(){
		if(!App.getTipoUser().equals("Administrador")){
			panel.btnAlterar.setEnabled(false);
			panel.btnRemover.setEnabled(false);
		}
	}
	
	public void setTreeModel(TreeModel treeModel)
	{
		panel.tree.setModel(treeModel);
	}
	
	public void updateTree()
	{
		panel.tree.updateUI();
	}
	
	public void addOuvintes(ActionListener ouvinte){
		panel.btnAdicionar.addActionListener(ouvinte);
		panel.btnAlterar.addActionListener(ouvinte);
		panel.btnRemover.addActionListener(ouvinte);
		panel.btnTerminar.addActionListener(ouvinte);
		panel.tree.addTreeSelectionListener((TreeSelectionListener) ouvinte);
//		btnOk.addActionListener(ouvinte);
	}
}
