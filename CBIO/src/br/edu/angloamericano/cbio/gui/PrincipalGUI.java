package br.edu.angloamericano.cbio.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;
import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.util.Funcoes;

@SuppressWarnings("serial")
public class PrincipalGUI extends JFrame{
	private ImageIcon imagemTituloJanela = new ImageIcon("images/icon.gif");  
	private Color azul = new Color(214,219,222);
	private JPanel pCen = new JPanel(){
		// Foi alterado o método paintComponent desse JPanel para usar uma imagem de background
		protected void paintComponent(Graphics g) {
			Image planoDeFundo = Toolkit.getDefaultToolkit().getImage("images/logo.jpg");
			super.paintComponent(g);   
			g.drawImage(planoDeFundo,(int) getSize().getWidth()/2-96,(int) getSize().getHeight()/2-99, this );   
		}
	};
	private JPanel pDir = new JPanel();
	private JPanel pEsq = new JPanel();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu mnSistema = new JMenu("  Sistema   ");
	private JMenu mnAcervo = new JMenu("   Acervo   ");
	public JMenuItem mntmAnimais = new JMenuItem("Manter Animais");
	public JMenuItem mntmPecas = new JMenuItem("Manter Pe\u00E7as");
	public JMenuItem mntmUsuario = new JMenuItem("Manter Usuarios");
	public JMenuItem mntmLocal = new JMenuItem("Manter Locais de Armazenamento"); // Menu Item Cadastro de local
	public JMenuItem mntmSair = new JMenuItem("Sair");
	public JMenuItem mntmTrocaUsuario = new JMenuItem("Trocar de usuário");
	private JMenu mnAjuda = new JMenu("Ajuda");
	public JMenuItem mntmManualDoSoftware = new JMenuItem("Manual do software");
	public JMenuItem mntmSobreOSoftware = new JMenuItem("Sobre o software");
	private JMenuBar mnStatus = new JMenuBar();
	public JLabel msgStatus = new JLabel("  :: Bem - vindo(a) :: ");
	public JLabel msgNomeUsuario = new JLabel(App.getUsuario().getNome());
	public JLabel msgSobrenomeUsuario = new JLabel();
	private JTabbedPane centro = new JTabbedPane();
	private JTabbedPane direita = new JTabbedPane();
	private JTabbedPane esquerda = new JTabbedPane();

	/**
	 * Construtor da classe. 
	 */
	public PrincipalGUI(){
		super("  :: C-BIO ::");
		setIconImage(imagemTituloJanela.getImage());
		getContentPane().setBackground(azul);
		setSize((int)(Funcoes.centraliza().getWidth()/1.5),(int) (Funcoes.centraliza().getHeight()/1.2));
		setLocation((int)(Funcoes.centraliza().getWidth()/2-getSize().getWidth()/2),
				(int) (Funcoes.centraliza().getHeight()/2-getSize().getHeight()/2-20));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/* Verifica se o usuário logado é administrador, se for passa 1 para o método showComponentes e 0 caso contrario
		 * Se o método showComponentes receber 0, restringe o acesso a algumas funcionalidades da janela*/
		int flag = App.getTipoUser().equals("Administrador") ?  1 :  0;
			showComponentes(flag);
	}

	/**
	 * Cria e configura os componentes da interface, por padrão todos os componentes são abilitados. O paramêtro flag, define 
	 * se alguma funcionalidade deve ou não ser restringida, se receber 0 como parâmetro então alguns componentes serão desabilitados
	 * @param flag
	 */
	private void showComponentes(int flag){
		pCen.setName("  C-BIO  ");
		centro.add(pCen);
		pCen.setBackground(Color.WHITE);
		pCen.setLayout(null);
		pDir.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pDir.setName("Diversos");
		direita.add(pDir);
		pDir.setBackground(Color.WHITE);
		pDir.setLayout(null);
		pDir.setPreferredSize(new Dimension(200,0));
		pEsq.setBackground(Color.WHITE);
		pEsq.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pEsq.setName("Ferramentas");
		esquerda.add(pEsq);
		pEsq.setLayout(null);
		pEsq.setPreferredSize(new Dimension(200,0));
		getContentPane().add(new JLabel(), BorderLayout.CENTER);
		getContentPane().add(centro, BorderLayout.CENTER);
		getContentPane().add(direita, BorderLayout.WEST);
		getContentPane().add(esquerda, BorderLayout.EAST);
		getContentPane().add(mnStatus, BorderLayout.SOUTH);
		mnStatus.add(msgStatus);
		msgNomeUsuario.setText(App.getUsuario().getNome());
		mnStatus.add(msgNomeUsuario);
		mnStatus.add(msgSobrenomeUsuario);
		String lblSobrenome = App.getUsuario().getSobrenome();

		if(lblSobrenome != null)
			msgSobrenomeUsuario.setText(" " + lblSobrenome);

		mnStatus.setPreferredSize(new Dimension(0,25));
		mnStatus.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setJMenuBar(menuBar);
		menuBar.add(mnAcervo);
		mnAcervo.add(mntmAnimais);
		mnAcervo.add(mntmPecas);
		mnAcervo.add(mntmUsuario);
		mnAcervo.add(mntmLocal); //adiciona ao menu o item de cadastrar local
		menuBar.add(mnSistema);
		mnSistema.add(mntmTrocaUsuario);
		mnSistema.add(mntmSair);
		menuBar.add(mnAjuda);
		mnAjuda.add(mntmManualDoSoftware);
		mnAjuda.add(mntmSobreOSoftware);
		
		if(flag == 0)
			configAdm();
	}
	
	/**
	 * Bloqueia as funções restritas à usuários administradores. 
	 * Por padrão, todas as funcionalidades são liberadas, porém não são todos os usuários que devem ter acesso
	 * portanto, se o usuário logado não for um administrador deve ser aplicadas algumas restrições.
	 * O método configAdm desabilita o uso dessas funcionalidades restritas.
	 */
	private void configAdm(){
		// Na versão corrente apenas a funcionalidade manter usuários é restrita a usuário administradores
		mntmUsuario.setEnabled(false);
	}

	/**
	 * Adiciona ouvintes aos componentes da janela.
	 * O método addOuvintes é chamado pela classe PrincipalController e o tratamento dos componentes 
	 * também é feita na classa PrincipalController.
	 * @param ouvinte
	 */
	public void addOuvintes(ActionListener ouvinte){
		mntmAnimais.addActionListener(ouvinte);
		mntmPecas.addActionListener(ouvinte);
		mntmUsuario.addActionListener(ouvinte);
		mntmTrocaUsuario.addActionListener(ouvinte);
		mntmLocal.addActionListener(ouvinte);
		mntmSair.addActionListener(ouvinte);
	}
}
