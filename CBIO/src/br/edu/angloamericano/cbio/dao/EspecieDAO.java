/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Tribo;

public class EspecieDAO extends DAO {
	private Especie especie;
	private List<Especie> listEspecie;
	
	public EspecieDAO(){
		colName = "tb_especie";
		columns = "id_especie, nm_especie, fk_id_tribo";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_especie=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador especie", "Nome especie", "Identificador Tribo" };
		columnsNames = cN;
		numColumns = 3;
		
		especie = new Especie();

		initConnection();
	}

	
	public void insert(Tribo tribo, String nm_especie){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		
		String newData = "insert into tb_especie (nm_especie , fk_id_tribo) values ('"+ nm_especie+ "' , " + tribo.getIdTribo() + ");" ; 

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}
	
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			especie.setIdEspecie(resultSet.getInt(1));
			especie.setNmEspecie(resultSet.getString(2));
			especie.setIdTribo(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
	
		return especie;
	}

	@Override
	public void setData(Data d) {
		Especie e = (Especie) d;
		especie.setIdEspecie(e.getIdEspecie());
		especie.setNmEspecie(e.getNmEspecie());
		especie.setIdTribo(e.getIdTribo());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return especie.getIdEspecie();
			case 2:
				return especie.getNmEspecie();
			case 3:
				return especie.getIdTribo();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_especie set nm_especie = '" + especie.getNmEspecie() + "' where id_especie = " 
				+ especie.getIdEspecie() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Especie especie) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_especie set nm_especie = '" + especie.getNmEspecie() + "' where id_Especie = " 
				+ especie.getIdEspecie() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Especie especie) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + especie.getIdEspecie() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + especie.getIdEspecie() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		Especie especie = (Especie) d;
		
		String newData = "insert into tb_especie (nm_especie , fk_id_tribo) values ('"+ especie.getNmEspecie()+ "' , " + especie.getIdTribo() + ");" ; 
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listEspecie = new LinkedList<Especie>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listEspecie.add(new Especie(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void setQueryTribo(int idTribo) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "select * from tb_especie where fk_id_tribo = "+ idTribo + ";";
			// especifica consulta e a executa
			listEspecie = new LinkedList<Especie>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listEspecie.add(new Especie(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	
	}
	
	public List<Especie> getList(){
		return listEspecie;
	}

	public Especie findEspecieById(int idEspecie) {
		Especie esp = new Especie();
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_especie where id_especie = "+ idEspecie + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				esp.setIdEspecie(resultSet.getInt(1));
				esp.setNmEspecie(resultSet.getString(2));
				esp.setIdTribo(resultSet.getInt(3));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return esp;
	}

}
