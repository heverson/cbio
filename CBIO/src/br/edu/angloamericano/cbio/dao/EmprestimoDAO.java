/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Emprestimo;

/**
 *
 */
public class EmprestimoDAO extends DAO {
	
	private Emprestimo emprestimo;
	
	/**
	 * Construtor da Classe EmprestimoDAO.
	 */
	public EmprestimoDAO(){
		colName = "tb_emprestimo";
		columns = "id_local, nm_local, ds_local, fl_fora_facul, ds_endereco, cd_cep, id_endereco";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador do Local", "Nome do Local", "Descrição do Local", "Fora da Faculdade",
						"Descrição do Endereço", "Código do CEP", "Identificador do Endereco" };
		columnsNames = cN;
		numColumns = 7;
		emprestimo = new Emprestimo();
		initConnection();
	}
	
	/**
	 * Fun��o que percorre at� a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			emprestimo.setIdLocal(resultSet.getInt(1));
			emprestimo.setNmLocal(resultSet.getString(2));
			emprestimo.setDsLocal(resultSet.getString(3));
			emprestimo.setForaFacul(resultSet.getBoolean(4));
			emprestimo.setDsEndereco(resultSet.getString(5));
			emprestimo.setCdCep(resultSet.getString(6));
			emprestimo.setIdEndereco(resultSet.getInt(7));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Data getData() {
		return emprestimo;
	}

	public void setData(Data d) {
		Emprestimo emp = (Emprestimo)d;
		emprestimo.setIdLocal(emp.getIdLocal());
		emprestimo.setNmLocal(emp.getNmLocal());
		emprestimo.setDsLocal(emp.getDsLocal());
		emprestimo.setForaFacul(emp.isForaFacul());
		emprestimo.setDsEndereco(emp.getDsEndereco());
		emprestimo.setCdCep(emp.getCdCep());
		emprestimo.setIdEndereco(emp.getIdEndereco());
	}

	//nao implementada
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Fun��o que obt�m coluna do atributo da classe.
	 * @param int column - N�mero da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return emprestimo.getIdLocal();
			case 2:
				return emprestimo.getNmLocal();
			case 3:
				return emprestimo.getDsLocal();
			case 4:
				return emprestimo.isForaFacul();
			case 5:
				return emprestimo.getDsEndereco();
			case 6:
				return emprestimo.getCdCep();
			case 7:
				return emprestimo.getIdEndereco();
		}
		return null;
	}

	/**
	 * Fun��o que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String newData = update + "('" + emprestimo.getIdLocal() + "', '"
				+ emprestimo.getNmLocal() + "', '" + emprestimo.getDsLocal() + "', '"
				+ emprestimo.isForaFacul() + "', '" + emprestimo.getDsEndereco() + "', '"
				+ emprestimo.getCdCep() + "', '" + emprestimo.getIdEndereco() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Fun��o que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + emprestimo.getIdLocal() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o que executa a query de insert(inser��o) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Emprestimo emp = (Emprestimo) d;
		setData(emp);
		String newData = insert + "(DEFAULT, '"
				+ emprestimo.getNmLocal() + "', '" + emprestimo.getDsLocal() + "', '"
				+ emprestimo.isForaFacul() + "', '" + emprestimo.getDsEndereco() + "', '"
				+ emprestimo.getCdCep() + "', '" + emprestimo.getIdEndereco() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o apenas de auxilio para cria��o das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obt�m metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o n�mero de linhas em ResultSet
			resultSet.last(); // move para a �ltima linha
			numberOfRows = resultSet.getRow(); // obt�m n�mero de linha
			resultSet.first();

			if (numberOfRows != 0) {
				emprestimo.setIdLocal(resultSet.getInt(1));
				emprestimo.setNmLocal(resultSet.getString(2));
				emprestimo.setDsLocal(resultSet.getString(3));
				emprestimo.setForaFacul(resultSet.getBoolean(4));
				emprestimo.setDsEndereco(resultSet.getString(5));
				emprestimo.setCdCep(resultSet.getString(6));
				emprestimo.setIdEndereco(resultSet.getInt(7));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
}
