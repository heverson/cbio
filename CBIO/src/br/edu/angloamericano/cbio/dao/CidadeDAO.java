/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Cidade;
import br.edu.angloamericano.cbio.dados.Data;

/**
 *
 */
public class CidadeDAO extends DAO {

	private Cidade cidade;
	private List<Cidade> listCidade;
	
	/**
	 * Construtor da Classe AnimalDAO.
	 */
	public CidadeDAO(){
		colName = "tb_cidade";
		columns = "id_cidade, nm_cidade, fk_id_estado";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador do Animal", "Nome da Cidade", "Identificador do Estado" };
		columnsNames = cN;
		numColumns = 3;
		cidade = new Cidade();
		initConnection();
	}
	
	/**
	 * Fun��o que percorre at� a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			cidade.setId(resultSet.getInt(1));
			cidade.setNome(resultSet.getString(2));
			cidade.setIdEstado(resultSet.getInt(3));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Data getData() {
		return (Data) cidade;
	}

	public void setData(Data d) {
		Cidade c = (Cidade) d;
		cidade.setId(c.getId());
		cidade.setNome(c.getNome());
		cidade.setIdEstado(c.getIdEstado());
	}

	//nao implementada
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Fun��o que obt�m coluna do atributo da classe.
	 * @param int column - N�mero da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return cidade.getId();
			case 2:
				return cidade.getNome();
			case 3:
				return cidade.getIdEstado();
		}
		return null;
	}

	/**
	 * Fun��o que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String newData = update + "('" + cidade.getId() + "', '"
				+ cidade.getNome() + "', '" + cidade.getIdEstado() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Fun��o que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + cidade.getId() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o que executa a query de insert(inser��o) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Cidade c = (Cidade) d;
		this.setData((Data) c);
		
		
		String newData = insert + "(DEFAULT, '"
				+ cidade.getNome() + "', '" + cidade.getIdEstado() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o apenas de auxilio para cria��o das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			listCidade = new LinkedList<Cidade>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){

				 listCidade.add(new Cidade(resultSet.getInt(1), resultSet.getString(2) , resultSet.getInt(3) ) );
	
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void setQueryEstado(int idEstado) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			listCidade = new LinkedList<Cidade>();
			query = "select * from tb_cidade where fk_id_estado = "+ idEstado + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){

				 listCidade.add(new Cidade(resultSet.getInt(1), resultSet.getString(2) , resultSet.getInt(3) ) );
	
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	
	public List<Cidade> getList(){
		return listCidade;
	}
	
	public Cidade findCidadeById(int id){
		cidade = new Cidade();
		try {
			if (statement.isClosed())
				return cidade;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
	
			query = "select * from tb_cidade where id_cidade = "+ id + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				cidade.setId(resultSet.getInt(1));
				cidade.setNome(resultSet.getString(2));
				cidade.setIdEstado(resultSet.getInt(3));
	
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		return cidade;
	}
	
}
