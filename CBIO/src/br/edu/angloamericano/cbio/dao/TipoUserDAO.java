package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.TipoUser;

public class TipoUserDAO extends DAO {

	private List<TipoUser> tipos = new LinkedList<TipoUser>();
	private TipoUser tipo = new TipoUser();
	
	public TipoUserDAO(){
		query = "SELECT * FROM tipoUser";
		update = "UPDATE `tipoUser` SET ";
		delete = "DELETE FROM `tipoUser` WHERE ";
		insert = "INSERT INTO `tipoUser` (`nomeTipo`) VALUES ";

		initConnection();
	}
	
	public int getIdTipo(String tipo){
		int aux = 0;
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM tipoUser WHERE nomeTipo = '" + tipo + "'";
		try {
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				aux = resultSet.getInt("idTipo");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aux;
	}
	
	
	public boolean isTipoExist(String tipo){
		boolean exist = true;
		try {
			if (statement.isClosed())
				return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "SELECT * FROM tipoUser WHERE nomeTipo = '" + tipo + "'";
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				exist = true;
			}
			else
				exist = false;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return exist;
	}
	
	public String getTipoUser(int Id){
		String user = "";
		try {
			if (statement.isClosed())
				return "";
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "SELECT * FROM tipoUser WHERE idTipo = " + Id;
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				user = resultSet.getString("nomeTipo");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
	
	public List<TipoUser> getTipos() throws SQLException{
		
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		resultSet = statement.executeQuery(query);
		while(resultSet.next()){
			tipo = new TipoUser();
			tipo.setId(resultSet.getInt("idTipo"));
			tipo.setNomeTipo(resultSet.getString("nomeTipo"));		
			tipos.add(tipo);
			
		}
		return tipos;
	}
	
	
	@Override
	public void moveToRow(int row) {
		// TODO Auto-generated method stub

	}

	@Override
	public Data getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setData(Data d) {
		// TODO Auto-generated method stub

	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveDB() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteDB() {
		// TODO Auto-generated method stub

	}

	public void insert(TipoUser tipo){
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			insert = "INSERT INTO `tipoUser` (`nomeTipo`) VALUES ('" + tipo.getNomeTipo() + "')";
			statement.execute(insert);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void insert(Data d) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setQuery() {
		// TODO Auto-generated method stub

	}

}
