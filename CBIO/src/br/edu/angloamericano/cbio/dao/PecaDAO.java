/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Peca;

/**
 *
 */
public class PecaDAO extends DAO {

	private Peca peca; //atributo com os dados da peça para manipular o DAO

	/**
	 * Construtor da Classe AnimalDAO.
	 */
	public PecaDAO(){
		colName = "tb_peca";
		columns = "id_peca, bl_animal, bl_Dna_Tecido_Visceral, Fl_Dna_Cartilagem, Fl_Dna_Tecido_Muscular," +
				" Fl_Pelo_Interescapular_Dorsal, Fl_Pelo_Abdominal, Fl_Pelo_Cabeca_Dorsal," +
				" ds_Ossos, dt_Entrada, dt_Retorno, bl_Peca_Emprestada, fk_id_Animal," +
				" fk_Emprestimo, fk_id_local_armazenado, cd_tombo_peca, ds_peca";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador da Peça", "Animal", "Dna Tecido Visceral", "Dna Cartilagem",
				"Dna Tecido Muscular", "Pelo Interescapular Dorsal", "Pelo Abdominal",
				"Pelo Cabeca Dorsal", "Ossos", "Entrada", "Retorno", "Peça Emprestada", "Animal", 
				"Identificador Peça Emprestimo", "Identificador Local Armazenado", "Tombo da Peça",
		"Descrição da Peça" };
		columnsNames = cN;
		numColumns = 17;
		peca = new Peca();
		initConnection();
	}

	public boolean emptyLocal(int idLocal){
		boolean empity = false;
		try {
			if (statement.isClosed())
				return false;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM " + colName + " WHERE `FK_ID_LOCAL_ARMAZENADO` = " + idLocal;
		try {
			resultSet = statement.executeQuery(query);
			if(resultSet.next())
				empity = false;
			else
				empity = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}
		return empity;
	}

	public void updatePeca(Peca peca){
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try 
		{	
			update = "UPDATE `tb_peca` SET `DS_OSSOS` = '" + peca.getDsOssos() + "', `FK_ID_LOCAL_ARMAZENADO` = " + peca.getIdLocalArmazenado() + 
					", `ds_peca` = '" + peca.getDsPeca() + "' WHERE ID_PECA = " + peca.getIdPeca();
			statement.execute(update);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
		}


	}

	/**
	 * Método que busca uma peça pelo Id
	 * @param idPeca - id da peça a ser buscada
	 * @return peca
	 */
	public Peca getPecaById(int idPeca)
	{
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {			
			query = "select * from tb_peca where ID_PECA="+idPeca+";"; //monta query para consultar na tabela de pecas
			resultSet = statement.executeQuery(query); // especifica consulta e a executa
			if(resultSet.next())
			{
				peca = new Peca();
				peca.setIdPeca(resultSet.getInt(1));
				peca.setBlAnimal(resultSet.getInt(2));
				peca.setBlDnaTecidoVisceral(resultSet.getInt(3));
				peca.setBlDnaCartilagem(resultSet.getInt(4));
				peca.setBlDnaTecidoMuscular(resultSet.getInt(5));
				peca.setBlPeloInterescapularDorsal(resultSet.getInt(6));
				peca.setBlPeloAbdominal(resultSet.getInt(7));
				peca.setBlPeloCabecaDorsal(resultSet.getInt(8));
				peca.setBlEscama(resultSet.getInt(9));
				peca.setBlPena(resultSet.getInt(10));
				peca.setDsOssos(resultSet.getString(11));
				peca.setDtEntrada(resultSet.getDate(12));
				peca.setDtRetorno(resultSet.getDate(13));
				peca.setBlPecaEmprestada(resultSet.getInt(14));
				peca.setIdAnimal(resultSet.getInt(15));
				peca.setIdEmprestimo(resultSet.getInt(16));
				peca.setIdLocalArmazenado(resultSet.getInt(17));
				peca.setCdTomboPeca(resultSet.getInt(18));
				peca.setDsPeca(resultSet.getString(19));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return peca;
	}

	/**
	 * Funçãoo que percorre até a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			peca.setIdPeca(resultSet.getInt(1));
			peca.setBlAnimal(resultSet.getInt(2));
			peca.setBlDnaTecidoVisceral(resultSet.getInt(3));
			peca.setBlDnaCartilagem(resultSet.getInt(4));
			peca.setBlDnaTecidoMuscular(resultSet.getInt(5));
			peca.setBlPeloInterescapularDorsal(resultSet.getInt(6));
			peca.setBlPeloAbdominal(resultSet.getInt(7));
			peca.setBlPeloCabecaDorsal(resultSet.getInt(8));
			peca.setBlEscama(resultSet.getInt(9));
			peca.setBlPena(resultSet.getInt(10));
			peca.setDsOssos(resultSet.getString(11));
			peca.setDtEntrada(resultSet.getDate(12));
			peca.setDtRetorno(resultSet.getDate(13));
			peca.setBlPecaEmprestada(resultSet.getInt(14));
			peca.setIdAnimal(resultSet.getInt(15));
			peca.setIdEmprestimo(resultSet.getInt(16));
			peca.setIdLocalArmazenado(resultSet.getInt(17));
			peca.setCdTomboPeca(resultSet.getInt(18));
			peca.setDsPeca(resultSet.getString(19));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	//nao implementado
	public Class<?> getColumnClassName(int column)
	{
		try
		{
			switch (column)
			{
			case 1:
			case 5:
				return Class.forName(Integer.class.getName());
			case 2:
			case 3:
			case 4:
				return Class.forName(String.class.getName());
			}
		}
		catch (ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		// se ocorrerem os problemas acima, assume tipo Object
		return Object.class;
	}

	public Peca getData() {
		return peca;
	}

	public void setData(Data d) {
		Peca p = (Peca) d;
		peca.setIdPeca(p.getIdPeca());
		peca.setBlAnimal(p.getBlAnimal());
		peca.setBlDnaTecidoVisceral(p.getBlDnaTecidoVisceral());
		peca.setBlDnaCartilagem(p.getBlDnaCartilagem());
		peca.setBlDnaTecidoMuscular(p.getBlDnaTecidoMuscular());
		peca.setBlPeloInterescapularDorsal(p.getBlPeloInterescapularDorsal());
		peca.setBlPeloAbdominal(p.getBlPeloAbdominal());
		peca.setBlPeloCabecaDorsal(p.getBlPeloCabecaDorsal());
		peca.setBlEscama(p.getBlEscama());
		peca.setBlPena(p.getBlPena());
		peca.setDsOssos(p.getDsOssos());
		peca.setDtEntrada(p.getDtEntrada());
		peca.setDtRetorno(p.getDtRetorno());
		peca.setBlPecaEmprestada(p.getBlPecaEmprestada());
		peca.setIdAnimal(p.getIdAnimal());
		peca.setIdEmprestimo(p.getIdEmprestimo());
		peca.setIdLocalArmazenado(p.getIdLocalArmazenado());
		peca.setCdTomboPeca(p.getCdTomboPeca());
		peca.setDsPeca(p.getDsPeca());
		peca.setIdPecaBase(p.getIdPecaBase());
	}

	/**
	 * Função que obtém coluna do atributo da classe.
	 * @param int column - Número da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
		case 1:
			return peca.getIdAnimal();
		case 2:
			return peca.getBlAnimal();
		case 3:
			return peca.getBlDnaTecidoVisceral();
		case 4:
			return peca.getBlDnaCartilagem();
		case 5:
			return peca.getBlDnaTecidoMuscular();
		case 6:
			return peca.getBlPeloInterescapularDorsal();
		case 7:
			return peca.getBlPeloAbdominal();
		case 8:
			return peca.getBlPeloCabecaDorsal();
		case 9:
			return peca.getBlEscama();
		case 10:
			return peca.getBlPena();
		case 11:
			return peca.getDsOssos();
		case 12:
			return peca.getDtEntrada();
		case 13:
			return peca.getDtRetorno();
		case 14:
			return peca.getBlPecaEmprestada();
		case 15:
			return peca.getIdAnimal();
		case 16:
			return peca.getIdEmprestimo();
		case 17:
			return peca.getIdLocalArmazenado();
		case 18:
			return peca.getCdTomboPeca();
		case 19:
			return peca.getDsPeca();
		}
		return null;
	}

	/**
	 * Função que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB() {
		//		try {
		//			if (statement.isClosed())
		//				return;
		//		} catch (SQLException e1) {
		//			e1.printStackTrace();
		//		}
		//		String newData = update + "('" + peca.getIdPeca() + "', '" + peca.getBlAnimal()
		//				+ "', '" + peca.getBlDnaTecidoVisceral() + "', '" + peca.getBlDnaCartilagem() 
		//				+ "', '" + peca.getBlDnaTecidoMuscular() + "', '" + peca.getBlPeloInterescapularDorsal() 
		//				+ "', '" + peca.getBlPeloAbdominal() + "', '" + peca.getBlPeloCabecaDorsal()
		//				+ "', '" + peca.getBlEscama() + "', '" + peca.getBlPena()
		//				+ "', '" + peca.getDsOssos() + "', '" + peca.getDtEntrada() + "', '" 
		//				+ peca.getDtRetorno() + "', '" + peca.getBlPecaEmprestada() + "', '" 
		//				+ peca.getIdAnimal() + "', '" + peca.getIdEmprestimo() + "', '"
		//				+ peca.getIdLocalArmazenado() + "', '" + peca.getCdTomboPeca() + "', '" 
		//				+ peca.getDsPeca() + "');";
		//		try {
		//			statement.executeUpdate(newData);
		//		} catch (SQLException e) {
		//			e.printStackTrace();
		//		}
	}

	/**
	 * Função que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB() {
		//		try {
		//			if (statement.isClosed())
		//				return;
		//		} catch (SQLException e1) {
		//			e1.printStackTrace();
		//		}
		//		String del = "DELETE FROM tb_peca where id_peca="+peca.getIdPeca()+" ;";
		//		try {
		//			statement.executeUpdate(del);
		//		} catch (SQLException e) {
		//			e.printStackTrace();
		//		}
	}

	/**
	 * Função que executa a query de insert(inserção) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			System.out.println("Erro Teste\nErro gerado na classe PecaDAO, metodo insert");
			e1.printStackTrace();
		}
		//this.setData(d);
		peca = (Peca) d;
		String idPB;
		if(peca.getIdPecaBase() == 0)
			idPB = "NULL";
		else
			idPB = String.format("'%d'", peca.getIdPecaBase());

		// A String insert e columns são reconstruidas para não adicionar dados aos campos relacionados a empréstimos, já que essa funcionalidade ainda não está implementada
		String columns = "id_peca, bl_animal, bl_Dna_Tecido_Visceral, Fl_Dna_Cartilagem, Fl_Dna_Tecido_Muscular," +
				" Fl_Pelo_Interescapular_Dorsal, Fl_Pelo_Abdominal, Fl_Pelo_Cabeca_Dorsal, Fl_Escama, Fl_Pena," + 
				" ds_Ossos, dt_Entrada, dt_Retorno, fk_id_Animal," +
				" fk_id_local_armazenado, cd_tombo_peca, ds_peca, id_pecaBase";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String newData = insert + "(DEFAULT, '"+ peca.getBlAnimal() + "', '"
				+ peca.getBlDnaTecidoVisceral() + "', '" + peca.getBlDnaCartilagem()+ "', '" 
				+ peca.getBlDnaTecidoMuscular() + "', '" + peca.getBlPeloInterescapularDorsal() 
				+ "', '" + peca.getBlPeloAbdominal() + "', '" + peca.getBlPeloCabecaDorsal()
				+ "', '" + peca.getBlEscama() + "', '" + peca.getBlPena()
				+ "', '" + peca.getDsOssos() + "', '" + peca.getDtEntrada() + "', '" 
				+ peca.getDtRetorno() + "', '" +  
				+ peca.getIdAnimal() + "', '" + 
				+ peca.getIdLocalArmazenado() + "', '" + peca.getCdTomboPeca() + "', '"
				+ peca.getDsPeca() +"', " + idPB + ");";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			System.out.println("Erro ao cadastrar Peça\nErro gerado na classe PecaDAO, metodo insert");
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função de consulta para peças por id do animal
	 * @param idAnimal - Id do animal que será consultado nas peças
	 * @return List<Peça> - Retorna uma lista encadeada com as peças encontradas no banco
	 */
	public List<Peca> findPecaByIdAnimal(int idAnimal){
		List<Peca> listPeca = new LinkedList<Peca>(); //cria uma lista de peças
		try{
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_peca where fk_id_animal="+idAnimal+";"; //monta query para consultar na tabela de pecas
			resultSet = statement.executeQuery(query); // especifica consulta e a executa

			// Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd e preenche a lista de pe�as
			while(resultSet.next()){
				Peca pecaAux = new Peca();
				pecaAux.setIdPeca(resultSet.getInt(1));
				pecaAux.setBlAnimal(resultSet.getInt("BL_ANIMAL"));
				pecaAux.setBlDnaTecidoVisceral(resultSet.getInt(3));
				pecaAux.setBlDnaCartilagem(resultSet.getInt(4));
				pecaAux.setBlDnaTecidoMuscular(resultSet.getInt(5));
				pecaAux.setBlPeloInterescapularDorsal(resultSet.getInt(6));
				pecaAux.setBlPeloAbdominal(resultSet.getInt(7));
				pecaAux.setBlPeloCabecaDorsal(resultSet.getInt(8));
				pecaAux.setBlEscama(resultSet.getInt(9));
				pecaAux.setBlPena(resultSet.getInt(10));
				pecaAux.setDsOssos(resultSet.getString(11));
				pecaAux.setDtEntrada(resultSet.getDate(12));
				pecaAux.setDtRetorno(resultSet.getDate(13));
				pecaAux.setBlPecaEmprestada(resultSet.getInt(14));
				pecaAux.setIdAnimal(resultSet.getInt(15));
				pecaAux.setIdEmprestimo(resultSet.getInt(16));
				pecaAux.setIdLocalArmazenado(resultSet.getInt(17));
				pecaAux.setCdTomboPeca(resultSet.getInt(18));
				pecaAux.setDsPeca(resultSet.getString(19));
				//pecaAux.setIdPecaBase(resultSet.getInt("idPecaBase"));
				listPeca.add(pecaAux);
				pecaAux=null;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return listPeca;
	}

	public List<Peca> pecaNaoApagada(int idAnimal){
		List<Peca> listPeca = new LinkedList<Peca>(); //cria uma lista de peças
		try{
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_peca where fk_id_animal="+idAnimal+" AND bl_animal = 1;"; //monta query para consultar na tabela de pecas
			resultSet = statement.executeQuery(query); // especifica consulta e a executa

			// Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd e preenche a lista de pe�as
			while(resultSet.next()){
				Peca pecaAux = new Peca();
				pecaAux.setIdPeca(resultSet.getInt(1));
				pecaAux.setBlAnimal(resultSet.getInt(2));
				pecaAux.setBlDnaTecidoVisceral(resultSet.getInt(3));
				pecaAux.setBlDnaCartilagem(resultSet.getInt(4));
				pecaAux.setBlDnaTecidoMuscular(resultSet.getInt(5));
				pecaAux.setBlPeloInterescapularDorsal(resultSet.getInt(6));
				pecaAux.setBlPeloAbdominal(resultSet.getInt(7));
				pecaAux.setBlPeloCabecaDorsal(resultSet.getInt(8));
				pecaAux.setBlEscama(resultSet.getInt(9));
				pecaAux.setBlPena(resultSet.getInt(10));
				pecaAux.setDsOssos(resultSet.getString(11));
				pecaAux.setDtEntrada(resultSet.getDate(12));
				pecaAux.setDtRetorno(resultSet.getDate(13));
				pecaAux.setBlPecaEmprestada(resultSet.getInt(14));
				pecaAux.setIdAnimal(resultSet.getInt(15));
				pecaAux.setIdEmprestimo(resultSet.getInt(16));
				pecaAux.setIdLocalArmazenado(resultSet.getInt(17));
				pecaAux.setCdTomboPeca(resultSet.getInt(18));
				pecaAux.setDsPeca(resultSet.getString(19));
				listPeca.add(pecaAux);
				pecaAux=null;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return listPeca;
	}

	/**
	 * Função que busca pela id da ultima pela inserida
	 * @return int - id da ultima peça inserida
	 */
	public int getLastIdPeca(){
		int idPeca = 0;
		try{
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select LAST_INSERT_ID(id_peca) from tb_peca order by id_peca desc limit 1 ;";
			resultSet = statement.executeQuery(query); // especifica consulta e a executa

			// seta o ultimo id inserido no idPeca para retorno
			if(resultSet.next())
				idPeca = resultSet.getInt(1);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return idPeca;
	}

	/**
	 * Função que seta a peça como uma peça unica
	 */
	public void baixaPeca(int idBaixa, int idPeca){
		try{
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "UPDATE tb_peca SET BL_ANIMAL= " + idBaixa + " WHERE ID_PECA="+ idPeca +" ;";
			statement.executeUpdate(query);
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Função de consulta para peças que monta a query a partir dos dados passados pelo peça
	 * @param p - Objeto peça com os dados para realizar consulta
	 * @return List<Peça> - Retorna uma lista encadeada com as peças encontradas no banco
	 */
	public List<Peca> obterListaPecas() {
		List<Peca> listPeca = new LinkedList<Peca>(); //cria uma lista de peças
		try{
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_peca"; //monta query inicial para consultar na tabela de pecas
			String where = new String(); //cria uma string para montar as condiçoes do where na busca
			where = " where 1=1 ";

			query = query+where+";"; //monta a query
			resultSet = statement.executeQuery(query); // especifica consulta e a executa

			// Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd e preenche a lista de pe�as
			while(resultSet.next()){
				Peca pecaAux = new Peca();
				pecaAux.setIdPeca(resultSet.getInt(1));
				pecaAux.setBlAnimal(resultSet.getInt(2));
				pecaAux.setBlDnaTecidoVisceral(resultSet.getInt(3));
				pecaAux.setBlDnaCartilagem(resultSet.getInt(4));
				pecaAux.setBlDnaTecidoMuscular(resultSet.getInt(5));
				pecaAux.setBlPeloInterescapularDorsal(resultSet.getInt(6));
				pecaAux.setBlPeloAbdominal(resultSet.getInt(7));
				pecaAux.setBlPeloCabecaDorsal(resultSet.getInt(8));
				pecaAux.setBlEscama(resultSet.getInt(9));
				pecaAux.setBlPena(resultSet.getInt(10));
				pecaAux.setDsOssos(resultSet.getString(11));
				pecaAux.setDtEntrada(resultSet.getDate(12));
				pecaAux.setDtRetorno(resultSet.getDate(13));
				pecaAux.setBlPecaEmprestada(resultSet.getInt(14));
				pecaAux.setIdAnimal(resultSet.getInt(15));
				pecaAux.setIdEmprestimo(resultSet.getInt(16));
				pecaAux.setIdLocalArmazenado(resultSet.getInt(17));
				pecaAux.setCdTomboPeca(resultSet.getInt(18));
				pecaAux.setDsPeca(resultSet.getString(19));
				listPeca.add(pecaAux);
				pecaAux=null;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return listPeca;
	}

	/**
	 * Função apenas de auxilio para criação das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} 
		try {
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obtém metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o número de linhas em ResultSet
			resultSet.last(); // move para a última linha
			numberOfRows = resultSet.getRow(); // obtém número de linha
			resultSet.first();

			if (numberOfRows != 0){
				peca.setIdPeca(resultSet.getInt(1));
				peca.setBlAnimal(resultSet.getInt(2));
				peca.setBlDnaTecidoVisceral(resultSet.getInt(3));
				peca.setBlDnaCartilagem(resultSet.getInt(4));
				peca.setBlDnaTecidoMuscular(resultSet.getInt(5));
				peca.setBlPeloInterescapularDorsal(resultSet.getInt(6));
				peca.setBlPeloAbdominal(resultSet.getInt(7));
				peca.setBlPeloCabecaDorsal(resultSet.getInt(8));
				peca.setBlEscama(resultSet.getInt(9));
				peca.setBlPena(resultSet.getInt(10));
				peca.setDsOssos(resultSet.getString(11));
				peca.setDtEntrada(resultSet.getDate(12));
				peca.setDtRetorno(resultSet.getDate(13));
				peca.setBlPecaEmprestada(resultSet.getInt(14));
				peca.setIdAnimal(resultSet.getInt(15));
				peca.setIdEmprestimo(resultSet.getInt(16));
				peca.setIdLocalArmazenado(resultSet.getInt(17));
				peca.setCdTomboPeca(resultSet.getInt(18));
				peca.setDsPeca(resultSet.getString(19));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
