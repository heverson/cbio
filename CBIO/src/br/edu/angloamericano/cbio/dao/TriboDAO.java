package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Familia;
import br.edu.angloamericano.cbio.dados.Tribo;
import br.edu.angloamericano.cbio.dados.Data;

public class TriboDAO extends DAO{
	private Tribo tribo;
	private List<Tribo> listTribo;
	
	public TriboDAO(){
		colName = "tb_tribo";
		columns = "id_tribo, nm_tribo, fk_id_familia";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_tribo=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Tribo", "Nome Tribo", "Identificador Familia" };
		columnsNames = cN;
		numColumns = 3;
		
		tribo = new Tribo();

		initConnection();
	}
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		Tribo tribo = (Tribo) d;
		
		String newData = "insert into tb_tribo (nm_tribo , fk_id_familia) values ('"+ tribo.getNmTribo() + "' , " + tribo.getIdFamilia() + ");" ; 
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	public void insert(Familia familia, String nm_tribo){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		
		String newData = "insert into tb_tribo (nm_tribo , fk_id_familia) values ('"+ nm_tribo + "' , " + familia.getIdFamilia() + ");" ; 
		//String newData = insert + "('"+  bloco.getIdBloco() + "', '"
		//		+ bloco.getNmBloco() + "', '"+ bloco.getIdLocal() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}
	
	
	
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			tribo.setIdTribo(resultSet.getInt(1));
			tribo.setNmTribo(resultSet.getString(2));
			tribo.setIdFamilia(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
	
		return (Data) tribo;
	}

	@Override
	public void setData(Data d) {
		Tribo b = (Tribo) d;
		tribo.setIdTribo(b.getIdTribo());
		tribo.setNmTribo(b.getNmTribo());
		tribo.setIdFamilia(b.getIdFamilia());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return tribo.getIdTribo();
			case 2:
				return tribo.getNmTribo();
			case 3:
				return tribo.getIdFamilia();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_tribo set nm_tribo = '" + tribo.getNmTribo() + "' where id_tribo = " 
				+ tribo.getIdTribo() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Tribo tribo) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_tribo set nm_tribo = '" + tribo.getNmTribo() + "' where id_tribo = " 
				+ tribo.getIdTribo() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Tribo tribo) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + tribo.getIdTribo() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + tribo.getIdTribo() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}


	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listTribo = new LinkedList<Tribo>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listTribo.add(new Tribo(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void setQueryFamilia(int idFamilia) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "select * from tb_tribo where fk_id_familia = "+ idFamilia + ";";
			// especifica consulta e a executa
			listTribo = new LinkedList<Tribo>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listTribo.add(new Tribo(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	
	}
	
	public List<Tribo> getList(){
		return listTribo;
	}

	public Tribo findTriboById(int idTribo) {
		Tribo tribo = new Tribo();
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_tribo where id_tribo = "+ idTribo + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				tribo.setIdTribo(resultSet.getInt(1));
				tribo.setNmTribo(resultSet.getString(2));
				tribo.setIdFamilia(resultSet.getInt(3));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return tribo;
	}

}
