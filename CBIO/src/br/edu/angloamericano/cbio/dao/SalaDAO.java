package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Sala;

public class SalaDAO extends DAO{
	private Sala sala;
	List<Sala> listSala;
	
	public SalaDAO(){
		colName = "tb_sala";
		columns = "id_sala, nm_sala, fk_id_bloco";
		query = "SELECT * FROM " + colName + ";";
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_sala=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Sala", "Nome Sala", "Identificador Bloco" };
		columnsNames = cN;
		numColumns = 3;

		sala = new Sala();

		initConnection();
	}
	
	public Sala getSalaById(int id){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "SELECT * FROM " + colName + " WHERE ID_SALA = " + id;
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				sala = new Sala();
				sala.setIdSala(resultSet.getInt(1));
				sala.setNmSala(resultSet.getString(2));
				sala.setIdBloco(resultSet.getInt(3));
			}
		}catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		return sala;
	}

	
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			sala.setIdSala(resultSet.getInt(1));
			sala.setNmSala(resultSet.getString(2));
			sala.setIdBloco(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
		return sala;
	}

	@Override
	public void setData(Data d) {
		Sala s = (Sala) d;
		sala.setIdSala(s.getIdSala());
		sala.setNmSala(s.getNmSala());
		sala.setIdBloco(s.getIdBloco());
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return sala.getIdSala();
			case 2:
				return sala.getNmSala();
			case 3:
				return sala.getIdBloco();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = update + "(" + sala.getIdSala() + ", '"
				+ sala.getNmSala() + "', "+ sala.getIdBloco() + ");" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Sala sala) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_sala set nm_sala = '" + sala.getNmSala() + "' where id_sala = " 
							+ sala.getIdSala() + ";";
		//String newData = update + "(" + sala.getIdSala() + ", '"
		//		+ sala.getNmSala() + "', "+ sala.getIdBloco() + ");" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void deleteDB(Sala sala) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + sala.getIdSala() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + sala.getIdSala() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	public void insert(Bloco bloco, String nm_Sala) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
	
		String newData = "insert into tb_sala (nm_sala , fk_id_bloco) values ('" + nm_Sala + "', " + bloco.getIdBloco() + ");" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		//Local local = (Local) d;
		
		String newData = insert + "('"+  sala.getIdSala() + "', '"
				+ sala.getNmSala() + "', '"+ sala.getIdBloco() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listSala = new LinkedList<Sala>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listSala.add(new Sala(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	
	}
	
	public void setQueryBloco(int idBloco) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "select * from tb_sala where fk_id_bloco = "+ idBloco + ";";
			// especifica consulta e a executa
			listSala = new LinkedList<Sala>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listSala.add(new Sala(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	
	}
	
	public List<Sala> getList(){
		return listSala;
	}


}
