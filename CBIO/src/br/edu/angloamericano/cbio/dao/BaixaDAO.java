package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Baixa;

public class BaixaDAO extends DAO{

	private Baixa baixa = new Baixa();
	
	public BaixaDAO(){
		query = "SELECT * FROM Baixa";
		update = "";
		delete = "DELETE from Baixa where idBaixa = ";
		insert = "INSERT INTO Baixa (`Descricao`, `Dt_Baixa`, `Usuario_Usuario`) VALUES ( ";
		
		initConnection();
	}
	
	@Override
	public void moveToRow(int row) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Data getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setData(Data d) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveDB() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteDB() {
		// TODO Auto-generated method stub
		
	}

	public void insert(Baixa b) {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		baixa = b;

		try {
//			INSERT INTO Baixa (`Descricao`, `Dt_Baixa`, `Usuario_Usuario`) VALUES ( 'teste', '2013-05-01', 1);
			insert = "INSERT INTO Baixa (`Descricao`, `Dt_Baixa`, `Usuario_Usuario`) VALUES ( '" + baixa.getDesc() + 
					"', '" + baixa.getDtBaixa() + "', " + baixa.getIdUser() + ")";
			statement.executeUpdate(insert);
		} catch (SQLException e) {
			System.out.println("Erro ao inserir Baixa\nErro gerado na classe BaixaDAO, metodo insert");
			e.printStackTrace();
		}
	}
	
	/*
	 * Pega o ultimo id cadastrado
	 */
	public int getLastId() throws SQLException{
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Baixa";
		resultSet = statement.executeQuery(query);
		if(resultSet.next()){
			resultSet.last();
			return resultSet.getInt("idBaixa");
		}
		else
			return 1;
		
	}

	@Override
	public void setQuery() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insert(Data d) {
		// TODO Auto-generated method stub
		
	}

}
