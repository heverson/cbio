package br.edu.angloamericano.cbio.dao;

import java.util.ArrayList;
import java.util.List;

//Classe gen�rica para cadastros menores criada para n�o haver necessidade
//da cria��o de v�rias classes de cadastros pequenos como: cadastro de cidades,
//localidades, classes de animais, fam�lia...
public class DadosDAO {

	public static List<String> getEstados() {
		List<String> obj = new ArrayList<String>();
		obj.add("estado teste");
		return obj;
	}

	public static List<String> getClasses() {
		List<String> obj = new ArrayList<String>();
		//obj.add("classes teste");
		return obj;
	}

	public static List<String> getCidades(String estado) {
		List<String> obj = new ArrayList<String>();
		obj.add("cidade teste");
		return obj;
	}

	public static List<String> getLocalidades(String cidade) {
		List<String> obj = new ArrayList<String>();
		obj.add("localidade teste");
		return obj;
	}

	public static List<String> getOrdens(String classe) {
		List<String> obj = new ArrayList<String>();
		//obj.add("ordem teste");
		return obj;
	}

	public static void addCidade(String nomeCidade,String estado){

	}

	public static void addLocalidade(String nomeLocalidade,String estado, String cidade){

	}

	public static List<String> getFamilias(String ordem) {
		List<String> obj = new ArrayList<String>();
		//obj.add("fam�lia teste");
		return obj;
	}

	public static List<String> getTribos(String familia) {
		List<String> obj = new ArrayList<String>();
		//obj.add("tribo teste");
		return obj;
	}

	public static List<String> getEspecies(String tribo) {
		List<String> obj = new ArrayList<String>();
		//obj.add("esp�cie teste");
		return obj;
	}

	public static void addOrdem(String ordem, String classe) {

	}

	public static void addFamilia(String familia, String ordem) {
		// TODO Auto-generated method stub
		
	}
	
	public static void addTribo(String tribo, String familia) {
		// TODO Auto-generated method stub
		
	}

	public static void addEspecie(String especie, String tribo) {
		// TODO Auto-generated method stub
		
	}
}
