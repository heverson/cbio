package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.LocalComplemento;
import br.edu.angloamericano.cbio.dados.Sala;

public class LocalComplementoDAO extends DAO{
	private LocalComplemento comp;
	List<LocalComplemento> listComplemento;

	public LocalComplementoDAO(){
		colName = "tb_complemento";
		columns = "id_complemento, nm_complemento, ds_complemento,fk_id_sala";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_complemento=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador complemento", "Nome complemento","Descrição Complemento", "Identificador Sala" };
		columnsNames = cN;
		numColumns = 4;

		comp = new LocalComplemento();

		initConnection();

	}

	public LocalComplemento getById(int id){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "SELECT * FROM " + colName + " WHERE ID_COMPLEMENTO = " + id;
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				comp = new LocalComplemento();
				comp.setIdComplemento(resultSet.getInt(1));
				comp.setNmComplemento(resultSet.getString(2));
				comp.setDsComplemento(resultSet.getString(3));
				comp.setIdSala(resultSet.getInt(4));
			}
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		return comp;
	}

	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			comp.setIdComplemento(resultSet.getInt(1));
			comp.setNmComplemento(resultSet.getString(2));
			comp.setDsComplemento(resultSet.getString(3));
			comp.setIdSala(resultSet.getInt(4));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public Data getData() {
		return comp;
	}

	@Override
	public void setData(Data d) {
		LocalComplemento c = (LocalComplemento) d;
		comp.setIdComplemento(c.getIdComplemento());
		comp.setNmComplemento(c.getNmComplemento());
		comp.setDsComplemento(c.getDsComplemento());
		comp.setIdSala(c.getIdSala());
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
		case 1:
			return comp.getIdComplemento();
		case 2:
			return comp.getNmComplemento();
		case 3:
			return comp.getDsComplemento();
		case 4:
			return comp.getIdSala();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_complemento set nm_complemento = '" + comp.getNmComplemento() + "', ds_complemento = '"+comp.getDsComplemento() + "' where id_complemento = " 
				+ comp.getIdComplemento() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}


	}

	public void saveDB(LocalComplemento comp) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_complemento set nm_complemento = '" + comp.getNmComplemento() + "', ds_complemento = '"+comp.getDsComplemento() + "' where id_complemento = " 
				+ comp.getIdComplemento() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(LocalComplemento comp) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + comp.getIdComplemento() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}


	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + comp.getIdComplemento() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}


	public void insert(Sala sala, String nm_complemento ,String ds_complemento) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "insert into tb_complemento (nm_complemento , ds_complemento , fk_id_sala) values('" 
				+ nm_complemento + "', '"+ ds_complemento + "'," + sala.getIdSala() + ");" ;

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();

	}
	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		//Local local = (Local) d;

		String newData = insert +"('" + comp.getIdComplemento() + "', '"
				+ comp.getNmComplemento() + "', '"+ comp.getDsComplemento() + "', '"+ comp.getIdSala() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();

	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listComplemento = new LinkedList<LocalComplemento>();
			resultSet = statement.executeQuery(query);

			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd

				listComplemento.add(new LocalComplemento(resultSet.getInt(1),resultSet.getString(2), resultSet.getString(3),resultSet.getInt(4)));

			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public void setQuerySala(int idSala) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listComplemento = new LinkedList<LocalComplemento>();
			query = "select * from tb_complemento where fk_id_sala = " + idSala + ";" ;
			resultSet = statement.executeQuery(query);

			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd

				listComplemento.add(new LocalComplemento(resultSet.getInt(1),resultSet.getString(2), resultSet.getString(3),resultSet.getInt(4)));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public List<LocalComplemento> getList(){
		return listComplemento;
	}
}
