package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Familia;
import br.edu.angloamericano.cbio.dados.Ordem;

public class FamiliaDAO extends DAO{
	private Familia familia;
	private List<Familia> listFamilia;
	
	public FamiliaDAO(){
		colName = "tb_familia";
		columns = "id_familia, nm_familia, fk_id_Ordem";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_familia=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Familia", "Nome Familia", "Identificador Ordem" };
		columnsNames = cN;
		numColumns = 3;
		
		familia = new Familia();

		initConnection();
	}

	
	public void insert(Ordem ordem, String nm_familia){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		
		String newData = "insert into tb_Familia (nm_familia , fk_id_orde) values ('"+ nm_familia + "' , " + ordem.getIdOrdem() + ");" ; 

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}
	
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			familia.setIdFamilia(resultSet.getInt(1));
			familia.setNmFamilia(resultSet.getString(2));
			familia.setIdOrdem(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
	
		return (Data) familia;
	}

	@Override
	public void setData(Data d) {
		Familia f = (Familia) d;
		
		familia.setIdFamilia(f.getIdFamilia());
		familia.setNmFamilia(f.getNmFamilia());
		familia.setIdOrdem(f.getIdOrdem());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return familia.getIdFamilia();
			case 2:
				return familia.getNmFamilia();
			case 3:
				return familia.getIdOrdem();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_Familia set nm_Familia = '" + familia.getNmFamilia() + "' where id_Familia = " 
				+ familia.getIdFamilia() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Familia familia) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_Familia set nm_Familia = '" + familia.getNmFamilia() + "' where id_Familia = " 
				+ familia.getIdFamilia() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Familia familia) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + familia.getIdFamilia() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + familia.getIdFamilia() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		Familia familia = (Familia) d;
		String newData = "insert into tb_familia (nm_familia , fk_id_ordem) values ('" + familia.getNmFamilia() + "', " + familia.getIdOrdem() + ");" ;

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listFamilia = new LinkedList<Familia>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listFamilia.add(new Familia(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	

	public void setQueryOrdem(int idOrdem) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			listFamilia = new LinkedList<Familia>();
			query = "select * from tb_familia where fk_id_ordem =" + idOrdem + ";";
			resultSet = statement.executeQuery(query);
				
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				listFamilia.add(new Familia(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}

		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public Familia findFamiliaById(int idFamilia) {
		Familia fam = new Familia();
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_familia where id_familia = "+ idFamilia + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				fam.setIdFamilia(resultSet.getInt(1));
				fam.setNmFamilia(resultSet.getString(2));
				fam.setIdOrdem(resultSet.getInt(3));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return fam;
	}
	
	public List<Familia> getList(){
		return listFamilia;
	}


}
