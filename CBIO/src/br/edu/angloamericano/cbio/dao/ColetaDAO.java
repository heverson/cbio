package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import br.edu.angloamericano.cbio.dados.Coleta;
import br.edu.angloamericano.cbio.dados.Data;

public class ColetaDAO extends DAO{

	Coleta coleta = new Coleta();
	
	/**
	 * Construtor da Classe AnimalDAO.
	 */
	public ColetaDAO (){
		colName = "tb_coleta";
		columns = "id_coleta, nm_coletor, dt_recolhimento, ds_origem_animal, ds_metodo_coleta, ds_objetivo_coleta, fk_endereco_coleta";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador da Coleta", "Data de Recolhimento", "Origem do Animal", "Método de Coleta",
						"Objetivo da Coleta", "Endere�o da Coleta"};
		columnsNames = cN;
		numColumns = 7;
		
		coleta = new Coleta();
		initConnection();
	}
	
	/**
	 * Fun��o que percorre at� a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			coleta.setIdColeta(resultSet.getInt(1));
			coleta.setNmColetor(resultSet.getString(2));
			coleta.setDtRecolhimento(resultSet.getDate(3));
			coleta.setDsOrigemAnimal(resultSet.getString(4));
			coleta.setDsMetodoColeta(resultSet.getString(5));
			coleta.setDsObjetivoColeta(resultSet.getString(6));
			coleta.setIdEnderecoColeta(resultSet.getInt(7));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Data getData() {
		return coleta;
	}

	@Override
	public void setData(Data d) {
		Coleta c = (Coleta) d;
		coleta.setIdColeta(c.getIdColeta());
		coleta.setNmColetor(c.getNmColetor());
		coleta.setDtRecolhimento(c.getDtRecolhimento());
		coleta.setDsOrigemAnimal(c.getDsOrigemAnimal());
		coleta.setDsMetodoColeta(c.getDsMetodoColeta());
		coleta.setDsObjetivoColeta(c.getDsObjetivoColeta());
		coleta.setIdEnderecoColeta(c.getIdEnderecoColeta());
	}

	//nao implementada
	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Fun��o que obt�m coluna do atributo da classe.
	 * @param int column - N�mero da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return coleta.getIdColeta();
			case 2:
				return coleta.getNmColetor();
			case 3:
				return coleta.getDtRecolhimento();
			case 4:
				return coleta.getDsOrigemAnimal();
			case 5:
				return coleta.getDsMetodoColeta();
			case 6:
				return coleta.getDsObjetivoColeta();
			case 7:
				return coleta.getIdEnderecoColeta();
		}
		return null;
	}

	/**
	 * Fun��o que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		String newData ="update tb_coleta set " 
				+"nm_coletor = '"+ coleta.getNmColetor() + "', "
				+"dt_recolhimento = '" +  date.format(coleta.getDtRecolhimento()) + "', " 
				+"ds_origem_animal = '"	+ coleta.getDsOrigemAnimal() + "', " 
				+"ds_metodo_coleta = '" + coleta.getDsMetodoColeta() + "', " 
				+"ds_objetivo_coleta ='"	+ coleta.getDsObjetivoColeta()+ "' "
				+"where id_coleta = "+ coleta.getIdColeta() + ";";
		try {
			statement.executeUpdate(newData);
			
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Fun��o que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + coleta.getIdColeta() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função que executa a query de insert(inserção) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Coleta c = (Coleta) d;
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		
		
		//JOptionPane.showMessageDialog(null, c.getIdEnderecoColeta());
		
		String newData = "insert into tb_coleta (nm_coletor, dt_recolhimento , ds_origem_animal , ds_metodo_coleta , " +
				"ds_objetivo_coleta , fk_endereco_coleta) values ('"
				+ c.getNmColetor() + "', '" + date.format(c.getDtRecolhimento()) + "', '"
				+ c.getDsOrigemAnimal() + "', '" + c.getDsMetodoColeta() + "', '"
				+ c.getDsObjetivoColeta()+ "', " + c.getIdEnderecoColeta() + ");";
		try {
			statement.executeUpdate(newData);
			
		} catch (SQLException e) {
			System.out.println("Erro ao inserir dados de Coleta\nErro gerado na classe ColetaDAO, método insert()");
			e.printStackTrace();
		}
		setQuery();
	}

	
	/**
	 * Funcao de auxilio para retornar o ultimo objeto inserido na tabela
	 */
	
	public int getIdLast(){
		try{
			if(statement.isClosed())
				return 0;
		}catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			resultSet = statement.executeQuery(query);
			
			if( resultSet.last() ) {
				coleta.setIdColeta(resultSet.getInt(1));
				coleta.setNmColetor(resultSet.getString(2));
				coleta.setDtRecolhimento(resultSet.getDate(3));
				coleta.setDsOrigemAnimal(resultSet.getString(4));
				coleta.setDsMetodoColeta(resultSet.getString(5));
				coleta.setDsObjetivoColeta(resultSet.getString(6));
				coleta.setIdEnderecoColeta(resultSet.getInt(7));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		return coleta.getIdColeta();
	}
	/**
	 * 
	 *
	 * Função apenas de auxilio para criação das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obtém metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o número de linhas em ResultSet
			resultSet.last(); // move para a última linha
			numberOfRows = resultSet.getRow(); // obtém número de linha
			resultSet.first(); // volta para a primeira linha

			if (numberOfRows != 0) {
				coleta.setIdColeta(resultSet.getInt(1));
				coleta.setNmColetor(resultSet.getString(2));
				coleta.setDtRecolhimento(resultSet.getDate(3));
				coleta.setDsOrigemAnimal(resultSet.getString(4));
				coleta.setDsMetodoColeta(resultSet.getString(5));
				coleta.setDsObjetivoColeta(resultSet.getString(6));
				coleta.setIdEnderecoColeta(resultSet.getInt(7));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public Coleta findColetaById(int id){
		coleta = new Coleta();
		try {
			if (statement.isClosed())
				return coleta;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_coleta where id_coleta = " + id +";";
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obtém metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o número de linhas em ResultSet
			resultSet.last(); // move para a última linha
			numberOfRows = resultSet.getRow(); // obtém número de linha
			resultSet.first(); // volta para a primeira linha

			if (numberOfRows != 0) {
				coleta.setIdColeta(resultSet.getInt(1));
				coleta.setNmColetor(resultSet.getString(2));
				coleta.setDtRecolhimento(resultSet.getDate(3));
				coleta.setDsOrigemAnimal(resultSet.getString(4));
				coleta.setDsMetodoColeta(resultSet.getString(5));
				coleta.setDsObjetivoColeta(resultSet.getString(6));
				coleta.setIdEnderecoColeta(resultSet.getInt(7));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		
		return coleta;
	}

}
