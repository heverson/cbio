/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Cidade;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Estado;

/**
 *
 */
public class EstadoDAO extends DAO{
	
	private Estado estado;
	private List<Estado> listEstado;
	
	/**
	 * Construtor da Classe EstadoDAO.
	 */
	public EstadoDAO(){
		colName = "tb_estado";
		columns = "id_estado, nm_estado";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador do Estado", "Nome do Estado" };
		columnsNames = cN;
		numColumns = 2;
		estado = new Estado();
		initConnection();
	}
	
	/**
	 * Fun��o que percorre at� a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			estado.setIdEstado(resultSet.getInt(1));
			estado.setNmEstado(resultSet.getString(2));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Data getData() {
		return (Data) estado;
	}

	public void setData(Data d) {
		Estado est = (Estado) d;
		estado.setIdEstado(est.getIdEstado());
		estado.setNmEstado(est.getNmEstado());
	}

	//nao implementada
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Fun��o que obt�m coluna do atributo da classe.
	 * @param int column - N�mero da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return estado.getIdEstado();
			case 2:
				return estado.getNmEstado();
		}
		return null;
	}

	/**
	 * Fun��o que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String newData = update + "('" + estado.getIdEstado() + "', '" + estado.getNmEstado() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Fun��o que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + estado.getIdEstado() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o que executa a query de insert(inser��o) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Cidade c = (Cidade) d;
		this.setData((Data) c);
		String newData = insert + "(DEFAULT, '" + estado.getNmEstado() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o apenas de auxilio para cria��o das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			listEstado = new LinkedList<Estado>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){

				 listEstado.add(new Estado(resultSet.getInt(1), resultSet.getString(2) ) );
	
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public List<Estado> getList(){
		return listEstado;
	}
	
	public Estado findEstadoById(int id){
		estado = new Estado();
		try {
			if (statement.isClosed())
				return estado;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_estado where id_estado =" + id + ";" ;
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				estado.setIdEstado(resultSet.getInt(1));
				estado.setNmEstado(resultSet.getString(2));
		
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return estado;
	}

}
