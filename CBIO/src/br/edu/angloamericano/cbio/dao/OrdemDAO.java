package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Ordem;

public class OrdemDAO extends DAO{
	private Ordem ordem;
	private List<Ordem> listOrdem;
	
	public OrdemDAO(){
		colName = "tb_ordem";
		columns = "id_ordem, nm_ordem, fk_id_classe";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_bloco=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Ordem", "Nome Ordem", "Identificador Classe" };
		columnsNames = cN;
		numColumns = 3;
		
		ordem = new Ordem();

		initConnection();
	}

	
	public void insert(Classe classe, String nm_ordem){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		
		String newData = "insert into tb_ordem (nm_ordem , fk_id_classe) values ('"+ nm_ordem + "' , " + classe.getIdClasse() + ");" ; 

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}
	
	public void insert(Ordem ordem){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		
		String newData = "insert into tb_ordem (nm_ordem , fk_id_classe) values ('"+ ordem.getNmOrdem() + "' , " + ordem.getIdClasse() + ");" ; 

		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			ordem.setIdOrdem(resultSet.getInt(1));
			ordem.setNmOrdem(resultSet.getString(2));
			ordem.setIdClasse(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
	
		return (Data) ordem;
	}

	@Override
	public void setData(Data d) {
		Ordem b = (Ordem) d;
		
		ordem.setIdOrdem(b.getIdOrdem());
		ordem.setNmOrdem(b.getNmOrdem());
		ordem.setIdClasse(b.getIdClasse());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return ordem.getIdOrdem();
			case 2:
				return ordem.getNmOrdem();
			case 3:
				return ordem.getIdClasse();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_ordem set nm_ordem = '" + ordem.getNmOrdem() + "' where id_ordem = " 
				+ ordem.getIdOrdem() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Bloco bloco) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_ordem set nm_ordem = '" + ordem.getNmOrdem() + "' where id_ordem = " 
				+ ordem.getIdOrdem() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Bloco bloco) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + ordem.getIdOrdem() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + ordem.getIdOrdem() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		//Local local = (Local) d;
		
		String newData = insert + "('"+  ordem.getIdOrdem() + "', '"
				+ ordem.getNmOrdem() + "', '"+ ordem.getIdClasse() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listOrdem = new LinkedList<Ordem>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listOrdem.add(new Ordem(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	

	public void setQueryClasse(int idClasse) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listOrdem = new LinkedList<Ordem>();
			query = "select * from tb_ordem where fk_id_classe = "+ idClasse + ";" ;
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listOrdem.add(new Ordem(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
	
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public Ordem findOrdemById(int idOrdem) {
		Ordem ord = new Ordem();
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_ordem where id_ordem = "+ idOrdem + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				ord.setIdOrdem(resultSet.getInt(1));
				ord.setNmOrdem(resultSet.getString(2));
				ord.setIdClasse(resultSet.getInt(3));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return ord;
	}
	
	public List<Ordem> getList(){
		return listOrdem;
	}


}
