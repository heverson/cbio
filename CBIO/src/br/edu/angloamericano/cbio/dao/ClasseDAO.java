package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Local;

public class ClasseDAO extends DAO{
	private Classe classe;
	private List<Classe> listClasse;

	public ClasseDAO(){
		colName = "tb_classe";
		columns = "id_classe, nm_classe";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_classe=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Classe", "Nome Classe" };
		columnsNames = cN;
		numColumns = 2;

		classe = new Classe();

		initConnection();
	}
	
	public List<Classe> getList(){
		return listClasse;
	}
	
	
	@Override
	public void moveToRow(int row) 
		{
			try
			{
				if (statement.isClosed())
					return;
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			try
			{
				resultSet.absolute(row);
				classe.setIdClasse(resultSet.getInt(1));
				classe.setNmClasse(resultSet.getString(2));

			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
		}

	@Override
	public Data getData() {
		
		return classe;
	}

	@Override
	public void setData(Data d) {
		
		classe = (Classe) d;
		classe.setIdClasse(((Classe) d).getIdClasse());
		classe.setNmClasse(((Classe) d).getNmClasse());

	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return classe.getIdClasse();
			case 2:
				return classe.getNmClasse();
		}
		return null;
	}


	public void saveDB(Classe classe) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_classe set nm_classe = '" + classe.getNmClasse() + "' where id_classe = " 
				+ classe.getIdClasse() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}
	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_classe set nm_classe = '" + classe.getNmClasse() + "' where id_classe = " 
				+ classe.getIdClasse() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}

	public void deleteDB(Local local) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + local.getIdLocal() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	
		
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + classe.getIdClasse() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	
		
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		Classe classe = (Classe) d;
		
		String newData = "insert into tb_classe (nm_classe) values ('" + classe.getNmClasse() + "');" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listClasse = new LinkedList<Classe>();
			resultSet = statement.executeQuery(query);
			
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listClasse.add(new Classe(resultSet.getInt(1),resultSet.getString(2)));

				
			}
			
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public Classe findClasseById(int idClasse) {
		Classe cla = new Classe();
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_classe where id_Classe = "+ idClasse + ";";
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){
				cla.setIdClasse(resultSet.getInt(1));
				cla.setNmClasse(resultSet.getString(2));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return cla;
	}
	
}
