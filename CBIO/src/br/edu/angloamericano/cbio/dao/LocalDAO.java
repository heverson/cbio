package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Local;


public class LocalDAO extends DAO{
	
	private Local local;
	public List<Local> listLocal;
	
	public LocalDAO(){
		colName = "tb_local";
		columns = "id_local, nm_local";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_local=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Local", "Nome Local" };
		columnsNames = cN;
		numColumns = 2;

		local = new Local();

		initConnection();
	}
	
	public Local getLocalById(int id){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "SELECT * FROM " + colName + " WHERE ID_LOCAL = " + id;
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				local = new Local();
				local.setIdLocal(resultSet.getInt(1));
				local.setNmLocal(resultSet.getString(2));
			}
		}catch (SQLException e1)
		{
			e1.printStackTrace();
		}		
		return local;
	}
	
	public List<Local> getList(){
		return listLocal;
	}
	
	@Override
	public void moveToRow(int row) 
		{
			try
			{
				if (statement.isClosed())
					return;
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			try
			{
				resultSet.absolute(row);
				local.setIdLocal(resultSet.getInt(1));
				local.setNmLocal(resultSet.getString(2));

			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
			}
		}

	@Override
	public Data getData() {
		
		return local;
	}

	@Override
	public void setData(Data d) {
		
		Local loc = (Local) d;
		loc.setIdLocal(local.getIdLocal());
		loc.setNmLocal(local.getNmLocal());

	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return local.getIdLocal();
			case 2:
				return local.getNmLocal();
		}
		return null;
	}


	public void saveDB(Local local) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_local set nm_local = '" + local.getNmLocal() + "' where id_local = " 
				+ local.getIdLocal() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}
	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_local set nm_local = '" + local.getNmLocal() + "' where id_local = " 
				+ local.getIdLocal() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}

	public void deleteDB(Local local) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + local.getIdLocal() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	
		
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + local.getIdLocal() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	
		
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		Local local = (Local) d;
		
		String newData = "insert into tb_local (nm_local) values ('" + local.getNmLocal() + "');" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listLocal = new LinkedList<Local>();
			resultSet = statement.executeQuery(query);
			
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listLocal.add(new Local(resultSet.getInt(1),resultSet.getString(2)));

				
			}
			
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
		

}
