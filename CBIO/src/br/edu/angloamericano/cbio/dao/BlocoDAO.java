package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Local;

public class BlocoDAO extends DAO {
	private Bloco bloco;
	private List<Bloco> listBloco;

	public BlocoDAO(){
		colName = "tb_bloco";
		columns = "id_bloco, nm_bloco, fk_id_local";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_bloco=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador bloco", "Nome Bloco", "Identificador Local" };
		columnsNames = cN;
		numColumns = 3;

		bloco = new Bloco();

		initConnection();
	}

	public Bloco getBlocoById(int id){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		query = "SELECT * FROM " + colName + " WHERE ID_BLOCO = " + id;
		try{
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				bloco = new Bloco();
				bloco.setIdBloco(resultSet.getInt(1));
				bloco.setNmBloco(resultSet.getString(2));
				bloco.setIdLocal(resultSet.getInt(3));
			}
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		return bloco;
	}


	public void insert(Local local, String nm_bloco){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}

		String newData = "insert into tb_bloco (nm_bloco , fk_id_local) values ('"+ nm_bloco + "' , " + local.getIdLocal() + ");" ; 
		//String newData = insert + "('"+  bloco.getIdBloco() + "', '"
		//		+ bloco.getNmBloco() + "', '"+ bloco.getIdLocal() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();		
	}


	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			bloco.setIdBloco(resultSet.getInt(1));
			bloco.setNmBloco(resultSet.getString(2));
			bloco.setIdLocal(resultSet.getInt(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public Data getData() {

		return bloco;
	}

	@Override
	public void setData(Data d) {
		Bloco b = (Bloco) d;
		bloco.setIdBloco(b.getIdBloco());
		bloco.setNmBloco(b.getNmBloco());
		bloco.setIdLocal(b.getIdLocal());

	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
		case 1:
			return bloco.getIdBloco();
		case 2:
			return bloco.getNmBloco();
		case 3:
			return bloco.getIdLocal();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_bloco set nm_bloco = '" + bloco.getNmBloco() + "' where id_bloco = " 
				+ bloco.getIdBloco() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}


	}

	public void saveDB(Bloco bloco) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_bloco set nm_bloco = '" + bloco.getNmBloco() + "' where id_bloco = " 
				+ bloco.getIdBloco() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Bloco bloco) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + bloco.getIdBloco() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + bloco.getIdBloco() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		//Local local = (Local) d;

		String newData = insert + "('"+  bloco.getIdBloco() + "', '"
				+ bloco.getNmBloco() + "', '"+ bloco.getIdLocal() + "');" ;
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();

	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listBloco = new LinkedList<Bloco>();
			resultSet = statement.executeQuery(query);

			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd

				listBloco.add(new Bloco(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
			}

		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void setQueryLocal(int idLocal) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "select * from tb_bloco where fk_id_local = "+ idLocal + ";";
			// especifica consulta e a executa
			listBloco = new LinkedList<Bloco>();
			resultSet = statement.executeQuery(query);

			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd

				listBloco.add(new Bloco(resultSet.getInt(1),resultSet.getString(2), resultSet.getInt(3)));
			}

		}catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public List<Bloco> getList(){
		return listBloco;
	}


}
