package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.PecaProcesso;

public class PecaProcessoDAO extends DAO{
	private PecaProcesso pecaProcesso;
	private List<PecaProcesso> listPecaProcesso;
	
	
	public PecaProcessoDAO(){
		colName = "tb_peca_processo";
		columns = "id_peca, id_peca_Processo, nm_responsavel_Processo, nm_usuario, ds_Processo_realizado";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_Processo=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Peca", "Identificado Processo", "Nome do Responsavel Pelo Processo", "Descri��o do Processo Realizado" };
		columnsNames = cN;
		numColumns = 4;
		
		pecaProcesso = new PecaProcesso();

		initConnection();
	}
	
	public void updatePecaProcesso(int idProc, String dsc) throws SQLException{
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			update = "UPDATE `tb_peca_processo` SET `DS_PROCESSO_REALIZADO` = '" + dsc + "' WHERE ID_PROCESSO = " + idProc;
			statement.execute(update);
		}catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	public PecaProcesso getProcessoByIdPeca(int idPeca){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "SELECT * FROM " + colName + " WHERE ID_PECA = " + idPeca;
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
			pecaProcesso = new PecaProcesso();
			pecaProcesso.setIdPeca(resultSet.getInt(1));
			pecaProcesso.setIdProcesso(resultSet.getInt(2));
			pecaProcesso.setNmResponsavelProcesso(resultSet.getString(3));
			pecaProcesso.setUsuarioResponsavel(resultSet.getString(4));
			pecaProcesso.setDsProcessoRealizado(resultSet.getString(5));
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return pecaProcesso;
	}
	
	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			pecaProcesso.setIdPeca(resultSet.getInt(1));
			pecaProcesso.setIdProcesso(resultSet.getInt(2));
			pecaProcesso.setNmResponsavelProcesso(resultSet.getString(3));
			pecaProcesso.setUsuarioResponsavel(resultSet.getString(4));
			pecaProcesso.setDsProcessoRealizado(resultSet.getString(5));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public Data getData() {
	
		return (Data) pecaProcesso;
	}

	@Override
	public void setData(Data d) {
		PecaProcesso b = (PecaProcesso) d;
		pecaProcesso.setIdPeca(b.getIdPeca());
		pecaProcesso.setIdProcesso(b.getIdProcesso());
		pecaProcesso.setNmResponsavelProcesso(b.getNmResponsavelProcesso());
		pecaProcesso.setUsuarioResponsavel(b.getUsuarioResponsavel());
		pecaProcesso.setDsProcessoRealizado(b.getDsProcessoRealizado());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return pecaProcesso.getIdPeca();
			case 2:
				return pecaProcesso.getIdProcesso();
			case 3:
				return pecaProcesso.getNmResponsavelProcesso();
			case 4:
				return pecaProcesso.getUsuarioResponsavel();
			case 5:
				return pecaProcesso.getDsProcessoRealizado();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_Processo set nm_Processo = '" + pecaProcesso.getNmResponsavelProcesso() + "' where id_Processo = " 
				+ pecaProcesso.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	public void saveDB(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		pecaProcesso = (PecaProcesso) d ;
		String newData = "update tb_Processo set nm_Processo = '" + pecaProcesso.getNmResponsavelProcesso() + "', ds_processo_realizado = '"
					+ pecaProcesso.getDsProcessoRealizado() +"' where id_Processo = " + pecaProcesso.getIdProcesso() + " and id_Peca = " + pecaProcesso.getIdPeca() + ";"; 
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		pecaProcesso = (PecaProcesso) d;
		String del = "delete from tb_peca_processo where id_peca ="+ pecaProcesso.getIdPeca() + " and id_processo ="+ pecaProcesso.getIdProcesso() + ";";
	
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = "delete from tb_peca_processo where id_peca ="+ pecaProcesso.getIdPeca() + " and id_processo ="+ pecaProcesso.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		pecaProcesso = (PecaProcesso) d;
		String newData = "insert into tb_peca_processo (id_peca, id_processo, nm_responsavel_Processo , nm_usuario, ds_Processo_realizado ) values "
		+ "("+pecaProcesso.getIdPeca()+", "+pecaProcesso.getIdProcesso()+", '"+pecaProcesso.getNmResponsavelProcesso()+"', '"+ pecaProcesso.getUsuarioResponsavel()+"', '"+pecaProcesso.getDsProcessoRealizado()+"');" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
		
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listPecaProcesso = new LinkedList<PecaProcesso>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listPecaProcesso.add(new PecaProcesso(resultSet.getInt(1),resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	public List<PecaProcesso> getList(){
		return listPecaProcesso;
	}



}
