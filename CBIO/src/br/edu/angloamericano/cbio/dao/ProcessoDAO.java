package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Processo;
import br.edu.angloamericano.cbio.dados.Data;

public class ProcessoDAO extends DAO {
	private Processo processo;
	private List<Processo> listProcesso;
	
	
	public ProcessoDAO(){
		colName = "tb_processo";
		columns = "id_Processo, nm_Processo, ds_processo";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_Processo=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Processo", "Nome Processo", "Descriçao Processo" };
		columnsNames = cN;
		numColumns = 3;
		
		processo = new Processo();

		initConnection();
	}
	
	public void updateProcesso(Processo proc){
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			update = "UPDATE `tb_processo` SET DS_PROCESSO = '" + proc.getDsProcesso() + 
					"' WHERE ID_PROCESSO = " + proc.getIdProcesso();
			statement.execute(update);
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}
	
	public Processo getProcessoById(int idProc){
		try
		{
			if (statement.isClosed())
				return null;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			query = "SELECT * FROM " + colName + " WHERE ID_PROCESSO = " + idProc;
//			System.out.println(query);
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
			processo = new Processo();
			processo.setIdProcesso(resultSet.getInt(1));
			processo.setNmProcesso(resultSet.getString(2));
			processo.setDsProcesso(resultSet.getString(3));
		}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		return processo;
	}

	
	@Override
	public void moveToRow(int row) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.absolute(row);
			processo.setIdProcesso(resultSet.getInt(1));
			processo.setNmProcesso(resultSet.getString(2));
			processo.setDsProcesso(resultSet.getString(3));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public Data getData() {
	
		return (Data) processo;
	}

	@Override
	public void setData(Data d) {
		Processo b = (Processo) d;
		processo.setIdProcesso(b.getIdProcesso());
		processo.setNmProcesso(b.getNmProcesso());
		processo.setDsProcesso(b.getDsProcesso());
		
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return processo.getIdProcesso();
			case 2:
				return processo.getNmProcesso();
			case 3:
				return processo.getDsProcesso();
		}
		return null;
	}

	@Override
	public void saveDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String newData = "update tb_Processo set nm_Processo = '" + processo.getNmProcesso() + "' where id_Processo = " 
				+ processo.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public void saveDB(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		processo = (Processo) d ;
		String newData = "update tb_Processo set nm_Processo = '" + processo.getNmProcesso() + "' where id_Processo = " 
				+ processo.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteDB(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		processo = (Processo) d;
		String del = delete + processo.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	@Override
	public void deleteDB() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		String del = delete + processo.getIdProcesso() + ";";
		try
		{
			statement.executeUpdate(del);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void insert(Data d) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		processo = (Processo) d;
		String newData = "insert into tb_processo (id_processo, nm_processo , ds_processo ) values "
		+ "(Default, '"+processo.getNmProcesso()+"', '"+processo.getDsProcesso()+"');" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}
	
	public void insert(Processo p) {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		processo = p;
		String newData = "insert into tb_processo (id_processo, nm_processo , ds_processo ) values "
		+ "(Default, '"+processo.getNmProcesso()+"', '"+processo.getDsProcesso()+"');" ;
		
		try
		{
			statement.executeUpdate(newData);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		setQuery();
	}

	@Override
	public void setQuery() {
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			// especifica consulta e a executa
			listProcesso = new LinkedList<Processo>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){        //Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas existentes no bd
				
				listProcesso.add(new Processo(resultSet.getInt(1),resultSet.getString(2), resultSet.getString(3)));
		}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
		
	public List<Processo> getList(){
		return listProcesso;
	}

	/**
	 * Fun��o que busca pelo id do ultimo processo inserido
	 * @return int - id do ultimo processo inserido
	 */
	public int getLastIdProcesso(){
		int idProcesso = 0;
		try{
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select LAST_INSERT_ID(id_processo) from tb_processo order by id_processo desc limit 1 ;";
			resultSet = statement.executeQuery(query); // especifica consulta e a executa
			
			// seta o ultimo id inserido no idProcesso para retorno
			if(resultSet.next())
				idProcesso = resultSet.getInt(1);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return idProcesso;
	}	
	
}
