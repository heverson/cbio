package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Login;

public class LoginDAO extends DAO{

	private Login login = new Login();

	public LoginDAO(){
		query = "SELECT * FROM Login";
		update = "UPDATE `Login` SET ";
		delete = "DELETE FROM `Login` WHERE ";
		insert = "INSERT INTO `Login`(`Login`, `Senha`, `Usuario_Usuario`) VALUES ";

		initConnection();
	}

	/**
	 * Método responsável por fazer a validação do usuário na base de dados
	 * @param login - Login do usuário
	 * @param senha - senha do usuário
	 * @return Retorna o ID do usuário se o login for valido e 0 caso contrario
	 * @throws Exception
	 */
	public int valida_login(String login, String senha) throws Exception{
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Login WHERE Login = '" + login + "' AND Senha = '" + senha + "'";
		resultSet = statement.executeQuery(query);

		if(resultSet.next()){
			return resultSet.getInt("Usuario_Usuario");
		}else{
			return 0;
		}
	}

	/*
	 * Método que busca um registro pelo Login, se existir retorna o id do usuário, caso contrario retorna 0
	 */
	public int valida_user(String nome)throws Exception{
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Login WHERE Login = '" + nome + "'";
		resultSet = statement.executeQuery(query);
		if(resultSet.next()){
			return resultSet.getInt("Usuario_Usuario");
		}else
		{
			return 0;
		}
	}

	/*
	 * Método que retorna os dados de login de um determinado usuário, determinado pelo idUser recebido como parâmetro
	 */
	public Login getLogin(int idUser) throws SQLException{
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Login WHERE Usuario_Usuario = " + idUser + "";
		resultSet = statement.executeQuery(query);

		if(resultSet.next()){
			login.setId(resultSet.getInt("idLogin"));
			login.setLogin(resultSet.getString("Login"));
			login.setSenha(resultSet.getString("Senha"));
			login.setIdUsuaio(resultSet.getInt("Usuario_Usuario"));
			return login;
		}else{
			return null;
		}
	}

	public void setLogin(Login d) {
		// TODO Auto-generated method stub
		login = d;
	}

	@Override
	public void moveToRow(int row) {
		// TODO Auto-generated method stub

	}
	@Override
	public Data getData() {
		// TODO Auto-generated method stub
		return (Data) login;
	}
	@Override
	public void setData(Data d) {
		// TODO Auto-generated method stub
		login = (Login) d;
	}
	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getColumn(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Método responsável por alterar as informações de um registro
	 */
	@Override
	public void saveDB() {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		update = "UPDATE `Login` SET `Login` = '" + login.getLogin() + "', `Senha`= '" + login.getSenha() + "' WHERE idLogin = " + login.getId();

		try {
			statement.executeUpdate(update);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}	
	}


	/**
	 * método responsável por deletar um registro da tabela Login
	 */
	@Override
	public void deleteDB() {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		delete += "`Usuario_Usuario` = " + login.getIdUsuaio();

		try {
			statement.execute(delete);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}	
	}

	/**
	 * Insere um novo registro a tabela Login
	 * @param d - dados do novo registro
	 */
	public void insert(Login d) {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		login = d;

		insert = "INSERT INTO `Login`(`Login`, `Senha`, `Usuario_Usuario`) VALUES ( '" + login.getLogin() + "', '"
				+ login.getSenha() + "', " + login.getIdUsuaio() + ")";

		try {
			statement.execute(insert);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: "+e);
		}
	}
	@Override
	public void setQuery() {
		// TODO Auto-generated method stub

	}
	@Override
	public void insert(Data d) {
		// TODO Auto-generated method stub
	}
}
