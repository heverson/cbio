package br.edu.angloamericano.cbio.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.edu.angloamericano.cbio.controller.DBConnection;
import br.edu.angloamericano.cbio.dados.Data;


public abstract class DAO
{
	protected String	colName;
	protected String	columns;
	protected Statement	statement;
	protected ResultSet	resultSet;
	protected int		numberOfRows;
	protected String	query;
	protected String	update;
	protected String	delete;
	protected String	insert;
	protected String[]	columnsNames;
	protected int		numColumns;

	public void initConnection()
	{
		try
		{
			// cria Statement para consultar banco de dados
			statement = DBConnection.getConnection().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			// configura consulta e a executa
			//setQuery();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	// fecha Statement e Connection
	public void endConnection()
	{
		// fecha Statement e Connection
		try
		{
			statement.close();
			DBConnection.closeConnection();
		}
		catch (SQLException sqlException)
		{
			sqlException.printStackTrace();
		}
	}

	public int getNumberOfRows()
	{
		return numberOfRows;
	}

	public int getNumColumns()
	{
		return numColumns;
	}

	public String getColumnName(int column)
	{
		if (column > 0 && column < columnsNames.length + 1)
			return columnsNames[column - 1];
		return "";
	}

	public void save()
	{
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.updateRow();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void delete()
	{
		try
		{
			if (statement.isClosed())
				return;
		}
		catch (SQLException e1)
		{
			e1.printStackTrace();
		}
		try
		{
			resultSet.deleteRow();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// fecha Statement e Connection
	public void disconnectFromDatabase()
	{
		// fecha Statement e Connection
		try
		{
			statement.close();
			DBConnection.getConnection().close();
		}
		catch (SQLException sqlException)
		{
			sqlException.printStackTrace();
		}
	}

	public abstract void moveToRow(int row);

	public abstract Data getData();

	public abstract void setData(Data d);

	public abstract Class<?> getColumnClassName(int column);

	public abstract Object getColumn(int column);

	public abstract void saveDB();

	public abstract void deleteDB();

	public abstract void insert(Data d);

	public abstract void setQuery();

}
