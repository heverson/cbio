/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Endereco;

/**
 *
 */
public class EnderecoDAO extends DAO {
	
	private Endereco endereco;
	private List<Endereco> listEndereco;
	
	/**
	 * Construtor da Classe AnimalDAO.
	 */
	public EnderecoDAO(){
		colName = "tb_endereco";
		columns = "id_endereco, fk_id_cidade, ds_local, cd_coordenada_geografica, nm_local, nm_rua, nu_complemento, cd_cep";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador do Endereco", "Identificador da Cidade", "Dados do Local", "Coordenada Geográfica",
						"Nome do Local", "Nome da Rua", "Número do Complemento", "CEP" };
		columnsNames = cN;
		numColumns = 8;
		endereco = new Endereco();
		initConnection();
	}
	
	/**
	 * Função que percorre até a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			endereco.setIdEndereco(resultSet.getInt(1));
			endereco.setIdCidade(resultSet.getInt(2));
			endereco.setDsLocal(resultSet.getString(3));
			endereco.setCdCoordenadaGeografica(resultSet.getString(4));
			endereco.setNmLocal(resultSet.getString(5));
			endereco.setNmRua(resultSet.getString(6));
			endereco.setNuComplemento(resultSet.getString(7));
			endereco.setCdCep(resultSet.getString(8));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Data getData() {
		return endereco;
	}

	public void setData(Data d) {
		Endereco e = (Endereco) d;
		endereco.setIdEndereco(e.getIdEndereco());
		endereco.setIdCidade(e.getIdCidade());
		endereco.setDsLocal(e.getDsLocal());
		endereco.setCdCoordenadaGeografica(e.getCdCoordenadaGeografica());
		endereco.setNmLocal(e.getNmLocal());
		endereco.setNmRua(e.getNmRua());
		endereco.setNuComplemento(e.getNuComplemento());
		endereco.setCdCep(e.getCdCep());
	}

	// nao implementada
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Função que obtém coluna do atributo da classe.
	 * @param int column - Número da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return endereco.getIdEndereco();
			case 2:
				return endereco.getIdCidade();
			case 3:
				return endereco.getDsLocal();
			case 4:
				return endereco.getCdCoordenadaGeografica();
			case 5:
				return endereco.getNmLocal();
			case 6:
				return endereco.getNmRua();
			case 7:
				return endereco.getNuComplemento();
			case 8:
				return endereco.getCdCep();
		}
		return null;
	}

	/**
	 * Função que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String newData = update + "('" + endereco.getIdEndereco() + "', '"
				+ endereco.getIdCidade() + "', '" + endereco.getDsLocal() + "', '"
				+ endereco.getCdCoordenadaGeografica() + "', '" + endereco.getNmLocal() + "', '"
				+ endereco.getNmRua() + "', '" + endereco.getNuComplemento() + "', '"
				+ endereco.getCdCep() + "');";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Função que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + endereco.getIdEndereco() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função que executa a query de insert(inserção) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Endereco endereco = (Endereco) d;
		
		String newData = "INSERT INTO `tb_endereco` (`FK_ID_CIDADE`, `DS_LOCAL`, `CD_COORDENADA_GEOGRAFICA`, `NM_LOCAL`, `NM_RUA`, `NU_COMPLEMENTO`, `CD_CEP`) VALUES ( " 
				+ endereco.getIdCidade() + 
		  ", '" + endereco.getDsLocal() + 
		  "', '" + endereco.getCdCoordenadaGeografica() + 
		  "', '" + endereco.getNmLocal() + 
		  "', '" + endereco.getNmRua() +
		  "', '" + endereco.getNuComplemento() + 
		  "', '" + endereco.getCdCep() + "')";
		
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função apenas de auxilio para criação das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			listEndereco = new LinkedList<Endereco>();
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){	
				endereco = new Endereco();								//popula o novo objeto Endereco
				endereco.setIdEndereco(resultSet.getInt(1));
				endereco.setIdCidade(resultSet.getInt(2));
				endereco.setDsLocal(resultSet.getString(3));
				endereco.setCdCoordenadaGeografica(resultSet.getString(4));
				endereco.setNmLocal(resultSet.getString(5));
				endereco.setNmRua(resultSet.getString(6));
				endereco.setNuComplemento(resultSet.getString(7));
				endereco.setCdCep(resultSet.getString(8));
				
				listEndereco.add(endereco);	//Adiciona o novo Endereco para lista de Enderecos
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}	
	
	
	public void setQueryCidade(int idCidade) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			listEndereco = new LinkedList<Endereco>();
			query = "select * from tb_endereco where fk_id_cidade = " + idCidade + ";" ;
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){	
				endereco = new Endereco();								//popula o novo objeto Endereco
				endereco.setIdEndereco(resultSet.getInt(1));
				endereco.setIdCidade(resultSet.getInt(2));
				endereco.setDsLocal(resultSet.getString(3));
				endereco.setCdCoordenadaGeografica(resultSet.getString(4));
				endereco.setNmLocal(resultSet.getString(5));
				endereco.setNmRua(resultSet.getString(6));
				endereco.setNuComplemento(resultSet.getString(7));
				endereco.setCdCep(resultSet.getString(8));
				
				listEndereco.add(endereco);								//Adiciona o novo Endereco para lista de Enderecos
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	
	public List<Endereco> getList(){
		return listEndereco;
	}
	
	public Endereco findEnderecoById(int id){
		endereco = new Endereco();
		try {
			if (statement.isClosed())
				return endereco;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
		
			query = "select * from tb_endereco where id_endereco = " + id + ";" ;
			resultSet = statement.executeQuery(query);
			
			while(resultSet.next()){	
				endereco = new Endereco();								//popula o novo objeto Endereco
				endereco.setIdEndereco(resultSet.getInt(1));
				endereco.setIdCidade(resultSet.getInt(2));
				endereco.setDsLocal(resultSet.getString(3));
				endereco.setCdCoordenadaGeografica(resultSet.getString(4));
				endereco.setNmLocal(resultSet.getString(5));
				endereco.setNmRua(resultSet.getString(6));
				endereco.setNuComplemento(resultSet.getString(7));
				endereco.setCdCep(resultSet.getString(8));
				

			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return endereco;
	}
	
}
