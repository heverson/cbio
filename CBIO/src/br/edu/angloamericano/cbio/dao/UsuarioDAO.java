package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Usuario;

public class UsuarioDAO extends DAO{

	private Usuario usuario = new Usuario();
	private List<Usuario> listUsers = new LinkedList<Usuario>();

	public UsuarioDAO(){
		query = "SELECT * FROM Usuario";
		update = "UPDATE `Usuario` SET ";
		insert = "INSERT INTO `Usuario` (`Nome`, `Sobrenome`, `Cargo`, `Registro`, `Tipo`, `TelRes`, `TelCel`, `Email`, `Obs`, `bl_usuario`) VALUES ";

		initConnection();
	}

	/*
	 * Busca um usuário pelo seu ID, retorna o usuário se o mesmo existir
	 */
	public Usuario getUsuario(int id) throws Exception {
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query += " WHERE Usuario = " + id;
		resultSet = statement.executeQuery(query);
		if(resultSet.next()){
			usuario.setId(resultSet.getInt("Usuario"));
			usuario.setNome(resultSet.getString("Nome"));
			usuario.setSobrenome(resultSet.getString("Sobrenome"));
			usuario.setCargo(resultSet.getString("Cargo"));
			usuario.setRegistro(resultSet.getString("Registro"));
			usuario.setTipo(resultSet.getInt("Tipo"));
			usuario.setTelRes(resultSet.getString("TelRes"));
			usuario.setTelCel(resultSet.getString("TelCel"));
			usuario.setEmail(resultSet.getString("Email"));
			usuario.setObs(resultSet.getString("Obs"));
			usuario.setBlUsuario(resultSet.getInt("bl_usuario"));
		}
		return usuario;
	}

	public List<Usuario> userByIdTipo(int idTipo){
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Usuario WHERE Tipo = " + idTipo;
		try {
			resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				usuario = new Usuario(); 
				usuario.setId(resultSet.getInt("Usuario"));
				usuario.setNome(resultSet.getString("Nome"));
				usuario.setSobrenome(resultSet.getString("Sobrenome"));
				usuario.setCargo(resultSet.getString("Cargo"));
				usuario.setRegistro(resultSet.getString("Registro"));
				usuario.setTipo(resultSet.getInt("Tipo"));
				usuario.setTelRes(resultSet.getString("TelRes"));
				usuario.setTelCel(resultSet.getString("TelCel"));
				usuario.setEmail(resultSet.getString("Email"));
				usuario.setObs(resultSet.getString("Obs"));
				usuario.setBlUsuario(resultSet.getInt("bl_usuario"));
				listUsers.add(usuario);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listUsers;
	}

	/*
	 * Retorna uma lista de usuários com o mesmo nome
	 */
	public List<Usuario> userByFiltro(String nomeColuna, String info) throws SQLException{
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query = "SELECT * FROM Usuario WHERE `" + nomeColuna + "` = '" + info + "'";
		resultSet = statement.executeQuery(query);
		while(resultSet.next()){
			usuario = new Usuario(); 
			usuario.setId(resultSet.getInt("Usuario"));
			usuario.setNome(resultSet.getString("Nome"));
			usuario.setSobrenome(resultSet.getString("Sobrenome"));
			usuario.setCargo(resultSet.getString("Cargo"));
			usuario.setRegistro(resultSet.getString("Registro"));
			usuario.setTipo(resultSet.getInt("Tipo"));
			usuario.setTelRes(resultSet.getString("TelRes"));
			usuario.setTelCel(resultSet.getString("TelCel"));
			usuario.setEmail(resultSet.getString("Email"));
			usuario.setObs(resultSet.getString("Obs"));
			usuario.setBlUsuario(resultSet.getInt("bl_usuario"));
			listUsers.add(usuario);
		}
		return listUsers;
	}

	public Usuario userByRegistro(String reg) throws SQLException{
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		query += " WHERE Registro = '" + reg + "'";
		resultSet = statement.executeQuery(query);
		if(resultSet.next()){ 
			usuario.setId(resultSet.getInt("Usuario"));
			usuario.setNome(resultSet.getString("Nome"));
			usuario.setSobrenome(resultSet.getString("Sobrenome"));
			usuario.setCargo(resultSet.getString("Cargo"));
			usuario.setRegistro(resultSet.getString("Registro"));
			usuario.setTipo(resultSet.getInt("Tipo"));
			usuario.setTelRes(resultSet.getString("TelRes"));
			usuario.setTelCel(resultSet.getString("TelCel"));
			usuario.setEmail(resultSet.getString("Email"));
			usuario.setObs(resultSet.getString("Obs"));
			usuario.setBlUsuario(resultSet.getInt("bl_usuario"));
			return usuario;
		}
		else
			return null;
	}

	/**
	 * Pega o ultimo Id cadastrado na base de dados
	 * @return
	 * @throws SQLException
	 */
	public int getLastId() throws SQLException{
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		resultSet = statement.executeQuery(query);
		if(resultSet.next()){
			resultSet.last();
			return resultSet.getInt("Usuario");
		}
		else
			return 0;
	}

	/* Insere o novo usuário na base de dados */
	public void insert(Usuario d) {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		usuario = d;
		insert += "( '" + usuario.getNome() + "', '" + usuario.getSobrenome() + "', '"
				+ usuario.getCargo() + "', '" + usuario.getRegistro() + "', " + usuario.getTipo() 
				+ ", '" + usuario.getTelRes() + "', '" + usuario.getTelCel() + "', '"
				+ usuario.getEmail() + "', '" + usuario.getObs() + "', " + usuario.getBlUsuario() + ")";

		try {
			statement.executeUpdate(insert);
		} catch (SQLException e) {
			System.out.println("Erro ao cadastrar Animal\nErro gerado na classe AnimalDAO, metodo insert");
			e.printStackTrace();
		}
	}

	public List<Usuario> getAllUsers() throws SQLException{
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		resultSet = statement.executeQuery(query);
		while(resultSet.next()){
			usuario = new Usuario();
			usuario.setId(resultSet.getInt("Usuario"));
			usuario.setNome(resultSet.getString("Nome"));
			usuario.setSobrenome(resultSet.getString("Sobrenome"));
			usuario.setCargo(resultSet.getString("Cargo"));
			usuario.setRegistro(resultSet.getString("Registro"));
			usuario.setTipo(resultSet.getInt("Tipo"));
			usuario.setTelRes(resultSet.getString("TelRes"));
			usuario.setTelCel(resultSet.getString("TelCel"));
			usuario.setEmail(resultSet.getString("Email"));
			usuario.setObs(resultSet.getString("Obs"));
			usuario.setBlUsuario(resultSet.getInt("bl_usuario"));
			listUsers.add(usuario);
		}

		return listUsers;
	}

	@Override
	public void moveToRow(int row) {
		// TODO Auto-generated method stub

	}


	public void setUsuario(Usuario d) {
		// TODO Auto-generated method stub
		usuario = d;
	}

	@Override
	public Data getData() {
		// TODO Auto-generated method stub
		return (Data) usuario;
	}

	@Override
	public void setData(Data d) {
		// TODO Auto-generated method stub
		usuario = (Usuario) d;
	}

	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getColumn(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveDB() {
		// TODO Auto-generated method stub
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		update = "UPDATE `Usuario` SET `Nome` = '" + usuario.getNome() + "', `Sobrenome` = '"+ 
				usuario.getSobrenome()+ "', `Cargo` = '" + usuario.getCargo() + "', `Registro` = '" + 
				usuario.getRegistro()+ "', `Tipo` = " + usuario.getTipo()+ ", `TelRes` = '" +
				usuario.getTelRes()+ "', `TelCel` = '" + usuario.getTelCel() + "', `Email` = '"+ 
				usuario.getEmail()+ "', `Obs` = '" + usuario.getObs() + "', `bl_usuario` = 0 WHERE Usuario = " + usuario.getId();
		try {
			statement.executeUpdate(update);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}
	}	

	/*
	 * Método que muda o estatus de um usuario de acordo com o bl
	 * 1 - usuario inativo
	 * 0 - usuario ativo
	 */
	public void baixaDB(int bl){
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		update = "UPDATE `Usuario` SET `bl_usuario` = " + bl + " WHERE Usuario = " + usuario.getId();
		try {
			statement.executeUpdate(update);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}
	}
	@Override
	public void deleteDB() {
		// TODO Auto-generated method stub
	}
	@Override
	public void setQuery() {
		// TODO Auto-generated method stub

	}

	@Override
	public void insert(Data d) {
		// TODO Auto-generated method stub

	}
}
