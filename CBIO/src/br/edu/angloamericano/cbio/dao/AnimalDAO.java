/**
 * 
 */
package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Data;

/**
 * Classe DAO para controlar o acesso a tabela animal da base de dados
 * Todas as inserções, consultas e alterações nessa tabela, são feitas, necessáriamente, nessa classe
 * @author Thiago R. M. Bitencourt
 */
public class AnimalDAO extends DAO {

	private Animal animal;
	
	/**
	 * Construtor da Classe AnimalDAO.
	 */
	public AnimalDAO(){
		colName = "tb_animal";
		columns = "id_animal, Bl_animal, cd_tombo, nm_animal, nm_cientifico, ds_idade, nu_peso, ds_sinais, sg_sexo, ds_cor, ds_dados_biometricos, fk_id_especie, fk_id_coleta";
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id_animal=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = { "Identificador do Animal", "Código do Tombo", "Nome do Animal", "Nome Cientifico", "Idade", "Peso", "Sinais",
				        "Sexo", "Cor", "Dados Biométricos", "Identificador da Especie", "Identificador da Coleta" };
		columnsNames = cN;
		numColumns = 12;
		animal = new Animal();
		initConnection();
	}

	/**
	 * Função que percorre até a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		} try {
			resultSet.absolute(row);
			animal.setIdAnimal(resultSet.getInt(1));
			animal.setBlAnimal(resultSet.getInt(2));
			animal.setCdTombo(resultSet.getString(3));
			animal.setNmAnimal(resultSet.getString(4));
			animal.setNmCientifico(resultSet.getString(5));
			animal.setDsIdade(resultSet.getString(6));
		    animal.setNuPeso(resultSet.getFloat(7));
		    animal.setDsSinais(resultSet.getString(8));
		    animal.setSgSexo(resultSet.getString(9).charAt(0));
			animal.setDsCor(resultSet.getString(10));
			animal.setDsDadosBiometricos(resultSet.getString(11));
			animal.setIdEspecie(resultSet.getInt(12));
			animal.setIdColeta(resultSet.getInt(13));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	//nao implementada
	public Class<?> getColumnClassName(int column)
	{
		try
		{
			switch (column)
			{
				case 1:
				case 5:
					return Class.forName(Integer.class.getName());
				case 2:
				case 3:
				case 4:
					return Class.forName(String.class.getName());
			}
		}
		catch (ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		// se ocorrerem os problemas acima, assume tipo Object
		return Object.class;
	}

	/**
	 * Pega o atributo Animal da classe.
	 * @return Animal - Atributo da classe.
	 */
	public Animal getData()
	{
		return animal;
	}

	/**
	 * Seta os dados do animal de acordo com o dado passado.
	 * @param Data d - Animal que deseja preencher o atributo da classe.
	 */
	public void setData(Data d)
	{
		Animal a = (Animal) d;
		animal.setIdAnimal(a.getIdAnimal());
		animal.setBlAnimal(a.getBlAnimal());
		animal.setCdTombo(a.getCdTombo());
		animal.setNmAnimal(a.getNmAnimal());
		animal.setNmCientifico(a.getNmCientifico());
		animal.setDsIdade(a.getDsIdade());
	    animal.setNuPeso(a.getNuPeso());
	    animal.setDsSinais(a.getDsSinais());
	    animal.setSgSexo(a.getSgSexo());
		animal.setDsCor(a.getDsCor());
		animal.setDsDadosBiometricos(a.getDsDadosBiometricos());
		animal.setIdEspecie(a.getIdEspecie());
		animal.setIdColeta(a.getIdColeta());
	}

	/**
	 * Função que obtém coluna do atributo da classe.
	 * @param int column - Número da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column)
	{
		switch (column)
		{
			case 1:
				return animal.getIdAnimal();
			case 2: 
				return animal.getBlAnimal();
			case 3:
				return animal.getCdTombo();
			case 4:
				return animal.getNmAnimal();
			case 5:
				return animal.getNmCientifico();
			case 6:
				return animal.getDsIdade();
			case 7:
				return animal.getNuPeso();
			case 8:
				return animal.getDsSinais();
			case 9:
				return animal.getSgSexo();
			case 10:
				return animal.getDsCor();
			case 11:
				return animal.getDsDadosBiometricos();
			case 12:
				return animal.getIdEspecie();
			case 13:
				return animal.getIdColeta();
		}
		return null;
	}

	/**
	 * Função que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		String newData = "update tb_animal set " 
				+"cd_tombo = '" + animal.getCdTombo() + "', "
				+"bl_animal = '" + animal.getBlAnimal() + "', "
				+"nm_animal = '" + animal.getNmAnimal() + "', "
				+"nm_cientifico = '" + animal.getNmCientifico() + "', " 
				+"ds_idade = '" + animal.getDsIdade() + "',"
				+"nu_peso = "+ animal.getNuPeso() + "," 
				+"sg_sexo = '" + animal.getSgSexo() +"',"
				+"ds_cor = '"+ animal.getDsCor() + "', "
				+"ds_dados_biometricos = '"+ animal.getDsDadosBiometricos() + "' " 
				+"where id_animal =" + animal.getIdAnimal() + ";";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Função que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB()
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = "DELETE FROM tb_animal where id_animal="+animal.getIdAnimal()+" ;";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}
	
	public void BaixaDB(int idAnimal){
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String up = "UPDATE `tb_animal` SET Bl_Animal = 1 WHERE `ID_ANIMAL`= " + idAnimal;
//		String up = "UPDATE `tb_animal` SET Bl_Animal = " + idBaixa + " WHERE `ID_ANIMAL`= " + idAnimal;
		try {
			statement.executeUpdate(up);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função que executa a query de insert(inserção) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d)
	{
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		//setData(d);
		Animal a = (Animal) d;
		String newData = "insert into tb_animal (bl_animal, cd_tombo , nm_animal , nm_cientifico, ds_idade , nu_peso , ds_sinais , sg_sexo , ds_cor , " +
					"ds_dados_biometricos , fk_id_especie , fk_id_coleta) values ('"
						+ a.getBlAnimal() + "', '"
						+ a.getCdTombo() + "', '" + a.getNmAnimal() + "', '"
						+ a.getNmCientifico() + "', '" + a.getDsIdade() + "', "
						+ a.getNuPeso() + ", '" + a.getDsSinais() + "','" + a.getSgSexo() +"','"  +a.getDsCor() + "', '"
						+ a.getDsDadosBiometricos() + "', " + a.getIdEspecie() 
						+ ", " + a.getIdColeta() + ");";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			System.out.println("Erro ao cadastrar Animal\nErro gerado na classe AnimalDAO, metodo insert");
			e.printStackTrace();
		}
		setQuery();
	}
	
	public List<Animal> getListAnimal(){
		List<Animal> listAnimal = new LinkedList<Animal>(); //cria uma lista de animais
		try{
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_animal"; //monta query inicial para consultar na tabela de animais
			resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				Animal animalAux = new Animal();
				animalAux.setIdAnimal(resultSet.getInt(1));
				animalAux.setBlAnimal(resultSet.getInt(2));
				animalAux.setCdTombo(resultSet.getString(3));
				animalAux.setNmAnimal(resultSet.getString(4));
				animalAux.setNmCientifico(resultSet.getString(5));
				animalAux.setDsIdade(resultSet.getString(6));
				animalAux.setNuPeso(resultSet.getFloat(7));
				animalAux.setDsSinais(resultSet.getString(8));
				animalAux.setSgSexo(resultSet.getString(9).charAt(0));
				animalAux.setDsCor(resultSet.getString(10));
				animalAux.setDsDadosBiometricos(resultSet.getString(11));
				animalAux.setIdEspecie(resultSet.getInt(12));
				animalAux.setIdColeta(resultSet.getInt(13));
				listAnimal.add(animalAux);
				animalAux=null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO " + e);
		}		
		return listAnimal;
	}
	

	/**
	 * Utilizado para buscar animal através de filtros
	 * Função de consulta para animais que monta a query a partir dos dados passados pelo animal
	 * @param a - Objeto animal com os dados para realizar consulta
	 * @return List<Animal> - Retorna uma lista encadeada com os animais encontrados no banco
	 */
	public List<Animal> obterListaAnimais(Animal a) {
		List<Animal> listAnimal = new LinkedList<Animal>(); //cria uma lista de animais
		try{
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_animal"; //monta query inicial para consultar na tabela de animais
			String where = new String(); //cria uma string para montar as condiçoes do where na busca
			where = " where 1=1 ";
			
			if(a != null) //se existir um animal, chama função que monta a clausa where com os dados desejados
				where = where+preencheWhere(a);
			
			query = query+where+";"; //monsta a query
			resultSet = statement.executeQuery(query); // especifica consulta e a executa
			
			/* Enquanto houver linhas no bd faz a leitura criando uma lista de objetos das linhas
			   existentes no bd e preenche a lista de animais */
			while(resultSet.next()){
				Animal animalAux = new Animal();
				animalAux.setIdAnimal(resultSet.getInt(1));
				animalAux.setBlAnimal(resultSet.getInt(2));
				animalAux.setCdTombo(resultSet.getString(3));
				animalAux.setNmAnimal(resultSet.getString(4));
				animalAux.setNmCientifico(resultSet.getString(5));
				animalAux.setDsIdade(resultSet.getString(6));
				animalAux.setNuPeso(resultSet.getFloat(7));
				animalAux.setDsSinais(resultSet.getString(8));
				animalAux.setSgSexo(resultSet.getString(9).charAt(0));
				animalAux.setDsCor(resultSet.getString(10));
				animalAux.setDsDadosBiometricos(resultSet.getString(11));
				animalAux.setIdEspecie(resultSet.getInt(12));
				animalAux.setIdColeta(resultSet.getInt(13));
				listAnimal.add(animalAux);
				animalAux=null;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return listAnimal;
	}

	/**
	 * Função auxiliar que preenche a query de consulta verificando se existem condições 
	 * que necessitem do adicionar no where.
	 * @param Animal a - Objeto que será verificado se possui dados específicos para consulta.
	 * @return String - Retorna uma String where preenchida para ser adicionada na query de consulta.
	 */
	private String preencheWhere(Animal a){
		String where = new String();
		
		if (a.getIdAnimal() > 0)
			where = where+" AND id_animal="+a.getIdAnimal();
		if (a.getCdTombo() != null && a.getCdTombo().trim() != "") //se tem codigo tombo adiciona no where
			where = where+" AND cd_tombo = '"+a.getCdTombo()+"' ";
		if (a.getNmAnimal() != null && a.getNmAnimal().trim() != "") //se tem nome do animal adiciona no where
			where = where+" AND nm_animal LIKE '%"+a.getNmAnimal()+"%' ";
		if (a.getNmCientifico() != null && a.getNmCientifico().trim() != "") //complementa where com nome cientifico
			where = where+" AND nm_cientifico LIKE '%"+a.getNmCientifico()+"%'";
		if (a.getDsIdade() != null && a.getDsIdade().trim() != "")
			where = where+" AND ds_idade = '"+a.getDsIdade()+"'";
		if (a.getNuPeso() > 0)
			where = where+" AND nu_peso="+a.getNuPeso();
		if (a.getSgSexo() != ' ' && ( a.getSgSexo() == 'M' || a.getSgSexo() == 'F' ) )
			where = where+" AND sg_sexo = '"+a.getSgSexo()+"'";
		if (a.getIdEspecie() > 0)
			where = where+" AND fk_id_especie="+a.getIdEspecie();

		return where;
	}
	
	/**
	 * Função apenas de auxilio para criação das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obtém metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o número de linhas em ResultSet
			resultSet.last(); // move para a última linha
			numberOfRows = resultSet.getRow(); // obtêm número de linha
			resultSet.first(); //volta para a primeira linha

			if (numberOfRows != 0) {
				animal.setIdAnimal(resultSet.getInt(1));
				animal.setBlAnimal(resultSet.getInt(2));
				animal.setCdTombo(resultSet.getString(3));
				animal.setNmAnimal(resultSet.getString(4));
				animal.setNmCientifico(resultSet.getString(5));
				animal.setDsIdade(resultSet.getString(6));
				animal.setNuPeso(resultSet.getFloat(7));
				animal.setDsSinais(resultSet.getString(8));
				animal.setSgSexo(resultSet.getString(9).charAt(0));
				animal.setDsCor(resultSet.getString(10));
				animal.setDsDadosBiometricos(resultSet.getString(11));
				animal.setIdEspecie(resultSet.getInt(12));
				animal.setIdColeta(resultSet.getInt(13));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public int getIdAnimal(String tombo, String nmClasse){
		int n = -1;
		try {
			if (statement.isClosed())
				return -1;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from ("
					+" select an.cd_tombo, an.id_animal from tb_animal as an, tb_especie, tb_tribo, tb_familia,tb_ordem, tb_classe"
						+ " where an.fk_id_especie = tb_especie.id_especie"
						+ " and tb_especie.fk_id_tribo = tb_tribo.id_tribo"
						+ " and tb_tribo.fk_id_familia = tb_familia.id_familia"
						+ " and tb_familia.fk_id_ordem = tb_ordem.id_ordem"
						+ " and tb_ordem.fk_id_classe = tb_classe.id_classe and tb_classe.nm_classe ='"
						+  nmClasse	+ "') as a"
						+ " where a.cd_tombo = '"+tombo +"'; ";
			
						resultSet = statement.executeQuery(query);
		
				if(resultSet.next())
					n= resultSet.getInt(2);		//a consulta acima retorna o tombo e id_animal este return retorna o id_animal da consulta...consulta so tem 1 retorno por estar consultando o tombo que � unico
				else return n;
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		return n;
}
	
	/**
	 * funcao verifica se o codigo tombo ja esta sendo utilizado 
	 * @param tombo
	 * @return devolve true se o tombo especificado ja esta sendo utilizado por algum animal
	 */
	public boolean isTomboDuplicate(String tombo, String nmClasse){
		boolean isDup = true;
		try {
			if (statement.isClosed())
				return isDup;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from ("
					+" select an.cd_tombo from tb_animal as an, tb_especie, tb_tribo, tb_familia,tb_ordem, tb_classe"
						+ " where an.fk_id_especie = tb_especie.id_especie"
						+ " and tb_especie.fk_id_tribo = tb_tribo.id_tribo"
						+ " and tb_tribo.fk_id_familia = tb_familia.id_familia"
						+ " and tb_familia.fk_id_ordem = tb_ordem.id_ordem"
						+ " and tb_ordem.fk_id_classe = tb_classe.id_classe and tb_classe.nm_classe ='"
						+  nmClasse	+ "') as a"
						+ " where a.cd_tombo = '"+tombo +"'; ";
			
						resultSet = statement.executeQuery(query);
		
				if(resultSet.next())
					isDup = true;
				else isDup = false;
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		return isDup;
	}
	
	public int getIdByTombo(String tombo){
		int r = 0;
		try {
			if (statement.isClosed())
				return 0;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_animal where cd_tombo='"+tombo+"' ;";
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				r = resultSet.getInt(1);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return r;

	}
	
	/**
	 * Busca um animal somente pelo codigo tombo
	 * @param tombo - Codigo tombo desejado
	 * @return Animal - Animal buscado.
	 */
	public Animal findAnimalByTombo(String tombo){
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_animal where cd_tombo='"+tombo+"' ;";
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				animal.setIdAnimal(resultSet.getInt(1));
				animal.setBlAnimal(resultSet.getInt(2));
				animal.setCdTombo(resultSet.getString(3));
				animal.setNmAnimal(resultSet.getString(4));
				animal.setNmCientifico(resultSet.getString(5));
				animal.setDsIdade(resultSet.getString(6));
				animal.setNuPeso(resultSet.getFloat(7));
				animal.setDsSinais(resultSet.getString(8));
				animal.setSgSexo(resultSet.getString(9).charAt(0));
				animal.setDsCor(resultSet.getString(10));
				animal.setDsDadosBiometricos(resultSet.getString(11));
				animal.setIdEspecie(resultSet.getInt(12));
				animal.setIdColeta(resultSet.getInt(13));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return animal;
	}

	/**
	 * Função que busca animal pelo id do animal
	 * @param idAnimal - id do animal que está buscando
	 * @return Animal - retorna o animal com o id passado
	 */
	public Animal findAnimalByIdAnimal(int idAnimal){
		try {
			if (statement.isClosed())
				return null;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			query = "select * from tb_animal where id_animal="+idAnimal+" ;";
			resultSet = statement.executeQuery(query);
			if(resultSet.next()){
				animal.setIdAnimal(resultSet.getInt(1));
				animal.setBlAnimal(resultSet.getInt(2));
				animal.setCdTombo(resultSet.getString(3));
				animal.setNmAnimal(resultSet.getString(4));
				animal.setNmCientifico(resultSet.getString(5));
				animal.setDsIdade(resultSet.getString(6));
				animal.setNuPeso(resultSet.getFloat(7));
				animal.setDsSinais(resultSet.getString(8));
				animal.setSgSexo(resultSet.getString(9).charAt(0));
				animal.setDsCor(resultSet.getString(10));
				animal.setDsDadosBiometricos(resultSet.getString(11));
				animal.setIdEspecie(resultSet.getInt(12));
				animal.setIdColeta(resultSet.getInt(13));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return animal;
	}
	
}