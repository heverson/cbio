package br.edu.angloamericano.cbio.dao;

import java.sql.SQLException;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Mamifero;

public class MamiferosDAO extends DAO {
	
	private Mamifero mamifero;
	
	/**
	 * Construtor da Classe MamiferoDAO.
	 */
	public MamiferosDAO(){
		colName = "tb_classe_mamiferos";
		columns = "id_classe_mamiferos, nu_comprimento_total_corpo, nu_comprimento_total_calda, nu_comprimento_total_cabeca," +
				" nu_largura_cabeca, nu_largura_toraxica, nu_largura_base_orelha, nu_comprimento_membro_posterior," +
				" nu_total_dentes, nu_caninos_superior_direito, nu_caninos_inferior_direito, nu_incisivos_superior_direito," +
				" nu_incisivos_inferior_direito, nu_premolares_superior_direito, nu_premolares_inferior_direito," +
				" nu_molares_superior_direito, nu_molares_inferior_direito, fk_id_Animal"; 
		query = "SELECT * FROM " + colName;
		update = "UPDATE (" + columns + ") from " + colName + " with ";
		delete = "DELETE from " + colName + " where id=";
		insert = "INSERT INTO " + colName + "(" + columns + ")" + " values ";
		String[] cN = {"Identificador Mamifero" , "Comprimento Total Corpo", "Comprimento Total Calda", "Comprimento Total Cabe�a",
						"Largura Cabe�a", "Largura Toraxica", "Largura Base Orelha", "Comprimento Membro Posterios",
						"Total Dentes", "Caninos Superior Direito", "Caninos Inferior Direito", "Incisivos Superior Direito",
						"Incisivos Inferior Direito", "Premolares Superior Direito", "Premolares Inferior Direito",
						"Molares Superior Direito", "Molares Inferior Direito", "Identificador Animal"};
	columnsNames = cN;
	numColumns = 18;
	mamifero = new Mamifero();
	initConnection();
	}
	
	/**
	 * Fun��o que percorre at� a linha desejada e retorna o objeto preenchido
	 * com os dados da linha no atributo da classe.
	 * @param int row - linha que deseja os dados.
	 */
	public void moveToRow(int row) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			resultSet.absolute(row);
			mamifero.setId_classe_mamiferos(resultSet.getInt(1));
			mamifero.setNu_comprimento_total_corpo(resultSet.getFloat(2));
			mamifero.setNu_comprimento_total_calda(resultSet.getFloat(3));
			mamifero.setNu_comprimento_total_cabeca(resultSet.getFloat(4));
			mamifero.setNu_largura_cabeca(resultSet.getFloat(5));
			mamifero.setNu_largura_toraxica(resultSet.getFloat(6));
			mamifero.setNu_largura_base_orelha(resultSet.getFloat(7));
			mamifero.setNu_comprimento_membro_posterior(resultSet.getFloat(8));
			mamifero.setNu_total_dentes(resultSet.getInt(9));
			mamifero.setNu_caninos_superior_direito(resultSet.getInt(10));
			mamifero.setNu_caninos_inferior_direito(resultSet.getInt(11));
			mamifero.setNu_incisivos_superior_direito(resultSet.getInt(12));
			mamifero.setNu_incisivos_inferior_direito(resultSet.getInt(13));
			mamifero.setNu_premolares_superior_direito(resultSet.getInt(14));
			mamifero.setNu_premolares_inferior_direito(resultSet.getInt(15));
			mamifero.setNu_molares_superior_direito(resultSet.getInt(16));
			mamifero.setNu_molares_inferior_direito(resultSet.getInt(17));
			mamifero.setIdAnimal(resultSet.getInt(18));
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public Data getData() {
		return mamifero;
	}

	public void setData(Data d) {
		Mamifero m = (Mamifero) d;
		mamifero.setId_classe_mamiferos(m.getId_classe_mamiferos());
		mamifero.setNu_comprimento_total_corpo(m.getNu_comprimento_total_corpo());
		mamifero.setNu_comprimento_total_calda(m.getNu_comprimento_total_calda());
		mamifero.setNu_comprimento_total_cabeca(m.getNu_comprimento_total_cabeca());
		mamifero.setNu_largura_cabeca(m.getNu_largura_cabeca());
		mamifero.setNu_largura_toraxica(m.getNu_largura_toraxica());
		mamifero.setNu_largura_base_orelha(m.getNu_largura_base_orelha());
		mamifero.setNu_comprimento_membro_posterior(m.getNu_comprimento_membro_posterior());
		mamifero.setNu_total_dentes(m.getNu_total_dentes());
		mamifero.setNu_caninos_superior_direito(m.getNu_caninos_superior_direito());
		mamifero.setNu_caninos_inferior_direito(m.getNu_caninos_inferior_direito());
		mamifero.setNu_incisivos_superior_direito(m.getNu_incisivos_superior_direito());
		mamifero.setNu_incisivos_inferior_direito(m.getNu_incisivos_inferior_direito());
		mamifero.setNu_premolares_superior_direito(m.getNu_premolares_superior_direito());
		mamifero.setNu_premolares_inferior_direito(m.getNu_premolares_inferior_direito());
		mamifero.setNu_molares_superior_direito(m.getNu_molares_superior_direito());
		mamifero.setNu_molares_inferior_direito(m.getNu_molares_inferior_direito());
		mamifero.setIdAnimal(m.getIdAnimal());
	}

	//nao implementada
	@Override
	public Class<?> getColumnClassName(int column) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Fun��o que obt�m coluna do atributo da classe.
	 * @param int column - N�mero da coluna desejada.
	 * @return Object - Retorna o dado que possui na coluna do atributo desejado.
	 */
	public Object getColumn(int column) {
		switch (column)
		{
			case 1:
				return mamifero.getId_classe_mamiferos();
			case 2:
				return mamifero.getNu_comprimento_total_corpo();
			case 3:
				return mamifero.getNu_comprimento_total_calda();
			case 4:
				return mamifero.getNu_comprimento_total_cabeca();
			case 5:
				return mamifero.getNu_largura_cabeca();
			case 6:
				return mamifero.getNu_largura_toraxica();
			case 7:
				return mamifero.getNu_largura_base_orelha();
			case 8:
				return mamifero.getNu_comprimento_membro_posterior();
			case 9:
				return mamifero.getNu_total_dentes();
			case 10:
				return mamifero.getNu_caninos_superior_direito();
			case 11:
				return mamifero.getNu_caninos_inferior_direito();
			case 12:
				return mamifero.getNu_incisivos_superior_direito();
			case 13:
				return mamifero.getNu_incisivos_inferior_direito();	
			case 14:
				return mamifero.getNu_premolares_superior_direito();
			case 15:
				return mamifero.getNu_premolares_inferior_direito();
			case 16:
				return mamifero.getNu_molares_superior_direito();
			case 17:
				return mamifero.getNu_molares_inferior_direito();
			case 18:
				return mamifero.getIdAnimal();
		}
		return null;
	}

	/**
	 * Fun��o que executa a query de upDate no DB a partir dos dados do atributo da classe.
	 */
	public void saveDB() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String newData ="update tb_classe_mamiferos " +
				"set nu_comprimento_total_corpo = " + mamifero.getNu_comprimento_total_corpo() +
				", nu_comprimento_total_calda = " + mamifero.getNu_comprimento_total_calda() +
				", nu_comprimento_total_cabeca =" + mamifero.getNu_comprimento_total_cabeca() + 
				",nu_largura_cabeca = " + mamifero.getNu_largura_cabeca() +
				", nu_largura_toraxica = " + mamifero.getNu_largura_toraxica() + 
				", nu_largura_base_orelha = " + mamifero.getNu_largura_base_orelha() + 
				", nu_comprimento_membro_posterior = " +  mamifero.getNu_comprimento_membro_posterior() + 
				",nu_total_dentes = " +mamifero.getNu_total_dentes() +
				", nu_caninos_superior_direito = " + mamifero.getNu_caninos_superior_direito() +
				", nu_caninos_inferior_direito =" + mamifero.getNu_caninos_inferior_direito() +
				", nu_incisivos_superior_direito =" + mamifero.getNu_incisivos_superior_direito() + 
				",nu_incisivos_inferior_direito =" + mamifero.getNu_incisivos_inferior_direito() + 
				", nu_premolares_superior_direito =" +  mamifero.getNu_premolares_superior_direito() +
				", nu_premolares_inferior_direito =" + mamifero.getNu_premolares_inferior_direito() +
				",nu_molares_superior_direito =" +  mamifero.getNu_molares_superior_direito() + 
				", nu_molares_inferior_direito = " + mamifero.getNu_molares_inferior_direito() + 
				" where id_classe_mamiferos = "+ mamifero.getId_classe_mamiferos() + ";";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	/**
	 * Fun��o que executa a query de delete no DB a partir dos dados do atributo da classe.
	 */
	public void deleteDB() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		String del = delete + mamifero.getId_classe_mamiferos() + ";";
		try {
			statement.executeUpdate(del);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Função que executa a query de insert(inserção) no DB a partir dos dados do atributo da classe.
	 */
	public void insert(Data d) {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Mamifero m = (Mamifero) d;
		this.setData(m);
		String newData = "insert into tb_classe_mamiferos (nu_comprimento_total_corpo, nu_comprimento_total_calda, nu_comprimento_total_cabeca," +
				" nu_largura_cabeca, nu_largura_toraxica, nu_largura_base_orelha, nu_comprimento_membro_posterior," +
				" nu_total_dentes, nu_caninos_superior_direito, nu_caninos_inferior_direito, nu_incisivos_superior_direito," +
				" nu_incisivos_inferior_direito, nu_premolares_superior_direito, nu_premolares_inferior_direito," +
				" nu_molares_superior_direito, nu_molares_inferior_direito, tb_animal_id_Animal ) values ("
					+ mamifero.getNu_comprimento_total_corpo() + ", " 
					+ mamifero.getNu_comprimento_total_calda() + ", "
					+ mamifero.getNu_comprimento_total_cabeca() + ", " 
					+ mamifero.getNu_largura_cabeca() + ", "
					+ mamifero.getNu_largura_toraxica() + ", " 
					+ mamifero.getNu_largura_base_orelha() + ", "
					+ mamifero.getNu_comprimento_membro_posterior() + ", " 
					+ mamifero.getNu_total_dentes()+ ", "
					+ mamifero.getNu_caninos_superior_direito() + ", " 
					+ mamifero.getNu_caninos_inferior_direito() + ", "
					+ mamifero.getNu_incisivos_superior_direito() + ", " 
					+ mamifero.getNu_incisivos_inferior_direito() + ", "
					+ mamifero.getNu_premolares_inferior_direito() + ", "
					+ mamifero.getNu_premolares_inferior_direito() + ", "
					+ mamifero.getNu_molares_superior_direito() + ", " 
					+ mamifero.getNu_molares_inferior_direito()+ ", "
					+ mamifero.getIdAnimal() + ");";
		try {
			statement.executeUpdate(newData);
		} catch (SQLException e) {
			System.out.println("Erro ao cadastrar Mamífero\nErro gerado na classe MamiferoDAO, metodo insert");
			e.printStackTrace();
		}
		setQuery();
	}

	/**
	 * Fun��o apenas de auxilio para cria��o das queries.
	 */
	public void setQuery() {
		try {
			if (statement.isClosed())
				return;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			// especifica consulta e a executa
			resultSet = statement.executeQuery(query);

			// obt�m metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o n�mero de linhas em ResultSet
			resultSet.last(); // move para a �ltima linha
			numberOfRows = resultSet.getRow(); // obt�m n�mero de linha
			resultSet.first();

			if (numberOfRows != 0) {
				mamifero.setId_classe_mamiferos(resultSet.getInt(1));
				mamifero.setNu_comprimento_total_corpo(resultSet.getFloat(2));
				mamifero.setNu_comprimento_total_calda(resultSet.getFloat(3));
				mamifero.setNu_comprimento_total_cabeca(resultSet.getFloat(4));
				mamifero.setNu_largura_cabeca(resultSet.getFloat(5));
				mamifero.setNu_largura_toraxica(resultSet.getFloat(6));
				mamifero.setNu_largura_base_orelha(resultSet.getFloat(7));
				mamifero.setNu_comprimento_membro_posterior(resultSet.getFloat(8));
				mamifero.setNu_total_dentes(resultSet.getInt(9));
				mamifero.setNu_caninos_superior_direito(resultSet.getInt(10));
				mamifero.setNu_caninos_inferior_direito(resultSet.getInt(11));
				mamifero.setNu_incisivos_superior_direito(resultSet.getInt(12));
				mamifero.setNu_incisivos_inferior_direito(resultSet.getInt(13));
				mamifero.setNu_premolares_superior_direito(resultSet.getInt(14));
				mamifero.setNu_premolares_inferior_direito(resultSet.getInt(15));
				mamifero.setNu_molares_superior_direito(resultSet.getInt(16));
				mamifero.setNu_molares_inferior_direito(resultSet.getInt(17));
				mamifero.setIdAnimal(resultSet.getInt(18));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public Mamifero findMamiferoByIDAnimal(int idAnimal){
		Mamifero mamifero = new Mamifero();
		try {
			if (statement.isClosed())
				return mamifero;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			query = "select * from tb_classe_mamiferos where tb_animal_id_animal = " + idAnimal + ";" ;
			resultSet = statement.executeQuery(query);

			// obt�m metadados para ResultSet
			// metaData = resultSet.getMetaData();

			// determina o n�mero de linhas em ResultSet
			resultSet.last(); // move para a �ltima linha
			numberOfRows = resultSet.getRow(); // obt�m n�mero de linha
			resultSet.first();

			if (numberOfRows != 0) {
				mamifero.setId_classe_mamiferos(resultSet.getInt(1));
				mamifero.setNu_comprimento_total_corpo(resultSet.getFloat(2));
				mamifero.setNu_comprimento_total_calda(resultSet.getFloat(3));
				mamifero.setNu_comprimento_total_cabeca(resultSet.getFloat(4));
				mamifero.setNu_largura_cabeca(resultSet.getFloat(5));
				mamifero.setNu_largura_toraxica(resultSet.getFloat(6));
				mamifero.setNu_largura_base_orelha(resultSet.getFloat(7));
				mamifero.setNu_comprimento_membro_posterior(resultSet.getFloat(8));
				mamifero.setNu_total_dentes(resultSet.getInt(9));
				mamifero.setNu_caninos_superior_direito(resultSet.getInt(10));
				mamifero.setNu_caninos_inferior_direito(resultSet.getInt(11));
				mamifero.setNu_incisivos_superior_direito(resultSet.getInt(12));
				mamifero.setNu_incisivos_inferior_direito(resultSet.getInt(13));
				mamifero.setNu_premolares_superior_direito(resultSet.getInt(14));
				mamifero.setNu_premolares_inferior_direito(resultSet.getInt(15));
				mamifero.setNu_molares_superior_direito(resultSet.getInt(16));
				mamifero.setNu_molares_inferior_direito(resultSet.getInt(17));
				mamifero.setIdAnimal(resultSet.getInt(18));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		return mamifero;
		
	}

}
