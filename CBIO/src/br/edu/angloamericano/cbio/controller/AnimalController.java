package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.EspecieDAO;
import br.edu.angloamericano.cbio.gui.AnimalGUI;

/**
 * 
 * @author Thiago R. M. Bitencourt
 *
 */
public class AnimalController {
	private String[] colPrincipal = {"Código Tombo", "Nome", "Nome Científico", "Espécie"}; //modelo para colunas
	private AnimalGUI gui;
	private List<Animal> listAnimal = new LinkedList<Animal>();
	private Animal a = new Animal();
	private String Tombo = "";

	/**
	 * Construtor da classe.
	 * @param Super - Janela 'Pai'
	 * @param flag - Define quem instanciou a classe.
	 */
	public AnimalController(JFrame Super, int flag){
		gui = new AnimalGUI(Super, flag);
		configPermissao();
		gui.addOuvintes(new Ouvinte());
		gui.setModal(true);
		gui.setVisible(true);
	}

	/**
	 * Verifica se o usuário logado é administrador, se não for restringe a funcionalidade de alterar e remover Animal
	 */
	private void configPermissao(){
		if(!App.getTipoUser().equals("Administrador")){
			gui.btnAlterar.setEnabled(false);
			//			gui.btnRemover.setEnabled(false);
		}
		else{
			gui.btnAlterar.setEnabled(true);
		}
	}

	/**
	 * Classe interna responsável por controlar as ações da interface
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			if(ev.getSource() == gui.btnAdicionar)
			{
				new CadastroAnimalController(null, gui);
			}
			if(ev.getSource() == gui.btnAlterar)
			{
				alterarAnimal();
			}
			if(ev.getSource() == gui.btnRemover)
			{	
				//				JOptionPane.showMessageDialog(gui, "Exibe o processo realizano no animal - Não implementados");
				//				verificaDeletaAnimal();
				processoAnimal();
			}
			if(ev.getSource() == gui.btnConsultar)
			{
				if(gui.chckbxAnimaisIndisponveis.isSelected()){
					gui.btnAlterar.setEnabled(false);
				}
				else
					configPermissao();
				consultarAllAnimal();
			}
			// Define Filtros de Busca para um animal
			if(ev.getSource() == gui.btnFiltros)
			{
				AnimalFiltrosController filtro = new AnimalFiltrosController(gui);
				if(filtro.getAnimalFiltro() != null)
					consultarAnimalFiltro(filtro.getAnimalFiltro());
			}
			// Busca por um número de tombo específico
			if(ev.getSource() == gui.btnBuscaTombo)
			{
				if(gui.chckbxAnimaisIndisponveis.isSelected())
					gui.btnAlterar.setEnabled(false);
				else
					configPermissao();
				consultarByTombo();
			}

			// Botão acionado apenas quando a chamada é feita pela classe PecaController
			if(ev.getSource() == gui.btnUsar){
				try{
					if( gui.tabela.isBackgroundSet() ){// se tiver alguma linha selecionada na tabela consultada
						int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
						String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 0); //pega o codigo tombo
						setTombo(cdTomboSelecionado);
						gui.dispose();
					}
					else{
						JOptionPane.showMessageDialog(null, "Selecione um Animal");
					}
				} catch (ArrayIndexOutOfBoundsException e){
				}
			}
		}
	}

	/**
	 * Consulta um animal pelo seu número tombo.
	 */
	private void consultarByTombo(){
		AnimalDAO aDAO = new AnimalDAO();
		listAnimal = new LinkedList<Animal>();
		if(!gui.txtClasse.getText().equals("") && !gui.txtNuTombo.getText().equals("")){
			String tombo = gui.lblCCBSZooFaa.getText() + gui.txtClasse.getText() + gui.txtNuTombo.getText();
			listAnimal.add(aDAO.findAnimalByTombo(tombo));

			if(gui.chckbxAnimaisIndisponveis.isSelected())
				MontaTabela(listAnimal, 1);
			else
				MontaTabela(listAnimal, 0);
		}
		else{
			JOptionPane.showMessageDialog(gui, "Código Tombo Imcompleto");
		}
	}

	/**
	 * Função que define se serão mostradas os animais disponíveis ou não disponíveis
	 */
	private void consultarAllAnimal(){
		AnimalDAO aDAO = new AnimalDAO();
		listAnimal = new LinkedList<Animal>();
		listAnimal = aDAO.getListAnimal(); // Lista com todos os animais cadastrados
		if(gui.chckbxAnimaisIndisponveis.isSelected())
			MontaTabela(listAnimal, 1);
		else
			MontaTabela(listAnimal, 0);
	}

	/**
	 * Consulta um animal de acordo com os filtros adicionados
	 */
	public void consultarAnimalFiltro (Animal a){
		AnimalDAO aDAO = new AnimalDAO();
		listAnimal = aDAO.obterListaAnimais(a); //obtem a lista de animal de acordo com os parametros do animal
		if(gui.chckbxAnimaisIndisponveis.isSelected())
			MontaTabela(listAnimal, 1);
		else
			MontaTabela(listAnimal, 0);
	}

	/**
	 * Método que cria a tabela de animais a serem exibidos
	 * @param listAnimal - Lista com as peças cadastradas
	 * @param flag - mostrar peças disponíveis e não disponíveis
	 */
	private void MontaTabela(List<Animal> listAnimal, int flag){
		String[][] dadosAnimal = null; //linhas de animais como nula
		String[][] dadosAnimalAux = null;
		Iterator<Animal> it = listAnimal.iterator(); //cria iterator para percorrer a lista

		if( listAnimal.size() > 0 ){ //verifica se foram encontradas animais
			dadosAnimalAux = new String[listAnimal.size()][4]; //cria o numero de linhas da lista
			int count = 0; //contador das linhas
			EspecieDAO eDAO = new EspecieDAO();
			while( it.hasNext() ){ //enquanto tiver linha
				a = new Animal();
				a = (Animal) it.next(); //pega o animal
				if(a.getBlAnimal() == flag ){//verifica se é um animal disponível ou não, mostra os animais de acordo com a flag recebida como parâmetro
					Especie esp = eDAO.findEspecieById(a.getIdEspecie());
					dadosAnimalAux[count][0] = a.getCdTombo(); //seta o codigo tombo na tabela
					dadosAnimalAux[count][1] = a.getNmAnimal(); //seta o nome do animal
					dadosAnimalAux[count][2] = a.getNmCientifico(); //seta nome científico do animal
					dadosAnimalAux[count][3] = esp.getNmEspecie(); //seta o nome da espécie do animal na tabela						
					count++;
				}
			}
			/*
			 * Cria a tabela a ser mostrada. Se mostrar a tabela auxiliar (montada acima) é exibido 
			 * linhas em branco, já que a tabela criada é do tamanho da lista de animais na base de dados, mas como não são mostrados todos os animais
			 * e sim apenas os animais disponíveis ou apenas os não disponíveis (Dependendo da flag recebida). Por isso é necessário criar uma tabela
			 * com o tamanho exato de animais a serem mostrados. O número de linhas necessárias está na variavel count, já que, quando um animal deve ser mostrado
			 * a variavel count é incrementada.
			 */
			if(count != 0){
				dadosAnimal = new String[count][4];
				for(int i = 0; i < dadosAnimal.length; i++){
					dadosAnimal[i] = dadosAnimalAux[i];
				}
			}
		}
		DefaultTableModel dtm = new DefaultTableModel(dadosAnimal, colPrincipal); //cria model para tabela com dados preenchidos
		gui.tabela.setModel(dtm); //seta o model da tabela
	}

	// Seta o número do tombo do animal selecionado (Botão Usar)
	public void setTombo(String Tombo){
		this.Tombo = Tombo;
	}
	// Retorna o número do tombo do animal selecionado
	public String getTombo(){
		return Tombo;
	}

	/**
	 * Função chama a classe responsável por alterar os dados do animal selecionado
	 */
	private void alterarAnimal(){
		try{
			if( gui.tabela.getSelectedRow() > -1 ){// se tiver alguma linha selecionada na tabela consultada
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 0); //pega o codigo tombo
				AnimalDAO aDAO = new AnimalDAO();
				Animal aniAux = aDAO.findAnimalByTombo(cdTomboSelecionado); //busca pelo animal no banco
				new AlterarAnimalController(aniAux, gui);
				//limpa consulta
			}else
				JOptionPane.showMessageDialog(null, "Selecionar um Animal!");
		} catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ERRO:" + e);
		}		
	}

	/**
	 * Método que passa o animal selecionado como parâmetro para a classe ProcessoController que é responsável
	 * por exibir todo o processo realizado com o animal em questão.
	 */
	private void processoAnimal(){
		try{
			if( gui.tabela.getSelectedRow() > -1 ){// se tiver alguma linha selecionada na tabela consultada
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 0); //pega o codigo tombo
				AnimalDAO aDAO = new AnimalDAO();
				Animal aniAux = aDAO.findAnimalByTombo(cdTomboSelecionado); //busca pelo animal no banco
				new ProcessoController(gui, aniAux);
			}
			else
				JOptionPane.showMessageDialog(null,"Selecionar um Animal!");
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ERRO:" + e);
		}
	}

	/*
	Remover Animal não é mais necessário. 
	Acontece no código abaixo:
	Verifica se o animal a ser excluido possui alguma peça, se possuir então o animal não pode ser excluido.
	O usuário é avisao e nada é feito.
	Se o animal não possuir nenhuma peça, então é aberta a janela Baixa, que peda a justificativa para apagar o animal.
	O que acontece agora: 
	Quando uma peça de um animal é apagada é verificado se ainda existem peças relacionadas ao animal, se não existir
	ou seja, se a peça excluida era a ultima relacionada ai animal, então o animal é excluido.

	Essa verificação ocorre na classe PecaController, e com isso a funcionalidade excluir animal não é mais necessária.

	No lugar da funcionalidade excluir animal será implementada a funcionalidade para listar o processo do aniaml.
	 */
	//	/**
	//	 * Verifica se possui animal selecionado e se não existe nenhuma peça relacionada a esse animal, se não tiver o animal é lógicamente apagado
	//	 */
	//	private void verificaDeletaAnimal() {
	//		try{
	//			if( gui.tabela.isBackgroundSet() ){// se tiver alguma linha selecionada na tabela consultada
	//				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
	//				String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 0); //pega o codigo tombo
	//				AnimalDAO aDAO = new AnimalDAO();
	//				PecaDAO pDao = new PecaDAO();
	//				Animal aniAux = aDAO.findAnimalByTombo(cdTomboSelecionado); //busca pelo animal no banco				
	//				listPeca = pDao.pecaNaoApagada(aniAux.getIdAnimal());
	//				if(listPeca.size() > 0){
	//					JOptionPane.showMessageDialog(gui, "O Animal não pode ser apagado!");
	//				}
	//				else{				
	//					BaixaController delPc = new BaixaController(gui);
	//					if(delPc.getIdBaixa() != -1)
	//					{
	//						aDAO.BaixaDB(aniAux.getIdAnimal());
	//					}
	//				}
	//			}
	//		} catch (ArrayIndexOutOfBoundsException e){
	//			System.out.println("ERRO:" + e);
	//		}
	//	}
}
