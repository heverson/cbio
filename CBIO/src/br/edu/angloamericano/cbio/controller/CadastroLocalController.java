package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Local;
import br.edu.angloamericano.cbio.dados.LocalComplemento;
import br.edu.angloamericano.cbio.dados.Sala;
import br.edu.angloamericano.cbio.dao.BlocoDAO;
import br.edu.angloamericano.cbio.dao.LocalComplementoDAO;
import br.edu.angloamericano.cbio.dao.LocalDAO;
import br.edu.angloamericano.cbio.dao.PecaDAO;
import br.edu.angloamericano.cbio.dao.SalaDAO;
import br.edu.angloamericano.cbio.gui.CadastroLocalGUI;
import br.edu.angloamericano.cbio.gui.LocalGUI;
import br.edu.angloamericano.cbio.gui.PanelLocalGUI;


public class CadastroLocalController {

	public CadastroLocalGUI gui;
	private Local local;
	private Bloco bloco;
	private Sala sala;
	private LocalComplemento localComp = new LocalComplemento();

	public PanelLocalGUI panel;

	private boolean treeSelected = false;
	private int depth = -1;

	public int flag = 0;			//flag para dizer quem esta chamando o cadastro de local;
	// se o cadastro de local esta sendo chamado pela Opcao Cadastro de Local no Menu flag = 0
	// Se o cadastro de local esta sendo chamado para escolher um local de armazenamento flag = 1

	public CadastroLocalController(JDialog Super, int flag){
		this.flag = flag;
		gui = new CadastroLocalGUI(Super);
		gui.addOuvintes(new Ouvinte());
		gui.setVisible(true);
	}	

	public int getFlag(){
		return flag;
	}

	public void setLocal(Local local){
		this.local = local;
	}

	public void setBloco(Bloco bloco){
		this.bloco = bloco;
	}

	public Bloco getBloco(){
		return bloco;
	}

	public Local getLocal(){
		return local;
	}

	public Sala getSala(){
		return sala;
	}

	public void setSala(Sala sala){
		this.sala = sala;
	}

	public LocalComplemento getLocalComplemento(){
		return localComp;
	}

	public void setLocalComplemento(LocalComplemento localComp){
		this.localComp = localComp;
	}

	public void setDepth(int n){
		this.depth = n;
	}

	public int getDepth(){
		return depth;
	}

	public void setTreeSelected(boolean isSelected){
		this.treeSelected = isSelected;
	}

	public boolean getTreeSelected(){
		return treeSelected;
	}


	/**
	 * Classe interana para controle das ações gerada na interface de manutenção dos locais de armazenamento
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener, TreeSelectionListener{
		@SuppressWarnings("static-access")
		public void actionPerformed(ActionEvent ev){
			/** 
			 * EVENTO BOTAO ADICIONAR
			 */
			//******************************************************************************************************
			if(ev.getSource() == gui.panel.btnAdicionar){
				int nivel = getDepth();
				String loc = "";
				String ds = "";
				if(nivel == 0){
					loc = "Entre com o Local a ser Cadastrado!";
					loc = JOptionPane.showInputDialog(loc);
				}
				else 
					if(nivel == 1){
						loc = "Entre com o Bloco a ser Cadastrado!";
						loc = JOptionPane.showInputDialog(loc);
					}
					else 
						if(nivel == 2){
							loc = "Entre com a Sala a ser Cadastrada!";
							loc = JOptionPane.showInputDialog(loc);
						}
						else 
							if(nivel == 3){
								LocalGUI local = new LocalGUI();
								loc = local.getNome();
								ds = local.getDesc();
							}
				if((!loc.equals("")) && (getTreeSelected())){

					if(nivel == 0){
						LocalDAO dao = new LocalDAO();
						local = new Local();
						local.setNmLocal(loc);
						dao.insert(local);
					}
					if(nivel == 1){
						BlocoDAO dao = new BlocoDAO();
						dao.insert(getLocal(), loc);

					}else
						if(nivel == 2){
							SalaDAO dao = new SalaDAO();
							dao.insert(getBloco(), loc);
						}else
							if(nivel == 3){
								LocalComplementoDAO dao = new LocalComplementoDAO();
								dao.insert(getSala() , loc,ds);
							}

					TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());			//pega os novos dados do BD e cria um novo TreeMOdel
					gui.setTreeModel(arvoreModel);															//Set o novo modelo na arvore
					gui.updateTree();
				}
				setTreeSelected(false);
				setDepth(-1);
			}
			//******************************************************************************************************			
			if(ev.getSource() == gui.panel.btnAlterar){
				int nivel = getDepth();
				String loc = "";
				String ds = "";

				if(getTreeSelected()){

					if(nivel == 1){

						loc = JOptionPane.showInputDialog("Entre com o novo nome do Local a ser alterado!");
					}
					else 
						if(nivel == 2){

							loc = JOptionPane.showInputDialog( "Entre com o novo nome do Bloco a ser alterado!");
						}
						else 
							if(nivel == 3){

								loc = JOptionPane.showInputDialog("Entre com o novo nome da Sala a ser alterado!");
							}
							else 
								if(nivel == 4){
									LocalGUI local = new LocalGUI();
									loc = local.getNome();
									ds = local.getDesc();
								}
					if(!loc.equals("")){
						if(nivel == 1){
							LocalDAO dao = new LocalDAO();
							local.setNmLocal(loc);
							dao.saveDB(local);						//da um update na tabela
						}
						if(nivel == 2){
							BlocoDAO dao = new BlocoDAO();
							bloco.setNmBloco(loc);
							dao.saveDB(bloco);
						}else
							if(nivel == 3){
								SalaDAO dao = new SalaDAO();
								sala.setNmSala(loc);
								dao.saveDB(sala);
							}else
								if(nivel == 4){
									LocalComplementoDAO dao = new LocalComplementoDAO();
									localComp.setNmComplemento(loc);
									localComp.setDsComplemento(ds);
									dao.saveDB(localComp);
								}
						TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());			//pega os novos dados do BD e cria um novo TreeMOdel
						gui.setTreeModel(arvoreModel);															//Set o novo modelo na arvore
						gui.updateTree();
					}
				}
				else
					JOptionPane.showMessageDialog(gui, "Nada Selecionado");
			}

			//******************************************************************************************************			
			/** EVENTO BOTAO REMOVER*/
			if(ev.getSource() == gui.panel.btnRemover){
				if(getTreeSelected()){
					JOptionPane msg = new JOptionPane();
					int nivel = getDepth();
					if(nivel == 4){
						int option = msg.showConfirmDialog(null, "Deseja Remover o Local?", "CUIDADO", 0);
						if(option == msg.OK_OPTION){
							PecaDAO pDAO = new PecaDAO();
							if(pDAO.emptyLocal(localComp.getIdComplemento()))
							{
								LocalComplementoDAO dao = new LocalComplementoDAO();
								dao.deleteDB(localComp);
							}
							else{
								JOptionPane.showMessageDialog(gui, "O Complemento não pode ser apagado!");
							}
						}
					}
					else
						JOptionPane.showMessageDialog(gui, "Apenas um Complemento pode ser apagado!");
				}
				else
					JOptionPane.showMessageDialog(gui, "Nada Selecionado");
				setTreeSelected(false);
				setDepth(-1);
				TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());			//pega os novos dados do BD e cria um novo TreeMOdel
				gui.setTreeModel(arvoreModel);															//Set o novo modelo na arvore
				gui.updateTree();
			}

			//*********************************************************************************************************
			/** EVENTO BOTAO TERMINAR*/
			if(ev.getSource() == gui.panel.btnTerminar){
				gui.dispose();
			}
		}

		@Override
		public void valueChanged(TreeSelectionEvent treeSel) {
			if(gui.panel.tree.getLastSelectedPathComponent() != null){
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) gui.panel.tree.getLastSelectedPathComponent();  //no de arvore recebe o componente selecionado

				int lvl = node.getLevel();
				setDepth(lvl);						//seta a altura dos niveis da arvore para fazer controle de qual classe esta sendo selecionada

				if(lvl == 0){
					setTreeSelected(true);
				}
				if(lvl == 1){									//se Selecionado um Local sera adicionado um bloco no bd
					Local local = (Local) node.getUserObject();	
					setLocal(local);
					setTreeSelected(true);
				}else 
					if(lvl ==2){								//se Selecionado um Bloco sera adicionado uma Sala no bd
						Bloco bloco = (Bloco) node.getUserObject();
						setBloco(bloco);
						setTreeSelected(true);

					}else
						if(lvl == 3){							//se Selecionado uma Sala sera adicionado um Complemento no bd
							Sala sala = (Sala) node.getUserObject();
							setSala(sala);
							setTreeSelected(true);
						}else
							if(lvl == 4){
								LocalComplemento localComp = (LocalComplemento) node.getUserObject();
								setLocalComplemento(localComp);

								gui.panel.tree.setToolTipText("Descrição : " + localComp.getDsComplemento());
								gui.panel.textDesc.setText(localComp.getDsComplemento());
								setTreeSelected(true);
							}
			}

		}
	}

}


