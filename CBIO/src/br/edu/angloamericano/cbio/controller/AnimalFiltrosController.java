package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dados.Familia;
import br.edu.angloamericano.cbio.dados.Ordem;
import br.edu.angloamericano.cbio.dados.Tribo;
import br.edu.angloamericano.cbio.dao.ClasseDAO;
import br.edu.angloamericano.cbio.dao.EspecieDAO;
import br.edu.angloamericano.cbio.dao.FamiliaDAO;
import br.edu.angloamericano.cbio.dao.OrdemDAO;
import br.edu.angloamericano.cbio.dao.TriboDAO;
import br.edu.angloamericano.cbio.gui.AnimalFiltrosGUI;

public class AnimalFiltrosController {
	private List<Classe> listClasse = new LinkedList<Classe>();
	private List<Ordem> listOrdem = new LinkedList<Ordem>();
	private List<Familia> listFamilia = new LinkedList<Familia>();
	private List<Tribo> listTribo = new LinkedList<Tribo>();
	private List<Especie> listEspecie = new LinkedList<Especie>();
	
	private ClasseDAO classeDAO;
	private OrdemDAO ordemDAO;
	private TriboDAO triboDAO;
	private FamiliaDAO familiaDAO;
	private EspecieDAO especieDAO;
	
	private Classe classe;
	private Ordem ordem;
	private Tribo tribo;
	private Familia familia;
	//private Especie especie;
	
	AnimalFiltrosGUI gui;
	Animal ani = null;
	
	public AnimalFiltrosController(JDialog Super){
		gui = new AnimalFiltrosGUI(Super);
		gui.addOuvintes(new Ouvinte());
		carregaClasses();
		gui.setModal(true);
		gui.setVisible(true);
	}
	
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			
			if(ev.getSource() == gui.classe){
				verificaClasse();
			}
			if(ev.getSource() == gui.ordem){
				verificaOrdem();
			}
			if(ev.getSource() == gui.familia){
				verificaFamilia();
			}
			if(ev.getSource() == gui.tribo){
				verificaTribo();
			}
			if(ev.getSource() == gui.especie){
				verificaEspecie();
			}
			
			if(ev.getSource() == gui.btnOk){
				preencheAnimalConsulta();
				gui.dispose();
				//JOptionPane.showMessageDialog(gui, "Botão OK");
			}
			
			// Fecha a janela de filtro
			if(ev.getSource() == gui.btnCancelar){
				gui.dispose();
			}
			
			// Limpa todos os campos da janela de filtro
			if(ev.getSource() == gui.btnLimpar){
				gui.cBoxIdade.setSelectedIndex(0);
				gui.cBoxSexo.setSelectedIndex(0);
				gui.classe.setSelectedIndex(0);
				gui.especie.setSelectedIndex(0);
				gui.familia.setSelectedIndex(0);
				gui.ordem.setSelectedIndex(0);
				gui.tribo.setSelectedIndex(0);
				gui.txtNmAnimal.setText("");
				gui.txtNmCientifico.setText("");
			}
		}
	}
	
	/**
	 * Armazena os dados dos campos de filtro para utilizar na consulta
	 */
	private void preencheAnimalConsulta() {
		ani = new Animal();
		if( !gui.txtNmAnimal.getText().trim().equals("") )
				ani.setNmAnimal(gui.txtNmAnimal.getText());
		
		if( !gui.txtNmCientifico.getText().trim().equals("") )
				ani.setNmCientifico(gui.txtNmCientifico.getText());

		if( gui.cBoxIdade.getSelectedIndex() > 0 )
				ani.setDsIdade(gui.cBoxIdade.getSelectedItem().toString() );
		
		if( gui.cBoxSexo.getSelectedIndex() > 0 )
				ani.setSgSexo(gui.cBoxSexo.getSelectedItem().toString().charAt(0));
		
		if( gui.especie.getSelectedIndex() > 0 )
				ani.setIdEspecie( gui.especie.getSelectedIndex() );
	}
	
	public Animal getAnimalFiltro(){
		if(ani != null)
			return ani;
		else 
			return null;
	}
	
	private void carregaClasses(){
		
		try {			
			classeDAO = new ClasseDAO();
			classeDAO.setQuery();				//carrega as classes do bd para uma lista
			listClasse = classeDAO.getList();
			for(int i = 0; i < listClasse.size(); i++)
				gui.classe.addItem(listClasse.get(i));
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(gui,
			"Estados ou/e classes não puderam ser carregados!");
			e.printStackTrace();
		}
		
	}
	
	private void verificaClasse() {
		if(gui.classe.getSelectedIndex() < 1){

			gui.ordem.removeAllItems();
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.ordem.addItem(":: Selecione uma ordem ::");
			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			
			gui.ordem.setEnabled(false);
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
		} else{
			
			try {				
				classe = (Classe) gui.classe.getSelectedItem() ;
				ordemDAO = new OrdemDAO();
				ordemDAO.setQueryClasse(classe.getIdClasse());
				listOrdem = ordemDAO.getList();
			
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as ordens!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma ordem ::");
			if(listOrdem!= null)
				for(int i=0; i<listOrdem.size(); i++)
					modelo.addElement(listOrdem.get(i));

			gui.ordem.setModel(modelo);
			gui.ordem.setEnabled(true);
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}
	
	private void verificaOrdem() {
		if(gui.ordem.getSelectedIndex() < 1){
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
		} else{
			
			try {
				ordem = (Ordem) gui.ordem.getSelectedItem();
				familiaDAO = new FamiliaDAO();
				familiaDAO.setQueryOrdem(ordem.getIdOrdem());
				listFamilia = familiaDAO.getList();
				
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as famílias!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma família ::");
			if(listFamilia != null)
				for(int i=0; i<listFamilia.size(); i++)
					modelo.addElement(listFamilia.get(i));
			
			gui.familia.setModel(modelo);
			gui.familia.setEnabled(true);
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}
	
	/**
	 * Função que Verifica a tribo na GUI 
	 */
	private void verificaTribo() {
		if(gui.tribo.getSelectedIndex() < 1){
			gui.especie.removeAllItems();

			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.especie.setEnabled(false);
		} else{
			
			try {
				tribo = (Tribo) gui.tribo.getSelectedItem();
				especieDAO = new EspecieDAO();
				especieDAO.setQueryTribo(tribo.getIdTribo());
				listEspecie = especieDAO.getList();
				
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as espécies!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma espécie ::");
			if(listEspecie != null)
				for(int i=0; i<listEspecie.size(); i++)
					modelo.addElement(listEspecie.get(i));
			gui.especie.setModel(modelo);
			gui.especie.setEnabled(true);
		}
	}

	/**
	 * Função que Verifica a familia na GUI 
	 */
	private void verificaFamilia() {
		if(gui.familia.getSelectedIndex() < 1){
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
		} else{
			
			try {
				familia = (Familia) gui.familia.getSelectedItem();
				triboDAO = new TriboDAO();
				triboDAO.setQueryFamilia(familia.getIdFamilia());
				listTribo = triboDAO.getList();
				

			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as tribos!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma tribo ::");
			if(listTribo != null)
				for(int i=0; i<listTribo.size(); i++)
					modelo.addElement(listTribo.get(i));
			
			gui.tribo.setModel(modelo);
			gui.tribo.setEnabled(true);
			gui.especie.removeAllItems();
			gui.especie.setEnabled(false);
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}
	
	/**
	 * Funçãoo que verifica a especie
	 */
	private void verificaEspecie()
	{
		if( (gui.especie.getSelectedItem() != null))
		{
			
		}
	}
	
}
