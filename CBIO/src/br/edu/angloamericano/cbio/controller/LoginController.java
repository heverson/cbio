package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.dados.Usuario;
import br.edu.angloamericano.cbio.dao.LoginDAO;
import br.edu.angloamericano.cbio.dao.TipoUserDAO;
import br.edu.angloamericano.cbio.dao.UsuarioDAO;
import br.edu.angloamericano.cbio.gui.LoginGUI;
import br.edu.angloamericano.cbio.util.SenhasHash;

public class LoginController {
	private LoginGUI gui;
	private Usuario user;
	private LoginDAO login = new LoginDAO();
	private UsuarioDAO userDAO;
	private TipoUserDAO tipoDAO;

	public LoginController(){
		gui = new LoginGUI();
		gui.addOuvinte(new Ouvinte());
		gui.setVisible(true);
	}

	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			if(ev.getSource() == gui.btnEntrar){
				validaLogin();
			}  
			if(ev.getSource() == gui.btnSair){
				gui.dispose();
			}
		}
	}

	/**
	 * Método privado que valida os dados de login do usuário.
	 */
	@SuppressWarnings("deprecation")
	private void validaLogin(){
		String aux_login = String.valueOf(gui.login.getText().trim());
		String aux_senha = String.valueOf(gui.senha.getPassword());
		
		if(aux_login.equals("") || aux_senha.equals("") || 
				gui.login.getText().contains("'") || gui.senha.getText().contains("'"))
		{
			JOptionPane.showMessageDialog(null,"Login ou Senha Inválida!");
			gui.login.setText("");
			gui.senha.setText("");
		}
		else {
			try{
				int id = login.valida_login(aux_login, SenhasHash.doHash(aux_senha));
				if (id != 0){
					userDAO = new UsuarioDAO();
					tipoDAO = new TipoUserDAO();
					user = new Usuario();
					user = userDAO.getUsuario(id);
					App.setUsuario(user);
					App.setTipoUser(tipoDAO.getTipoUser(user.getTipo()));
					new PrincipalController();
					gui.dispose();
				}
				else{
					JOptionPane.showMessageDialog(null,"Login ou Senha Inválida!");
					gui.login.setText("");
					gui.senha.setText("");
				}
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}