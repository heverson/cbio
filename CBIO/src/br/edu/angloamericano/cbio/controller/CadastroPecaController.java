package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dados.Familia;
import br.edu.angloamericano.cbio.dados.Local;
import br.edu.angloamericano.cbio.dados.LocalComplemento;
import br.edu.angloamericano.cbio.dados.Ordem;
import br.edu.angloamericano.cbio.dados.Peca;
import br.edu.angloamericano.cbio.dados.PecaProcesso;
import br.edu.angloamericano.cbio.dados.Processo;
import br.edu.angloamericano.cbio.dados.Sala;
import br.edu.angloamericano.cbio.dados.Tribo;
import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.BlocoDAO;
import br.edu.angloamericano.cbio.dao.ClasseDAO;
import br.edu.angloamericano.cbio.dao.EspecieDAO;
import br.edu.angloamericano.cbio.dao.FamiliaDAO;
import br.edu.angloamericano.cbio.dao.LocalComplementoDAO;
import br.edu.angloamericano.cbio.dao.LocalDAO;
import br.edu.angloamericano.cbio.dao.OrdemDAO;
import br.edu.angloamericano.cbio.dao.PecaDAO;
import br.edu.angloamericano.cbio.dao.PecaProcessoDAO;
import br.edu.angloamericano.cbio.dao.ProcessoDAO;
import br.edu.angloamericano.cbio.dao.SalaDAO;
import br.edu.angloamericano.cbio.dao.TriboDAO;
import br.edu.angloamericano.cbio.gui.CadastroPecaGUI;
import br.edu.angloamericano.cbio.gui.LocalGUI;

/**
 * Classe responsável por cadastrar peças a base de dados.
 * @author Thiago R. M. Bitencourt
 */
public class CadastroPecaController {
	private CadastroPecaGUI gui;
	private List<Peca> listPeca = new LinkedList<Peca>();
	private Animal a = new Animal();
	private Peca peca = new Peca();
	private PecaProcesso pecaProc = new PecaProcesso();
	private Processo proc = new Processo();
	Especie esp = new Especie();
	Tribo tribo = new Tribo();
	Familia familia = new Familia();
	Ordem ordem = new Ordem();
	Classe classe = new Classe();
	private boolean testaAdd = false;
	public Local local;
	public Bloco bloco;
	public Sala sala;
	public LocalComplemento localComplemento;

	private int select = 0; 
	private int QtPeca = 0; // Quantidade de novas peças a serem criadas
	private int nivel = -1; //controle para arvore de localidade
	private boolean treeSelected = false; //controle para adicionar, alterar e remover items da arvore... 

	JFrame OutroSuper;
	JDialog Super;

	/**
	 * Construtores. 
	 * O parâmetro flag cerve para identificar quem chamou a classe. 
	 * Se flag for 1 então a chamada vem da classe PecaController, se for 2 vem da classe Animal Controller.
	 * @param flag - identificador da chamada
	 * @param Super - Janela 'Pai'
	 * @param OutroSuper - Janela 'Pai' - A janela 'Pai' usada depende de quem fez a chamada
	 */
	public CadastroPecaController(int flag, JDialog Super, JFrame OutroSuper)
	{
		this.OutroSuper = OutroSuper;
		this.Super = Super;
		CriaJanela(flag);
	}
	public CadastroPecaController(int flag, JDialog Super, Animal animal, int id)
	{
		this.Super = Super;
		a = animal;
		a.setIdAnimal(id);
		CriaJanela(flag);
	}

	/**
	 *  Cria a janela e configura de acordo com a flag passada como parâmetro.
	 *  Se a flag for 1, a janela é usada para cadastrar uma peça independente e se for 2 é usada para
	 *  cadastrar a primeira peça de um animal e nesse caso, não é permitido fechar a janela e cancelar o cadastro
	 *  ou seja, o cadastro deve, obrigatóriamente, ser concluído.
	 * @param flag
	 */
	@SuppressWarnings("static-access")
	private void CriaJanela(int flag){

		gui = new CadastroPecaGUI(Super);
		/**
		 * Se a flag for 2 então, o botão cancelar fica desativado e não é possivel fechar a janela e ainda o número de peças
		 * a ser criado fica restripo à apenas uma peça, já que é a primeira peça do animal
		 */
		if(flag == 2)
		{
			gui.btnCancelar.setEnabled(false);
			gui.spinner.setValue(1);
			gui.spinner.setEnabled(false);
			gui.setDefaultCloseOperation(gui.DO_NOTHING_ON_CLOSE);
			preencheDadosTela2();
		}
		gui.addOuvintes(new Ouvinte());
		gui.setTitle("Cadastrar Peça");
		gui.setModal(true);
		gui.setVisible(true);
	}

	/**
	 *Quando o número de peças a ser criado é maior do que 1 então essa função é chamada para cadastrar uma nova peça
	 *Quando mais de uma peça será cadastrada, todas as peças devem, obrigatóriamente, ser cadastradas e portanto o método
	 *novaPeca() desabilita o botão cancelar e também não permite que a janela seja fechada. Ainda desabilita o botão de criar novas peças 
	 */
	@SuppressWarnings("static-access")
	private void NovaPeca()
	{
		QtPeca --;
		gui.btnCancelar.setEnabled(false);
		gui.setDefaultCloseOperation(gui.DO_NOTHING_ON_CLOSE);
		gui.spinner.setEnabled(false);
		gui.btnAddPeca.setEnabled(false);
		preencheDadosTela2();
	}

	/**
	 * Classe interna responsável por controlar as ações geradas pela interface gráfica
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener, TreeSelectionListener, MouseListener{

		public void actionPerformed(ActionEvent ev){
			// Quando o botão consultar á precionado
			if(ev.getSource() == gui.btnConsultar)
			{
				AnimalController Animal = new AnimalController(OutroSuper, 1);
				AnimalByTombo(Animal.getTombo());
			}
			if(ev.getSource() == gui.btnFiltros)
			{
				if(!gui.textFieldIdTombo.getText().equals("") && !gui.textFieldIdTomboFinal.getText().equals("")){
					AnimalByTombo(gui.lblCdTombo.getText() + gui.textFieldIdTombo.getText() + gui.textFieldIdTomboFinal.getText());
				}
				else
					JOptionPane.showMessageDialog(null, "Tombo Imcompleto");
			}

			if(ev.getSource() == gui.btnLimparP2){
				limpaCamposP2();
			}
			if(ev.getSource() == gui.btnLimparP3){
				limpaCamposP3();
			}
			if(ev.getSource() == gui.btnCancelar){
				gui.dispose();
			}

			if(ev.getSource() == gui.btnProximoP1)
				verificaAnimalSegueTela2();

			if(ev.getSource() == gui.btnProximoP2)
				validaDadosPecaTela2();

			if(ev.getSource() == gui.btnProximoP3)
				validaProcessoTela3();

			if(ev.getSource() == gui.btnAddPeca){
				addNovaPeca();
			}

			if(ev.getSource() == gui.btnAdicionar){
				adicionaPecaDesejada();
			}
			if(ev.getSource() == gui.btnLimparP1){
				limpaCamposP1();
			}


			/** 
			 * EVENTO BOTAO ADICIONAR Arvore
			 */
			//******************************************************************************************************
			if(ev.getSource() == gui.btnAdicionarArvore){
				adicionarArvore();
			}
			//******************************************************************************************************			
			if(ev.getSource() == gui.btnAlterarArvore){
				alterarArvore();
			}
			//******************************************************************************************************			
			/** EVENTO BOTAO REMOVER*/
			if(ev.getSource()== gui.btnRemoverArvore){
				removerArvore();
			}
		}

		public void valueChanged(TreeSelectionEvent treeSel) {
			if(gui.tree.getLastSelectedPathComponent() != null){

				DefaultMutableTreeNode node = (DefaultMutableTreeNode) gui.tree.getLastSelectedPathComponent();  //no de arvore recebe o componente selecionado
				int lvl = node.getLevel();
				setNivel(lvl);						//seta a altura dos niveis da arvore para fazer controle de qual classe esta sendo selecionada

				if(lvl == 0){
					setTreeSelected(true);
				}
				if(lvl == 1){									//se Selecionado um Local sera adicionado um bloco no bd
					local = (Local) node.getUserObject();	
					setLocal(local);
					setTreeSelected(true);

				}else 
					if(lvl ==2){								//se Selecionado um Bloco sera adicionado uma Sala no bd
						bloco = (Bloco) node.getUserObject();
						setBloco(bloco);
						setTreeSelected(true);

					}else
						if(lvl == 3){							//se Selecionado uma Sala sera adicionado um Complemento no bd
							sala = (Sala) node.getUserObject();
							setSala(sala);
							setTreeSelected(true);
						}else
							if(lvl == 4){
								localComplemento = (LocalComplemento) node.getUserObject();
								setLocalComplemento(localComplemento);
								gui.textDesc.setText(localComplemento.getDsComplemento());
								gui.tree.setToolTipText("Descrição : " + localComplemento.getDsComplemento());
								setTreeSelected(true);
							}
			}
		}

		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == gui.tabbed){
				int n = gui.tabbed.getSelectedIndex();
				if(n == 0){
					gui.tabbed.setEnabledAt(1,false);
					gui.tabbed.setEnabledAt(2,false);
					gui.tabbed.setEnabledAt(3,false);
				}else
					if(n==1){
						if(gui.tabbed.isEnabledAt(1)){
							gui.tabbed.setEnabledAt(2,false);
							gui.tabbed.setEnabledAt(3,false);
						}
					}else
						if(n==2){
							if(gui.tabbed.isEnabledAt(2))
								gui.tabbed.setEnabledAt(3,false);
						}
			}
		}

		public void mouseEntered(MouseEvent arg0) {}
		public void mouseExited(MouseEvent arg0) {}
		public void mousePressed(MouseEvent arg0) {}
		public void mouseReleased(MouseEvent arg0) {}

	}

	/**
	 * Função de consulta de animais
	 */
	public void AnimalByTombo(String tombo){	
		AnimalDAO aDAO = new AnimalDAO();
		a = aDAO.findAnimalByTombo(tombo);
		if(a.getCdTombo() != null){
			EspecieDAO eDAO = new EspecieDAO(); 
			esp = eDAO.findEspecieById(a.getIdEspecie()); //busca a especie pelo ID
			TriboDAO tDAO = new TriboDAO();
			tribo = tDAO.findTriboById(esp.getIdTribo()); //busca tribo pelo ID
			FamiliaDAO fDAO = new FamiliaDAO();
			familia = fDAO.findFamiliaById(tribo.getIdFamilia()); //busca familia pelo ID
			OrdemDAO oDAO = new OrdemDAO();
			ordem = oDAO.findOrdemById(familia.getIdOrdem()); //busca ordem pelo ID
			ClasseDAO cDAO = new ClasseDAO();
			classe = cDAO.findClasseById(ordem.getIdClasse()); //busca classe pelo ID
			preencheCamposAnimal();
		} 
		else {
			JOptionPane.showMessageDialog(null, "Animal não encontrado!");
			limpaCamposP1();
		}
	}

	/**
	 * Função que preenche campos do objeto animal buscado na tela da PecaGUI na aba 1
	 */
	private void preencheCamposAnimal(){
		gui.txtNmAnimal.setText(a.getNmAnimal());
		gui.txtNmCientifico.setText(a.getNmCientifico());
		gui.txtIdade.setText(a.getDsIdade());
		gui.txtPeso.setText(String.valueOf(a.getNuPeso()));
		gui.txtCorPelagem.setText(a.getDsCor());
		gui.txtSinais.setText(a.getDsSinais());
		if( a.getSgSexo() == 'M' || a.getSgSexo() == 'm')
			gui.txtSexo.setText("Macho");
		else
			gui.txtSexo.setText("Fêmea");
		gui.txtEspecie.setText(esp.getNmEspecie());
		gui.txtTribo.setText(tribo.getNmTribo());
		gui.txtFamilia.setText(familia.getNmFamilia());
		gui.txtOrdem.setText(ordem.getNmOrdem());
		gui.txtClasse.setText(classe.getNmClasse());
	}

	/**
	 * Função que limpa os campos do animal na tela PecaGUI na aba 2
	 */
	private void limpaCamposP1(){
		gui.txtNmAnimal.setText("");
		gui.txtNmCientifico.setText("");
		gui.txtIdade.setText("");
		gui.txtPeso.setText("");
		gui.txtCorPelagem.setText("");
		gui.txtSinais.setText("");
		gui.txtSexo.setText("");
		gui.txtEspecie.setText("");
		gui.txtClasse.setText("");
		gui.txtOrdem.setText("");
		gui.txtTribo.setText("");
		gui.txtTribo.setText("");
		gui.txtFamilia.setText("");
		gui.textFieldIdTombo.setText("");
		gui.textFieldIdTomboFinal.setText("");
		a.setIdAnimal(0);
	}

	private void limpaCamposP2(){
		gui.cBoxPecas.setSelectedIndex(0);
		gui.cBoxPecas.setEnabled(false);
		gui.cBoxPecas.removeAllItems();
		gui.cBoxPecas.addItem(":: Selecione uma Peça ::");
		testaAdd = false;
		gui.txtAreaOssos.setText("");
		gui.chckbxDnaCartilagem.setSelected(false);
		gui.chckbxDnaTecidoMuscular.setSelected(false);
		gui.chckbxDNAVisceral.setSelected(false);
		gui.chckbxPeloAbdominal.setSelected(false);
		gui.chckbxPeloCabeaDorsal.setSelected(false);
		gui.chckbxPeloInterescapularDorsal.setSelected(false);
		gui.chckbxEscama.setSelected(false);
		gui.chckbxPena.setSelected(false);
		gui.spinner.setValue(1);
		gui.txtDsPeca.setText("");
	}

	private void limpaCamposP3(){
		gui.cBoxNmProcesso.setSelectedIndex(0);
		gui.txtNmResp.setText("");
		gui.txtDsProc.setText("");
	}

	/**
	 * Função que verifica se animal foi consultado e parte para a tela 2 preenchendo dados necessarios
	 */
	private void verificaAnimalSegueTela2() {
		if(a.getIdAnimal() > 0)//verifica se buscou um animal do banco
			preencheDadosTela2(); //se animal está ok, preenche e segue para tela 2
		else //senao dispara dialogo de erro
			JOptionPane.showMessageDialog(null, "Por Favor, consulte um animal!");
	}

	/**
	 * Preenche os dados da Tela 2, verifica se existe peças cadastradas do animal
	 */
	private void preencheDadosTela2() {
		PecaDAO pDAO = new PecaDAO();
		listPeca = pDAO.pecaNaoApagada(a.getIdAnimal());
		gui.textTombo.setText(a.getCdTombo());
		DefaultComboBoxModel modelo = new DefaultComboBoxModel(); //cria modelo para combo box
		modelo.addElement(":: Selecione uma Peça ::");
		Iterator<Peca> it = listPeca.iterator(); //cria iterator para percorrer a lista

		if( listPeca.size() > 0 ) //verifica se foram encontradas peças
		{
			Peca pe = new Peca();

			gui.chckbxEsteAnimalAinda.setSelected(false);
			gui.tabbed.setEnabledAt(1, true); //habilita aba de peças
			gui.tabbed.setSelectedIndex(1); //move para a aba da peça

			pe = (Peca) it.next();

			for(int i = 0; i<listPeca.size(); i++)
			{ //preeche modelo para combo box de peças
				modelo.addElement("PC"+pe.getCdTomboPeca());
				pe = new Peca();
				if(it.hasNext()){
					pe = (Peca) it.next(); //pega a peça
				}
			}
			if(modelo.getSize() > 1){
				gui.cBoxBase.setEnabled(true);
				gui.cBoxBase.setModel(modelo);
				gui.cBoxBase.setSelectedIndex(select);
				if(select != 0)
					gui.cBoxBase.setEnabled(false);
			}
			else{
				gui.cBoxBase.setEnabled(false);
			}

		} else {
			gui.cBoxBase.setEnabled(false);
			gui.tabbed.setEnabledAt(1, true); //habilita aba de peças
			gui.tabbed.setSelectedIndex(1); //move para a aba da peça
			gui.chckbxEsteAnimalAinda.setSelected(true);
		}
	}

	/**
	 * Função que cria cd tombo da Peça e adiciona nova peça na lista
	 */
	private void addNovaPeca() {
		if( !testaAdd ){ // Novas peças podem ser criadas apenas uma vez.
			int v = (Integer) gui.spinner.getValue(); // pega o número de peças que deve ser criadas
			if(v >= 1){ // Só aceita o valor mairo que 1, avisando que valores menores são inválidos
				
				QtPeca = v;
				/*
				 * Faz a verificação de peças
				 * - Se o animal não possuir nenhuma peça cadastrada, então apenas uma peça pode ser criada.
				 * - Se o animal ja possuir peças então um peça base deve, obrigatóriamente, ser selecionada.
				 */
				int aux = 0;
				if(!gui.chckbxEsteAnimalAinda.isSelected() 
						&& gui.cBoxBase.getSelectedIndex() > 0)
					aux = 1;
				if(gui.chckbxEsteAnimalAinda.isSelected())
					aux = 1;

				if(aux == 1){
					DefaultComboBoxModel modelo = new DefaultComboBoxModel(); //cria modelo para combo box
					int cdNovaPeca;
					if(listPeca.size() == 0){ // se nenhuma peça existir, então o código da nova peça é 1.
						cdNovaPeca = 1;
					}
					else
					{ 
						/*
						 * Se existir peça ja cadastradas para o animal, então pega a ultima peça
						 * para incrementar o código e atribuir a nova peça
						 */
						Peca an = listPeca.get(listPeca.size() -1);
						cdNovaPeca = an.getCdTomboPeca() + 1;
					}
					// Cria o comboBox com as novas peças criadas.
					for(int i = 0; i < v; i++){
						String cdTomboPeca = "PC"+cdNovaPeca; //cria o nome do codigo
						modelo = (DefaultComboBoxModel) gui.cBoxPecas.getModel();
						modelo.addElement(cdTomboPeca);
						cdNovaPeca++;
					}
					//adiciona cdTomboPeca no modelo do comboBox de peças
					gui.cBoxPecas.setModel(modelo); //adiciona modelo no comboBox
					gui.cBoxPecas.setEnabled(true);
					this.testaAdd = true;
				}
				else
				{
					/* Se Existir peças cadastradas para o animal, então uma peça base deve ser selecionada
					 *	se isso não ocorrer uma mensagem é exibida 
					 */
					JOptionPane.showMessageDialog(null, "Selecione uma peça do animal");
				}
			}else
			{
				/*
				 * O sistema só aceita números positivos maiores que 0. para criar peças
				 * ou seja, logicamente não deve ser possivel criar -2 peças, por exemplo.
				 * E também o mínimo de novas peças é 1.
				 * Se isso não ocorrer uma mensagem é exibida
				 */
				JOptionPane.showMessageDialog(null, "Número de novas peças invalido");
			}
		} 
		/*
		 * É possivel criar novas peças apenas uma vez. 
		 * Se o usuário tentar criar novas peças mais de uma vez, uma mensagem de aviso será exibida
		 */
		else
			JOptionPane.showMessageDialog(null, "Já foi criado novas peças, termine o processo de inclusão para incluir novas peças"); //lança msg que criou a peça
	}

	/**
	 * Função que valida os dados da Peça na tela 2 e segue para tela 3.
	 * Nessa tela, obrigatóriamente uma peça deve ser criada e selecionada, e também é obrigatório a descrição
	 * do processo realizado na peça.
	 * Se essas informações não forem corretamente adicionadas o sistema mostra um aviso e não continua até as informações 
	 * obrigatórias seja adicionadas.
	 */
	private void validaDadosPecaTela2() {
		String msgErro = "";
		if(gui.cBoxPecas.getSelectedIndex() > 0){
			String cdTomboPeca = (String) gui.cBoxPecas.getSelectedItem();
			peca.setCdTomboPeca( Integer.parseInt((cdTomboPeca.substring(2, cdTomboPeca.length()))));
		} else
			msgErro += "Código da Peça não foi selecionado. \n";

		if( !gui.txtDsPeca.getText().equals("") )
			peca.setDsPeca(gui.txtDsPeca.getText());
		else
			msgErro += "Descrição da peça está vazio. \n";

		if( msgErro.equals("") ){
			gui.textNmUser.setText(App.getUsuario().getNome());
			gui.tabbed.setEnabledAt(2, true); //habilita aba de processos
			gui.tabbed.setSelectedIndex(2); //move para a aba de processos
		} else
			JOptionPane.showMessageDialog(null, msgErro);
	}

	/**
	 * Função que valida os dados do processo da peça na tela 3 e segue para tela 4 de localidade 
	 * Nessa tela, obrigatóriamente deve ser selecionado o nome do processo realizado, e também é obrigatório o
	 * do responsável pelo processo realizado na peça.
	 * Se essas informações não forem corretamente adicionadas o sistema mostra um aviso e não continua até as informações 
	 * obrigatórias seja adicionadas.
	 */
	private void validaProcessoTela3() {
		String msgErro = "";
		if( gui.cBoxNmProcesso.getSelectedIndex() > 0)
			proc.setNmProcesso((String)gui.cBoxNmProcesso.getSelectedItem());
		else
			msgErro += "Selecione o nome do processo. \n";

		if( !gui.txtNmResp.getText().equals("") )
			pecaProc.setNmResponsavelProcesso(gui.txtNmResp.getText());
		else
			msgErro += "Nome do Responsável pelo processo está vazio. \n";

		if( msgErro.equals("") ){			
			gui.tabbed.setEnabledAt(3, true); //habilita aba de localidade
			gui.tabbed.setSelectedIndex(3); //move para a aba de localidade
		} else
			JOptionPane.showMessageDialog(null, msgErro);
	}

	/**
	 * Função adiciona a nova peça no banco de dados.
	 * Verifica se um local de armazenamento foi selecionado e se é um local apropriado. (Apenas um local de nível 4 é aceito, ou seja, 
	 * apenas um local específico dentro de uma sala, por exemplo, um freezer ou um armário).  
	 */
	private void adicionaPecaDesejada() {
		if((getNivel() == 4) && (isTreeSelected() == true)) {
			preencheDadosPeca(); // cria o objeto a ser cadastrado na base de dados
			preencheDadosProcesso(); // preenche os dados referentes ao processo realizado
			/*
			 * Verifica quantas peças devem ser criadas, se for mais de uma peça volta para tela 2, para que as demais peças sejam cadastradas.
			 */
			if(QtPeca > 1){
				configP2();
				limpaCamposP3();
				NovaPeca();
			}
			else
			{
			baixaPecaBase(); // Remove a peça base.
			gui.dispose();
			}
		}
		else
		{
			JOptionPane.showMessageDialog(gui, "Selecione o Local de Armazenamento");
		}
	}

	/**
	 * Método responsável por configurar a janela 2, quando mais de uma peça deve ser cadastrada.
	 * - Limpa os dados da peça enterior
	 * - Desabilita o campo de adição de novas peças
	 * - Remove a peça ja cadastrada
	 * - Desabilita o combobox de peça base e mantem selecionado a mesma peça base
	 */
	public void configP2(){
		select = gui.cBoxBase.getSelectedIndex();
		gui.chckbxEsteAnimalAinda.setSelected(false);;
		gui.cBoxPecas.removeItemAt(gui.cBoxPecas.getSelectedIndex());
		gui.cBoxPecas.validate();
		gui.txtAreaOssos.setText("");
		gui.chckbxDnaCartilagem.setSelected(false);
		gui.chckbxDnaTecidoMuscular.setSelected(false);
		gui.chckbxDNAVisceral.setSelected(false);
		gui.chckbxPeloAbdominal.setSelected(false);
		gui.chckbxPeloCabeaDorsal.setSelected(false);
		gui.chckbxPeloInterescapularDorsal.setSelected(false);
		gui.chckbxEscama.setSelected(false);
		gui.chckbxPena.setSelected(false);
		gui.spinner.setValue(1);
		gui.txtDsPeca.setText("");
	}

	/**
	 * Funçãoo que preenche os dados da peça para gravar no BD
	 */
	private void preencheDadosPeca() {
		// Pega o id da peça base
		if(gui.cBoxBase.getSelectedIndex() > 0){
			Peca pecaBase = new Peca();
			pecaBase = listPeca.get(gui.cBoxBase.getSelectedIndex()-1);
			peca.setIdPecaBase(pecaBase.getIdPeca());
		}
		peca.setBlAnimal(1);
		peca.setIdAnimal(a.getIdAnimal());
		verificaCheckBox();
		java.sql.Date dtEntrada = new java.sql.Date( ((Date) gui.spDtEntrada.getValue()).getTime() );
		peca.setDtEntrada(dtEntrada);
		java.sql.Date dtRetorno = new java.sql.Date( ((Date) gui.spDtSaida.getValue()).getTime() );
		peca.setDtRetorno(dtRetorno);
		peca.setDsOssos(gui.txtAreaOssos.getText());
		peca.setIdLocalArmazenado( getLocalComplemento().getIdComplemento() );

		peca.setIdEmprestimo(0); //0 pois nao possui ainda
		peca.setBlPecaEmprestada(0); //nao implementado emprestimo ainda

		PecaDAO pDAO = new PecaDAO();
		pDAO.insert((Data) peca ); // insere a peça na base de dados
	}

	/**
	 * Altera uma peça indicando que a peça foi usada como base para outras peças
	 */
	private void baixaPecaBase(){
		PecaDAO pDAO = new PecaDAO();
		pDAO.baixaPeca(2, peca.getIdPecaBase());
	}

	/**
	 * Função que preenche os dados do processo da peça para gravar no BD
	 */
	private void preencheDadosProcesso(){		
		PecaDAO pDAO = new PecaDAO();

		//dados do processo
		proc.setDsProcesso(gui.txtDsProc.getText());
		ProcessoDAO procDAO = new ProcessoDAO();
		procDAO.insert( proc );
		//dados da peca processo
		pecaProc.setIdPeca(pDAO.getLastIdPeca());
		pecaProc.setIdProcesso(procDAO.getLastIdProcesso());
		pecaProc.setDsProcessoRealizado(gui.txtDsProc.getText());//txtDsProcRealizado.getText());
		pecaProc.setUsuarioResponsavel(gui.textNmUser.getText());
		PecaProcessoDAO pecProcDAO = new PecaProcessoDAO();
		pecProcDAO.insert( (Data) pecaProc);
	}

	private void verificaCheckBox(){
		if( gui.chckbxPeloInterescapularDorsal.isSelected() ) 
			peca.setBlPeloInterescapularDorsal(1);
		else
			peca.setBlPeloInterescapularDorsal(0);

		if( gui.chckbxPeloCabeaDorsal.isSelected() )
			peca.setBlPeloCabecaDorsal(1);
		else
			peca.setBlPeloCabecaDorsal(0);

		if( gui.chckbxEscama.isSelected() )
			peca.setBlEscama(1);
		else
			peca.setBlEscama(0);

		if( gui.chckbxPena.isSelected() )
			peca.setBlPena(1);
		else
			peca.setBlPena(0);

		if( gui.chckbxPeloAbdominal.isSelected() )
			peca.setBlPeloAbdominal(1);
		else
			peca.setBlPeloAbdominal(0);

		if( gui.chckbxDNAVisceral.isSelected() )
			peca.setBlDnaTecidoVisceral(1);
		else
			peca.setBlDnaTecidoVisceral(0);

		if( gui.chckbxDnaTecidoMuscular.isSelected() )
			peca.setBlDnaTecidoMuscular(1);
		else
			peca.setBlDnaTecidoMuscular(0);

		if( gui.chckbxDnaCartilagem.isSelected() )
			peca.setBlDnaCartilagem(1);
		else
			peca.setBlDnaCartilagem(0);
	}

	//---- funções para manipular arvore de local

	public void setLocal(Local local){
		this.local = local;
	}

	public void setBloco(Bloco bloco){
		this.bloco = bloco;
	}

	public Bloco getBloco(){
		return bloco;
	}

	public Local getLocal(){
		return local;
	}

	public Sala getSala(){
		return sala;
	}

	public void setSala(Sala sala){
		this.sala = sala;
	}

	public LocalComplemento getLocalComplemento(){
		return localComplemento;
	}

	public void setLocalComplemento(LocalComplemento localComp){
		this.localComplemento = localComp;
	}

	public boolean isTreeSelected() {
		return treeSelected;
	}

	public void setTreeSelected(boolean treeSelected) {
		this.treeSelected = treeSelected;
	}

	public void adicionarArvore(){

		int lvl = getNivel();
		if(lvl == -1){
			JOptionPane.showMessageDialog(null, "Selecionar qual nivel da Arvore desejas Adicionar !");
		}
		else{


			String loc = "";
			String ds = "";

			if(nivel == 0){
				loc = "Entre com o Local a ser Cadastrado!!!";
				loc = JOptionPane.showInputDialog(loc);
			}
			else 
				if(nivel == 1){
					loc = "Entre com o Bloco a ser Cadastrado!!!";
					loc = JOptionPane.showInputDialog(loc);
				}
				else 
					if(nivel == 2){
						loc = "Entre com a Sala a ser Cadastrada!!!";
						loc = JOptionPane.showInputDialog(loc);
					}
					else 
						if(nivel == 3){
							LocalGUI local = new LocalGUI();
							loc = local.getNome();
							ds = local.getDesc();
						}


			if((!loc.equals("")) && (isTreeSelected())){

				if(nivel == 0){
					LocalDAO dao = new LocalDAO();
					local = new Local();
					local.setNmLocal(loc);
					dao.insert(local);

				}
				if(nivel == 1){
					BlocoDAO dao = new BlocoDAO();
					dao.insert(getLocal(), loc);

				}else
					if(nivel == 2){
						SalaDAO dao = new SalaDAO();
						dao.insert(getBloco(), loc);
					}else
						if(nivel == 3){
							LocalComplementoDAO dao = new LocalComplementoDAO();

							dao.insert(getSala() , loc,ds);

						}

				TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());			//pega os novos dados do BD e cria um novo TreeMOdel															//Set o novo modelo na arvore
				gui.tree.setModel(arvoreModel);
				gui.tree.updateUI();
			}
			setTreeSelected(false);
			setNivel(-1);
		}
	}

	public void alterarArvore(){
		int nivel = getNivel();
		String loc = "";
		String ds = "";

		int n = JOptionPane.showConfirmDialog(null, "O Local está Selecionado ?");

		if((n == JOptionPane.YES_OPTION) && (isTreeSelected())){

			if(nivel == 1){

				loc = JOptionPane.showInputDialog("Entre com o novo nome do  Local a ser alterado!!!");
			}
			else 
				if(nivel == 2){

					loc = JOptionPane.showInputDialog( "Entre com o novo nome do Bloco a ser alterado!!!");
				}
				else 
					if(nivel == 3){

						loc = JOptionPane.showInputDialog("Entre com o novo nome da Sala a ser alterado!!!");
					}
					else 
						if(nivel == 4){
							LocalGUI local = new LocalGUI();
							loc = local.getNome();
							ds = local.getDesc();
						}


			if(!loc.equals("")){

				if(nivel == 1){
					LocalDAO dao = new LocalDAO();
					local.setNmLocal(loc);
					dao.saveDB(local);	//da um update na tabela

				}
				if(nivel == 2){
					BlocoDAO dao = new BlocoDAO();
					bloco.setNmBloco(loc);
					dao.saveDB(bloco);

				}else
					if(nivel == 3){
						SalaDAO dao = new SalaDAO();
						sala.setNmSala(loc);
						dao.saveDB(sala);
					}else
						if(nivel == 4){
							LocalComplementoDAO dao = new LocalComplementoDAO();
							localComplemento.setNmComplemento(loc);
							localComplemento.setDsComplemento(ds);
							dao.saveDB(localComplemento);

						}
				TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());	//pega os novos dados do BD e cria um novo TreeMOdel														//Set o novo modelo na arvore
				gui.tree.setModel(arvoreModel);
				gui.tree.updateUI();
			}
		}
	}

	@SuppressWarnings("static-access")
	public void removerArvore(){

		if(isTreeSelected()){
			JOptionPane msg = new JOptionPane();
			int nivel = getNivel();
			if(nivel == 4){
				int option = msg.showConfirmDialog(null, "Deseja Remover o Local?", "CUIDADO", 0);
				if(option == msg.OK_OPTION){
					PecaDAO pDAO = new PecaDAO();
					if(pDAO.emptyLocal(localComplemento.getIdComplemento()))
					{
						LocalComplementoDAO dao = new LocalComplementoDAO();
						dao.deleteDB(localComplemento);
					}
					else{
						JOptionPane.showMessageDialog(gui, "O Complemento não pode ser apagado!");
					}
				}
			}
			else
				JOptionPane.showMessageDialog(gui, "Apenas um Complemento pode ser apagado!");
		}
		else
			JOptionPane.showMessageDialog(gui, "Nada Selecionado");

		setTreeSelected(false);
		setNivel(-1);

		TreeModel arvoreModel = (new HierarquiaArvoreLocalController().getTreeModel());			//pega os novos dados do BD e cria um novo TreeMOdel
		gui.tree.setModel(arvoreModel);
		gui.tree.updateUI();
	}

	//set e get dos niveis selecionados da arvores
	private void setNivel(int nivel){
		this.nivel = nivel;
	}

	private int getNivel(){
		return nivel;
	}
}
