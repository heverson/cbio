package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.dados.Baixa;
import br.edu.angloamericano.cbio.dao.BaixaDAO;
import br.edu.angloamericano.cbio.gui.BaixaGUI;

public class BaixaController {

	BaixaGUI gui;
	private int idDel = -1;
	private BaixaDAO baixa;

	/*
	 * 
	 */
	public BaixaController(JDialog Super){
		gui = new BaixaGUI(Super);
		gui.Ouvintes(new Ouvintes());
		gui.setModal(true);
		gui.setVisible(true);
	}

	private class Ouvintes implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent ev) {
			// TODO Auto-generated method stub
			if(ev.getSource() == gui.btnSalvar){
				if(gui.textArea.getText().equals("")){
					JOptionPane.showMessageDialog(gui, "Desreva o motivo da Baixa");
				}
				else
				{
					AtualizaBase();
					JOptionPane.showMessageDialog(gui, "Operação realizada com sucesso");
					gui.dispose();
				}
			}
			if(ev.getSource() == gui.btnCancelar){
				gui.dispose();
			}
		}		
	}
	
	private void AtualizaBase(){
		Baixa Delete = new Baixa();
		Delete.setDesc(gui.textArea.getText());
		Delete.setIdUser(App.getUsuario().getId());
		
		java.sql.Date dtBaixa = new java.sql.Date( ((Date) gui.spinner.getValue()).getTime() );
		Delete.setDtBaixa(dtBaixa);
		
		try {
			baixa = new BaixaDAO();
			baixa.insert(Delete);
			setIdBaixa(baixa.getLastId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}
	}
	
	// seta o id da baixa cadastrada
	private void setIdBaixa(int idBaixa){
		this.idDel = idBaixa;
	}
	
	// retorna o Id da baixa cadastrada
	public int getIdBaixa(){
		return idDel;
	}
}
