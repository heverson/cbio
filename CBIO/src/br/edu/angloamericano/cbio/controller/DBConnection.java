package br.edu.angloamericano.cbio.controller;

import java.sql.Connection;
import java.sql.DriverManager;

import br.edu.angloamericano.cbio.util.FileTXT;

/**
 * @author mdmatrakas
 * 
 */
public class DBConnection
{
    private static Connection connection = null; // gerencia a conexão
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";        
    private static String DATABASE_URL = "";//"jdbc:mysql://localhost/bd_cbio";
    private static String DB_NAME = "bd_cbio"; //nome da base de dados
    private static String USR = "";//nome do usuário da base de dados
    private static String PASS = "";//senha do usuário da base de dados

    private static int connectionCount = 0;
    
    public static Connection getConnection()
    {
    	if(connection == null)
    	{
		    try 
		    {
				Class.forName(JDBC_DRIVER); // carrega classe de driver do banco
											// de dados
		
		       //PASS = JOptionPane.showInputDialog("Senha", "Digite a senha de acesso ao MySql");
		       
				try {
					//Carrega as configurações dea cesso a base de dados
					carregaConfig();
					 // estabelece conexão com o banco de dados
					connection = DriverManager.getConnection( DATABASE_URL, USR, PASS );
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("ERRO no arquivo de configuração \n -> " + e);
				}	      
		    }
		    catch (ClassNotFoundException classNotFound)                     
		    {                                                                  
		       classNotFound.printStackTrace();            
		       System.exit( 1 );                                               
		    } 
    	}
    	connectionCount++;
    	return connection;
    }
    
    // Verifica se existe conexão com o banco de dados
    public static boolean isConnected()
    {
    	if(connection == null)
    		return false;
    	return true;
    }
    
    // Fecha a conexão com o banco de dados
    public static void closeConnection() throws IllegalStateException
    {
    	if(connectionCount == 0) 
    		throw new IllegalStateException("Not Connected to Database");
    	connectionCount--;
    	if(connectionCount == 0)
    	{
	        try                                                        
	        {                                                          
	           connection.close();                                     
	        } 
	        catch ( Exception exception )                              
	        {                                                          
	           exception.printStackTrace();                            
	           System.exit( 1 );                                       
	        } 
    	}
    }
    
    /*
     * Abre o arquivo de configuração e extari o nome do local onde está a base de dados
     * o nome do usuário para acessar a base de dados
     * e a senha para o usuário
     */
	public static void carregaConfig() throws Exception {
		String dados = FileTXT.readFileTXT("files/config");
		String computador = null;
		String loginSQL = null;
		String senhaSQL = null;

		int ini = -1, fim = -1, cont = 0;
		for(int i=0; i<dados.length(); i++){
			if(dados.charAt(i) == '\"'){
				if(ini == -1)
					ini = i;
				else
					if(ini >= 0){
						cont++;
						fim = i+1;
						if(cont == 1)
							computador = dados.substring(ini, fim).replace("\"", "");
						if(cont == 2)
							loginSQL = dados.substring(ini, fim).replace("\"", "");
						if(cont == 3)
							senhaSQL = dados.substring(ini, fim).replace("\"", "");
						ini = -1;
						fim = -1;
					}
			}
		}
		if(computador == null || loginSQL == null || senhaSQL == null)
			throw new Exception();
		else{ 
			/*
			 * Se a laitura dos dados foi feita corretamente, então carragas as informações
			 * para as respectivas variaveis.
			 */
			setComputador(computador);
			setLoginSQL(loginSQL);
			setSenhaSQL(senhaSQL);
		}
	}
	
	// Método privado utilizado para configurar a URL com o endereço da base de dados
	private static void setComputador(String computador){
		DATABASE_URL = "jdbc:mysql://" + computador + "/" + DB_NAME;
	}
	
	// Método privado utilizado para definir o login utilizado para acessar a base de dados.
	private static void setLoginSQL(String loginSQL){
		USR = loginSQL;
	}
	
	// Método privado utilizado para definir a senha do usuario da base de dados
	private static void setSenhaSQL(String senhaSQL){
		PASS = senhaSQL;
	}
}
