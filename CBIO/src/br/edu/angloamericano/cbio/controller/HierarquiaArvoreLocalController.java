package br.edu.angloamericano.cbio.controller;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Local;
import br.edu.angloamericano.cbio.dados.LocalComplemento;
import br.edu.angloamericano.cbio.dados.Sala;
import br.edu.angloamericano.cbio.dao.BlocoDAO;
import br.edu.angloamericano.cbio.dao.LocalComplementoDAO;
import br.edu.angloamericano.cbio.dao.LocalDAO;
import br.edu.angloamericano.cbio.dao.SalaDAO;

public class HierarquiaArvoreLocalController implements TreeModel{

	DefaultTreeModel innerModel;

	DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Local");// = new DefaultMutableTreeNode("Local");

	DefaultMutableTreeNode blocoNode = new DefaultMutableTreeNode();// = new DefaultMutableTreeNode("Bloco");

	DefaultMutableTreeNode salaNode = new DefaultMutableTreeNode();// = new DefaultMutableTreeNode("Sala");

	DefaultMutableTreeNode complementoNode = new DefaultMutableTreeNode();// = new DefaultMutableTreeNode("Complemento");

	List<Local> listLocal = new LinkedList<Local>();
	List<Bloco> listBloco = new LinkedList<Bloco>();
	List<Sala> listSala = new LinkedList<Sala>();

	public void setListas(){

		LocalDAO localDAO = new LocalDAO();
		localDAO.setQuery();
		listLocal = localDAO.getList();

		BlocoDAO blocoDAO = new BlocoDAO();
		blocoDAO.setQuery();
		listBloco = blocoDAO.getList();

		SalaDAO salaDAO = new SalaDAO();
		salaDAO.setQuery();
		listSala = salaDAO.getList();
	}


	public TreeModel getTreeModel(){
		return innerModel;
	}

	@SuppressWarnings("unused")
	public  HierarquiaArvoreLocalController() {
		innerModel = new DefaultTreeModel(rootNode);

		Local localKeyName;
		Bloco blocoKeyName;
		Sala salaKeyName;
		LocalComplemento complementoKeyName;

		Object valueForKey;

		setListas();

		complementoKeyName = new LocalComplemento();

		BlocoDAO blocoDAO = new BlocoDAO();
		SalaDAO salaDAO = new SalaDAO();
		LocalComplementoDAO compDAO = new LocalComplementoDAO();

		Iterator<Local> it = listLocal.iterator();
		while(it.hasNext()){
			DefaultMutableTreeNode newLocalNode;

			try {

				localKeyName = it.next();
				valueForKey = localKeyName.getNmLocal();

				newLocalNode = new DefaultMutableTreeNode(localKeyName);            //retorna local.toString(); nao endereco de mem cria um novo N� do local

				blocoDAO.setQueryLocal(localKeyName.getIdLocal());					//faz consulta dos blocos que estao cadastrados neste Local

				/** insere os blocos dentro de seu local*/
				Iterator<Bloco> itBloco = blocoDAO.getList().iterator();		//iteracao para fazer while da lista
				while(itBloco.hasNext()){

					blocoKeyName = itBloco.next();

					if(blocoKeyName.getIdLocal() == localKeyName.getIdLocal()){
						blocoNode = new DefaultMutableTreeNode(blocoKeyName);

						salaDAO.setQueryBloco(blocoKeyName.getIdBloco());
						/** insere a sala dentro do bloco*/
						Iterator<Sala> itSala = salaDAO.getList().iterator();
						while(itSala.hasNext()){

							salaKeyName = itSala.next();
							if(salaKeyName.getIdBloco() == blocoKeyName.getIdBloco()){
								salaNode = new DefaultMutableTreeNode(salaKeyName);

								compDAO.setQuerySala(salaKeyName.getIdSala());
								//insere complemento dentro da sala
								Iterator<LocalComplemento> itComp = compDAO.getList().iterator();
								while(itComp.hasNext()){

									complementoKeyName = itComp.next();
									if(complementoKeyName.getIdSala() == salaKeyName.getIdSala()){
										complementoNode = new DefaultMutableTreeNode(complementoKeyName);
										salaNode.add(complementoNode);
									}


								}
								blocoNode.add(salaNode);
							}
						}
						newLocalNode.add(blocoNode);
					}
				}
				innerModel.insertNodeInto(newLocalNode, rootNode, 0);


			}catch (NullPointerException e) {
			}

		} 

	}  

	public Object getRoot() {
		return innerModel.getRoot();
	}

	public Object getChild(Object parm1, int parm2) {
		return innerModel.getChild(parm1, parm2);
	}

	public int getChildCount(Object parm1) {
		return innerModel.getChildCount(parm1);
	}

	public boolean isLeaf(Object parm1) {
		return innerModel.isLeaf(parm1);
	}

	public void valueForPathChanged(TreePath parm1, Object parm2) {
		innerModel.valueForPathChanged(parm1, parm2);
	}

	public int getIndexOfChild(Object parm1, Object parm2) {
		return innerModel.getIndexOfChild(parm1, parm2);
	}

	public void addTreeModelListener(TreeModelListener parm1) {
		innerModel.addTreeModelListener(parm1);
	}

	public void removeTreeModelListener(TreeModelListener parm1) {
		innerModel.removeTreeModelListener(parm1);
	}
}


