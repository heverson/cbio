package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import br.edu.angloamericano.cbio.dados.Login;
import br.edu.angloamericano.cbio.dados.Usuario;
import br.edu.angloamericano.cbio.dao.LoginDAO;
import br.edu.angloamericano.cbio.dao.TipoUserDAO;
import br.edu.angloamericano.cbio.dao.UsuarioDAO;
import br.edu.angloamericano.cbio.gui.UsuarioGUI;

public class UsuarioController {
	private UsuarioGUI gui;
	private List<Usuario> listUsers = new LinkedList<Usuario>();
	private Usuario usuario;
	private UsuarioDAO uDAO;
	private LoginDAO lDAO;
	String[] colunaPrincipal = {"ID", "Nome", "Cargo", "Tipo", "Registro"};
	String[] Filtros = {":: Buscar Por ::", "Nome", "Cargo", "Tipo", "Registro"};
	String[][] dadosUsers;

	public UsuarioController(JFrame Super){
		gui = new UsuarioGUI(Super);
		gui.addOuvintes(new Ouvinte());
		carregaFiltros();
		gui.setModal(true);
		gui.setVisible(true);
	}

	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			if(ev.getSource() == gui.btnAdicionar){
				try {
					new CadastroUsuarioController(gui);
					limpaBusca();
				} catch (Exception e) {
					System.out.println("ERRO: " + e);
				}
			}
			if(ev.getSource() == gui.btnAlterar){
				alterarUsuario();
			}
			if(ev.getSource() == gui.btnConsultar){
				if(gui.cbxInativos.isSelected())
					gui.btnRemover.setEnabled(false);
				else
					gui.btnRemover.setEnabled(true);
				AllUsers();
			}
			if(ev.getSource() == gui.btnRemover){
				removeUsuario();
				limpaBusca();
			}
			if(ev.getSource() == gui.btnProcurar){
				if(gui.buscaPor.getSelectedIndex() > 0)
				{
					if(gui.cbxInativos.isSelected())
						gui.btnRemover.setEnabled(false);
					else
						gui.btnRemover.setEnabled(true);
					listarByFiltro(gui.buscaPor.getSelectedIndex());
				}
				else{
					JOptionPane.showMessageDialog(gui, "Selecione um filtro de busca");
				}
			}
		}
	}

	/*
	 * Método que carrega os filtros
	 */
	private void carregaFiltros(){
		DefaultComboBoxModel modeloFiltros = new DefaultComboBoxModel();
		for(int i  = 0; i < Filtros.length; i++){
			modeloFiltros.addElement(Filtros[i]);
		}
		gui.buscaPor.setModel(modeloFiltros);	
	}

	/*
	 * Lista os usuário de acordo com o filtro selecionado
	 */
	private void listarByFiltro(int filtro){
		try {
			uDAO = new UsuarioDAO();
			switch (filtro) {
			case 1: // Busca e lista os usuario por nome
				listUsers = uDAO.userByFiltro("Nome", gui.textField.getText());
				break;
			case 2: // Busca e lista os usuário por cargo
				listUsers = uDAO.userByFiltro("Cargo", gui.textField.getText());
				//				JOptionPane.showMessageDialog(gui, "Busca por cargo - Não Implementado");
				break;
			case 3: // Busca e lista os usuário por tipo de usuário
			{
				TipoUserDAO tuDAO = new TipoUserDAO();
				int idTipo = tuDAO.getIdTipo(gui.textField.getText());
				if(idTipo != 0)
					listUsers = uDAO.userByIdTipo(idTipo);
				break;
			}
			case 4: // Busca e lista os usuário por número de registro
				listUsers = uDAO.userByFiltro("Registro", gui.textField.getText());
				break;
			default:
				break;
			}
			// Chama a funcão que monta e mostra a tabela
			if(!gui.cbxInativos.isSelected())
				montarTabela(0);
			if(gui.cbxInativos.isSelected())
				montarTabela(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Lista todos os usuários cadastrados
	 */
	private void AllUsers(){
		uDAO = new UsuarioDAO();
		try {
			listUsers = uDAO.getAllUsers();
			if(!gui.cbxInativos.isSelected())
				montarTabela(0);
			if(gui.cbxInativos.isSelected())
				montarTabela(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Método que monta a tabela com os usuário a serem mostrados
	 */
	private void montarTabela(int flag){
		String[][] dadosUserAux = null;
		dadosUsers = null;
		if(listUsers.size() > 0){ // Se a lista não for vazia, carrega os dados na tabela
			Iterator<Usuario> it = listUsers.iterator(); //cria iterator para percorrer a lista
			int count = 0; //contador das linhas
			TipoUserDAO tDAO = new TipoUserDAO(); // Objeto para buscar a informação de tipo do usuário na base de dados
			dadosUserAux = new String[listUsers.size()][5]; 
			while( it.hasNext() ){ // Para cada usuario da lista cria uma linha na tabela
				usuario = new Usuario();
				usuario = it.next();
				if(usuario.getBlUsuario() == flag){ // De acordo com a flag, mostra os usuário ativos ou os inativos
					dadosUserAux[count][0] = String.format("%d",usuario.getId());
					dadosUserAux[count][1] = usuario.getNome();
					dadosUserAux[count][2] = usuario.getCargo();
					dadosUserAux[count][3] = tDAO.getTipoUser(usuario.getTipo());
					dadosUserAux[count][4] = usuario.getRegistro();
					count++;
				}
			}
			/*
			 * Cria a tabela a ser mostrada. Se mostrar a tabela auxiliar (montada acima) é exibido 
			 * linhas em branco, já que a tabela criada é do tamanho da lista de usuários na base de dados, mas como não são mostrados todos os usuários
			 * e sim apenas os disponíveis ou apenas os não disponíveis (Dependendo da flag recebida). Por isso é necessário criar uma tabela
			 * com o tamanho exato de usuários a serem mostradas. O número de linhas necessárias está na variavel count, já que, quando um usuário deve ser mostrado
			 * a variavel count é incrementada.
			 */
			if(count != 0){
				dadosUsers = new String[count][5];
				for(int i = 0; i < dadosUsers.length; i++){
					dadosUsers[i] = dadosUserAux[i];
				}
			}
		}
		// Adiciona a tabela a interface gráfica
		DefaultTableModel dtm = new DefaultTableModel(dadosUsers, colunaPrincipal);
		gui.tabela.setModel(dtm);		
	}

	/*
	 * Remove o usuário selecionado.
	 */
	private void removeUsuario(){

		try{
			// Verifica se uma linha da tabela está selecionada
			if(gui.tabela.getSelectedRow() > -1){// se tiver alguma linha selecionada na tabela consultada
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				int idUser = Integer.parseInt((String) gui.tabela.getValueAt(linhaSelecionada, 0)); //pega o id do Usuário
				try{
					lDAO = new LoginDAO();
					Login login = new Login();
					usuario = uDAO.getUsuario(idUser);
					uDAO = new UsuarioDAO();
					uDAO.setUsuario(usuario);
					login.setIdUsuaio(idUser);
					lDAO.setLogin(login);
					try{
						uDAO.baixaDB(1);
						lDAO.deleteDB();
					}catch(Exception e){
						System.out.println("ERRO: " + e);
					}
				}
				catch(Exception e){
					System.out.println("ERRO: " + e);
				}
			}else{
				JOptionPane.showMessageDialog(gui, "Selecione um Usuário");
			}
		} catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ERRO: " + e);
		}
	}

	/*
	 * Pega o usuário selecionado e carrega seus dados para serem alterados.
	 */
	private void alterarUsuario(){
		try{
			// Verifica se uma linha da tabela está selecionada
			if(gui.tabela.getSelectedRow() > -1){// se tiver alguma linha selecionada na tabela consultada
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				int idUser = Integer.parseInt((String) gui.tabela.getValueAt(linhaSelecionada, 0)); //pega o id do Usuário
				try{
					uDAO = new UsuarioDAO();	
					Usuario user = new Usuario();
					user = uDAO.getUsuario(idUser);
					new CadastroUsuarioController(gui, user); // Abre a janela de alteração e passa o usuário como parâmetro
				}
				catch(Exception e){
					e.printStackTrace();
					System.out.println("ERRO: " + e);
				}
			}else{
				JOptionPane.showMessageDialog(gui, "Selecione um Usuário");
			}
		} catch (ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("ERRO: " + e);
		}
	}

	/*
	 * Método que limpa a tabela de busca
	 */
	private void limpaBusca(){
		// Adiciona a tabela a interface gráfica
		dadosUsers = null;
		DefaultTableModel dtm = new DefaultTableModel(dadosUsers, colunaPrincipal);
		gui.tabela.setModel(dtm);
	}
}
