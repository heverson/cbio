package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import br.edu.angloamericano.cbio.dao.DadosDAO;
import br.edu.angloamericano.cbio.gui.CadastroDadosGUI;

// Classe genérica de cadastros menores criada para não haver necessidade
// da criação de várias classes de cadastros pequenos como: cadastro de cidades,
// localidades, classes de animais, família... Enfim, o objetivo é reaproveitar a mesma classe
// simplesmente reajustando a janela conforme o tipo de cadastro.
public class CadastroDadosController {
	private CadastroDadosGUI gui;
	private int flag;
 
	// CONSTRUTOR - A flag identifica que tipo de janela será:
	// flag = 1 - Cadastro de cidade;
	// flag = 2 - Cadastro de localidade;
	// flag = 3 - Cadastro de ordem;
	// flag = 4 - Cadastro de família;
	// flag = 5 - Cadastro de tribo;
	public CadastroDadosController(int flag, JDialog Super) {
		this.flag = flag;
		gui = new CadastroDadosGUI(Super);
		gui.configuraJanela(flag);
		carregaInfo(flag);
		gui.addOuvintes(new Ouvinte());
		gui.setModal(true);
		gui.setVisible(true);
	}

	private void carregaInfo(int flag) {
		if(flag == 1 || flag == 2){ // Caso seja cadastro de cidades, carrega-se os estados
			List<String> estados = new ArrayList<String>();
			try {
				estados = DadosDAO.getEstados();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui,
				"Os estados não puderam ser carregados!");
			}
			DefaultComboBoxModel modeloEstados = new DefaultComboBoxModel();
			modeloEstados.addElement(":: Selecione um estado ::");
			if(estados != null)
				for(int i=0; i<estados.size(); i++)
					modeloEstados.addElement(estados.get(i));

			gui.estado.setModel(modeloEstados);
		}else
			if(flag == 3 || flag == 4 || flag == 5 || flag == 6){
				List<String> classes = new ArrayList<String>();
				try {
					classes = DadosDAO.getClasses();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui,
					"As classes não puderam ser carregadas!");
				}
				DefaultComboBoxModel modeloClasses = new DefaultComboBoxModel();
				modeloClasses.addElement(":: Selecione uma classe ::");
				if(classes != null)
					for(int i=0; i<classes.size(); i++)
						modeloClasses.addElement(classes.get(i));

				gui.classe.setModel(modeloClasses);
			}
	}

	private class Ouvinte implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent ev) {
			if(ev.getSource() == gui.btnAdd){
				gui.reconfigLabels();
				if(gui.validar(flag)){
					if(flag == 1){ // Cadastro de cidade
						try{
							DadosDAO.addCidade(gui.cidade.getText(), 
									String.format("%s",gui.estado.getSelectedItem()));
							estado = String.format("%s",gui.estado.getSelectedItem());
							JOptionPane.showMessageDialog(gui, "Cidade adicionada com sucesso!");
							gui.dispose();
						}catch(Exception ex){
							if(ex.getMessage().contains("Duplicate entry")){
								JOptionPane.showMessageDialog(gui, 
								"Já existe uma cidade com este nome!");
							}
						}
					}else
						if(flag == 2){ // Cadastro de localidade
							try{
								DadosDAO.addLocalidade(gui.localidade.getText(), 
										String.format("%s",gui.estado.getSelectedItem()),
										String.format("%s",gui.cidadeCombo.getSelectedItem()));
								cidade = String.format("%s",gui.cidadeCombo.getSelectedItem());
								JOptionPane.showMessageDialog(gui, "Localidade adicionada com sucesso!");
								gui.dispose();
							}catch(Exception ex){
								if(ex.getMessage().contains("Duplicate entry")){
									JOptionPane.showMessageDialog(gui, 
									"Já existe uma localidade com este nome!");
								}
							}
						}else
							if(flag == 3){ // Cadastro de ordem
								try{
									DadosDAO.addOrdem(gui.ordem.getText(), 
											String.format("%s",gui.classe.getSelectedItem()));
									classe = String.format("%s",gui.classe.getSelectedItem());
									JOptionPane.showMessageDialog(gui, "Ordem adicionada com sucesso!");
									gui.dispose();
								}catch(Exception ex){
									if(ex.getMessage().contains("Duplicate entry")){
										JOptionPane.showMessageDialog(gui, 
										"Já existe uma ordem com este nome!");
									}
								}
							}else
								if(flag == 4){ // Cadastro de fam�lia
									try{
										DadosDAO.addFamilia(gui.familia.getText(), 
												String.format("%s",gui.ordemCombo.getSelectedItem()));
										ordem = String.format("%s",gui.ordemCombo.getSelectedItem());
										JOptionPane.showMessageDialog(gui, "Família adicionada com sucesso!");
										gui.dispose();
									}catch(Exception ex){
										if(ex.getMessage().contains("Duplicate entry")){
											JOptionPane.showMessageDialog(gui, 
											"Já existe uma família com este nome!");
										}
									}
								}else
									if(flag == 5){ // Cadastro de tribo
										try{
											DadosDAO.addTribo(gui.tribo.getText(), 
													String.format("%s",gui.familiaCombo.getSelectedItem()));
											familia = String.format("%s",gui.familiaCombo.getSelectedItem());
											JOptionPane.showMessageDialog(gui, "Tribo adicionada com sucesso!");
											gui.dispose();
										}catch(Exception ex){
											if(ex.getMessage().contains("Duplicate entry")){
												JOptionPane.showMessageDialog(gui, 
												"Já existe uma tribo com este nome!");
											}
										}
									}else
										if(flag == 6){ // Cadastro de esp�cie
											try{
												DadosDAO.addEspecie(gui.especie.getText(), 
														String.format("%s",gui.triboCombo.getSelectedItem()));
												tribo = String.format("%s",gui.triboCombo.getSelectedItem());
												JOptionPane.showMessageDialog(gui, "Espécie adicionada com sucesso!");
												gui.dispose();
											}catch(Exception ex){
												if(ex.getMessage().contains("Duplicate entry")){
													JOptionPane.showMessageDialog(gui, 
													"Já existe uma espécie com este nome!");
												}
											}
										}
				}
			}

			if(ev.getSource() == gui.estado){
				if(flag == 2){ // S� carregar� as cidades se for cadastro de localidades, sen�o n�o precisa!
					if(gui.estado.getSelectedIndex() < 1){
						gui.cidadeCombo.removeAllItems();
						gui.cidadeCombo.addItem(":: Selecione uma cidade ::");
						gui.cidadeCombo.setEnabled(false);
					} else{
						List<String> cidades = null;
						try {
							cidades = DadosDAO.getCidades(
									String.format("%s",gui.estado.getSelectedItem()));
						} catch (Exception e) {
							JOptionPane.showMessageDialog(gui, "Não foi possível carregar as cidades!");
						}
						DefaultComboBoxModel modelo = new DefaultComboBoxModel();
						modelo.addElement(":: Selecione uma cidade ::");
						if(cidades != null)
							for(int i=0; i<cidades.size(); i++)
								modelo.addElement(cidades.get(i));
						gui.cidadeCombo.setModel(modelo);
						gui.cidadeCombo.setEnabled(true);
					}
				}
			}

			if(ev.getSource() == gui.classe){
				if(flag > 3){
					if(gui.classe.getSelectedIndex() < 1){
						gui.ordemCombo.removeAllItems();
						gui.ordemCombo.addItem(":: Selecione uma classe ::");
						gui.ordemCombo.setEnabled(false);
						gui.familiaCombo.removeAllItems();
						gui.familiaCombo.addItem(":: Selecione uma família ::");
						gui.familiaCombo.setEnabled(false);
					} else{
						List<String> ordens = null;
						try {
							ordens = DadosDAO.getOrdens(
									String.format("%s",gui.classe.getSelectedItem()));
						} catch (Exception e) {
							JOptionPane.showMessageDialog(gui, "Não foi possível carregar as ordens!");
						}
						DefaultComboBoxModel modelo = new DefaultComboBoxModel();
						modelo.addElement(":: Selecione uma ordem ::");
						if(ordens != null)
							for(int i=0; i<ordens.size(); i++)
								modelo.addElement(ordens.get(i));
						gui.ordemCombo.setModel(modelo);
						gui.ordemCombo.setEnabled(true);
					}
				}
			}
			
			if(ev.getSource() == gui.ordemCombo){
				if(flag > 4){
					if(gui.ordemCombo.getSelectedIndex() < 1){
						gui.familiaCombo.removeAllItems();
						gui.familiaCombo.addItem(":: Selecione uma família ::");
						gui.familiaCombo.setEnabled(false);
						gui.triboCombo.removeAllItems();
						gui.triboCombo.addItem(":: Selecione uma tribo ::");
						gui.triboCombo.setEnabled(false);
					} else{
						List<String> familias = null;
						try {
							familias = DadosDAO.getFamilias(
									String.format("%s",gui.ordemCombo.getSelectedItem()));
						} catch (Exception e) {
							JOptionPane.showMessageDialog(gui, "Não foi possível carregar as famílias!");
						}
						DefaultComboBoxModel modelo = new DefaultComboBoxModel();
						modelo.addElement(":: Selecione uma família ::");
						if(familias != null)
							for(int i=0; i<familias.size(); i++)
								modelo.addElement(familias.get(i));
						gui.familiaCombo.setModel(modelo);
						gui.familiaCombo.setEnabled(true);
					}
				}
			}
			
			if(ev.getSource() == gui.familiaCombo){
				if(flag == 6){
					if(gui.familiaCombo.getSelectedIndex() < 1){
						gui.triboCombo.removeAllItems();
						gui.triboCombo.addItem(":: Selecione uma tribo ::");
						gui.triboCombo.setEnabled(false);
					} else{
						List<String> tribos = null;
						try {
							tribos = DadosDAO.getTribos(
									String.format("%s",gui.triboCombo.getSelectedItem()));
						} catch (Exception e) {
							JOptionPane.showMessageDialog(gui, "Não foi possível carregar as tribos!");
						}
						DefaultComboBoxModel modelo = new DefaultComboBoxModel();
						modelo.addElement(":: Selecione uma tribo ::");
						if(tribos != null)
							for(int i=0; i<tribos.size(); i++)
								modelo.addElement(tribos.get(i));
						gui.triboCombo.setModel(modelo);
						gui.triboCombo.setEnabled(true);
					}
				}
			}
		} // fim do actionPerformed

	}

	private String estado = null;
	private String cidade = null;
	private String classe = null;
	private String ordem = null;
	private String familia = null;
	private String tribo = null;

	// Para o cadastro de cidade
	// Usado por classes externas para que saibam se houve cadastro (estado  onde a cidade foi cadastada != null)
	// e se o estado em que a cidade foi cadastrada � o mesmo que est� sendo mostrado em outro lugar.
	// Caso sim, deve se atualizar esse 'outro lugar'. Esse � o objetivo aqui.
	public String getEstado(){
		return estado;
	}

	// Mesmo caso acima. Por�m para cadastro de localidade
	public String getCidade(){
		return cidade;
	}

	// Mesmo caso acima. Por�m para cadastro de ordem
	public String getClasse(){
		return classe;
	}

	// Mesmo caso acima. Por�m para cadastro de fam�lia
	public String getOrdem(){
		return ordem;
	}

	// Mesmo caso acima. Por�m para cadastro de tribo
	public String getFamilia(){
		return familia;
	}
	
	// Mesmo caso acima. Por�m para cadastro de esp�cie
	public String getTribo(){
		return tribo;
	}
}
