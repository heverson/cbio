package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;


import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Cidade;
import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Coleta;
import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Endereco;
import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dados.Estado;
import br.edu.angloamericano.cbio.dados.Familia;

import br.edu.angloamericano.cbio.dados.Mamifero;
import br.edu.angloamericano.cbio.dados.Ordem;
import br.edu.angloamericano.cbio.dados.Tribo;
import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.CidadeDAO;
import br.edu.angloamericano.cbio.dao.ClasseDAO;
import br.edu.angloamericano.cbio.dao.ColetaDAO;
import br.edu.angloamericano.cbio.dao.EnderecoDAO;
import br.edu.angloamericano.cbio.dao.EspecieDAO;
import br.edu.angloamericano.cbio.dao.EstadoDAO;
import br.edu.angloamericano.cbio.dao.FamiliaDAO;
import br.edu.angloamericano.cbio.dao.MamiferosDAO;
import br.edu.angloamericano.cbio.dao.OrdemDAO;
import br.edu.angloamericano.cbio.dao.TriboDAO;
import br.edu.angloamericano.cbio.gui.CadastroAnimalGUI;

public class AlterarAnimalController {

		private CadastroAnimalGUI gui;
		
		private Classe classe;
		private Ordem ordem;
		private Tribo tribo;
		private Familia familia;
		private Especie especie;
		public Cidade cidade;
		private Coleta coleta;
		private Mamifero mamifero;
		private ClasseDAO classeDAO;
		private OrdemDAO ordemDAO;
		private TriboDAO triboDAO;
		private FamiliaDAO familiaDAO;
		
		public Endereco endereco;
		
		private List<String> estados = new ArrayList<String>();
		private List<Estado> listEstado = new LinkedList<Estado>();

		
		public Animal objAnimal = new Animal();
		
		
		public AlterarAnimalController(Animal objAnimal, JDialog Super){
			gui = new CadastroAnimalGUI(Super);
			gui.addOuvintes(new Ouvinte());
			gui.setTitle("Alterar Animal");
			gui.btnCadastrar.setText("Alterar Animal");
			this.objAnimal = objAnimal;
			
			carregaInfo();

			gui.setModal(true);
			gui.setVisible(true);
		}
		
		public void carregaDadosAnimal(){
			especie = new Especie();
			especie.setIdEspecie(objAnimal.getIdEspecie());
			EspecieDAO especieDAO = new EspecieDAO();
			especie = especieDAO.findEspecieById(especie.getIdEspecie());
			 triboDAO = new TriboDAO();
			 tribo = new Tribo();
			tribo = triboDAO.findTriboById(especie.getIdTribo());
			familiaDAO = new FamiliaDAO();
			familia = new Familia();
			familia = familiaDAO.findFamiliaById(tribo.getIdFamilia());
			ordemDAO = new OrdemDAO();
			ordem = new Ordem();
			ordem = ordemDAO.findOrdemById(familia.getIdOrdem());
			classeDAO = new ClasseDAO();
			classe = new Classe();
			classe = classeDAO.findClasseById(ordem.getIdClasse());

			gui.classe.addItem(classe);
			gui.classe.setSelectedIndex(1);
			gui.ordem.addItem(ordem);
			gui.ordem.setSelectedIndex(1);
			gui.familia.addItem(familia);
			gui.familia.setSelectedIndex(1);
			gui.tribo.addItem(tribo);
			gui.tribo.setSelectedIndex(1);
			gui.especie.addItem(especie);
			gui.especie.setSelectedIndex(1);
			
			gui.classe.setEnabled(false);
			gui.ordem.setEnabled(false);
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			
			gui.txtNmAnimal.setText(objAnimal.getNmAnimal());
			gui.txtNmCientifico.setText(objAnimal.getNmCientifico());
			gui.txtFieldCorPelagem.setText(objAnimal.getDsCor());
			gui.txtSinais.setText(objAnimal.getDsSinais());
			gui.cBoxIdade.setSelectedItem(objAnimal.getDsIdade());
			gui.peso.setValue(objAnimal.getNuPeso());
			
			if(objAnimal.getSgSexo() == 'M')
				gui.sexo.setSelectedIndex(1);
				else
					gui.sexo.setSelectedIndex(2);
			
			String tombo = objAnimal.getCdTombo();
			String tombo1 = tombo.substring(10,tombo.length());
			String tombo2 = tombo1.substring(0,2);
			String tombo3 = tombo1.substring(2,tombo1.length());
			
			gui.tombo2.setText(tombo2);
			gui.tombo3.setText(tombo3);
		}

		public void carregaDadosMamiferos(){
			// Carrega Mamiferos
			if(classe.getNmClasse().equals("Mamíferos")){
				MamiferosDAO mamiferoDAO = new MamiferosDAO();
				mamifero = mamiferoDAO.findMamiferoByIDAnimal(objAnimal.getIdAnimal());

				gui.canInfDir.setValue(mamifero.getNu_caninos_inferior_direito());
				gui.canSupDir.setValue(mamifero.getNu_caninos_superior_direito());
				gui.incSupDir.setValue(mamifero.getNu_incisivos_superior_direito());
				gui.incInfDir.setValue(mamifero.getNu_incisivos_inferior_direito());
				
				gui.molSupDir.setValue(mamifero.getNu_molares_superior_direito());
				gui.molInfDir.setValue(mamifero.getNu_molares_inferior_direito());
				
				gui.preMolSupDir.setValue(mamifero.getNu_molares_superior_direito());
				gui.preMolInfDir.setValue(mamifero.getNu_premolares_inferior_direito());
				
				gui.comprimentoCabeca.setValue(mamifero.getNu_comprimento_total_cabeca());
				gui.comprimentoCalda.setValue(mamifero.getNu_comprimento_total_calda());
				gui.comprimentoCorpo.setValue(mamifero.getNu_comprimento_total_corpo());
				gui.comprimentoMembroPost.setValue(mamifero.getNu_comprimento_membro_posterior());
				
				gui.larguraBaseOrelha.setValue(mamifero.getNu_largura_base_orelha());
				gui.larguraCabeca.setValue(mamifero.getNu_largura_cabeca());
				gui.larguraToraxica.setValue(mamifero.getNu_largura_toraxica());
				
				gui.textAreaDadosBiometricos.setText(objAnimal.getDsDadosBiometricos());
			}
		}
	
		public void carregaDadosEnderecoColeta(){
			//Carrega Estados Cidades Endereco Coleta
			DefaultComboBoxModel modeloEstados = new DefaultComboBoxModel();
			modeloEstados.addElement(":: Selecione um estado ::");
			if(estados != null)
				for(int i=0; i<listEstado.size(); i++)
					modeloEstados.addElement(listEstado.get(i));
			
			ColetaDAO coletaDAO = new ColetaDAO();
			coleta = coletaDAO.findColetaById(objAnimal.getIdColeta());
			gui.coletor.setText(coleta.getNmColetor());
			gui.objetivoColeta.setText(coleta.getDsObjetivoColeta());
			gui.metodoColeta.setText(coleta.getDsMetodoColeta());
			gui.origem.setText(coleta.getDsOrigemAnimal());
			
			gui.dtRecolhim.setValue(coleta.getDtRecolhimento());
			
			EnderecoDAO enderecoDAO = new EnderecoDAO();
			endereco = enderecoDAO.findEnderecoById(coleta.getIdEnderecoColeta());

			
			CidadeDAO cidadeDAO = new CidadeDAO();
			cidade = cidadeDAO.findCidadeById(endereco.getIdCidade());
			
			EstadoDAO estadoDAO = new EstadoDAO();
			Estado estado = estadoDAO.findEstadoById(cidade.getIdEstado());
	
			gui.estado.addItem(estado);
			gui.estado.setSelectedItem(estado);
			gui.estado.setEnabled(false);
			gui.cidade.addItem(cidade);
			gui.cidade.setSelectedItem(cidade);
			gui.cidade.setEnabled(false);
			gui.localidade.addItem(endereco);
			gui.localidade.setSelectedItem(endereco);
			gui.localidade.setEnabled(false);
			
			gui.localidadeAdd.setEnabled(false);
			gui.cidadeAdd.setEnabled(false);
		}
		private void carregaInfo() {
			try {
				carregaDadosAnimal();
				carregaDadosMamiferos();
				carregaDadosEnderecoColeta();	
				
			
			gui.tabbed.setEnabledAt(1, false);
			gui.tabbed.setEnabledAt(2, false);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui,
				"Estados ou/e classes não puderam ser carregados!");
				e.printStackTrace();
			}
		
		}

		private class Ouvinte implements ActionListener, MouseListener{
			
			public void actionPerformed(ActionEvent ev){
				
				//TAB 1 = P2
				if(ev.getSource() == gui.btnProximoP2){
					
					if(gui.especie.isEnabled()){
						if( ! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::"))
							especie = (Especie) gui.especie.getSelectedItem();
					}
					
					//se a classe selecionada for mamiferos ira abrir o painels dos dados biometricos para os animais mamiferos se nao ira direto para o painel de coleta
					
						
						if((! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::")) && (! gui.cBoxIdade.getSelectedItem().equals(":: Selecione uma idade ::")) && (! gui.txtNmAnimal.getText().equals(""))
								&& (! gui.txtNmCientifico.getText().equals("")) && (! gui.sexo.getSelectedItem().equals(":: Selecione o Sexo ::")) && (!gui.tombo3.equals("")) && (! gui.txtFieldCorPelagem.getText().equals("")) )
						{
							classe = (Classe )gui.classe.getSelectedItem();		
							if(classe.getNmClasse().equals("Mamíferos")){
								gui.tabbed.setEnabledAt(1,true);
								gui.tabbed.setSelectedIndex(1);	
							}
								else{
									gui.tabbed.setEnabledAt(2,true);
									gui.tabbed.setSelectedIndex(2);
								}
						}else
							 JOptionPane.showMessageDialog(gui, "POR FAVOR PREENCHER OS CAMPOS OBRIGATÓRIOS!", "erro", 0);
				}

				
				//TAB2 = p4
				if(ev.getSource() == gui.btnProximoP4){
					
					
						//fazer tratamento se os campos estao preenchidos
						gui.tabbed.setEnabledAt(2,true);
						gui.tabbed.setSelectedIndex(2);
						preencheColeta();
				}
				
				
				//TAB3 = p1 BOTAO PARA ALTERAR O ANIMAL!!!
				if(ev.getSource() == gui.btnCadastrar){
					//fazer tratamento se os campos estao preenchidos
					if(gui.localidade.isEnabled()){
						if(! gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::"))
							endereco = (Endereco) gui.localidade.getSelectedItem();
					}
					if((!gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::") || (gui.localidade.getSelectedItem() != null)) && (! gui.coletor.getText().equals("")) && (! gui.dtRecolhim.equals(null) )){
						MamiferosDAO mamiferoDAO = new MamiferosDAO();
						AnimalDAO animalDAO = new AnimalDAO();
						ColetaDAO coletaDAO = new ColetaDAO();
						
					
						preencheMamifero();			//pega os dados dos campos mamiferos e preence o objeto mamiferos
						preencheAnimal();			//pega os dados dos campos de animais e preenche o objeto animal
						preencheColeta();			//pega os dados dos campos de coleta e preenche o objeto coleta
						
						mamiferoDAO.setData((Data) mamifero);
						mamiferoDAO.saveDB();

						animalDAO.setData((Data) objAnimal);
						animalDAO.saveDB();

						coletaDAO.setData((Data)coleta);
						coletaDAO.saveDB();
						
						gui.setVisible(false);
						
					}else
						JOptionPane.showConfirmDialog(null,"Por favor preencher os campos obrigatorios. (*)");
				}

				if(ev.getSource() == gui.tombo3){
					if( (! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::")) && (gui.especie.getSelectedItem() != null)){
						gui.tombo3.setEnabled(true);
						gui.tombo3.setEditable(true);
					}
				}
				
				if(ev.getSource() == gui.calcularArcada) //se evento for na gui.calcularArcada calculaTotalArcada 
					gui.totalArcada.setText(String.format("%d",calculaTotalArcada()));	
				
				if(ev.getSource() == gui.limparArcada) //se evento for na gui.limparArcada limparArcada
					limparArcada();
			}
				
			public void mouseClicked(MouseEvent e) {
				if(e.getSource() == gui.tabbed){
					int n = gui.tabbed.getSelectedIndex();
					
					if(n == 0){
						gui.tabbed.setEnabledAt(1,false);
						gui.tabbed.setEnabledAt(2,false);
						  
					}else
						if(n==1){
							if(gui.tabbed.isEnabledAt(1)){
								gui.tabbed.setEnabledAt(2,false);
								  
							}
						}else
							if(n==2){
								
									  
							}
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

		} 
		
		/**
		 * Função que limpa os valores da arcada na GUI 
		 */
		private void limparArcada() {
			gui.canSupDir.setValue(0);
			gui.canInfDir.setValue(0);
			gui.incSupDir.setValue(0);
			gui.incInfDir.setValue(0);
			gui.preMolSupDir.setValue(0);
			gui.preMolInfDir.setValue(0);
			gui.molSupDir.setValue(0);
			gui.molInfDir.setValue(0);
			gui.totalArcada.setText("");
		}

		/**
		 * Função que preenche dados do mamifero
		 */
		private void preencheMamifero() {
				System.out.println(gui.comprimentoCorpo.getValue());//Float.valueOf(gui.comprimentoCorpo.getValue().toString()));//gui.comprimentoCorpo.getValue().toString());
				//Float.valueOf(gui.comprimentoCorpo.getValue().toString());
			mamifero.setNu_comprimento_total_corpo((Float) gui.comprimentoCorpo.getValue());//Float.valueOf(gui.comprimentoCorpo.getValue().toString()));
			mamifero.setNu_comprimento_total_cabeca(Float.valueOf(gui.comprimentoCabeca.getValue().toString()));
			mamifero.setNu_comprimento_total_calda( Float.valueOf(gui.comprimentoCalda.getValue().toString()));
			mamifero.setNu_comprimento_membro_posterior( Float.valueOf(gui.comprimentoMembroPost.getValue().toString()));
			mamifero.setNu_largura_cabeca( Float.valueOf(gui.larguraCabeca.getValue().toString()));
			mamifero.setNu_largura_toraxica( Float.valueOf(gui.larguraToraxica.getValue().toString()));
			mamifero.setNu_largura_base_orelha( Float.valueOf(gui.larguraBaseOrelha.getValue().toString()));
			
			mamifero.setNu_caninos_inferior_direito(Integer.valueOf(gui.canInfDir.getValue().toString()));
			mamifero.setNu_caninos_superior_direito(Integer.valueOf(gui.canSupDir.getValue().toString()));
			mamifero.setNu_incisivos_inferior_direito(Integer.valueOf(gui.incInfDir.getValue().toString()));
			mamifero.setNu_incisivos_superior_direito(Integer.valueOf(gui.incSupDir.getValue().toString()));
			mamifero.setNu_molares_inferior_direito(Integer.valueOf(gui.molInfDir.getValue().toString()));
			mamifero.setNu_molares_superior_direito(Integer.valueOf(gui.molSupDir.getValue().toString()));
			mamifero.setNu_premolares_inferior_direito(Integer.valueOf(gui.preMolInfDir.getValue().toString()));
			mamifero.setNu_premolares_superior_direito(Integer.valueOf(gui.preMolSupDir.getValue().toString()));
			
			mamifero.setNu_total_dentes(calculaTotalArcada());
		}
		
		/**
		 * Função que calcula valor total dos valores da arcada na GUI 
		 */
		private int calculaTotalArcada() {
			return  ( Integer.parseInt(gui.canSupDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.canInfDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.incSupDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.incInfDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.preMolSupDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.preMolInfDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.molSupDir.getValue().toString().substring(0,1)) +
					Integer.parseInt(gui.molInfDir.getValue().toString().substring(0,1))
					) * 2;
			
		}
		
		/**
		 * Apos verificar dados do animail monta o objeto animal para o insert
		 */
		private void preencheAnimal(){
			
			String tombo = gui.tombo1.getText()+ gui.tombo2.getText().toUpperCase()+gui.tombo3.getText(); //realizar a verificacao do gui.tombo3
			objAnimal.setCdTombo(tombo);
			objAnimal.setNmAnimal(gui.txtNmAnimal.getText());
			objAnimal.setNmCientifico(gui.txtNmCientifico.getText());
			//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
			objAnimal.setDsIdade(gui.cBoxIdade.getSelectedItem().toString());
			objAnimal.setNuPeso(Float.parseFloat(gui.peso.getValue().toString()));
			objAnimal.setDsSinais(gui.txtSinais.getText());
			//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
			objAnimal.setSgSexo(gui.sexo.getSelectedItem().toString().charAt(0));
			//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
			objAnimal.setDsCor(gui.txtFieldCorPelagem.getText());
			objAnimal.setDsDadosBiometricos(gui.textAreaDadosBiometricos.getText());
			//a.setIdColeta(idColeta); //buscar pela id coleta apos criar ela
			objAnimal.setIdEspecie(gui.especie.getSelectedIndex());
		}
		
		/**
		 * Apos verificar dados da coleta monta o objeto coleta para o insert
		 */
		private void preencheColeta(){
			coleta.setNmColetor(gui.coletor.getText());
			coleta.setDsMetodoColeta(gui.metodoColeta.getText());
			coleta.setDsObjetivoColeta(gui.objetivoColeta.getText());
			coleta.setDsOrigemAnimal(gui.origem.getText());
			coleta.setDtRecolhimento( (java.util.Date) gui.dtRecolhim.getValue() );
			
			
			//pego o endereco selecionado  e seta na tabela coleta o valor do FK_endereco_coleta
			if(!gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::")){
				endereco = (Endereco) gui.localidade.getSelectedItem();
				coleta.setIdEnderecoColeta(endereco.getIdEndereco());
			}
		}
}
