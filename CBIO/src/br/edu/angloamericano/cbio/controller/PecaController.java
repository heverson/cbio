package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Peca;
import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.PecaDAO;
import br.edu.angloamericano.cbio.gui.PecaGUI;


/**
 * Classe de controle da interface de peça.
 * Chama as determinadas classes ou métodos responsáveis por:
 * - Listar todas as peças (Disponíveis e não disponíveis)
 * - Buscar as peças por número de tombo de um animal
 * - Inserir uma nova peça
 * - Alterar uma peça selecionada
 * - Deletar a peça selecionada
 * @author Thiago R. M. Bitencourt
 */
public class PecaController {
	private PecaGUI gui;
	private PecaDAO pDAO = new PecaDAO();
	private AnimalDAO aDAO;
	private List<Peca> listPeca = new LinkedList<Peca>();
	JFrame Super;
	String[] idPeca;

	String[] colPrincipal = {"Código Tombo Peça", "Descrição", "Código Tombo Animal", "Nome Animal"}; //modelo para colunas

	/**
	 * Construtor da classe de controle, responsável por controlar o que acontece na interface
	 * @param Super
	 */
	public PecaController(JFrame Super){
		this.Super = Super;
		gui = new PecaGUI(Super);
		gui.addOuvintes(new Ouvinte());
		gui.setVisible(true);
	}
	
	/**
	 * Verifica se o usuário logado é administrador, se não for restringe a funcionalidade de alterar e remover Peça
	 */
	private void configPermissao(){
		if(!App.getTipoUser().equals("Administrador")){
			gui.btnAlterar.setEnabled(false);
			gui.btnRemover.setEnabled(false);
			//			gui.btnRemover.setEnabled(false);
		}
		else{
			gui.btnAlterar.setEnabled(true);
			gui.btnRemover.setEnabled(true);
		}
	}

	/**
	 * Controla as ações geradas pela interface
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			if(ev.getSource() == gui.btnAdicionar){
				new CadastroPecaController(1, gui, Super);
				limpaConsulta();
			}

			if(ev.getSource() == gui.btnAlterar){
				VerificaAltera();
			}

			if(ev.getSource() == gui.btnRemover){
				verificaDeletaPeca();
			}

			if(ev.getSource() == gui.btnConsultar){
				if(gui.chckbxMostrarTudo.isSelected()){
					gui.btnAlterar.setEnabled(false);
					gui.btnRemover.setEnabled(false);
				}
				else{
					configPermissao();
				}
				consultarAllPeca();
			}
			if(ev.getSource() == gui.btnFiltro){
				if(gui.chckbxMostrarTudo.isSelected()){
					gui.btnAlterar.setEnabled(false);
					gui.btnRemover.setEnabled(false);
				}
				else{
					configPermissao();
				}
				consultarByTombo();
			}
		}
	}
	
	/**
	 * Método responsável pela funcionalidade de alterar uma peça.
	 * Verifica se uma peça está selecionada e em caso afirmativo chama a classe responsável
	 * por alter a peça selecionada.
	 */
	private void VerificaAltera(){
		try{
			// se tiver alguma linha selecionada na tabela consultada
			if(gui.tabela.getSelectedRow() != -1){
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 2); //pega o codigo tombo do animal
				String cdTomboPeca = (String) gui.tabela.getValueAt(linhaSelecionada, 0); // pra o código tombo da peça
				try{
					AnimalDAO aDAO = new AnimalDAO();
					Animal animal = aDAO.findAnimalByTombo(cdTomboSelecionado);
					listPeca = pDAO.pecaNaoApagada(animal.getIdAnimal()); // Pega todas as peças que estão disponíveis na base de dados
					Iterator<Peca> it = listPeca.iterator(); //cria iterator para percorrer a lista
					/*
					 * Varre a lista e procura se a peça a ser alterada
					 * quando encontrar, abre a janela para alteração.
					 */
					while( it.hasNext() ){ //enquanto tiver linha
						Peca pe = new Peca();
						pe = (Peca) it.next(); //pega a peça
						if(String.format("Pc"+pe.getCdTomboPeca()).equals(cdTomboPeca)){ // Se for a peça a selecionada: 
							new AlterarPecaController(gui, pe); // Abre a janela para alteração e passa como parâmetro a peça a ser alterada
							break; //Sai do laço, A peça ja foi alterada
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("ERRO: " + e);
				}
			}
			else{
				JOptionPane.showMessageDialog(gui, "Selecione uma peça!");
			}
		}
		catch(Exception e)
		{
			System.out.println("ERRO :" + e);
		}
	}


	/**
	 * Função que busca todas as peças de um determinado animal (definifo pelo número do tombo do animal)
	 */
	private void consultarByTombo(){

		aDAO = new AnimalDAO();
		if(!gui.textTombo2.getText().equals("") && !gui.textTombo3.getText().equals("")){
			String tombo = gui.tombo1.getText() + gui.textTombo2.getText() + gui.textTombo3.getText();
			int idAnimal = aDAO.getIdByTombo(tombo);
			listPeca = pDAO.findPecaByIdAnimal(idAnimal);
			if(gui.chckbxMostrarTudo.isSelected())
				MontaTabela(listPeca, 1);
			else
				MontaTabela(listPeca, 0);
		}
		else{
			JOptionPane.showMessageDialog(gui, "Código Tombo Imcompleto");
		}
	}

	/**
	 * Função que consulta todas as peças cadastradas
	 */
	private void consultarAllPeca() {		
		listPeca = pDAO.obterListaPecas(); //obtem a lista de peças de acordo com os parametros da peça

		if(gui.chckbxMostrarTudo.isSelected())
			MontaTabela(listPeca, 1);
		else
			MontaTabela(listPeca, 0);

	}

	/**
	 * Método que cria a tabela de Peças a serem exibidos
	 * @param listPeca - Lista com as peças cadastradas
	 * @param flag - mostrar peças disponíveis ou não disponíveis
	 */
	private void MontaTabela(List<Peca> listPeca, int flag){
		String[][] dadosPecaAux = null; //linhas de peca como nula
		String[][] dadosPeca = null;
		Iterator<Peca> it = listPeca.iterator(); //cria iterator para percorrer a lista

		if( listPeca.size() > 0 ){ //verifica se foram encontradas pecas
			dadosPecaAux = new String[listPeca.size()][4]; //cria o numero de linhas da lista
			idPeca = new String[listPeca.size()];
			int count = 0; //contador das linhas
			if(flag == 0){
				while( it.hasNext() ){ //enquanto tiver linha
					Peca pe = new Peca();
					pe = (Peca) it.next(); //pega a peça
					if(pe.getBlAnimal() == 1){	// mostra penas as peças disponiveis
						AnimalDAO aDAO = new AnimalDAO();
						Animal ani = aDAO.findAnimalByIdAnimal(pe.getIdAnimal());
						dadosPecaAux[count][0] = "Pc"+String.valueOf(pe.getCdTomboPeca()); //seta o codigo tombo na tabela
						dadosPecaAux[count][1] = pe.getDsPeca(); //seta descricao da peça
						dadosPecaAux[count][2] = ani.getCdTombo(); //seta codigo tombo do animal
						dadosPecaAux[count][3] = ani.getNmAnimal(); //seta o nome do animal na tabela						
						count++;
					}
				}
			}
			else{
				while( it.hasNext() ){ //enquanto tiver linha
					Peca pe = new Peca();
					pe = (Peca) it.next(); //pega a peça
					if(pe.getBlAnimal() != 1){// adiciona apenas as peças não disponiveis
						AnimalDAO aDAO = new AnimalDAO();
						Animal ani = aDAO.findAnimalByIdAnimal(pe.getIdAnimal());
						dadosPecaAux[count][0] = "Pc"+String.valueOf(pe.getCdTomboPeca()); //seta o codigo tombo na tabela
						dadosPecaAux[count][1] = pe.getDsPeca(); //seta descricao da peça
						dadosPecaAux[count][2] = ani.getCdTombo(); //seta codigo tombo do animal
						dadosPecaAux[count][3] = ani.getNmAnimal(); //seta o nome do animal na tabela						
						count++;
					}
				}
			}
			/*
			 * Cria a tabela a ser mostrada. Se mostrar a tabela auxiliar (montada acima) é exibido 
			 * linhas em branco, já que a tabela criada é do tamanho da lista de peças na base de dados, mas como não são mostrados todas as peças
			 * e sim apenas as disponíveis ou apenas as não disponíveis (Dependendo da flag recebida). Por isso é necessário criar uma tabela
			 * com o tamanho exato de peças a serem mostradas. O número de linhas necessárias está na variavel count, já que, quando uma peça deve ser mostrada
			 * a variavel count é incrementada.
			 */
			if(count != 0){
				dadosPeca = new String[count][4];
				for(int i = 0; i < dadosPeca.length; i++){
					dadosPeca[i] = dadosPecaAux[i];
				}
			}
		}
		DefaultTableModel dtm = new DefaultTableModel(dadosPeca, colPrincipal); //cria model para tabela com dados preenchidos
		gui.tabela.setModel(dtm); //seta o model da tabela
	}

	/**
	 * Método responsável por "Deletar" uma peça da base de dados.
	 * Verifica se um peça está selecionada.
	 * Em caso afirmativo chama a classe responsável pela funcionalidade baixa
	 * irá abrir uma janela onde deve ser descrito o motivo pelo qual a peça está sendo removida do sistema
	 * e a data em que a peça deichou de fazer parte do acervo e qual usuário está cadastrando a baixa.
	 * O usuário é automaticamente preenchido, com o nome do usuário atual do sistema
	 * e a data também é automatica, sendo usada a data atual do sistema operacional (A data pode ser alterada. O usuário não)
	 * Ao cadastrar a baixa, o id da baixa e relacionado a peça selecionada o que indica que a peça deichou de fazer parte do acervo
	 * porém, é apenas uma exclusão lógica, ou seja, o registro continua na base de dados mas o sistema só mostra essa peça quando for solicitado
	 */
	private void verificaDeletaPeca() {
		try{
			if( gui.tabela.getSelectedRow() != -1){// se tiver alguma linha selecionada na tabela consultada
				int linhaSelecionada = gui.tabela.getSelectedRow(); // pega a linha seleciona
				String cdTomboSelecionado = (String) gui.tabela.getValueAt(linhaSelecionada, 2); //pega o codigo tombo do animal
				String cdTomboPeca = (String) gui.tabela.getValueAt(linhaSelecionada, 0); // pra o código tombo da peça
				try{
					AnimalDAO aDAO = new AnimalDAO();
					Animal animal = aDAO.findAnimalByTombo(cdTomboSelecionado);
					listPeca = pDAO.pecaNaoApagada(animal.getIdAnimal()); // Pega todas as peças que estão disponíveis na base de dados
					Iterator<Peca> it = listPeca.iterator(); //cria iterator para percorrer a lista
					/*
					 * Varre a lista e procura se a peça a ser excluida
					 * quando encontrar, abre a janela para justificar a baixa.
					 * Se a baixa for registrada, então pega o id da baixa registrada e altera a peça na base de dados
					 * informando que a mesma não está mais disponivel
					 */
					while( it.hasNext() ){ //enquanto tiver linha
						Peca pe = new Peca();
						pe = (Peca) it.next(); //pega a peça
						if(String.format("Pc"+pe.getCdTomboPeca()).equals(cdTomboPeca)){ 
							BaixaController delPc = new BaixaController(gui);
							if(delPc.getIdBaixa() != -1)
							{
								pDAO.baixaPeca(delPc.getIdBaixa(), pe.getIdPeca());
							}
							break;
						}
					}
					/*
					 * Após remover a peça, busca novamente todas as peças ativas na base de dados
					 * se a lista retornada for igual a zero (0), isso que dizer que peça que foi excluida
					 * era a ultima peça relacionada ao animal e então atualiza o animal correspondente e informa 
					 * que o animal não está mais disponivel, ja que nenhuma peça daquele animal está disponivel
					 */
					listPeca = pDAO.pecaNaoApagada(animal.getIdAnimal());
					if(listPeca.size() == 0)
					{
						aDAO.BaixaDB(animal.getIdAnimal());
					}
				}
				catch(Exception e){
					System.out.println("ERRO: " + e);
				}
			}
			else
			{
				JOptionPane.showMessageDialog(gui, "Selecione uma peça!");
			}
		} catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ERRO: " + e);
		}
	}

	/**
	 * Função que limpa a tabela de exibição de consulta
	 */
	private void limpaConsulta() {
		String[][] dadosPeca = null; //linhas de peca como nula
		DefaultTableModel dtm = new DefaultTableModel(dadosPeca, colPrincipal); //cria model para tabela com dados preenchidos
		gui.tabela.setModel(dtm); //seta o model da tabela
	}
}
