package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.dados.Login;
import br.edu.angloamericano.cbio.dados.TipoUser;
import br.edu.angloamericano.cbio.dados.Usuario;
import br.edu.angloamericano.cbio.dao.LoginDAO;
import br.edu.angloamericano.cbio.dao.TipoUserDAO;
import br.edu.angloamericano.cbio.dao.UsuarioDAO;
import br.edu.angloamericano.cbio.gui.CadastroUsuarioGUI;
import br.edu.angloamericano.cbio.util.SenhasHash;


@SuppressWarnings("serial")
public class CadastroUsuarioController extends JDialog{
	private List<TipoUser> listaTipos = new LinkedList<TipoUser>();
	private CadastroUsuarioGUI gui;
	private Usuario user = new Usuario();
	private Login login;
	private LoginDAO lDAO;
	private UsuarioDAO userDAO;
	private TipoUserDAO tiposDAO = new TipoUserDAO();

	private int flag = -1;

	/**
	 * Construtor da classe - Abre a janela para adição de um novo usuário
	 * @param Super - Janela 'Pai' 
	 * @throws Exception
	 */
	public CadastroUsuarioController(JDialog Super) throws Exception{
		AbreJanela(Super, 0);
	}

	/**
	 * Construtor da classe - Abre a janela para alteração de um usuário
	 * @param Super - Janela 'Pai'
	 * @param user - Recebeo usuário a ser alterado
	 * @throws Exception
	 */
	public CadastroUsuarioController(JDialog Super, Usuario User) throws Exception{
		user = User;
		AbreJanela(Super, 1);
	}

	// Abre a janel para adição ou edição
	public void AbreJanela(JDialog Super, int flag) throws Exception{
		gui = new CadastroUsuarioGUI(Super, flag); // flag == 0 - Abre para adição. flag == 1 - Abre para adição
		CarregarTipos();
		this.flag = flag;
		gui.addOuvintes(new Ouvinte());
		if(flag == 1){
			CarregaDadosGUI(); // Carrega as informações na base de dados
		}
		gui.setModal(true);
		gui.setVisible(true);
	}

	/**
	 * Método que carrega os tipos de usuários
	 * @throws Exception
	 */
	public void CarregarTipos(){
		try {
			tiposDAO = new TipoUserDAO();
			gui.BoxTipo.setModel(new DefaultComboBoxModel(new String[] {":: Selecione um Tipo ::"}));
			listaTipos = tiposDAO.getTipos();
			if(listaTipos.size() > 0){	
				DefaultComboBoxModel modeloTipos = new DefaultComboBoxModel();
				modeloTipos.addElement(":: Selecione um Tipo ::");
				if(listaTipos != null){
					for(int i=0; i<listaTipos.size(); i++){
						modeloTipos.addElement(listaTipos.get(i).getNomeTipo());
					}
				}
				gui.BoxTipo.setModel(modeloTipos);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Retorna a lista com todos os tipos de usuários
	public List<TipoUser> getListTipos(){
		return listaTipos;
	}

	/**
	 * Classe interna que faz o controle das ações da janel
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			if(ev.getSource() == gui.btnAdicionar)
			{
				if(validaDados())
				{
					if(UserLogin()){ // Verifica se o registro de usuário e login de usuário podem ser usados
						CarregaUsuario();
						int id = 0;
						id = InsereDados(); //Carrega os dados do novo usuario, insere na base de dados e retorna o ID
						CarregaLogin(); // Carrega as informações de Login do usuário
						InsereLogin(id); // Insere o Login do usuário na base de dados
						JOptionPane.showMessageDialog(gui, "Usuario Cadastrdo com Sucesso");
						limpaCampos();
						gui.dispose();
					}
				}
			}
			if(ev.getSource() == gui.btnAlterar)
			{
				/*
				 * Variave do tipo inteiro que define as alterações que foram feitas, ou se nada foi alterado
				 */
				int status = -1; // quanso status estiver com valor -1 nada foi alterado
				if(validaDados()) // Verifica se todos os dados obrigatórios estão corretamente inseridos
				{
					if(verificaNovosDadosUser()) // se algum dado foi modificado então altera o usuário
					{
						updateUsuario();
						status = 0; // Usuario alterado, status recebe valor 0
					}
					if(login != null){
						if(verificaNovosDadosLogin()){ // Se algum dado de login foi alterado, então altera o login do usuário
							updateLogin();
							status = 1; // Login alterado, status recebe valor 1
						}
					}
					else{
						if(Login()){ // Quando vai criar um novo login para o usuário, verifica se o mesmo pode ser usado
							CarregaLogin(); // Se o usuário não tiver um login associado, então um login é cadastrado. Usado para 'Reativar' um usuário inativo
							InsereLogin(user.getId());
							userDAO = new UsuarioDAO();
							userDAO.setUsuario(user);
							userDAO.baixaDB(0);
							status = 2; // Login criado, usuario existia mas não estava ativo. status recebe valor 2
						}
						else
							status = 3; // Se o login ja existir na base de dados, status recebe valor 3
					}
					/*
					 * Quando o status tiver um valor diferente de -1, significa que alguma alteração foi feita ou deveria ter sido feita
					 * Quando o status tiver valor 3 significa que o login a ser cadastrado ja existe e não pode ser usado.
					 * Portanto quando status for 3, algum dado deveria ter sido alterada mas não foi.
					 * Então, se status for diferente de -1 e também for diferente de 3 significa que os dados foram alterados sem nenhum problema
					 */
					if(status != -1 && status != 3) // Se alguma alteração foi feita, avisa o usuário e fecha a janela
					{
						JOptionPane.showMessageDialog(gui, "Dados Alterados Com Sucesso");
						gui.dispose();
					}
					// Se status tiver valor -1 significa que nenhum alteração precisa ser feita
					if(status == -1) // Se nenhuma alteração foi feita, avisa o usuário e fecha a janela
					{
						JOptionPane.showMessageDialog(gui, "Nada foi Alterado");
						gui.dispose();
					}
				}
			}
			if(ev.getSource() == gui.btnLimpar) // Limpa todos os campos da janela
			{
				limpaCampos();
			}
			if(ev.getSource() == gui.btnCancelar) // Cancela a operação, limpa os campos e fecha a janela
			{
				limpaCampos();
				gui.dispose();
			}
			if(ev.getSource() == gui.btnAddTipo) // Adiciona um novo tipo de usuário
			{
				addTipo(); 
			}
		}
	}

	/**
	 * Valida os dados inseridos na interface
	 * @return - true se os dados forão aceitos e false caso contrário
	 */
	private boolean validaDados(){
		int aux;
		try {
			aux = VerificaUser();
			// Verifica os dados inseridos
			switch (aux) {
			case 0:{ // Caso algum campo obrigatório não tenha sido preenchido
				JOptionPane.showMessageDialog(gui, "Preencha os Campos Obrigatórios (*)");
				return false;
			}
			case 2:{ // Caso os Campos estejam todos OK
				return true;
			}
			case 1:{ // Caso a senha digita e a repetição da senha sejam diferentes
				JOptionPane.showMessageDialog(gui, "Senha não confere");
				return false;
			}
			case 3:{ // Caso de a senha ser menor que 8 caracteres
				JOptionPane.showMessageDialog(gui, "Senha deve ter mais de 8 caracteres");
				return false;
			}
			case 4:{ // Caso o registro seja um número incompleto
				JOptionPane.showMessageDialog(gui, "Registro Incompleto");
				return false;
			}
			default:
				break;
			}
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	/**
	 * Verifica se algum campo foi modificado
	 * @return verdadeiro se houve modificação e falso caso contrário
	 */
	private boolean verificaNovosDadosUser(){
		boolean altera = false;

		/*
		 * Compara os dados dos campos da janela com os dados do usuário atual, se algum dado estiver diferente
		 * então, existe dados a ser alterados, nesse caso a variavél altera é verdadeira.
		 */

		if(!user.getNome().equals(gui.textNome.getText())){
			user.setNome(gui.textNome.getText());
			altera = true;
		}

		if((user.getSobrenome() == null) && (!gui.textSobrenome.getText().equals(""))){
			user.setSobrenome(gui.textSobrenome.getText());
			altera = true;
		}
		if(user.getSobrenome() != null)
			if(!user.getSobrenome().equals(gui.textSobrenome.getText())){
				user.setSobrenome(gui.textSobrenome.getText());
				altera = true;
			}

		if(!user.getCargo().equals(gui.textCargo.getText())){
			user.setCargo(gui.textCargo.getText());
			altera = true;
		}
		if(gui.textRegistro.getValue() != null)
			if(!user.getRegistro().equals(gui.textRegistro.getValue().toString()))
			{
				user.setRegistro(gui.textRegistro.getValue().toString());
				altera = true;	
			}

		if((user.getEmail() == null) && (!gui.textEmail.getText().equals("")))
		{
			user.setEmail(gui.textEmail.getText());
			altera = true;
		}
		if(user.getEmail() != null)
			if(!user.getSobrenome().equals(gui.textSobrenome.getText()))
			{
				user.setEmail(gui.textEmail.getText());
				altera = true;
			}

		if((user.getTelCel() == null) && (gui.textTelCel.getValue() != null))
		{
			user.setTelCel(gui.textTelCel.getValue().toString());
			altera = true;
		}
		if(user.getTelCel() != null){
			if(!user.getTelCel().equals(gui.textTelCel.getValue().toString()))
			{
				user.setTelCel(gui.textTelCel.getValue().toString());
				altera = true;
			}
		}

		if(gui.textTelRes.getValue() != null){
			if((user.getTelRes() == null) && (!gui.textTelRes.getValue().toString().equals(""))){
				user.setTelRes(gui.textTelRes.getValue().toString());
				altera = true;
			}
			if(user.getTelRes() != null)
				if(!user.getTelRes().equals(gui.textTelRes.getValue().toString()))
				{
					altera = true;
				}
		}
		if(user.getTipo() != getListTipos().get(gui.BoxTipo.getSelectedIndex() -1 ).getId()){
			user.setTipo(getListTipos().get(gui.BoxTipo.getSelectedIndex() -1 ).getId());
			altera = true;
		}

		if((user.getObs() == null) && (!gui.textObs.getText().equals("")))
			altera = true;
		if(user.getObs() != null)
			if(!user.getObs().equals(gui.textObs.getText()))
				altera = true;		

		return altera;
	}

	/**
	 * Método que verifica se os dados de login foram ou não alterados
	 * @return true se os dados foram alterados e false caso contrário
	 */
	@SuppressWarnings("deprecation")
	private boolean verificaNovosDadosLogin(){
		boolean altera = false;
		// Compara os dados da interface com os dados do login do usuário, se algum dado estiver diferente então altera o login

		/*
		 * Verifica apenas o login, a senha é em hash, portanto se a senha for alterada será substituida 
		 * independente se a senha for ou não a mesma que a senha cadastrada na base de dados
		 */
		if(!login.getLogin().equals(gui.textLogin.getText())){
			login.setLogin(gui.textLogin.getText());
			altera = true;
		}
		if(!gui.textSenha.getText().equals("")){
			login.setSenha(SenhasHash.doHash(gui.textSenha.getText()));
			altera = true;
		}
		return altera;
	}

	/**
	 * Método que limpa todos os campos da janela
	 */
	private void limpaCampos(){
		gui.textNome.setText("");
		gui.textSobrenome.setText("");
		gui.textCargo.setText("");
		gui.textRegistro.setValue("");
		gui.textEmail.setText("");
		gui.textLogin.setText("");
		gui.textRepSenha.setText("");
		gui.textSenha.setText("");
		gui.textTelCel.setValue("");
		gui.textTelRes.setValue("");
		gui.BoxTipo.setSelectedIndex(0);
		gui.textObs.setText("");
	}

	/**
	 * Faz a verificação dos campos obrigatórios. 
	 * Mostra quais os campos obrigatórios não foram preenchidos.
	 * E também faz a verificação de usuário existente, ou seja, verifica se o nome de usuário ja existe na base de dados
	 * @return int
	 * 0 - se algum campo obrigatório não foi preenchido, 
	 * 1 - se a senha repetida for diferente 
	 * 2 - se todos os campos estiverem OK e 
	 * 3 - se a senha for muito pequena
	 * 4 - se o registro estiver incompleto
	 * @throws Exception 
	 */
	@SuppressWarnings("deprecation")
	private int VerificaUser() throws Exception
	{
		if(gui.textNome.getText().equals(""))
			return 0;
		if(gui.textCargo.getText().equals(""))
			return 0;
		if(gui.textRegistro.getValue() == null)
			return 4;
		if(gui.textLogin.getText().equals(""))
			return 0;
		if(gui.BoxTipo.getSelectedIndex() == 0)
			return 0;

		/*
		 * Verifica se foi inserida um senha, se nada foi inserido verifica se o usuario está sendo adicionado ou alterado
		 * se estiver sendo alterado, então verifica se existe um login para o usuário, 
		 * se existir um login o compo senha pode ser vazio, e nesse caso a senha do usuário permanesse a mesma.
		 */
		if(gui.textSenha.getText().equals("")){
			if(flag == 1 && login != null){
				return 2;
			}
			else {
				return 0;
			}
		}
		else
			if(gui.textRepSenha.getText().equals(""))
				return 0;

		if(gui.textSenha.getText().length() < 7)
			return 3;
		if(!gui.textSenha.getText().equals(gui.textRepSenha.getText()))
			return 1;
		return 2;
	}

	/*
	 * Método que verifica se o usuário e login ja existem na base de dados
	 */
	private boolean UserLogin(){ 		// Verifica usuario e senha
		userDAO = new UsuarioDAO();
		lDAO = new LoginDAO();
		Usuario us;
		boolean ja = true;
		int l;
		try {
			us = userDAO.userByRegistro(gui.textRegistro.getValue().toString());
			if(us != null){ // se o usuário existir, retorna e avisa ao usuário
				ja = false;
			}
			l = lDAO.valida_user(gui.textLogin.getText());
			if(l != 0){ // se o login existir, retorna e avisa ao usuário
				JOptionPane.showMessageDialog(gui, "Login já Cadastrado");
				ja = false;	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ja;
	}

	/*
	 * Método que verifica se o login já é cadastrado
	 */
	private boolean Login(){
		lDAO = new LoginDAO();
		int l;
		try {
			l = lDAO.valida_user(gui.textLogin.getText());
			if(l != 0){ // se o login existir, retorna e avisa ao usuário
				JOptionPane.showMessageDialog(gui, "Login já Cadastrado");
				return false;	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * Se os campos obrigatórios foram validados então o objeto do tipo Usuario é criado e os dados carregados
	 */
	private void CarregaUsuario()
	{
		user = new Usuario();
		user.setNome(gui.textNome.getText());
		user.setSobrenome(gui.textSobrenome.getText());
		user.setCargo(gui.textCargo.getText());
		user.setEmail(gui.textEmail.getText());
		user.setRegistro(gui.textRegistro.getValue().toString());
		if(gui.textTelRes.getValue() != null)
			user.setTelRes(gui.textTelRes.getValue().toString());
		if(gui.textTelCel.getValue() != null)
			user.setTelCel(gui.textTelCel.getValue().toString());
		user.setObs(gui.textObs.getText());
		user.setTipo(getListTipos().get(gui.BoxTipo.getSelectedIndex() -1 ).getId());
	}

	/**
	 * Carrega as informações de login
	 */
	@SuppressWarnings("deprecation")
	private void CarregaLogin(){
		login = new Login();
		login.setLogin(gui.textLogin.getText());
		// Faz hash da senha para armazenar na base de dados.
		String senhaHash = SenhasHash.doHash(gui.textSenha.getText());
		login.setSenha(senhaHash);
	}

	/**
	 * Método que insere um novo tipo de usuário.
	 * Faz a leitura do novo tipo por um Input Dialog
	 * verifica se o tipo ja existe na base de dados para evitar que um mesmo tipo seja cadastrado
	 * Insere no novo tipo.
	 */
	private void addTipo()
	{
		TipoUserDAO tipoDAO = new TipoUserDAO();
		String novoTipo = JOptionPane.showInputDialog("Novo Tipo de Usuário:");
		if(!novoTipo.equals("")){
			if(!tipoDAO.isTipoExist(novoTipo)){
				TipoUser tipo = new TipoUser();
				tipo.setNomeTipo(novoTipo);
				tipoDAO.insert(tipo);
				CarregarTipos();
			}
			else
			{
				JOptionPane.showMessageDialog(gui, "Tipo de Usuário ja Cadastrado!");
			}	
		}
	}


	/**
	 * Adiciona o novo usuário a base de dados
	 * @throws SQLException 
	 */
	private int InsereDados()
	{
		int lastId = 0;
		userDAO = new UsuarioDAO();
		try {
			userDAO.insert(user);
			lastId = userDAO.getLastId();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRO: " + e);
		}
		return lastId;
	}

	/**
	 * Método que altera os dados de um usuário
	 */
	private void updateUsuario(){
		userDAO = new UsuarioDAO();
		userDAO.setUsuario(user);
		userDAO.saveDB();
	}

	/**
	 * Adiciona o login do usuário a base de dados
	 */
	private void InsereLogin(int idUser)
	{
		login.setIdUsuaio(idUser);
		lDAO = new LoginDAO();
		lDAO.insert(login);
	}

	/**
	 * Método que altera os dados de login
	 */
	private void updateLogin(){
		LoginDAO loginD = new LoginDAO();
		loginD.setLogin(login);
		loginD.saveDB();
	}

	/**
	 * Quando a janela é aberta para alterar os dados do usuário, o método CarregaDadosGUI é responsável
	 * por mostrar os dados do usuário na janela.
	 */
	private void CarregaDadosGUI() {
		lDAO = new LoginDAO();
		try {
			// Busca um login pelo id do usuário
			login = lDAO.getLogin(user.getId());
			/*
			 * Se o login não for null, ou seja, se existir um login associado ao usuário então insere da interface
			 * o login do usuário. 
			 * Se o login for null, significa que o usuário está inativo e não tem um login associado
			 */
			if(login != null){
				gui.textLogin.setText(login.getLogin());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gui.textNome.setText(user.getNome());
		gui.textSobrenome.setText(user.getSobrenome());
		gui.textCargo.setText(user.getCargo());
		gui.textEmail.setText(user.getEmail());
		gui.textRegistro.setValue(user.getRegistro());
		gui.textTelCel.setValue(user.getTelCel());
		gui.textTelRes.setValue(user.getTelRes());
		gui.textObs.setText(user.getObs());
		List<TipoUser> listaTipos = new LinkedList<TipoUser>();
		listaTipos = getListTipos();
		TipoUser tipo = new TipoUser();
		/*
		 * Pega o id do tipo do usuário e compara com todos os tipos presentes na lista de tipos de usuário
		 * quando encontrar qual é o tipo do usuário, seta a seleção no comboBox.
		 */
		for (int i = 0; i < listaTipos.size(); i++){
			tipo = new TipoUser();
			tipo = listaTipos.get(i);
			if(tipo.getId() == user.getTipo()){
				gui.BoxTipo.setSelectedIndex(i+1);
				i = listaTipos.size();
			}
		}
	}
}
