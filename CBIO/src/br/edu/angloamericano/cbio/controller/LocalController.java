package br.edu.angloamericano.cbio.controller;

import java.util.LinkedList;
import java.util.List;
import br.edu.angloamericano.cbio.dados.Local;


public class LocalController {
	public List<Local> listLocal;
	
	public LocalController(){
		listLocal = new LinkedList<Local>();
	}
	
	public void addList(Local local){
		listLocal.add(local);
	}
	
	public List<Local> getList(){
		return listLocal;
	}
}
