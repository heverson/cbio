package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import br.edu.angloamericano.cbio.app.App;
import br.edu.angloamericano.cbio.gui.PrincipalGUI;

/**
 * Classe que controla as ações da janela principal do sistema.
 * @author Thiago R. M. Bitencourt
 *
 */
public class PrincipalController {
	private PrincipalGUI gui;

	public PrincipalController(){
		gui = new PrincipalGUI();
		gui.addOuvintes(new Ouvinte());
		gui.setVisible(true);
		
	}
	/**
	 * Classe interna, utilizada para controlar os componentes da interface Principal do sistema
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			// menu Manter Animais
			if(ev.getSource() == gui.mntmAnimais){
				new AnimalController(gui, 0);
			}
			// menu Manter Peças
			if(ev.getSource() == gui.mntmPecas){
				new PecaController(gui);
			}
			// menu Manter Usuário
			if(ev.getSource() == gui.mntmUsuario){
				try {
					new UsuarioController(gui);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// menu Manter Locais de Armazenamento
			if(ev.getSource() == gui.mntmLocal){
				new CadastroLocalController(null, 0);
			}
			// Menu Sair
			if(ev.getSource() == gui.mntmSair){
				gui.dispose();
			}
			// Menu Trocar Usuário
			if(ev.getSource() == gui.mntmTrocaUsuario){
				App.setTipoUser("");
				App.setUsuario(null);
				gui.dispose();
				new LoginController();
			}
		}
	}
}
