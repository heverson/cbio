package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.dados.Endereco;
import br.edu.angloamericano.cbio.dao.EnderecoDAO;
import br.edu.angloamericano.cbio.gui.AdicionarEnderecoGUI;

/**
 * Classe de controle. Responsável por cadastrar um novo endereço
 * @author Thiago R. M. Bitencourt
 */
public class AdicionarEnderecoController {
	public AdicionarEnderecoGUI gui;
	public Endereco endereco = new Endereco();

	/**
	 * Construtor da classe
	 * @param Super
	 */
	public AdicionarEnderecoController(JDialog Super, int idCidade){
		endereco.setIdCidade(idCidade);
		gui = new AdicionarEnderecoGUI(Super);
		gui.addOuvintes(new Ouvinte());
		gui.setModal(true);
		gui.setVisible(true);
	}

	// Retorna o endereço cadastrado
	public Endereco getEndereco(){
		return endereco;
	}
	// Adiciona um endereço a classe - Pode ser usado para uma função de alterar Endereço
	public void setEndereco(Endereco end){
		endereco = end;
	}

	/**
	 * Classe interna, responsável por controlar as ações da interface
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener{
		public void actionPerformed(ActionEvent ev){
			// Botão OK
			if(ev.getSource() == gui.btnOk){
				if(ValidaCampos()){ // faz a validação dos dados inseridos
					try
					{
						CarregaEndereco(); // Carrega os dados
						InsereEndereco(); // Insere o novo endereço na base de dados
					}
					catch(Exception e)
					{
						System.out.println("ERRO: " + e);
					}
					JOptionPane.showMessageDialog(gui, "Endereço Cadastrado");
					gui.dispose();
				}
				else
				{
					JOptionPane.showMessageDialog(gui, "Preencha os campos obrigatórios (**)");
				}
			}
			// Botão Cancelar
			if(ev.getSource() == gui.btnCancelar){
				gui.dispose();
			}
		}
	}

	/*
	 * método que verifica se os campos Obrigatórios foram preenchidos
	 */
	private boolean ValidaCampos(){
		if(gui.textFieldNmRua.getText().equals(""))
			return false;
		if(gui.textField_CEP.getValue() == null)
			return false;
		if(gui.textField_ComplementoRua.getText().equals(""))
			return false;
		return true;
	}

	// Carrega as informações ao objeto endereço
	private void CarregaEndereco()
	{
		endereco.setNmRua(gui.textFieldNmRua.getText());
		endereco.setNuComplemento(gui.textField_ComplementoRua.getText());
		endereco.setCdCep(gui.textField_CEP.getValue().toString());
		endereco.setNmLocal(gui.textField_NomeLocal.getText());
		endereco.setDsLocal(gui.textDsLocal.getText());
		if(gui.textField_CoordenadaGeografica.getValue() != null)
		{
			String coordenada = "";
			coordenada = gui.textField_CoordenadaGeografica.getText().replace("º", "").replace("\'", "").replace("\"", "").replace("-", "");
			endereco.setCdCoordenadaGeografica(coordenada);
		}
		else
			endereco.setCdCoordenadaGeografica("");
	}

	// Insere o novo endereco na base de dados
	private void InsereEndereco(){
		EnderecoDAO endD = new EnderecoDAO();
		endD.insert(endereco);
	}
}
