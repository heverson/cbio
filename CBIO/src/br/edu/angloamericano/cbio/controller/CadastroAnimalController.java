package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.CidadeDAO;
import br.edu.angloamericano.cbio.dao.ClasseDAO;
import br.edu.angloamericano.cbio.dao.ColetaDAO;

import br.edu.angloamericano.cbio.dao.EnderecoDAO;
import br.edu.angloamericano.cbio.dao.EspecieDAO;
import br.edu.angloamericano.cbio.dao.EstadoDAO;
import br.edu.angloamericano.cbio.dao.FamiliaDAO;
import br.edu.angloamericano.cbio.dao.MamiferosDAO;
import br.edu.angloamericano.cbio.dao.OrdemDAO;
import br.edu.angloamericano.cbio.dao.TriboDAO;
import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Cidade;
import br.edu.angloamericano.cbio.dados.Classe;
import br.edu.angloamericano.cbio.dados.Coleta;

import br.edu.angloamericano.cbio.dados.Data;
import br.edu.angloamericano.cbio.dados.Endereco;
import br.edu.angloamericano.cbio.dados.Especie;
import br.edu.angloamericano.cbio.dados.Estado;
import br.edu.angloamericano.cbio.dados.Familia;
import br.edu.angloamericano.cbio.dados.Local;
import br.edu.angloamericano.cbio.dados.LocalComplemento;

import br.edu.angloamericano.cbio.dados.Mamifero;
import br.edu.angloamericano.cbio.dados.Ordem;
import br.edu.angloamericano.cbio.dados.Sala;
import br.edu.angloamericano.cbio.dados.Tribo;
import br.edu.angloamericano.cbio.gui.CadastroAnimalGUI;

public class CadastroAnimalController {
	private CadastroAnimalGUI gui;

	private Classe classe;
	private Ordem ordem;
	private Tribo tribo;
	private Familia familia;
	private Especie especie;
	public Cidade cidade;
	private Coleta coleta;
	private Animal animal;
	private Local local;
	private Bloco bloco;

	private Sala sala;
	private LocalComplemento localComplemento;

	private Mamifero mamifero;

	private ClasseDAO classeDAO;
	private OrdemDAO ordemDAO;
	private TriboDAO triboDAO;
	private FamiliaDAO familiaDAO;
	private EspecieDAO especieDAO;
	private AnimalDAO animalDAO;

	private CidadeDAO cidadeDAO;
	private EnderecoDAO enderecoDAO;

	private ColetaDAO coletaDAO;

	public Endereco endereco;

	private List<String> estados = new ArrayList<String>();
	private List<Classe> listClasse = new LinkedList<Classe>();
	private List<Ordem> listOrdem = new LinkedList<Ordem>();
	private List<Familia> listFamilia = new LinkedList<Familia>();
	private List<Tribo> listTribo = new LinkedList<Tribo>();
	private List<Especie> listEspecie = new LinkedList<Especie>();
	private List<Estado> listEstado = new LinkedList<Estado>();
	private List<Cidade> listCidade = new LinkedList<Cidade>();
	private List<Endereco> listEndereco = new LinkedList<Endereco>();


	/*
	 * Construtor da Classe
	 */
	public CadastroAnimalController(Animal obj, JDialog Super){
		gui = new CadastroAnimalGUI(Super);
		gui.addOuvintes(new Ouvinte());
		gui.setTitle("Cadastrar Animal");
		carregaInfo();

		gui.setModal(true);
		gui.setVisible(true);
	}

	/* Método responsável por "povoar" a lista de Estado e Classe
	Essas Listas aparecem no campo de seleção de classe e de estado na interface de cadastro de um novo animal
	 */
	private void carregaInfo() {
		try {
			EstadoDAO estadoDAO = new EstadoDAO();
			estadoDAO.setQuery();
			listEstado = estadoDAO.getList();

			classeDAO = new ClasseDAO();
			classeDAO.setQuery();				//carrega as classes do bd para uma lista
			listClasse = classeDAO.getList();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(gui,
					"Estados ou/e classes não puderam ser carregados!");
			e.printStackTrace();
		}
		DefaultComboBoxModel modeloEstados = new DefaultComboBoxModel();
		DefaultComboBoxModel modeloClasses = new DefaultComboBoxModel();
		modeloEstados.addElement(":: Selecione um estado ::");
		modeloClasses.addElement(":: Selecione uma classe ::");
		if(estados != null)
			for(int i=0; i<listEstado.size(); i++)
				modeloEstados.addElement(listEstado.get(i));

		if(listClasse != null)
			for(int i=0; i<listClasse.size(); i++)
				modeloClasses.addElement(listClasse.get(i));


		gui.rdbtnArmazenamento.setSelected(false);
		gui.rdbtnProcesso.setSelected(false);

		gui.estado.setModel(modeloEstados);
		gui.classe.setModel(modeloClasses);

		gui.tabbed.setEnabledAt(1, false);
		gui.tabbed.setEnabledAt(2, false);
	}

	// Limpa os campos 
	public void LimparP2(){
		gui.txtNmAnimal.setText("");
		gui.txtNmCientifico.setText("");
		gui.txtFieldCorPelagem.setText("");
		gui.txtSinais.setText("");
		gui.tombo2.setText("");
		gui.tombo3.setText("");
		gui.cBoxIdade.setSelectedIndex(0);
		gui.sexo.setSelectedIndex(0);
		gui.classe.setSelectedIndex(0);
	}

	/**
	 * Classe interna, responsável pelo tratamento dos eventos da interface
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener, MouseListener{

		public void actionPerformed(ActionEvent ev){

			//TAB 1 = P2
			if(ev.getSource() == gui.btnProximoP2){

				if(gui.especie.isEnabled()){
					if( ! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::"))
						especie = (Especie) gui.especie.getSelectedItem();
				}

				//se a classe selecionada for mamiferos ira abrir o painels dos dados biometricos para os animais mamiferos se nao ira direto para o painel de coleta


				if((! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::")) && (! gui.cBoxIdade.getSelectedItem().equals(":: Selecione uma idade ::")) && (! gui.txtNmAnimal.getText().equals(""))
						&& (! gui.txtNmCientifico.getText().equals("")) && (! gui.sexo.getSelectedItem().equals(":: Selecione o Sexo ::")) && (!gui.tombo3.equals("")) && (! gui.txtFieldCorPelagem.getText().equals("")) )
				{
					classe = (Classe )gui.classe.getSelectedItem();		
					animalDAO = new AnimalDAO();
					String nmClasse = classe.getNmClasse();
					String tombo1 = gui.tombo1.getText()+ gui.tombo2.getText().toUpperCase()+gui.tombo3.getText();

					boolean isTomboDup = animalDAO.isTomboDuplicate(tombo1, nmClasse);  //devolve true se o tombo ja existir no banco
					if(isTomboDup==false){

						if(classe.getNmClasse().equals("Mamíferos")){
							gui.tabbed.setEnabledAt(1,true);
							gui.tabbed.setSelectedIndex(1);	

						}
						else{
							gui.tabbed.setEnabledAt(2,true);
							gui.tabbed.setSelectedIndex(2);
						}
					}else
						JOptionPane.showMessageDialog(gui, "O código tombo inserido ja existe no Banco de Dados\nPor favor inserir outro código.\nCaso tenha dúvidas faça uma consulta para visualizar quais os códigos tombos ja existentes", "erro",0);

				}else
					JOptionPane.showMessageDialog(gui, "POR FAVOR PREENCHER OS CAMPOS OBRIGATÓRIOS!", "erro",0);
			}

			//TAB2 = p4
			if(ev.getSource() == gui.btnProximoP4){

				//fazer tratamento se os campos estao preenchidos
				gui.tabbed.setEnabledAt(2,true);
				gui.tabbed.setSelectedIndex(2);
				preencheColeta();

			}

			//Cadastra o animal
			if(ev.getSource() == gui.btnCadastrar){
				//fazer tratamento se os campos estao preenchidos
				if(gui.localidade.isEnabled()){
					if(!gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::")){
						endereco = (Endereco) gui.localidade.getSelectedItem();
					}
				}
				if((!gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::") || 
						(gui.localidade.getSelectedItem() != null)) && 
						(! gui.coletor.getText().equals("")) && (! gui.dtRecolhim.equals(null) ))
				{
					JOptionPane.showMessageDialog(null, "É necessário cadastrar as informações da peça");
					CadastraAnimal();
				}
				else
					JOptionPane.showMessageDialog(null,"Por favor preencher os campos obrigatorios. (*)");
			}

			//TAB4 = p3
			if(ev.getSource() == gui.estado) //se evento for na gui.estado verificaEstado
				verificaEstado();

			if(ev.getSource() == gui.cidade) //se evento for na gui.cidade verificaCidade
				verificaCidade();

			if(ev.getSource() == gui.cidadeAdd)//se evento for na gui.cidadeAdd adicionaCidade
				adicionaCidade();

			// Chama a classe de controle da interface para adicionar um novo endereço
			if(ev.getSource() == gui.localidadeAdd){
				cidade = (Cidade) gui.cidade.getSelectedItem();
				new AdicionarEnderecoController(gui, cidade.getId());
				verificaCidade();
			}

			if(ev.getSource() == gui.classe) //se evento for na gui.classe verificaClasse
				verificaClasse();

			if(ev.getSource() == gui.ordem) //se evento for na gui.ordem verificaOrdem
				verificaOrdem();

			if(ev.getSource() == gui.familia) //se evento for na gui.familia verificaFamilia
				verificaFamilia();

			if(ev.getSource() == gui.tribo) //se evento for na gui.tribo verificaTribo
				verificaTribo();

			if(ev.getSource() == gui.especie) //se o event for no gui.especie
				verificaEspecie();

			if(ev.getSource() == gui.ordemAdd) //se evento for na gui.ordemAdd adicionaOrdem
				adicionaOrdem();

			if(ev.getSource() == gui.familiaAdd) //se evento for na gui.familiaAdd adicionaFamilia
				adicionaFamilia();

			if(ev.getSource() == gui.triboAdd) //se evento for na gui.triboAdd adicionaTribo
				adicionaTribo();

			if(ev.getSource() == gui.especieAdd) //se evento for na gui.especieAdd adicionaEspecie
				adicionaEspecie();

			if(ev.getSource() == gui.tombo3){
				if( (! gui.especie.getSelectedItem().equals(":: Selecione uma espécie ::")) && (gui.especie.getSelectedItem() != null)){
					gui.tombo3.setEnabled(true);
					gui.tombo3.setEditable(true);
				}
			}

			if(ev.getSource() == gui.calcularArcada) //se evento for na gui.calcularArcada calculaTotalArcada 
				gui.totalArcada.setText(String.format("%d",calculaTotalArcada()));	

			if(ev.getSource() == gui.limparArcada) //se evento for na gui.limparArcada limparArcada
				limparArcada();

			if(ev.getSource() ==  gui.btnLimparP2){
				LimparP2();
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if(e.getSource() == gui.tabbed){
				int n = gui.tabbed.getSelectedIndex();

				if(n == 0){
					gui.tabbed.setEnabledAt(1,false);
					gui.tabbed.setEnabledAt(2,false);
				}else
					if(n==1){
						if(gui.tabbed.isEnabledAt(1)){
							gui.tabbed.setEnabledAt(2,false);
						}
					}
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}
	} 

	public void setLocal(Local local){
		this.local = local;
	}

	public void setBloco(Bloco bloco){
		this.bloco = bloco;
	}

	public Bloco getBloco(){
		return bloco;
	}

	public Local getLocal(){
		return local;
	}

	public Sala getSala(){
		return sala;
	}

	public void setSala(Sala sala){
		this.sala = sala;
	}

	public LocalComplemento getLocalComplemento(){
		return localComplemento;
	}

	public void setLocalComplemento(LocalComplemento localComp){
		this.localComplemento = localComp;
	}

	/**
	 * Função que cadastra o animal na base de dados
	 */
	private void CadastraAnimal(){
		preencheColeta();
		coletaDAO = new ColetaDAO(); 
		coletaDAO.insert(coleta);
		preencheAnimal();

		int idColeta = coletaDAO.getIdLast();

		int idEspecie = especie.getIdEspecie();

		animal.setIdColeta(idColeta);
		animal.setIdEspecie(idEspecie); 

		animalDAO = new AnimalDAO();
		animalDAO.insert(animal);

		int idAnimal = animalDAO.getIdAnimal(animal.getCdTombo(),classe.getNmClasse());		//retorna o id do animal que acabou de ser inserido com o codigo tombo

		classe = (Classe) gui.classe.getSelectedItem();
		if(classe.getNmClasse().equals("Mamíferos")){
			mamifero = new Mamifero();
			preencheMamifero();			//preenche a tabela mamifero com os dados do tab
			mamifero.setIdAnimal(idAnimal);

			MamiferosDAO mamiferoDAO = new MamiferosDAO();
			mamiferoDAO.insert((Data)mamifero);
		}
		gui.setVisible(false);
		new CadastroPecaController(2, gui, animal, idAnimal);
	}


	/**
	 * Função que verifica o estado na GUI
	 */
	private void verificaEstado() {
		if(gui.estado.getSelectedIndex() < 1){
			gui.cidade.removeAllItems();
			gui.localidade.removeAllItems();

			gui.cidade.addItem(":: Selecione uma cidade ::");
			gui.localidade.addItem(":: Selecione uma localidade ::");
			gui.cidade.setEnabled(false);
			gui.localidade.setEnabled(false);
			gui.cidadeAdd.setEnabled(false);
			gui.localidadeAdd.setEnabled(false);
		} else{
			try{
				Estado estado = (Estado)gui.estado.getSelectedItem();

				cidadeDAO = new CidadeDAO();
				cidadeDAO.setQueryEstado(estado.getIdEstado());
				listCidade = cidadeDAO.getList();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as cidades!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma cidade ::");

			if(listCidade != null)
				for(int i=0; i<listCidade.size(); i++)
					modelo.addElement(listCidade.get(i));

			gui.cidade.setModel(modelo);
			gui.cidade.setEnabled(true);
			gui.cidadeAdd.setEnabled(true);
			gui.localidade.removeAllItems();
			gui.localidade.addItem(":: Selecione uma localidade ::");
			gui.localidade.setEnabled(false);
			gui.localidadeAdd.setEnabled(false);
		}
	}

	/**
	 * Função que verifica o cidade na GUI
	 */
	private void verificaCidade() {
		if(gui.cidade.getSelectedIndex() < 1){
			gui.localidade.removeAllItems();
			gui.localidade.addItem(":: Selecione uma localidade ::");
			gui.localidade.setEnabled(false);
			gui.localidadeAdd.setEnabled(false);
		} else
		{
			try {
				Cidade cidade = (Cidade) gui.cidade.getSelectedItem();
				enderecoDAO = new EnderecoDAO();
				enderecoDAO.setQueryCidade(cidade.getId());
				listEndereco = enderecoDAO.getList();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as localidades!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma localidade ::");
			
			if(listEndereco.size() > 0){	
				for(int i=0; i<listEndereco.size(); i++)
					modelo.addElement(listEndereco.get(i));
			}
			gui.localidade.setModel(modelo);
			gui.localidade.setEnabled(true);
			gui.localidadeAdd.setEnabled(true);
		}
	}
	/**
	 * Função que adiciona a cidade quando o usuario pede na GUI
	 */
	private void adicionaCidade() {

		Estado estado = (Estado) gui.estado.getSelectedItem() ;
		String nmCidade = JOptionPane.showInputDialog("Entre com a Cidade a ser Adicionada para o estado :" + estado.getNmEstado() );
		if(nmCidade != null)
		{
			if(!nmCidade.equals("")){
				try {
					cidade = new Cidade(0 , nmCidade , estado.getIdEstado());
					cidadeDAO = new CidadeDAO();

					cidadeDAO.insert(cidade);
					cidadeDAO.setQueryEstado(cidade.getIdEstado());
					listCidade = cidadeDAO.getList();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui, "Não foi possível carregar as ordens!");
				}
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				modelo.addElement(":: Selecione uma Cidade ::");
				if(listOrdem != null)
					for(int i=0; i<listCidade.size(); i++)
						modelo.addElement(listCidade.get(i));

				gui.cidade.setModel(modelo);
				gui.cidade.setEnabled(true);
				gui.cidadeAdd.setEnabled(true);
				gui.localidade.removeAllItems();
				gui.localidade.addItem(":: Selecione uma localidade ::");
				gui.localidade.setEnabled(false);
				gui.localidadeAdd.setEnabled(false);
				gui.cidade.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Função que Verifica a classe na GUI 
	 */
	private void verificaClasse() {
		if(gui.classe.getSelectedIndex() < 1){

			gui.ordem.removeAllItems();
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.ordem.addItem(":: Selecione uma ordem ::");
			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.tombo2.setText("");

			gui.ordem.setEnabled(false);
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.ordemAdd.setEnabled(false);
			gui.familiaAdd.setEnabled(false);
			gui.triboAdd.setEnabled(false);
			gui.especieAdd.setEnabled(false);
		} else{
			try {
				classe = (Classe) gui.classe.getSelectedItem() ;
				if (classe.getNmClasse().equalsIgnoreCase("Mamíferos")){			//O Painel p2p1p4 q contem os dados da arcada dentada do animal sera visivel
					gui.p4p1p1.setVisible(true);									//somente se  o animal for da classe mamiferos!!!
					gui.p4p1p1.setEnabled(true);
				}else{
					gui.p4p1p1.setVisible(false);									
					gui.p4p1p1.setEnabled(false);
				}
				ordemDAO = new OrdemDAO();
				ordemDAO.setQueryClasse(classe.getIdClasse());
				listOrdem = ordemDAO.getList();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as ordens!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma ordem ::");
			if(listOrdem!= null)
				for(int i=0; i<listOrdem.size(); i++)
					modelo.addElement(listOrdem.get(i));

			gui.tombo2.setText(String.format("%s",gui.classe.getSelectedItem()).substring(0, 2));
			gui.ordem.setModel(modelo);
			gui.ordem.setEnabled(true);
			gui.ordemAdd.setEnabled(true);
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}

	/**
	 * Função que Verifica a ordem na GUI 
	 */
	private void verificaOrdem() {
		if(gui.ordem.getSelectedIndex() < 1){
			gui.familia.removeAllItems();
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.familia.addItem(":: Selecione uma família ::");
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.familia.setEnabled(false);
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.familiaAdd.setEnabled(false);
			gui.triboAdd.setEnabled(false);
			gui.especieAdd.setEnabled(false);
		} else{

			try {
				ordem = (Ordem) gui.ordem.getSelectedItem();
				familiaDAO = new FamiliaDAO();
				familiaDAO.setQueryOrdem(ordem.getIdOrdem());
				listFamilia = familiaDAO.getList();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as famílias!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma família ::");
			if(listFamilia != null)
				for(int i=0; i<listFamilia.size(); i++)
					modelo.addElement(listFamilia.get(i));

			gui.familia.setModel(modelo);
			gui.familia.setEnabled(true);
			gui.familiaAdd.setEnabled(true);
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}

	/**
	 * Funçãoo que adiciona a especie na GUI 
	 */
	private void adicionaEspecie() {
		String nmEspecie = JOptionPane.showInputDialog("Entre com a nova Especie a ser Adicionada!!!");

		if(nmEspecie != null){
			Tribo tribo= (Tribo) gui.tribo.getSelectedItem() ;

			if(!nmEspecie.equals("")){
				try {
					Especie especie = new Especie(0,nmEspecie, tribo.getIdTribo());
					especieDAO = new EspecieDAO();
					especieDAO.insert((Data)especie);
					especieDAO.setQueryTribo(tribo.getIdTribo());
					listEspecie = especieDAO.getList();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui, "Não foi possível carregar as espécies!");
				}
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				modelo.addElement(":: Selecione uma espécie ::");
				if(listEspecie != null)
					for(int i=0; i<listEspecie.size(); i++)
						modelo.addElement(listEspecie.get(i));
				gui.especie.setModel(modelo);
				gui.especie.setEnabled(true);
				gui.especieAdd.setEnabled(true);

				gui.especie.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Função que adiciona a tribo na GUI 
	 */
	private void adicionaTribo() {
		String nmTribo = JOptionPane.showInputDialog("Entre com a nova Tribo a ser Adicionada!!!");

		if(nmTribo != null){
			Familia familia= (Familia) gui.familia.getSelectedItem() ;

			if(! nmTribo.equals("")){
				try {
					Tribo tribo = new Tribo(0,nmTribo, familia.getIdFamilia());
					triboDAO = new TriboDAO();
					triboDAO.insert(tribo);
					triboDAO.setQueryFamilia(familia.getIdFamilia());
					listTribo = triboDAO.getList();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui, "Não foi possível carregar as tribos!");
				}
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				modelo.addElement(":: Selecione uma tribo ::");
				if(listTribo != null)
					for(int i=0; i<listTribo.size(); i++)
						modelo.addElement(listTribo.get(i));
				gui.tribo.setModel(modelo);
				gui.tribo.setEnabled(true);
				gui.triboAdd.setEnabled(true);
				gui.especie.removeAllItems();
				gui.especie.addItem(":: Selecione uma espécie ::");
				gui.especie.setEnabled(false);
				gui.especieAdd.setEnabled(false);

				gui.tribo.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Função que adiciona a familia na GUI 
	 */
	private void adicionaFamilia() {
		String nmFamilia = JOptionPane.showInputDialog("Entre com a nova Familia a ser Adicionada!!!");

		if(nmFamilia != null){
			Ordem ordem= (Ordem) gui.ordem.getSelectedItem() ;

			if(!nmFamilia.equals("")){
				try {
					Familia familia = new Familia(0,nmFamilia, ordem.getIdOrdem());
					familiaDAO = new FamiliaDAO();
					familiaDAO.insert(familia);
					familiaDAO.setQueryOrdem(ordem.getIdOrdem());
					listFamilia = familiaDAO.getList();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui, "Não foi possível carregar as famílias!");
				}
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				modelo.addElement(":: Selecione uma família ::");

				if(listFamilia != null)
					for(int i=0; i<listFamilia.size(); i++)
						modelo.addElement(listFamilia.get(i));

				gui.familia.setModel(modelo);
				gui.familia.setEnabled(true);
				gui.familiaAdd.setEnabled(true);
				gui.tribo.removeAllItems();
				gui.tribo.addItem(":: Selecione uma tribo ::");
				gui.tribo.setEnabled(false);
				gui.triboAdd.setEnabled(false);
				gui.especie.removeAllItems();
				gui.especie.addItem(":: Selecione uma espécie ::");
				gui.especie.setEnabled(false);
				gui.especieAdd.setEnabled(false);

				gui.familia.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Função que adiciona a ordem na GUI 
	 */
	private void adicionaOrdem() {
		String nmOrdem = JOptionPane.showInputDialog("Entre com a nova Ordem a ser Adicionada!!!");
		if(nmOrdem != null){

			if(! nmOrdem.equals("")){
				Classe classe = (Classe) gui.classe.getSelectedItem() ;

				try {
					ordem = new Ordem(0,nmOrdem, classe.getIdClasse());
					ordemDAO = new OrdemDAO();
					//ordemDAO.setQueryClasse(classe.getIdClasse());
					//listOrdem = ordemDAO.getList();
					ordemDAO.insert(ordem);
					ordemDAO.setQueryClasse(classe.getIdClasse());
					listOrdem = ordemDAO.getList();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(gui, "Não foi possível carregar as ordens!");
				}
				DefaultComboBoxModel modelo = new DefaultComboBoxModel();
				modelo.addElement(":: Selecione uma ordem ::");
				if(listOrdem != null)
					for(int i=0; i<listOrdem.size(); i++)
						modelo.addElement(listOrdem.get(i));

				gui.ordem.setModel(modelo);
				gui.ordem.setEnabled(true);
				gui.ordemAdd.setEnabled(true);
				gui.familia.removeAllItems();
				gui.familia.addItem(":: Selecione uma família ::");
				gui.familia.setEnabled(false);
				gui.familiaAdd.setEnabled(false);
				gui.tribo.removeAllItems();
				gui.tribo.addItem(":: Selecione uma tribo ::");
				gui.tribo.setEnabled(false);
				gui.triboAdd.setEnabled(false);
				gui.especie.removeAllItems();
				gui.especie.addItem(":: Selecione uma espécie ::");
				gui.especie.setEnabled(false);
				gui.especieAdd.setEnabled(false);
				gui.ordem.setSelectedIndex(0);
			}
		}
	}

	/**
	 * Função que Verifica a tribo na GUI 
	 */
	private void verificaTribo() {
		if(gui.tribo.getSelectedIndex() < 1){
			gui.especie.removeAllItems();

			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.especie.setEnabled(false);
			gui.especieAdd.setEnabled(false);
		} else{

			try {
				tribo = (Tribo) gui.tribo.getSelectedItem();
				especieDAO = new EspecieDAO();
				especieDAO.setQueryTribo(tribo.getIdTribo());
				listEspecie = especieDAO.getList();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as espécies!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma espécie ::");
			if(listEspecie != null)
				for(int i=0; i<listEspecie.size(); i++)
					modelo.addElement(listEspecie.get(i));
			gui.especie.setModel(modelo);
			gui.especie.setEnabled(true);
			gui.especieAdd.setEnabled(true);
		}
	}

	/**
	 * Função que Verifica a familia na GUI 
	 */
	private void verificaFamilia() {
		if(gui.familia.getSelectedIndex() < 1){
			gui.tribo.removeAllItems();
			gui.especie.removeAllItems();

			gui.tribo.addItem(":: Selecione uma tribo ::");
			gui.especie.addItem(":: Selecione uma espécie ::");
			gui.tribo.setEnabled(false);
			gui.especie.setEnabled(false);
			gui.triboAdd.setEnabled(false);
			gui.especieAdd.setEnabled(false);
		} else{

			try {
				familia = (Familia) gui.familia.getSelectedItem();
				triboDAO = new TriboDAO();
				triboDAO.setQueryFamilia(familia.getIdFamilia());
				listTribo = triboDAO.getList();


			} catch (Exception e) {
				JOptionPane.showMessageDialog(gui, "Não foi possível carregar as tribos!");
			}
			DefaultComboBoxModel modelo = new DefaultComboBoxModel();
			modelo.addElement(":: Selecione uma tribo ::");
			if(listTribo != null)
				for(int i=0; i<listTribo.size(); i++)
					modelo.addElement(listTribo.get(i));

			gui.tribo.setModel(modelo);
			gui.tribo.setEnabled(true);
			gui.triboAdd.setEnabled(true);
			gui.especie.removeAllItems();
			gui.especie.setEnabled(false);
			gui.especie.addItem(":: Selecione uma espécie ::");
		}
	}

	/**
	 * Função que verifica a especie
	 */
	private void verificaEspecie(){
		if( (gui.especie.getSelectedItem() != null)){
			gui.tombo3.setEnabled(true);
			gui.tombo3.setEditable(true);

		}
	}

	/**
	 * Função que limpa os valores da arcada na GUI 
	 */
	private void limparArcada() {
		gui.canSupDir.setValue(0);
		gui.canInfDir.setValue(0);
		gui.incSupDir.setValue(0);
		gui.incInfDir.setValue(0);
		gui.preMolSupDir.setValue(0);
		gui.preMolInfDir.setValue(0);
		gui.molSupDir.setValue(0);
		gui.molInfDir.setValue(0);
		gui.totalArcada.setText("");
	}

	/**
	 * Função que preenche dados do mamifero
	 */
	private void preencheMamifero(){
		mamifero.setNu_comprimento_total_corpo(Float.valueOf(gui.comprimentoCorpo.getValue().toString()));
		mamifero.setNu_comprimento_total_cabeca(Float.valueOf(gui.comprimentoCabeca.getValue().toString()));
		mamifero.setNu_comprimento_total_calda( Float.valueOf(gui.comprimentoCalda.getValue().toString()));
		mamifero.setNu_comprimento_membro_posterior( Float.valueOf(gui.comprimentoMembroPost.getValue().toString()));
		mamifero.setNu_largura_cabeca( Float.valueOf(gui.larguraCabeca.getValue().toString()));
		mamifero.setNu_largura_toraxica( Float.valueOf(gui.larguraToraxica.getValue().toString()));
		mamifero.setNu_largura_base_orelha( Float.valueOf(gui.larguraBaseOrelha.getValue().toString()));

		mamifero.setNu_caninos_inferior_direito(Integer.valueOf(gui.canInfDir.getValue().toString()));
		mamifero.setNu_caninos_superior_direito(Integer.valueOf(gui.canSupDir.getValue().toString()));
		mamifero.setNu_incisivos_inferior_direito(Integer.valueOf(gui.incInfDir.getValue().toString()));
		mamifero.setNu_incisivos_superior_direito(Integer.valueOf(gui.incSupDir.getValue().toString()));
		mamifero.setNu_molares_inferior_direito(Integer.valueOf(gui.molInfDir.getValue().toString()));
		mamifero.setNu_molares_superior_direito(Integer.valueOf(gui.molSupDir.getValue().toString()));
		mamifero.setNu_premolares_inferior_direito(Integer.valueOf(gui.preMolInfDir.getValue().toString()));
		mamifero.setNu_premolares_superior_direito(Integer.valueOf(gui.preMolSupDir.getValue().toString()));		

		mamifero.setNu_total_dentes(calculaTotalArcada());
	}

	/**
	 * Função que calcula valor total dos valores da arcada na GUI 
	 */
	private int calculaTotalArcada() {
		return  ( Integer.parseInt(gui.canSupDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.canInfDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.incSupDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.incInfDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.preMolSupDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.preMolInfDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.molSupDir.getValue().toString().substring(0,1)) +
				Integer.parseInt(gui.molInfDir.getValue().toString().substring(0,1))
				) * 2;
	}

	/**
	 * Apos verificar dados do animail monta o objeto animal para o insert
	 */
	private void preencheAnimal(){
		animal = new Animal();
		animal.setBlAnimal(0);
		String tombo = gui.tombo1.getText()+ gui.tombo2.getText().toUpperCase()+gui.tombo3.getText(); //realizar a verificacao do gui.tombo3
		animal.setCdTombo(tombo);
		animal.setNmAnimal(gui.txtNmAnimal.getText());
		animal.setNmCientifico(gui.txtNmCientifico.getText());
		//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
		animal.setDsIdade(gui.cBoxIdade.getSelectedItem().toString());
		animal.setNuPeso(Float.parseFloat(gui.peso.getValue().toString()));
		animal.setDsSinais(gui.txtSinais.getText());
		//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
		animal.setSgSexo(gui.sexo.getSelectedItem().toString().charAt(0));
		//tem que tratar caso selecione o item 0, no caso isso direto na interface fica mais facil
		animal.setDsCor(gui.txtFieldCorPelagem.getText());
		animal.setDsDadosBiometricos(gui.textAreaDadosBiometricos.getText());
		//a.setIdColeta(idColeta); //buscar pela id coleta apos criar ela
		animal.setIdEspecie(gui.especie.getSelectedIndex());
	}

	/**
	 * Apos verificar dados da coleta monta o objeto coleta para o insert
	 */
	private void preencheColeta(){
		coleta = new Coleta();
		coleta.setNmColetor(gui.coletor.getText());
		coleta.setDsMetodoColeta(gui.metodoColeta.getText());
		coleta.setDsObjetivoColeta(gui.objetivoColeta.getText());
		coleta.setDsOrigemAnimal(gui.origem.getText());
		coleta.setDtRecolhimento( (Date) gui.dtRecolhim.getValue() );

		//pego o endereco selecionado  e seta na tabela coleta o valor do FK_endereco_coleta
		if(!gui.localidade.getSelectedItem().equals(":: Selecione uma localidade ::")){
			endereco = (Endereco) gui.localidade.getSelectedItem();
			coleta.setIdEnderecoColeta(endereco.getIdEndereco());
		}
	}
}


