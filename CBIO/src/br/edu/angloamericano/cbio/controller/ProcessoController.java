package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import br.edu.angloamericano.cbio.dados.Animal;
import br.edu.angloamericano.cbio.gui.ProcessoGUI;

public class ProcessoController {

	private ProcessoGUI gui;
	private Animal animal;

	public ProcessoController(JDialog Super, Animal a){
		this.animal = a;
		gui = new ProcessoGUI();
		gui.addOuvintes(new Ouvintes());
		gui.setTitle("Processo Realizado");

		CarregaDadosAnimal();
		configInterface();

		gui.setVisible(true);
	}

	private void configInterface(){
		gui.textTombo.setEditable(false);
		gui.textColetor.setEditable(false);
		gui.textCor.setEditable(false);
		gui.textDataColeta.setEditable(false);
		gui.textEspecie.setEditable(false);
		gui.textGenero.setEditable(false);
		gui.textIdade.setEditable(false);
		gui.textLocal.setEditable(false);
		gui.textNomeCie.setEditable(false);
		gui.textNomeComum.setEditable(false);
		gui.textPeso.setEditable(false);
		gui.chckbxDisponvel.setEnabled(false);
	}

	private void CarregaDadosAnimal(){
		gui.textTombo.setText(animal.getCdTombo());
		if(animal.getBlAnimal() == 0)
			gui.chckbxDisponvel.setSelected(true);

		gui.textCor.setText(animal.getDsCor());
		gui.textIdade.setText(animal.getDsIdade());
		gui.textNomeComum.setText(animal.getNmAnimal());
		gui.textNomeCie.setText(animal.getNmCientifico());
		gui.textPeso.setText(String.format("%s", animal.getNuPeso()));
		
		if(animal.getSgSexo() == 'M')
			gui.textGenero.setText("Masculino");
		else
			gui.textGenero.setText("Feminino");
		
		
	}

	private class Ouvintes implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent ev) {
			// TODO Auto-generated method stub		
			if(ev.getSource() == gui.btnDetalhes){

				JOptionPane.showMessageDialog(gui, "Exibir Detalhes do Processo Selecionado");
			}
			if(ev.getSource() == gui.btnFechar){
				gui.dispose();
			}
		}
	}
}
