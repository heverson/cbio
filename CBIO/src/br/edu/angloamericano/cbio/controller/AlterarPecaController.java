package br.edu.angloamericano.cbio.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import br.edu.angloamericano.cbio.dados.Bloco;
import br.edu.angloamericano.cbio.dados.Local;
import br.edu.angloamericano.cbio.dados.LocalComplemento;
import br.edu.angloamericano.cbio.dados.Peca;
import br.edu.angloamericano.cbio.dados.PecaProcesso;
import br.edu.angloamericano.cbio.dados.Processo;
import br.edu.angloamericano.cbio.dados.Sala;
import br.edu.angloamericano.cbio.dao.AnimalDAO;
import br.edu.angloamericano.cbio.dao.BlocoDAO;
import br.edu.angloamericano.cbio.dao.LocalComplementoDAO;
import br.edu.angloamericano.cbio.dao.LocalDAO;
import br.edu.angloamericano.cbio.dao.PecaDAO;
import br.edu.angloamericano.cbio.dao.PecaProcessoDAO;
import br.edu.angloamericano.cbio.dao.ProcessoDAO;
import br.edu.angloamericano.cbio.dao.SalaDAO;
import br.edu.angloamericano.cbio.gui.CadastroPecaGUI;

public class AlterarPecaController {

	private CadastroPecaGUI gui;
	private Peca peca = new Peca();
	private PecaProcesso pecaProc = new PecaProcesso();
	private Processo proc = new Processo();
	private boolean dscProc = false;
	private boolean dscPeca = false;
	public Local local;
	public Bloco bloco;
	public Sala sala;
	public LocalComplemento localComplemento;
	private int nivel = -1; //controle para arvore de localidade
	private boolean treeSelected = false; //controle para adicionar, alterar e remover items da arvore... 
	private String path;

	public AlterarPecaController(JDialog Super, Peca pc){	
		peca = pc;
		gui = new CadastroPecaGUI(Super);
		ConfigAcesso();
		preencheDadosTela2();
		gui.addOuvintes(new Ouvinte());
		gui.setTitle("Alterar Peça");
		gui.setModal(true);
		gui.setVisible(true);
	}

	/**
	 * Classe interna responsável por controlar as ações geradas pela interface gráfica
	 * @author Thiago R. M. Bitencourt
	 */
	private class Ouvinte implements ActionListener, TreeSelectionListener, MouseListener{

		public void actionPerformed(ActionEvent ev){
			// Quando o botão consultar á precionado

			if(ev.getSource() == gui.btnLimparP2){
				limpaCamposP2();
			}
			if(ev.getSource() == gui.btnLimparP3){
				limpaCamposP3();
			}
			if(ev.getSource() == gui.btnCancelar){
				gui.dispose();
			}
			if(ev.getSource() == gui.btnProximoP2)
				validaDadosPecaTela2();

			if(ev.getSource() == gui.btnProximoP3){
				validaProcessoTela3();
			}
			if(ev.getSource() == gui.btnAdicionar){
				alteraPecaDesejada();
			}
		}

		/*
		 * Ao clicar em uma das abas da janela as outras perdem a seleção. A aba Animal não pode ser selecionada
		 */
		public void mouseClicked(MouseEvent arg0) {
			if(arg0.getSource() == gui.tabbed){
				int n = gui.tabbed.getSelectedIndex();
				if(n == 0){
					gui.tabbed.setEnabledAt(1,false);
					gui.tabbed.setEnabledAt(2,false);
					gui.tabbed.setEnabledAt(3,false);
				}else
					if(n==1){
						if(gui.tabbed.isEnabledAt(1)){
							gui.tabbed.setEnabledAt(2,false);
							gui.tabbed.setEnabledAt(3,false);
						}
					}else
						if(n==2)
						{
							if(gui.tabbed.isEnabledAt(2))
								gui.tabbed.setEnabledAt(3,false);
						}
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {}

		@Override
		public void mouseExited(MouseEvent arg0) {}

		@Override
		public void mousePressed(MouseEvent arg0) {}

		@Override
		public void mouseReleased(MouseEvent arg0) {}

		@Override
		public void valueChanged(TreeSelectionEvent arg0) 
		{
			if(gui.tree.getLastSelectedPathComponent() != null){
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) gui.tree.getLastSelectedPathComponent();  //no de arvore recebe o componente selecionado
				int lvl = node.getLevel();
				setNivel(lvl);						//seta a altura dos niveis da arvore para fazer controle de qual classe esta sendo selecionada

				if(lvl == 0){
					setTreeSelected(true);
				}
				if(lvl == 1){									//se Selecionado um Local sera adicionado um bloco no bd
					local = (Local) node.getUserObject();	
					setLocal(local);
					setTreeSelected(true);

				}else 
					if(lvl ==2){								//se Selecionado um Bloco sera adicionado uma Sala no bd
						bloco = (Bloco) node.getUserObject();
						setBloco(bloco);
						setTreeSelected(true);

					}else
						if(lvl == 3){							//se Selecionado uma Sala sera adicionado um Complemento no bd
							sala = (Sala) node.getUserObject();
							setSala(sala);
							setTreeSelected(true);
						}else
							if(lvl == 4){
								localComplemento = (LocalComplemento) node.getUserObject();
								setLocalComplemento(localComplemento);
								gui.textDesc.setText(localComplemento.getDsComplemento());

								gui.tree.setToolTipText("Descrição : " + localComplemento.getDsComplemento());
								setTreeSelected(true);
							}
			}
		}
	}


	/**
	 * Função que valida os dados da Peça na tela 2 e segue para tela 3.
	 * Nessa tela, obrigatóriamente uma peça deve ser criada e selecionada, e também é obrigatório a descrição
	 * do processo realizado na peça.
	 * Se essas informações não forem corretamente adicionadas o sistema mostra um aviso e não continua até as informações 
	 * obrigatórias seja adicionadas.
	 */
	private void validaDadosPecaTela2() {
		String msgErro = "";
		if(gui.cBoxPecas.getSelectedIndex() > -1){
			String cdTomboPeca = (String) gui.cBoxPecas.getSelectedItem();
			peca.setCdTomboPeca( Integer.parseInt((cdTomboPeca.substring(2, cdTomboPeca.length()))));
		} else
			msgErro += "Código da Peça não foi selecionado. \n";

		if( !gui.txtDsPeca.getText().equals("") ){
			if(!peca.getDsPeca().equals(gui.txtDsPeca.getText())){
				peca.setDsPeca(gui.txtDsPeca.getText());
				dscPeca = true;
			}
		}
		else
			msgErro += "Descrição da peça está vazio. \n";

		if( !gui.txtAreaOssos.getText().equals("")){
			if(!peca.getDsOssos().equals(gui.txtAreaOssos.getText())){
				peca.setDsOssos(gui.txtAreaOssos.getText());
				dscPeca = true;
			}
		}

		if( msgErro.equals("") ){
			preencheDadosTela3();
			gui.tabbed.setEnabledAt(2, true); //habilita aba de processos
			gui.tabbed.setSelectedIndex(2); //move para a aba de processos
		} else
			JOptionPane.showMessageDialog(null, msgErro);
	}

	/**
	 * Função que valida os dados do processo da peça na tela 3 e segue para tela 4 de localidade 
	 * Nessa tela, obrigatóriamente deve ser selecionado o nome do processo realizado, e também é obrigatório o
	 * do responsável pelo processo realizado na peça.
	 * Se essas informações não forem corretamente adicionadas o sistema mostra um aviso e não continua até as informações 
	 * obrigatórias seja adicionadas.
	 */
	private void validaProcessoTela3() {
		String msgErro = "";		
		if( !gui.txtDsProc.getText().equals("") ){
			if(!proc.getDsProcesso().equals(gui.txtDsProc.getText())){
				dscProc = true;
				proc.setDsProcesso(gui.txtDsProc.getText());
			}
		}
		else
			msgErro += "Descrição do Processo está vazio. \n";				

		if( msgErro.equals("") ){
			preencheDadoLocal();
			gui.tabbed.setEnabledAt(3, true); //habilita aba de localidade
			gui.tabbed.setSelectedIndex(3); //move para a aba de localidade
		} else
			JOptionPane.showMessageDialog(null, msgErro);
	}

	// Limpa os campos da tela 2. Apenas os campos de descrição podem ser alterados, portanto apenas esses campos podem ser limpos.
	private void limpaCamposP2(){
		gui.txtDsPeca.setText("");
		gui.txtAreaOssos.setText("");
	}

	// Limpa os campos da tela 3. Apenas o campo descrição do processo pode ser alterado
	private void limpaCamposP3(){
		gui.txtDsProc.setText("");
	}

	/*
	 * Método que configura quais opções podem ser alteradas
	 */
	private void ConfigAcesso(){
		// Desabilita o acesso as informações que não podem ser alteradas
		// Tela 2
		gui.spinner.setEnabled(false);
		gui.textTombo.setEnabled(false);
		gui.btnAddPeca.setEnabled(false);
		gui.chckbxDnaCartilagem.setEnabled(false);
		gui.chckbxDnaTecidoMuscular.setEnabled(false);
		gui.chckbxDNAVisceral.setEnabled(false);
		gui.chckbxEscama.setEnabled(false);
		gui.chckbxPeloAbdominal.setEnabled(false);
		gui.chckbxPeloInterescapularDorsal.setEnabled(false);
		gui.chckbxPeloCabeaDorsal.setEnabled(false);
		gui.chckbxPena.setEnabled(false);

		// Tela 3
		gui.txtNmResp.setEnabled(false);
		gui.textNmUser.setEnabled(false);
		gui.cBoxNmProcesso.setEnabled(false);
		gui.spDtEntrada.setEnabled(false);
		gui.spDtSaida.setEnabled(false);

		// Tela Local de Armazenamento
		gui.btnAdicionarArvore.setEnabled(false);
		gui.btnAlterarArvore.setEnabled(false);
		gui.btnRemoverArvore.setEnabled(false);
		gui.btnAdicionar.setText("Alterar");
	}

	/**
	 * Preenche os dados da Tela 2
	 */
	private void preencheDadosTela2() {
		PecaDAO pDAO = new PecaDAO();
		AnimalDAO aDAO = new AnimalDAO();

		DefaultComboBoxModel modeloBase = new DefaultComboBoxModel(); //cria modelo para combo box base
		DefaultComboBoxModel modeloPeca = new DefaultComboBoxModel(); //cria modelo para combo box peca

		gui.textTombo.setText(aDAO.findAnimalByIdAnimal(peca.getIdAnimal()).getCdTombo());

		gui.chckbxEsteAnimalAinda.setSelected(false);
		gui.tabbed.setEnabledAt(0, false); // Não é possivel acessar a tela 0, com a descrição do animal
		gui.tabbed.setEnabledAt(1, true); //habilita aba de peças
		gui.tabbed.setSelectedIndex(1); //move para a aba da peça

		modeloBase.addElement("PC" + pDAO.getPecaById(peca.getIdPecaBase()).getCdTomboPeca());

		gui.cBoxBase.setEnabled(false);
		gui.cBoxBase.setModel(modeloBase);
		gui.cBoxBase.setSelectedIndex(0);

		modeloPeca.addElement("PC" + peca.getCdTomboPeca());

		gui.cBoxPecas.setEnabled(false);
		gui.cBoxPecas.setModel(modeloPeca);
		gui.cBoxPecas.setSelectedIndex(0);	

		gui.txtDsPeca.setText(peca.getDsPeca());
		gui.txtAreaOssos.setText(peca.getDsOssos());

		checkBox();
	}

	/**
	 * Preenche os dados da tela 3.
	 */
	private void preencheDadosTela3(){
		PecaProcessoDAO pcDAO = new PecaProcessoDAO();
		ProcessoDAO prDAO = new ProcessoDAO();

		pecaProc = pcDAO.getProcessoByIdPeca(peca.getIdPeca());

		if(pecaProc != null){
			proc = prDAO.getProcessoById(pecaProc.getIdProcesso());

			DefaultComboBoxModel modelo = new DefaultComboBoxModel(); //cria modelo para combo box
			modelo.addElement(proc.getNmProcesso());

			gui.cBoxNmProcesso.setModel(modelo);
			gui.cBoxNmProcesso.setSelectedIndex(0);
			gui.cBoxNmProcesso.setEnabled(false);
			gui.txtNmResp.setText(pecaProc.getNmResponsavelProcesso());
			gui.textNmUser.setText(pecaProc.getUsuarioResponsavel());
			gui.txtDsProc.setText(pecaProc.getDsProcessoRealizado());
			gui.spDtEntrada.setValue(peca.getDtEntrada());
			gui.spDtSaida.setValue(peca.getDtRetorno());			
		}
	}

	// Configura a janela de local de armazenamento
	private void preencheDadoLocal(){
		LocalComplementoDAO lDAO = new LocalComplementoDAO();
		LocalComplemento complemento = lDAO.getById(peca.getIdLocalArmazenado());
		if(complemento != null){
			SalaDAO sDAO = new SalaDAO();
			Sala sala = sDAO.getSalaById(complemento.getIdSala());
			if(sala != null){
				BlocoDAO bDAO = new BlocoDAO();
				Bloco bloco = bDAO.getBlocoById(sala.getIdBloco());
				if(bloco != null){
					LocalDAO loDAO = new LocalDAO(); 
					Local local = loDAO.getLocalById(bloco.getIdLocal());
					if(local != null)
					{
						// Monta o caminho do local de armazenamento da peça
						path = "Local Atual: Local -> " + local.getNmLocal() + ", " +
								bloco.getNmBloco() + " -> " + sala.getNmSala() + " -> " + complemento.getNmComplemento();
						gui.lblLocalAtual.setText(path); // Mostra o local de armazenamento atual da peça
						//JOptionPane.showMessageDialog(gui, "Caminho do local atual: " + path + "\n Selecionar Caminho: Não Implementado");
						
					}
				}
			}
		}
}

// Configura a seleção dos checkBox da janela
private void checkBox(){
	if(peca.getBlDnaCartilagem() == 1)
		gui.chckbxDnaCartilagem.setSelected(true);
	if(peca.getBlDnaTecidoMuscular() == 1)
		gui.chckbxDnaTecidoMuscular.setSelected(true);
	if(peca.getBlDnaTecidoVisceral() == 1)
		gui.chckbxDNAVisceral.setSelected(true);
	if(peca.getBlEscama() == 1)
		gui.chckbxEscama.setSelected(true);
	if(peca.getBlPeloAbdominal() == 1)
		gui.chckbxPeloAbdominal.setSelected(true);
	if(peca.getBlPeloCabecaDorsal() == 1)
		gui.chckbxPeloCabeaDorsal.setSelected(true);
	if(peca.getBlPeloInterescapularDorsal() == 1)
		gui.chckbxPeloInterescapularDorsal.setSelected(true);
	if(peca.getBlPena() == 1)
		gui.chckbxPena.setSelected(true);
}

/**
 * Função adiciona a nova peça no banco de dados.
 * Verifica se um local de armazenamento foi selecionado e se é um local apropriado. (Apenas um local de nível 4 é aceito, ou seja, 
 * apenas um local específico dentro de uma sala, por exemplo, um freezer ou um armário).  
 */
private void alteraPecaDesejada() {
	if((getNivel() == 4) && (isTreeSelected() == true)) { // O local de armazenamento deve estar selecionado

		// Se o local de armazenamento for alterado, altera a informação no registro da peça
		if(getLocalComplemento().getIdComplemento() != peca.getIdLocalArmazenado())
		{
			peca.setIdLocalArmazenado(getLocalComplemento().getIdComplemento());
			dscPeca = true;
		}

		// Se houve alguma alteração na peça, então a mesma é alterada
		if(dscPeca)
		{
			alteraPeca();
		}

		// Se houve alguma alteração no processo, então o mesmo é alterado
		if(dscProc)
		{
			alteraProc();
		}
		// Fecha a janela
		gui.setVisible(false);
	}
	else
	{
		JOptionPane.showMessageDialog(gui, "Selecione o Local de Armazenamento");
	}
}

// Altera a peça na base de dados
private void alteraPeca(){
	PecaDAO pDAO = new PecaDAO();
	pDAO.updatePeca(peca);
}

// Altera o processo na base de dados
private void alteraProc(){
	ProcessoDAO prDAO = new ProcessoDAO();
	PecaProcessoDAO pcDAO = new PecaProcessoDAO();

	try {
		pcDAO.updatePecaProcesso(proc.getIdProcesso(), proc.getDsProcesso());
		prDAO.updateProcesso(proc);	
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

//---- funções para manipular arvore de local

public void setLocal(Local local){
	this.local = local;
}

public void setBloco(Bloco bloco){
	this.bloco = bloco;
}

public Bloco getBloco(){
	return bloco;
}

public Local getLocal(){
	return local;
}

public Sala getSala(){
	return sala;
}

public void setSala(Sala sala){
	this.sala = sala;
}

public LocalComplemento getLocalComplemento(){
	return localComplemento;
}

public void setLocalComplemento(LocalComplemento localComp){
	this.localComplemento = localComp;
}

public boolean isTreeSelected() {
	return treeSelected;
}

public void setTreeSelected(boolean treeSelected) {
	this.treeSelected = treeSelected;
}

//set e get dos niveis selecionados da arvores
private void setNivel(int nivel){
	this.nivel = nivel;
}

private int getNivel(){
	return nivel;
}
}
