package br.edu.angloamericano.cbio.app;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import br.edu.angloamericano.cbio.controller.LoginController;
import br.edu.angloamericano.cbio.dados.Usuario;

/**
 * Classe Inicial.
 * A partir dessa classe o sistema é iniciado. 
 * As informações do usuário logado e do tipo de usuário estão contidas nessa classe
 * @author Thiago R. M. Bitencourt
 */
public class App {
	private static Usuario Usuario;  // Objeto do tipo Usuario que contém as informações do usuário logado
	private static String TipoUser;  // String que armazena a informação do tipo do usuário cadastrado

	public static void main(String args[]){
		boolean ok = true;

		// Carrega para a aplicação o look and feel 'Nimbus'
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		}
		catch (Exception e) {
			ok = false;
			JOptionPane.showMessageDialog(null, "Problema na abertura do programa!");
		}

		// Se carregou look and feel, chama a tela de login
		if(ok){
			new LoginController();
		}
	}

	// método que retorna o objeto do usuário logado no sistema
	public static Usuario getUsuario() {
		return Usuario;
	}

	// método utilizado para atribuir as infomações do usuário logado no sistema
	public static void setUsuario(Usuario usuario) {
		Usuario = usuario;
	}

	// método que retorna o tipo de usuáro logado
	public static String getTipoUser(){
		return TipoUser;
	}

	// método utilizado para atribuir o tipo de usuário logado no sistema
	public static void setTipoUser(String tipoUser){
		TipoUser = tipoUser;
	}
}
