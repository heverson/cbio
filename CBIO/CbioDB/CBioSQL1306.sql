SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
CREATE SCHEMA IF NOT EXISTS `bd_cbio` DEFAULT CHARACTER SET latin1 ;
USE `mydb` ;
USE `bd_cbio` ;

-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_estado`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_estado` (
  `ID_ESTADO` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_ESTADO` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ID_ESTADO`) )
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_cidade`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_cidade` (
  `ID_CIDADE` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_CIDADE` VARCHAR(45) NOT NULL ,
  `FK_ID_ESTADO` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_CIDADE`) ,
  INDEX `fk_TB_CIDADE_TB_ESTADO1_idx` (`FK_ID_ESTADO` ASC) ,
  CONSTRAINT `fk_TB_CIDADE_TB_ESTADO1`
    FOREIGN KEY (`FK_ID_ESTADO` )
    REFERENCES `bd_cbio`.`tb_estado` (`ID_ESTADO` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_endereco`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_endereco` (
  `ID_ENDERECO` INT(11) NOT NULL AUTO_INCREMENT ,
  `FK_ID_CIDADE` INT(11) NOT NULL ,
  `DS_LOCAL` VARCHAR(500) NULL DEFAULT NULL ,
  `CD_COORDENADA_GEOGRAFICA` VARCHAR(45) NULL DEFAULT NULL ,
  `NM_LOCAL` VARCHAR(45) NULL DEFAULT NULL ,
  `NM_RUA` VARCHAR(45) NOT NULL ,
  `NU_COMPLEMENTO` VARCHAR(45) NOT NULL ,
  `CD_CEP` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ID_ENDERECO`) ,
  INDEX `FK_ID_CIDADE_COLETA_idx` (`FK_ID_CIDADE` ASC) ,
  CONSTRAINT `FK_ID_CIDADE_COLETA`
    FOREIGN KEY (`FK_ID_CIDADE` )
    REFERENCES `bd_cbio`.`tb_cidade` (`ID_CIDADE` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_coleta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_coleta` (
  `ID_COLETA` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_COLETOR` VARCHAR(45) NOT NULL ,
  `DT_RECOLHIMENTO` DATE NOT NULL ,
  `DS_ORIGEM_ANIMAL` VARCHAR(500) NULL DEFAULT NULL ,
  `DS_METODO_COLETA` VARCHAR(500) NULL DEFAULT NULL ,
  `DS_OBJETIVO_COLETA` VARCHAR(500) NULL DEFAULT NULL ,
  `FK_ENDERECO_COLETA` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_COLETA`) ,
  INDEX `FK_LOCAL_COLETA_idx` (`FK_ENDERECO_COLETA` ASC) ,
  CONSTRAINT `FK_LOCAL_COLETA`
    FOREIGN KEY (`FK_ENDERECO_COLETA` )
    REFERENCES `bd_cbio`.`tb_endereco` (`ID_ENDERECO` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_classe`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_classe` (
  `ID_CLASSE` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_CLASSE` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ID_CLASSE`) )
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_ordem`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_ordem` (
  `ID_ORDEM` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_ORDEM` VARCHAR(45) NOT NULL ,
  `FK_ID_CLASSE` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_ORDEM`) ,
  INDEX `fk_TB_ORDEM_TB_CLASSE1_idx` (`FK_ID_CLASSE` ASC) ,
  CONSTRAINT `fk_TB_ORDEM_TB_CLASSE1`
    FOREIGN KEY (`FK_ID_CLASSE` )
    REFERENCES `bd_cbio`.`tb_classe` (`ID_CLASSE` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_familia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_familia` (
  `ID_FAMILIA` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_FAMILIA` VARCHAR(45) NOT NULL ,
  `FK_ID_ORDEM` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_FAMILIA`) ,
  INDEX `fk_TB_FAMILIA_TB_ORDEM1_idx` (`FK_ID_ORDEM` ASC) ,
  CONSTRAINT `fk_TB_FAMILIA_TB_ORDEM1`
    FOREIGN KEY (`FK_ID_ORDEM` )
    REFERENCES `bd_cbio`.`tb_ordem` (`ID_ORDEM` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_tribo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_tribo` (
  `ID_TRIBO` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_TRIBO` VARCHAR(45) NOT NULL ,
  `FK_ID_FAMILIA` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_TRIBO`) ,
  INDEX `fk_TB_TRIBO_TB_FAMILIA1_idx` (`FK_ID_FAMILIA` ASC) ,
  CONSTRAINT `fk_TB_TRIBO_TB_FAMILIA1`
    FOREIGN KEY (`FK_ID_FAMILIA` )
    REFERENCES `bd_cbio`.`tb_familia` (`ID_FAMILIA` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_especie`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_especie` (
  `ID_ESPECIE` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_ESPECIE` VARCHAR(45) NOT NULL ,
  `FK_ID_TRIBO` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_ESPECIE`) ,
  INDEX `fk_TB_ESPECIE_TB_TRIBO1_idx` (`FK_ID_TRIBO` ASC) ,
  CONSTRAINT `fk_TB_ESPECIE_TB_TRIBO1`
    FOREIGN KEY (`FK_ID_TRIBO` )
    REFERENCES `bd_cbio`.`tb_tribo` (`ID_TRIBO` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_animal`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_animal` (
  `ID_ANIMAL` INT(11) NOT NULL AUTO_INCREMENT ,
  `BL_ANIMAL` VARCHAR(45) NOT NULL ,
  `CD_TOMBO` VARCHAR(45) NOT NULL ,
  `NM_ANIMAL` VARCHAR(45) NOT NULL ,
  `NM_CIENTIFICO` VARCHAR(45) NOT NULL ,
  `DS_IDADE` VARCHAR(45) NOT NULL ,
  `NU_PESO` FLOAT(7,3) NULL DEFAULT NULL ,
  `DS_SINAIS` VARCHAR(500) NULL DEFAULT NULL ,
  `SG_SEXO` CHAR(1) NULL DEFAULT NULL ,
  `DS_COR` VARCHAR(45) NULL DEFAULT NULL ,
  `DS_DADOS_BIOMETRICOS` VARCHAR(45) NULL DEFAULT NULL ,
  `FK_ID_ESPECIE` INT(11) NOT NULL ,
  `FK_ID_COLETA` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_ANIMAL`) ,
  INDEX `fk_TB_ANIMAL_TB_ESPECIE1_idx` (`FK_ID_ESPECIE` ASC) ,
  INDEX `fk_TB_ANIMAL_TB_COLETA1_idx` (`FK_ID_COLETA` ASC) ,
  CONSTRAINT `fk_TB_ANIMAL_TB_COLETA1`
    FOREIGN KEY (`FK_ID_COLETA` )
    REFERENCES `bd_cbio`.`tb_coleta` (`ID_COLETA` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_TB_ANIMAL_TB_ESPECIE1`
    FOREIGN KEY (`FK_ID_ESPECIE` )
    REFERENCES `bd_cbio`.`tb_especie` (`ID_ESPECIE` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_local`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_local` (
  `ID_LOCAL` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_LOCAL` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ID_LOCAL`) )
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_bloco`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_bloco` (
  `ID_BLOCO` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_BLOCO` VARCHAR(45) NOT NULL ,
  `FK_ID_LOCAL` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_BLOCO`) ,
  INDEX `fk_TB_BLOCO_TB_LOCAL_idx` (`FK_ID_LOCAL` ASC) ,
  CONSTRAINT `fk_TB_BLOCO_TB_LOCAL`
    FOREIGN KEY (`FK_ID_LOCAL` )
    REFERENCES `bd_cbio`.`tb_local` (`ID_LOCAL` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_classe_mamiferos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_classe_mamiferos` (
  `ID_CLASSE_MAMIFEROS` INT(11) NOT NULL AUTO_INCREMENT ,
  `NU_COMPRIMENTO_TOTAL_CORPO` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_COMPRIMENTO_TOTAL_CALDA` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_COMPRIMENTO_TOTAL_CABECA` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_LARGURA_CABECA` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_LARGURA_TORAXICA` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_LARGURA_BASE_ORELHA` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_COMPRIMENTO_MEMBRO_POSTERIOR` FLOAT(7,4) NULL DEFAULT NULL ,
  `NU_TOTAL_DENTES` INT(11) NULL DEFAULT NULL ,
  `NU_CANINOS_SUPERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_CANINOS_INFERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_INCISIVOS_SUPERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_INCISIVOS_INFERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_PREMOLARES_SUPERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_PREMOLARES_INFERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_MOLARES_SUPERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `NU_MOLARES_INFERIOR_DIREITO` INT(11) NULL DEFAULT NULL ,
  `TB_ANIMAL_ID_ANIMAL` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_CLASSE_MAMIFEROS`) ,
  INDEX `fk_TB_CLASSE_MAMIFEROS_TB_ANIMAL1_idx` (`TB_ANIMAL_ID_ANIMAL` ASC) ,
  CONSTRAINT `fk_TB_CLASSE_MAMIFEROS_TB_ANIMAL1`
    FOREIGN KEY (`TB_ANIMAL_ID_ANIMAL` )
    REFERENCES `bd_cbio`.`tb_animal` (`ID_ANIMAL` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_sala`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_sala` (
  `ID_SALA` INT(11) NOT NULL AUTO_INCREMENT ,
  `nm_sala` VARBINARY(45) NULL DEFAULT NULL ,
  `FK_ID_BLOCO` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_SALA`) ,
  INDEX `fk_TB_SALA_TB_BLOCO1_idx` (`FK_ID_BLOCO` ASC) ,
  CONSTRAINT `fk_TB_SALA_TB_BLOCO1`
    FOREIGN KEY (`FK_ID_BLOCO` )
    REFERENCES `bd_cbio`.`tb_bloco` (`ID_BLOCO` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = binary;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_complemento`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_complemento` (
  `ID_COMPLEMENTO` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_COMPLEMENTO` VARCHAR(45) NOT NULL ,
  `DS_COMPLEMENTO` VARCHAR(45) NULL DEFAULT NULL ,
  `FK_ID_SALA` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_COMPLEMENTO`) ,
  INDEX `fk_TB_LOCAL_PECA_TB_SALA1_idx` (`FK_ID_SALA` ASC) ,
  CONSTRAINT `fk_TB_LOCAL_PECA_TB_SALA1`
    FOREIGN KEY (`FK_ID_SALA` )
    REFERENCES `bd_cbio`.`tb_sala` (`ID_SALA` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_emprestimo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_emprestimo` (
  `ID_LOCAL` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_LOCAL` VARCHAR(45) NOT NULL ,
  `DS_LOCAL` VARCHAR(45) NULL DEFAULT NULL ,
  `FL_FORA_FACUL` TINYINT(1) NOT NULL ,
  `FK_ID_LOCAL_FORA` INT(11) NULL DEFAULT NULL ,
  `DS_ENDERECO` VARCHAR(45) NULL DEFAULT NULL ,
  `CD_CEP` MEDIUMTEXT NULL DEFAULT NULL ,
  `ID_ENDERECO` INT(11) NOT NULL ,
  PRIMARY KEY (`ID_LOCAL`) ,
  INDEX `FK_EMPRESTIMO_ENDERECO_idx` (`ID_ENDERECO` ASC) ,
  CONSTRAINT `FK_EMPRESTIMO_ENDERECO`
    FOREIGN KEY (`ID_ENDERECO` )
    REFERENCES `bd_cbio`.`tb_endereco` (`ID_ENDERECO` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tipoUser`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tipoUser` (
  `idTipo` INT NOT NULL AUTO_INCREMENT ,
  `nomeTipo` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idTipo`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_cbio`.`Usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`Usuario` (
  `Usuario` INT NOT NULL AUTO_INCREMENT ,
  `Nome` VARCHAR(45) NOT NULL ,
  `Sobrenome` VARCHAR(45) NULL ,
  `Tipo` INT NOT NULL ,
  `Cargo` VARCHAR(45) NOT NULL ,
  `Registro` VARCHAR(45) NOT NULL ,
  `TelRes` VARCHAR(45) NULL ,
  `TelCel` VARCHAR(45) NULL ,
  `Email` VARCHAR(45) NULL ,
  `Obs` VARCHAR(500) NULL ,
  `bl_usuario` INT NULL ,
  PRIMARY KEY (`Usuario`) ,
  INDEX `fk_Usuario_tipoUser1_idx` (`Tipo` ASC) ,
  CONSTRAINT `fk_Usuario_tipoUser1`
    FOREIGN KEY (`Tipo` )
    REFERENCES `bd_cbio`.`tipoUser` (`idTipo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_cbio`.`Baixa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`Baixa` (
  `idBaixa` INT NOT NULL AUTO_INCREMENT ,
  `Descricao` VARCHAR(45) NOT NULL ,
  `Dt_Baixa` DATE NOT NULL ,
  `Usuario_Usuario` INT NOT NULL ,
  PRIMARY KEY (`idBaixa`) ,
  INDEX `fk_Baixa_Usuario1_idx` (`Usuario_Usuario` ASC) ,
  CONSTRAINT `fk_Baixa_Usuario1`
    FOREIGN KEY (`Usuario_Usuario` )
    REFERENCES `bd_cbio`.`Usuario` (`Usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_peca`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_peca` (
  `ID_PECA` INT(11) NOT NULL AUTO_INCREMENT ,
  `BL_ANIMAL` INT NULL ,
  `BL_DNA_TECIDO_VISCERAL` TINYINT(1) NOT NULL ,
  `FL_DNA_CARTILAGEM` TINYINT(1) NOT NULL ,
  `FL_DNA_TECIDO_MUSCULAR` TINYINT(1) NOT NULL ,
  `FL_PELO_INTERESCAPULAR_DORSAL` TINYINT(1) NOT NULL ,
  `FL_PELO_ABDOMINAL` TINYINT(1) NOT NULL ,
  `FL_PELO_CABECA_DORSAL` TINYINT(1) NOT NULL ,
  `FL_ESCAMA` TINYINT(1) NOT NULL ,
  `FL_PENA` TINYINT(1) NOT NULL ,
  `DS_OSSOS` VARCHAR(500) NOT NULL ,
  `DT_ENTRADA` DATE NOT NULL ,
  `DT_RETORNO` DATE NULL DEFAULT NULL ,
  `BL_PECA_EMPRESTADA` TINYINT(1) NOT NULL DEFAULT '0' ,
  `FK_ID_ANIMAL` INT(11) NOT NULL ,
  `FK_EMPRESTIMO` INT(11) NULL DEFAULT NULL ,
  `FK_ID_LOCAL_ARMAZENADO` INT(11) NULL DEFAULT NULL ,
  `cd_tombo_peca` INT(11) NULL DEFAULT NULL ,
  `ds_peca` VARCHAR(500) NULL DEFAULT NULL ,
  `id_pecaBase` INT(11) NULL ,
  PRIMARY KEY (`ID_PECA`) ,
  INDEX `fk_TB_PECA_TB_ANIMAL1_idx` (`FK_ID_ANIMAL` ASC) ,
  INDEX `fk_TB_PECA_TB_EMPRESTIMO1_idx` (`FK_EMPRESTIMO` ASC) ,
  INDEX `fk_TB_PECA_TB_LOCAL_PECA1_idx` (`FK_ID_LOCAL_ARMAZENADO` ASC) ,
  INDEX `fk_tb_peca_Baixa1_idx` (`BL_ANIMAL` ASC) ,
  INDEX `fk_tb_peca_tb_peca1_idx` (`id_pecaBase` ASC) ,
  CONSTRAINT `fk_TB_PECA_TB_ANIMAL1`
    FOREIGN KEY (`FK_ID_ANIMAL` )
    REFERENCES `bd_cbio`.`tb_animal` (`ID_ANIMAL` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_TB_PECA_TB_EMPRESTIMO1`
    FOREIGN KEY (`FK_EMPRESTIMO` )
    REFERENCES `bd_cbio`.`tb_emprestimo` (`ID_LOCAL` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_TB_PECA_TB_LOCAL_PECA1`
    FOREIGN KEY (`FK_ID_LOCAL_ARMAZENADO` )
    REFERENCES `bd_cbio`.`tb_complemento` (`ID_COMPLEMENTO` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_peca_Baixa1`
    FOREIGN KEY (`BL_ANIMAL` )
    REFERENCES `bd_cbio`.`Baixa` (`idBaixa` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_peca_tb_peca1`
    FOREIGN KEY (`id_pecaBase` )
    REFERENCES `bd_cbio`.`tb_peca` (`ID_PECA` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_processo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_processo` (
  `ID_PROCESSO` INT(11) NOT NULL AUTO_INCREMENT ,
  `NM_PROCESSO` VARCHAR(45) NOT NULL ,
  `DS_PROCESSO` VARCHAR(500) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID_PROCESSO`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`tb_peca_processo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`tb_peca_processo` (
  `ID_PECA` INT(11) NOT NULL ,
  `ID_PROCESSO` INT(11) NOT NULL ,
  `NM_RESPONSAVEL_PROCESSO` VARCHAR(45) NOT NULL ,
  `NM_USUARIO` VARCHAR(45) NOT NULL ,
  `DS_PROCESSO_REALIZADO` VARCHAR(500) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID_PECA`, `ID_PROCESSO`) ,
  INDEX `ID_PECA_idx` (`ID_PECA` ASC) ,
  INDEX `ID_PROCESSO_idx` (`ID_PROCESSO` ASC) ,
  CONSTRAINT `ID_PECA`
    FOREIGN KEY (`ID_PECA` )
    REFERENCES `bd_cbio`.`tb_peca` (`ID_PECA` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ID_PROCESSO`
    FOREIGN KEY (`ID_PROCESSO` )
    REFERENCES `bd_cbio`.`tb_processo` (`ID_PROCESSO` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bd_cbio`.`Login`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bd_cbio`.`Login` (
  `idLogin` INT NOT NULL AUTO_INCREMENT ,
  `Login` VARCHAR(45) NOT NULL ,
  `Senha` VARCHAR(200) NOT NULL ,
  `Usuario_Usuario` INT NOT NULL ,
  PRIMARY KEY (`idLogin`) ,
  INDEX `fk_Login_Usuario1_idx` (`Usuario_Usuario` ASC) ,
  CONSTRAINT `fk_Login_Usuario1`
    FOREIGN KEY (`Usuario_Usuario` )
    REFERENCES `bd_cbio`.`Usuario` (`Usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
