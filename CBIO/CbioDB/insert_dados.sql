-- -----------------------------
-- Cria o tipo de usuário Administrador
-- -----------------------------

INSERT INTO tipoUser (NomeTipo) VALUES ('Administrador');
INSERT INTO tipoUser (NomeTipo) VALUES ('Professor');
INSERT INTO tipoUser (NomeTipo) VALUES ('Estagiario');

-- -----------------------------
-- Cria o Usuário Administrador
-- -----------------------------
INSERT INTO Usuario (nome, cargo, registro, tipo) VALUES ('Administrador', 'Administrador do Sistema', '123456789', 1);

-- --------------------------------------------------
-- Cria o ligin e senha para o usuario administrador
-- --------------------------------------------------
INSERT INTO Login (login, senha, Usuario_Usuario) VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 1);

-- ---------------------------------------
-- Insere o registro para relacionar com as peças não removidas
-- ---------------------------------------

INSERT INTO Baixa (Descricao, Dt_Baixa, Usuario_Usuario) VALUES ('Peça Não Removida', '2013-05-15', 1);
INSERT INTO Baixa (Descricao, Dt_Baixa, Usuario_Usuario) VALUES ('Peça Base Para Outra Peça', '2013-05-25', 1);

-- -----------------------------
-- Insere as Classes dos Animais
-- -----------------------------
INSERT INTO tb_classe (nm_classe) VALUES ('Mamíferos'), ('Répteis'),('Peixes'),('Aves'),('Insetos'),('Anfíbios');

-- ----------------------------
-- Grava os Estados
-- ----------------------------
INSERT INTO `tb_estado` VALUES ('1', 'Acre');
INSERT INTO `tb_estado` VALUES ('2', 'Alagoas');
INSERT INTO `tb_estado` VALUES ('3', 'Amazonas');
INSERT INTO `tb_estado` VALUES ('4', 'Amapa');
INSERT INTO `tb_estado` VALUES ('5', 'Bahia');
INSERT INTO `tb_estado` VALUES ('6', 'Ceara');
INSERT INTO `tb_estado` VALUES ('7', 'Distrito Federal');
INSERT INTO `tb_estado` VALUES ('8', 'Espirito Santo');
INSERT INTO `tb_estado` VALUES ('9', 'Goiais');
INSERT INTO `tb_estado` VALUES ('10', 'Maranhao');
INSERT INTO `tb_estado` VALUES ('11', 'Minas Gerais');
INSERT INTO `tb_estado` VALUES ('12', 'Mato Grosso Do Sul');
INSERT INTO `tb_estado` VALUES ('13', 'Mato Grosso');
INSERT INTO `tb_estado` VALUES ('14', 'Para');
INSERT INTO `tb_estado` VALUES ('15', 'Paraiba');
INSERT INTO `tb_estado` VALUES ('16', 'Pernambuco');
INSERT INTO `tb_estado` VALUES ('17', 'Piaui');
INSERT INTO `tb_estado` VALUES ('18', 'Parana');
INSERT INTO `tb_estado` VALUES ('19', 'Rio De Janeiro');
INSERT INTO `tb_estado` VALUES ('20', 'Rio Grande Do Norte');
INSERT INTO `tb_estado` VALUES ('21', 'Rondonia');
INSERT INTO `tb_estado` VALUES ('22', 'Roraima');
INSERT INTO `tb_estado` VALUES ('23', 'Rio Grande Do Sul');
INSERT INTO `tb_estado` VALUES ('24', 'Santa Catarina');
INSERT INTO `tb_estado` VALUES ('25', 'Sergipe');
INSERT INTO `tb_estado` VALUES ('26', 'Sao Paulo');
INSERT INTO `tb_estado` VALUES ('27', 'Tocantins');

