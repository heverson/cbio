Para que o sistema se conecta à base de dados e funcione corretamente é necessário criar a base de dados e cadastrar alguns dados básicos.

Para criar a base de dados execute o script SQL do arquivo CBioSQL1306.sql
Após a base de dados ter cido criada com sucesso execute as instruções SQL do arquivo insert_dados.sql

Para que o sistema encontre a base de dados e se conecte à mesma, é necessario alterar o arquivo de configura "config" que fica na pasta files
O arquivo está assim: 
"localhost" "root" "thiago"

localhost - é o nome dá máquina onde está o servidor MySQL (pode ser necessário por a porta do serviço, caso a base de dados esteja em outra máquina, como por exemplo "nomemaquina:3360").

root - é o nome do usuário do MySQL. Eu estava usando root, porém não é legal fazer isso. Para a versão final do sistema deve ser criado um usuário exclusivo para o sistema. Para testar o sistema ou continuar o desenvolvimento o nome do usuario deve ser alterado para o usuario que está sendo utilizado para acessar o servidor MySQL.

thiago - é a senha para o usuário (No meu caso, usuário root). Deve ser mudado para a senha do usuário do MySQL

Feito isso a base de dados está pronta para ser acessada e utilizada pelo sistema.
Ao executar o sistema será solicitado um usuário e uma senha para acessar o sistema, o usuário pré-cadastrado é 'admin' com senha 'admin'.



By Thiago R. M. Bitencourt. 
Em 20/06/2013
